# deleted_name

## Required versions

NodeJS: `v10.11.0`
Npm: `6.4.1`
PostgreSQL: `10.5`
ReactJS: `v16.5.2`

## Backend

How to start the server:

```
yarn start
```

It will be running at: `http://localhost:3000`

### Database

To initialise database:
`yarn run db:create`

To run the migrations:
`yarn run db:migrate`

```
docker exec -i deleted_name_api_1 yarn db:migrate
```

Seeding:

Note: If starting from an empty database (no users in the `users` table), send a POST request to `/config/user_seed`. A user will be created and you will receive credentials you can use to log in and continue below.

1. There must be a `./api/db/seed_[environment].sql` file containing all seed data in the form of `INSERT` SQL statements.
2. WARNING: Running the seeding process erases **all** current data in the database except for two tables: `config` and `SequelizeMeta`.
3. To run the seeder script, send an authorized POST request to the `/config/db_seed` endpoint. Note that the `users` table will also be overwritten.
4. The seeder script places a row in the `config` table labeled `auto_seedLock` with the time of seeding as the value. To run the seeding process again you must remove that row.

Note:

### Documentation

Using Slate: https://github.com/lord/slate

#### Run docs locally

Either follow Slate readme's instructions to run directly or run `vagrant up` in the docs dir. The devault vm provider is VirtualBox. Slate will run at `http://localhost:4567`