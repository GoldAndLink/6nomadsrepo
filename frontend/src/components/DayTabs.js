import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: 30px;
`

const DayContainer = styled.div`
  position: relative;
`

const Notification = styled.div`
  padding-top: 2px;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #0eb48f;
  background-color: #d2f8d8;
  position: absolute;
  top: -14px;
  right: -4px;
  height: 18px;
  width: 22px;
  border-radius: 16.5px;
  border-color: #0eb48f;
  border-width: 2px;
  border-style: solid;
  padding: 3px 0 0 0;
`

const Day = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 28px;
  width: 28px;
  border: 1px solid ${props => props.status === 'selected' ? '#61c9b5' : '#ebebeb'};
  border-radius: 2px;
  margin: 0 10px 0 5px;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  background-color: ${props => props.status === 'past' ? '#f3f6f6' : '#ffffff'};
  color: ${props => props.status === 'selected' ? '#61c9b5' : '#898989'};
`

class DayTabs extends Component {
  render () {
    const { appointmentCounts, handleClick, days } = this.props

    return (
      <Container>
        { days.map(day => (
          <DayContainer key={day.id}>
            {
              (appointmentCounts && appointmentCounts.get(day.countKey, 0) > 0) && <Notification> {appointmentCounts.get(day.countKey, 0)} </Notification>
            }
            <Day status={day.status} onClick={e => handleClick(day.id)}>
              {day.date.month() + 1}/{day.date.date()}
            </Day>
          </DayContainer>
        )) }
      </Container>
    )
  }
}

DayTabs.propTypes = {
  days: PropTypes.object,
  handleClick: PropTypes.func,
  onChangeStartDate: PropTypes.func,
  appointmentCounts: PropTypes.object
}

export default DayTabs
