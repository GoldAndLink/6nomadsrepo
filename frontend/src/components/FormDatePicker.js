import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import calendarIcon from '../assets/images/calendar-icon.svg'

const CalendarIcon = styled.img`
  position: absolute;
  width: 16px;
  height: 16px;
  top: 12px;
  right: 30px;
  background: url(${calendarIcon}) no-repeat;
`

const Container = styled.div`
  position: relative;

  input {
    border-width: 0px 0px 1px 0px;
    border-color: #ebebeb;
    width: 100%;
    height: 35px;
  }
`

const Label = styled.label`
  position: absolute;
  top: -14px;
  left: 0px;
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 600;
  font-size: 10px;

  ${props => props.error && css`
    color: red;
  `}
`

const HelpText = styled.label`
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 300;
  font-size: 12px;

  ${props => props.error && css`
    color: red;
  `}
`

const PickerWrap = styled.div`
  margin-top: 18px;
`

class FormDatePicker extends Component {
  constructor (props) {
    super(props)
    this.picker = null
  }

  onOutsideClick = () => {
    this.picker.cancelFocusInput()
    this.picker.setOpen(false)
  }

  render () {
    const { label, error, showIcon } = this.props

    return (
      <Container>
        <PickerWrap>
          <DatePicker {...this.props} ref={node => { this.picker = node }} onClickOutside={this.onOutsideClick} onSelect={this.onOutsideClick} />
        </PickerWrap>
        <Label error={error}>{label}</Label>
        {error &&
          <HelpText error={error}>{error}</HelpText>
        }
        { showIcon && <CalendarIcon onClick={e => { this.picker.setOpen(true) }} /> }
      </Container>
    )
  }
}

FormDatePicker.propTypes = {
  label: PropTypes.any,
  error: PropTypes.any,
  showIcon: PropTypes.bool
}

export default FormDatePicker
