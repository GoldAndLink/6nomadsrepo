import React, { Component } from 'react'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import styled from 'styled-components'
import calendarIcon from '../assets/images/calendar-icon.svg'

const CalendarIcon = styled.img`
  position: absolute;
  width: 16px;
  height: 16px;
  top: 12px;
  right: 10px;
  background: url(${calendarIcon}) no-repeat;
`

const SearchOrderDatePickerContainer = styled.div`
  position: relative;
  display: flex;
  background-color: white;
  height: 40px;
  input {
    align-self: center;
    padding-left: 11px;
    width: 206px;
    height: 36px;
    border-width: 0;
    font-style: normal;
    color: #898989;
  }
`

const SearchOrderDatePickerComponent = styled(DatePicker)`
  align-self: center;
  padding-left: 11px;
  width: 206px;
  height: 36px;
  border-width: 0;
  font-style: normal;
  color: #898989;
`

const HeaderGridDatePickerContainer = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 22px;
  padding-bottom: 22px;
  padding-left: 22px;
  width: 200px;

  div.column {
    display: flex;
    flex-direction: column;
  }

  div.row {
    display: flex;
    flex-direction: row;
    align-self: flex-start;

    p {
      margin: 2px 4px 0 1px;
      color: #898989;
      font-size: 14px;
      font-weight: 300;
    }
  }
`

const HeaderGridDatePickerComponent = styled(DatePicker)`
  height: 14px;
  width: 200px;
  border-width: 0;
  font-size: 16px;
  font-weight: 300;
  font-style: normal;
  color: #898989;
  align-self: flex-start;
`

class DatePickerCustom extends Component {
  constructor (props) {
    super(props)
    this.picker = null
  }

  handleChange = date => {
    const { onChange } = this.props
    onChange(date)
    if (this.picker) {
      this.picker.cancelFocusInput()
      this.picker.setOpen(false)
    }
  }

  render () {
    const {
      HeaderGridDatePicker = false,
      SearchOrderDatePicker = false,
      SearchAppointmentDatePicker = false,
      disabled = false,
      dateFormat,
      onChangeStartDate,
      startDate,
      placeholderText,
      selected,
      onChange,
      showIcon
    } = this.props

    if (HeaderGridDatePicker) {
      return (
        <HeaderGridDatePickerContainer>
          <div className={'column'}>
            <HeaderGridDatePickerComponent
              selected={startDate}
              dateFormat={dateFormat}
              onChange={onChangeStartDate}
            />
          </div>
          { showIcon && <CalendarIcon onClick={e => { this.picker.setOpen(true) }} /> }
        </HeaderGridDatePickerContainer>
      )
    }
    if (SearchOrderDatePicker) {
      return (
        <SearchOrderDatePickerContainer>
          <DatePicker
            style={{ 'background-color': 'red' }}
            ref={node => { this.picker = node }}
            placeholderText={placeholderText}
            dateFormat={dateFormat}
            selected={selected}
            onChange={this.handleChange}
            onClickOutside={() => {
              this.picker.cancelFocusInput()
              this.picker.setOpen(false)
            }}
          />
          { showIcon && <CalendarIcon onClick={e => { this.picker.setOpen(true) }} /> }
        </SearchOrderDatePickerContainer>
      )
    }
    if (SearchAppointmentDatePicker) {
      return (
        <SearchOrderDatePickerContainer>
          <SearchOrderDatePickerComponent
            disabled={disabled}
            placeholderText={placeholderText}
            dateFormat={dateFormat}
            selected={selected}
            onChange={this.handleChange}
          />
          { showIcon && <CalendarIcon onClick={e => { this.picker.setOpen(true) }} /> }
        </SearchOrderDatePickerContainer>
      )
    }
    return (
      <DatePicker
        dateFormat={dateFormat}
        placeholderText={placeholderText}
        selected={selected}
        onChange={onChange}
      />
    )
  }
}

DatePickerCustom.propTypes = {
  HeaderGridDatePicker: PropTypes.bool,
  SearchOrderDatePicker: PropTypes.bool,
  SearchAppointmentDatePicker: PropTypes.bool,
  disabled: PropTypes.bool,
  dateFormat: PropTypes.string,
  placeholderText: PropTypes.string,
  selected: PropTypes.any,
  leftArrowIcon: PropTypes.string,
  rightArrowIcon: PropTypes.string,
  onChangeStartDate: PropTypes.func,
  onChangeEndDate: PropTypes.func,
  startDate: PropTypes.any,
  endDate: PropTypes.any,
  startTime: PropTypes.any,
  onChangeStartTime: PropTypes.func,
  endTime: PropTypes.any,
  onChangeEndTime: PropTypes.func,
  onChange: PropTypes.func,
  showIcon: PropTypes.bool
}

export default DatePickerCustom
