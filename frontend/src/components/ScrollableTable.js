import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const TableElement = styled.table`
  border-collapse: collapse;
`

const Tr = styled.tr`
  white-space: nowrap; // Scrolling
`

const Th = styled.th`
  text-align: left;
  font-weight: 300;
  padding: 10px 24px;
  border-bottom: 1px solid ${props => props.theme.lighterGray};
  padding-left: 215px;
  z-index: 1;
`

const StickyTh = styled.th`
width: 48px;
text-align: left;
font-weight: 300;
padding: 10px 24px;
border-bottom: 1px solid ${props => props.theme.lighterGray};
position: absolute; 
background-color: white;
`

const Sticky2ndTh = styled.th`
width: 48px;
text-align: left;
font-weight: 300;
padding: 10px 24px;
border-bottom: 1px solid ${props => props.theme.lighterGray};
position: absolute;
margin-left: 78px;
background-color: white;
z-index: 2;
`

const Td = styled.td`
  padding: 10px 24px;
  border-bottom: 1px solid ${props => props.theme.lighterGray};
  padding-left: 215px;
  z-index: 1;
`
const StickyTd = styled.td`
  width: 48px;
  padding: 10px 24px;
  border-bottom: 1px solid ${props => props.theme.lighterGray};
  left: 0;
  position: absolute;
  top: auto;
  background-color: white;
  z-index: 2;
`
const Sticky2ndTd = styled.td`
  width: 48px;
  padding: 10px 24px;
  border-bottom: 1px solid ${props => props.theme.lighterGray};
  left: 0;
  position: absolute;
  top: auto;
  background-color: white;
  margin-left: 78px;
  z-index: 2;
`

class Table extends Component {
  render () {
    const { data, children, className } = this.props
    const columns = React.Children.map(children, ({ props }) => props)

    return (
      <TableElement className={className}>
        <thead>
          <Tr>
            {columns.map((column, i) => i < 1 ? (
              <StickyTh key={i}>{column.title}</StickyTh>
            ) : i < 2 ? (
              <Sticky2ndTh key={i}>{column.title}</Sticky2ndTh>
            ) : (
              <Th key={i}>{column.title}</Th>
            )
            )}
          </Tr>
        </thead>
        <tbody>
          {data && data.map((item, i) => (
            <Tr key={i}>
              {columns.map((column, j) => j < 1 ? (
                <StickyTd key={j}>{column.content && column.content(item)}</StickyTd>
              ) : j < 2 ? (
                <Sticky2ndTd key={j}>{column.content && column.content(item)}</Sticky2ndTd>
              ) : (
                <Td key={j}>{column.content && column.content(item)}</Td>
              ))}
            </Tr>
          ))}
        </tbody>
      </TableElement>
    )
  }
}

Table.propTypes = {
  data: PropTypes.array,
  children: PropTypes.node,
  className: PropTypes.string
}

export const Column = () => null

export default Table
