import React, { Component } from 'react'
import { Map } from 'immutable'
import PropTypes from 'prop-types'
import moment from 'moment'
import styled, { css } from 'styled-components'
import { Droppable, Draggable } from 'react-beautiful-dnd'
import { appointmentStatuses } from '../containers/Appointments'
import { requestStatuses } from '../containers/Requests'
import { isRequestLate } from '../utils/time'

import { generateDraggableId, generateDropppableId, convertToCamelCase, convertToKebabCase } from '../utils/utils'
import cartIcon from '../assets/images/group.svg'
const images = require.context('../assets/images/', true)

const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: ${props => props.isDraggingOver ? (props.width - 4) : (props.width - 2)}px;
  align-self: stretch;
  border-radius: 2px;
  border: solid ${props => props.isDraggingOver ? '2px' : '1px'} ${props => props.isDraggingOver ? '#0eb48f' : props.theme.appointmentStatuses[props.status] ? props.theme.appointmentStatuses[props.status].text : '#AAB0C0'};
  background-color: ${props => props.theme.appointmentStatuses[props.status] ? props.theme.appointmentStatuses[props.status].background : '#F3F6F6'};
  opacity: ${props => props.theme.appointmentStatuses[props.status] ? props.theme.appointmentStatuses[props.status].opacity : 0.3};
  margin: 0;
  transform : ${props => props.isDraggingOver ? 'none !important' : 'initial'};
  height: ${props => (props.height - 2)}px;
  user-select: none;
  overflow: hidden;
`
const DroppableContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

const Title = styled.div`
  display: flex;
  flex-wrap: nowrap;
  font-size: 14px;
  font-weight: 300;
  color: ${props => props.theme.appointmentStatuses[props.status] ? props.theme.appointmentStatuses[props.status].text : '#AAB0C0'};
  margin: 7px 0 0 0;
`

const SmallTitle = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  font-size: 10px;
  font-weight: 300;
  color: ${props => props.theme.appointmentStatuses[props.status] ? props.theme.appointmentStatuses[props.status].text : '#AAB0C0'};
  margin: 2px 0 0 0;
`

const Info = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: nowrap;
  font-size: 10px;
  color: ${props => props.theme.appointmentStatuses[props.status] ? props.theme.appointmentStatuses[props.status].text : '#AAB0C0'};
  margin-top: 5px;

  ${props => props.small && css`
    margin-top: 2px;
    flex-direction: column;
    align-items: flex-start;
  `}
`

const Issues = styled.span`
  margin: 9.6px 0 0 0;

  ${props => props.small && css`
    margin: 2px 0 0 0;
  `}
`

const TotalOrders = styled.span`
  margin: 9.6px 5px 0 0;
  animation-duration: 1s;

  @keyframes text-animation {
    0%   {font-size: 11px; color: red;}
    25%  {font-size: 9px; color: red;}
    50%  {font-size: 11px; color: red;}
    75%  {font-size: 9px; color: red;}
    100% {font-size: 11px; color: red;}
  }

  ${props => props.small && css`
    margin: 2px 0 0 0;
  `}
`

const Buttons = styled.div`
  display: flex;
  flex-direction: column;
  margin: 5px 5px 5px 0;
`

const Button = styled.img`
  margin: 5px;
  cursor: pointer;
`

const SmallButton = styled.img`
  margin: 5px;
  height: 10px;
  cursor: pointer;
`

const CartIconContainer = styled.div`
  position: relative;
`

const CartIcon = styled.img`
  position: absolute;
  top: 10px;
  left: 55px;
  width: 30px;
`

const AppointmentContent = styled.div`
  display: flex;
  flex-direction:column;
  margin-left: 10px;
`

const DragHandle = styled.div`
  position: absolute;
  bottom: -6px;
  right: -6px;
  height: 12px;
  width: 12px;
  transform: rotate(45deg);
  background-color: #aab0c0;
  cursor: ns-resize;
`

class Event extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isTotalOrdersChanged: false,
      draggingDuration: false,
      newDuration: props.duration,
      draggingStartY: 0,
      draggingStartDuration: 0,
      isRequestLate: false
    }
    this.calculateLateness()
  }

  calculateLateness = () => {
    const { appointmentStatuses, appointment, carrierRequest } = this.props
    this.setState({
      isRequestLate: isRequestLate(carrierRequest, appointment, appointmentStatuses)
    })
  }

  componentDidMount () {
    document.addEventListener('mouseup', this.onMouseUp)
    console.log('CALCULATE LATENESS')
    this.calculateLateness()
    this.interval = setInterval(() => {
      this.calculateLateness()
    }, 4000)
  }

  componentWillUnmount () {
    document.removeEventListener('mouseup', this.onMouseUp)
    clearInterval(this.interval)
  }

  componentDidUpdate (prevProps, prevState) {
    const { duration, totalOrders } = this.props
    if (prevProps.totalOrders !== totalOrders) {
      this.setState({
        isTotalOrdersChanged: true
      })

      setTimeout(
        () => this.setState({
          isTotalOrdersChanged: false
        }),
        1000
      )
    }
    if (duration !== prevProps.duration) {
      this.setState({ newDuration: duration })
    }
  }

  onHandleMouseDown = e => {
    e.stopPropagation()
    document.addEventListener('mousemove', this.onMouseMove)
    this.setState({
      draggingDuration: true,
      draggingStartY: e.clientY,
      draggingStartDuration: this.props.duration,
      newDuration: this.props.duration
    })
  }

  onMouseMove = e => {
    const { size } = this.props
    const { draggingStartY, draggingStartDuration } = this.state
    const diffY = e.clientY - draggingStartY

    const diffDuration = Math.round(diffY * 100 * 4 / size / 140) * 15
    const newDuration = Math.min(Math.max(draggingStartDuration + diffDuration, 15), 240)

    this.setState({ newDuration })
  }

  onMouseUp = () => {
    const { onDurationChange, duration } = this.props
    const { newDuration } = this.state

    document.removeEventListener('mousemove', this.onMouseMove)

    this.setState({
      draggingDuration: false
    })

    if (newDuration !== duration && onDurationChange) {
      onDurationChange(newDuration)
    }
  }

  getTotalOrderStyle = isTotalOrdersChanged => ({
    animationName: isTotalOrdersChanged ? 'text-animation' : 'none'
  })

  getEventStatusIcons = (appointment, requestStatus, appointmentStatus, isRequestLate) => {
    const requestException = [requestStatuses.canceled, requestStatuses.reschedule, requestStatuses.carrierLate].includes(requestStatus)
    const appointmentStatusWithPossibleException = [appointmentStatuses.draft, appointmentStatuses.scheduled].includes(appointmentStatus)

    const appointmentStatusValue = isRequestLate ? convertToKebabCase(requestStatuses.carrierLate) : convertToKebabCase(appointmentStatus)
    const requestStatusValue = convertToKebabCase(requestStatus)
    const inventoryIssues = appointment.get('inventoryIssues')
    const inventoryReviewUserId = appointment.get('inventoryReviewUserId')

    let calendarIcon = ''
    if (appointmentStatusWithPossibleException && (inventoryIssues && inventoryIssues.size > 0 && !inventoryReviewUserId)) {
      calendarIcon = images(`./appointment-icon-${appointmentStatusValue}-inventory-issue.svg`)
    } else if (requestException && appointmentStatusWithPossibleException) {
      calendarIcon = images(`./appointment-icon-${appointmentStatusValue}-${requestStatusValue}.svg`)
    } else {
      calendarIcon = images(`./appointment-icon-${appointmentStatusValue}.svg`)
    }

    const infoIcon = images(`./info-icon-${appointmentStatusValue}.svg`)
    const deleteIcon = images(`./delete-icon-${appointmentStatusValue}.svg`)

    return {
      deleteIcon,
      calendarIcon,
      infoIcon
    }
  }

  render () {
    const {
      id,
      appointment,
      totalOrders,
      isUpsertAppointmentVisible,
      onDelete,
      className,
      size,
      onEdit,
      onMessage,
      duration,
      inProgress,
      width,
      requestStatus,
      appointmentStatus
    } = this.props
    const {
      isRequestLate,
      isTotalOrdersChanged,
      draggingDuration,
      newDuration
    } = this.state

    const inventoryIssues = appointment && appointment.get('inventoryIssues')
    const issues = (inventoryIssues && inventoryIssues.size) || 0
    const height = (draggingDuration ? newDuration : duration) * size * 140 / 6000

    const validAppointmentStatuses = ['Draft', 'Scheduled', 'Checked In', 'Loading', 'Checked Out']
    const { deleteIcon, calendarIcon, infoIcon } = this.getEventStatusIcons(appointment, requestStatus, validAppointmentStatuses.find(as => as === appointmentStatus), isRequestLate)

    const status = inProgress ? convertToCamelCase('inProgress') : convertToCamelCase(isRequestLate ? requestStatuses.carrierLate : appointmentStatus)

    return (
      <Droppable key={id} droppableId={generateDropppableId('event', id)} isDropDisabled={isUpsertAppointmentVisible}>
        {(droppableProvided, droppableSnapshot) => (
          <DroppableContainer
            className={className}
            innerRef={droppableProvided.innerRef}
            {...droppableProvided.droppableProps}>
            <Draggable key={id} draggableId={generateDraggableId('event', id)} >
              {provided =>
                <Container
                  isDraggingOver={droppableSnapshot.isDraggingOver}
                  height={height}
                  width={width}
                  innerRef={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  status={status}
                >
                  {status === appointmentStatuses.loading && <CartIconContainer><CartIcon src={cartIcon} alt='cartIcon' /></CartIconContainer>}
                  {size > 25 &&
                    <AppointmentContent>
                      {size >= 100 &&
                        <Title
                          status={status}>
                          {`Appt #${id}`}
                        </Title>
                      }
                      { size < 100 && size >= 75 &&
                        <SmallTitle
                          status={status}>
                          <div>Appt</div>
                          <div>{id}</div>
                        </SmallTitle>
                      }
                      {height > 35 && <Info
                        small={size < 100}
                        status={status}
                      >
                        <TotalOrders small={size < 100} style={this.getTotalOrderStyle(isTotalOrdersChanged)}>{`Orders (${totalOrders})`}</TotalOrders>
                        <Issues small={size < 100}>{`Issues (${issues})`}</Issues>
                      </Info>}
                    </AppointmentContent>
                  }
                  <Buttons>
                    <Button src={calendarIcon} onClick={onEdit} alt='calendarIcon' />
                    { size > 50 &&
                      <Button src={infoIcon} onClick={onMessage} alt='infoIcon' />
                    }
                    { (size <= 50 && size > 25) &&
                      <SmallButton src={infoIcon} onClick={onMessage} alt='infoIcon' />
                    }
                    { size > 50 &&
                      <Button src={deleteIcon} onClick={onDelete} alt='deleteIcon' />
                    }
                    { (size <= 50 && size > 25) &&
                      <SmallButton src={deleteIcon} onClick={onDelete} alt='deleteIcon' />
                    }
                  </Buttons>
                  <DragHandle onMouseDown={this.onHandleMouseDown} />
                </Container>
              }
            </Draggable>
            {droppableProvided.placeholder}
          </DroppableContainer>
        )
        }
      </Droppable>
    )
  }
}

Event.propTypes = {
  id: PropTypes.number,
  initTime: PropTypes.object,
  endTime: PropTypes.object,
  primaryRefValue: PropTypes.string,
  PO: PropTypes.string
}

Event.defaultProps = {
  id: 0,
  initTime: moment(),
  endTime: moment().add(1, 'hours'),
  primaryRefValue: '',
  PO: '',
  size: 100,
  appointmentStatus: Map({})
}

export default Event
