import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import styled, { css } from 'styled-components'

import downArrow from '../assets/images/down-arrow.svg'
import userIcon from '../assets/images/user-icon.png'

import UsersActions from '../modules/users/actions'
import { getLoggedUser, isAdmin } from '../modules/users/selectors'

const Wrapper = styled.div`
  min-width: 240px;
`

const Container = styled.div`
  position: relative;
`

const Trigger = styled.div`
  height: 36px;
  display: flex;
  border: 1px solid #ebebeb;
  border-radius; 2px;
  cursor: pointer;
  font-size: 12px;
  font-weight: 300;
  align-items: center;
  float: right;
  position: relative;

  &:hover {
    border-color: ${props => props.theme.lightGray}
  }
`

const DownArrow = styled.img`
  margin-left: 10px;
  margin-right: 18px;
`

const UserIcon = styled.img`
  height: 25px;
  width: 25px;
  margin: 0px 10px;
`

const Menu = styled.div`
  position: absolute;
  top: 38px;
  display: none;
  border-radius: 2px;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.17);
  border: solid 1px #ebebeb;
  background-color: white;
  left: 0px;
  right: 0px;

  ${props => props.open && css`
    display: block;
  `}
`

const MenuItem = styled.div`
  display: flex;
  align-items: center;
  padding: 0px 12px;
  height: 32px;
  cursor: pointer;

  &:hover {
    color: #0eb48f;
    background-color: #d2f8d8;
  }
`

const LogoutLink = styled(Link)`
  color: #898989;
  text-decoration: none;
`

const DropdownLabel = styled.h2`
    font-size: 12px;
    align-self: center;
    font-weight: 300;
    font-style: normal;
    color: #898989;
    margin-right: 5px;
    margin-top: 13px;
    float: right;
`

class UserDropdown extends Component {
  constructor (props) {
    super(props)

    this.button = React.createRef()
    this.state = { open: false }
  }

  componentDidMount () {
    window.document.addEventListener('click', this.onBodyClick)
  }

  componentWillUnmount () {
    window.document.removeEventListener('click', this.onBodyClick)
  }

  onBodyClick = e => {
    const node = this.button.current
    if (node && node.contains(e.target)) {
      this.setState(state => ({ open: !state.open }))
    } else {
      this.setState({ open: false })
    }
  }

  onLogout = () => this.props.logout()

  render () {
    const { user, isAdmin, showSystemSettings } = this.props
    const { open } = this.state

    return (
      <Wrapper>
        {user &&
          <Container>
            <Trigger innerRef={this.button}>
              <DownArrow src={downArrow} alt="Down arrow" />
              {user.get('firstName')} {user.get('lastName')}
              <UserIcon src={userIcon} alt="User icon" />
              <Menu open={open}>
                { isAdmin &&
                <MenuItem onClick={showSystemSettings}>
                  System Settings
                </MenuItem>
                }
                <LogoutLink to={'/'} onClick={() => this.onLogout()}>
                  <MenuItem>Logout</MenuItem>
                </LogoutLink>
              </Menu>
            </Trigger>
            <DropdownLabel>User</DropdownLabel>
          </Container>
        }
      </Wrapper>
    )
  }
}

UserDropdown.propTypes = {
  user: PropTypes.object,
  isAdmin: PropTypes.bool,
  showSystemSettings: PropTypes.func,
  logout: PropTypes.func
}

const mapStateToProps = state => ({
  user: getLoggedUser(state),
  isAdmin: isAdmin(state)
})

const mapDispatchToProps = dispatch => ({
  showSystemSettings: () => dispatch(UsersActions.showSystemSettings()),
  logout: () => dispatch(UsersActions.logout())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDropdown)
