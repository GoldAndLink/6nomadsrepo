import ReactModal from 'react-modal'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

function ReactModalAdapter ({ className, ...props }) {
  const contentClassName = `${className}__content`
  const overlayClassName = `${className}__overlay`
  return (
    <ReactModal
      portalClassName={className}
      className={contentClassName}
      overlayClassName={overlayClassName}
      {...props}
    />
  )
}

const StyledModal = styled(ReactModalAdapter)`
  &__overlay {
    position: fixed;
    z-index: 2;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(255, 255, 255, 0.75);
  }

  &__content {
    padding: 0;
    position: absolute;
    top: 40px;
    left: 40px;
    right: 40px;
    bottom: 40px;
    border: 1px solid #ccc;
    background: #fff;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    borderRadius: 4px;
    outline: none;
    .header {
      display: flex;
      flex-direction: row;
      justify-content: center;
      background-color: #white;
      font-size: 16px;
      font-weight: 600;
      font-style: normal;
      font-stretch: normal;
      text-transform: uppercase;
      line-height: normal;
      letter-spacing: 2px;
      text-align: center;
      color: #3c414e;
      padding: 20px 0 20px 0;
      border-bottom: 1px solid #3c414e;

      .title {
        flex: 1;
        align-self: center;
      }
    }
  }
`
const Container = styled.div`
  margin-top: 8px;
  margin-bottom: 20px;
  width: 500px;
`
const Text = styled.p`
  font-size: 18px;
  font-style: normal;
  line-height: normal;
  font-weight: 300;
  letter-spacing: 1px;
  text-align: center;
  color: #3c414e;
  margin-bottom: 20px;
`
const Buttons = styled.div`
  display: flex;
  justify-content: space-around;
  margin-top: 20px;
`

const Cancel = styled.button`
  font-size: 18px;
  width: 140px;
  border-radius: 3px;
  border: 1px solid #AAB1C0;
  background-color: #F6F6F6;
  color: #AAB1C0;
  text-align: center;
  font-weight: normal;
  height: 50px;
  padding: 10px 15px;
  cursor: pointer;
  letter-spacing: 1px;

  &:focus {
    outline: none;
  }
`

const NextButton = styled.button`
  font-size: 18px;
  width: 140px;
  border-radius: 3px;
  border: 1px solid ${props => props.theme.brandDarker};
  background-color: ${props => props.theme.brand};
  color: white;
  text-align: center;
  font-weight: normal;
  height: 50px;
  padding: 10px 15px;
  cursor: pointer;
  letter-spacing: 1px;

  &:focus {
    outline: none;
  }
`

const SelectADoorButton = styled.button`
  font-size: 18px;
  width: 155px;
  border-radius: 3px;
  border: 1px solid ${props => props.theme.brandDarker};
  background-color: ${props => props.theme.brand};
  color: white;
  text-align: center;
  font-weight: normal;
  height: 50px;
  padding: 10px 15px;
  cursor: pointer;
  letter-spacing: 1px;

  &:focus {
    outline: none;
  }
`

const DatesContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  border-bottom: 1px solid #f3f6f6;
`

const DateItem = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  height: 31px;
  ${({ selected }) => selected && `background-color: #d2f8d8;`}
  &:hover {
    background-color: #d2f8d8;
  }
`

const DateLabel = styled.div`
  align-self: center;
  height: 12px;
  font-size: 10px;
  font-weight: 300;
  line-height: 1.2;
  letter-spacing: 1.1px;
  color: #4a4a4a;
`

const DoorsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  border-bottom: 1px solid #f3f6f6;
`

const DoorItem = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  height: 31px;
  ${({ selected }) => selected && `background-color: #d2f8d8;`}
  &:hover {
    background-color: #d2f8d8;
  }
`

const DoorLabel = styled.div`
  align-self: center;
  height: 12px;
  font-size: 10px;
  font-weight: 300;
  line-height: 1.2;
  letter-spacing: 1.1px;
  color: #4a4a4a;
`

const steps = {
  SELECT_DATE: 0,
  SELECT_DOOR: 1,
  CONFIRM: 2
}

class NextAvailableLoadingDatesModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currentStep: 0,
      selectedDate: null,
      selectedDoor: null
    }
  }

  onNextStep = () => {
    const { currentStep } = this.state
    if (currentStep < steps.CONFIRM) {
      this.setState(state => ({ currentStep: state.currentStep + 1 }))
    } else {
      this.setState({ currentStep: 0 })
      this.props.onClose()
    }
  }

  onSelectDate = (date, index) => {
    this.setState({ selectedDate: { index, date } })
  }

  onSelectDoor = (door, index) => {
    this.setState({ selectedDoor: { index, door } })
  }

  _renderDatesList = dates => {
    const { selectedDate } = this.state
    return (
      <DatesContainer>
        {dates.map((date, index) => (
          <DateItem
            key={index}
            selected={selectedDate && index === selectedDate.index}
            onClick={() => this.onSelectDate(date, index)}
          >
            <DateLabel>{date}</DateLabel>
          </DateItem>
        ))}
      </DatesContainer>
    )
  }

  _renderDoorsList = doors => {
    const { selectedDoor } = this.state
    return (
      <DoorsContainer>
        {doors.map((door, index) => (
          <DoorItem
            key={index}
            selected={selectedDoor && index === selectedDoor.index}
            onClick={() => this.onSelectDoor(door, index)}
          >
            <DoorLabel>{door}</DoorLabel>
          </DoorItem>
        ))}
      </DoorsContainer>
    )
  }

  render () {
    const { isOpen = false, onClose } = this.props
    const { currentStep } = this.state

    /** * Mock Dates ****/
    const dates = [
      '01 . 14 . 2019    |    12:00PM',
      '01 . 15 . 2019    |    12:00PM'
    ]
    /********************/

    /** * Mock Doors ****/
    const doors = [
      'DOOR 12',
      'DOOR 05'
    ]
    /********************/
    return (
      <div>
        <StyledModal
          isOpen={isOpen}
          onAfterOpen={this.afterOpenModal}
          style={customStyles}
          contentLabel='Create User'
        >
          <div className='header'>
            <span className='title'>Next available loading date(s)</span>
          </div>
          <div>
            {currentStep === steps.SELECT_DATE && <Container>
              <Text>SELECT A DATE FROM THE LIST BELOW</Text>
              {this._renderDatesList(dates)}
              <Buttons>
                <Cancel onClick={onClose}>
                        Cancel
                </Cancel>
                <SelectADoorButton onClick={this.onNextStep}>
                        Select a door
                </SelectADoorButton>
              </Buttons>
            </Container>}
            {currentStep === steps.SELECT_DOOR && <Container>
              <Text>SELECT A DOOR FROM THE LIST BELOW</Text>
              {this._renderDoorsList(doors)}
              <Buttons>
                <Cancel onClick={onClose}>
                        Cancel
                </Cancel>
                <NextButton onClick={this.onNextStep}>
                        Done
                </NextButton>
              </Buttons>
            </Container>}
            {currentStep === steps.CONFIRM && <Container>
              <Text>Are you sure you want to continue the scheduled appointment with the shortage of inventory ?</Text>
              <Buttons>
                <Cancel onClick={onClose}>
                        Cancel
                </Cancel>
                <NextButton onClick={this.onNextStep}>
                        Yes
                </NextButton>
              </Buttons>
            </Container>}
          </div>
        </StyledModal>
      </div>
    )
  }
}
NextAvailableLoadingDatesModal.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func
}
export default NextAvailableLoadingDatesModal
