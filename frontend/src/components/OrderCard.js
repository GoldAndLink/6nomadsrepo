import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'
import { List } from 'immutable'
import { Card, Checkbox } from '@material-ui/core'

import { getAllOrderStatuses } from '../modules/orders/selectors'
import { getOrderItems } from '../modules/orderItems/selectors'
import { getItems } from '../modules/items/selectors'

const images = require.context('../assets/images/', true)

const CustomCheckbox = styled(Checkbox)`
  && {
    color: ${props => props.theme.orderStatuses[props.status] ? props.theme.orderStatuses[props.status].text : '#AAB0C0'};
  }
`

const OrderCardContainer = styled(Card)`
    position: relative;
    ${({ hide }) => hide ? `height: 36px;` : `height: 175px;`}
    border-radius: 2px;
    border: solid 1px ${props => props.theme.orderStatuses[props.status] ? props.theme.orderStatuses[props.status].text : '#AAB0C0'};
    background-color: #ffffff;
    margin-left: 10px;
    margin-right: 10px;
    margin-top: 6px;
    margin-bottom: 6px;
    display: flex;
    flex-direction: column;
    opacity: ${props => props.isDisabled ? '0.25' : '1'};
`

const OrderCardHeader = styled.div`
    height: 36px;
    border-bottom: solid 1px ${props => props.theme.orderStatuses[props.status] ? props.theme.orderStatuses[props.status].text : '#AAB0C0'};
    background-color: ${props => props.theme.orderStatuses[props.status] ? props.theme.orderStatuses[props.status].background : '#F3F6F6'};
    display: flex;
`

const CheckboxContainer = styled.div`
    display: flex;
    justify-content: center;
    align-self: center;
    width: 35px;
    height: 36px;
    border-right-width: 1px;
    border-right-color: ${props => props.theme.orderStatuses[props.status] ? props.theme.orderStatuses[props.status].text : '#AAB0C0'};
    border-right-style: solid;
`

const OrderCardLabel = styled.h2`
    align-self: center;
    margin-left: 8px;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    color: ${props => props.theme.orderStatuses[props.status] ? props.theme.orderStatuses[props.status].text : '#AAB0C0'};
`

const OrderCardButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-self: center;
    margin-right: 3px;
    margin-left: auto;
    img {
        margin: 0px 5px;
        cursor: pointer;
        width: 20px;
        height: 20px;
    }
`

const OrderCardContent = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4px 0 4px;
`

const Notification = styled.div`
  padding-top: 2px;
  font-size: 14px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #0eb48f;
  background-color: #d2f8d8;
  position: absolute;
  top: -5px;
  right: -5px;
  height: 23px;
  width: 23px;
  border-radius: 16.5px;
  border-color: #0eb48f;
  border-width: 2px;
  border-style: solid;
  z-index: 2;
`

const Label = styled.span`
  font-weight: bold;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;

  &::after {
    content: ':';
  }
`
const BigValue = styled.span`
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const Value = styled.span`
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const Section = styled.div`
  display: flex;
  flex-direction: column;
`

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 2.5px;
  margin-top: 2.5px;
  flex-wrap: nowrap;
  flex-basis: 40%;
`

const SectionBottom = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

class OrderCard extends Component {
    state = {
      orderCheckbox: false,
      selectedLocalState: null,
      hide: false,
      prevExpandAll: this.props.expandAll,
      prevCollapseAll: this.props.collapseAll
    }

    componentDidMount () {
      this.setState({ selectedLocalState: this.props.selected })
    }

    componentDidUpdate = (prevProps, prevState) => {
      if (prevProps.selected !== this.props.selected) {
        this.setState({ selectedLocalState: this.props.selected })
      }
    }

    shouldComponentUpdate = (nextProps, nextState) => {
      return (
        (nextProps.selected !== this.props.selected) ||
        (nextState.hide !== this.state.hide) ||
        (nextState.selectedLocalState !== this.state.selectedLocalState) ||
        (nextProps.isSelectAllOrders !== this.props.isSelectAllOrders) ||
        (nextProps.isDragging !== this.props.isDragging) ||
        (nextProps.checkedOrders &&
          nextProps.checkedOrders.size !== this.props.checkedOrders &&
          this.props.checkedOrders.size) ||
        (nextProps.orderStatuses !== this.props.orderStatuses) ||
        (nextProps.isDisabled !== this.props.isDisabled) ||
        (nextProps.order.get('orderStatusId') !== this.props.order.get('orderStatusId'))
      )
    }

    static getDerivedStateFromProps (props, state) {
      const { collapseAll, expandAll } = props
      const { prevCollapseAll, prevExpandAll } = state

      if ((prevCollapseAll !== collapseAll || prevExpandAll !== expandAll) && (collapseAll || expandAll)) {
        return {
          hide: collapseAll || !expandAll
        }
      } else {
        return null
      }
    }

    handleSelectedChange = event => {
      const { onSelectedChange } = this.props
      if (onSelectedChange) {
        onSelectedChange(event.target.checked)
      }
    }

    toggleHide = () => {
      const { collapseAll, expandAll, resetCollapseAndExpand } = this.props
      if (collapseAll || expandAll) {
        resetCollapseAndExpand()
      }
      this.setState(state => ({ hide: !state.hide }))
    }

    getIconsForOrderStatus = name => {
      return {
        createIcon: images(`./order-icon-${name}.svg`),
        infoIcon: images(`./info-icon-list-${name}.svg`),
        deleteIcon: images(`./delete-icon-${name}.svg`),
        arrowUpIcon: images(`./arrow-up-icon-${name}.svg`),
        arrowDownIcon: images(`./arrow-down-icon-${name}.svg`)
      }
    }

    getSKUs = () => {
      const { order, orderSKUs } = this.props
      if (!orderSKUs) {
        const items = order.get('items', List([]))
        const uniqSKUs = Array.from(new Set(items.map(s => s.get('sku'))))
        return { value: uniqSKUs }
      }
      return orderSKUs
    }

    render () {
      const {
        order,
        orderStatuses,
        onCreateAppointment,
        onOpenOrderDetailsModal,
        className,
        isCreateAppointment,
        onDeleteOrder,
        onEditAppointment,
        isDragging,
        isDisabled,
        checkedOrders,
        orderCases,
        orderWeight,
        orderPallets,
        isSelectAllOrders } = this.props
      const { selectedLocalState, hide } = this.state
      const orderStatus = orderStatuses && orderStatuses.find(s => s.get('id') === order.get('orderStatusId'))
      const isScheduled = orderStatus && orderStatus.get('name', 'draft').toLowerCase() === 'scheduled'
      const validStatusNames = ['open', 'scheduled', 'cancelled']
      const orderStatusName = orderStatus ? orderStatus.get('name', 'draft').toLowerCase() : 'draft'

      const destination = order.get('destination')

      const primaryRefValue = order.get('primaryRefValue') && order.get('primaryRefValue').length > 12 ? order.get('primaryRefValue').substring(0, 12) + '...' : order.get('primaryRefValue')

      const { createIcon, infoIcon, deleteIcon, arrowUpIcon, arrowDownIcon } = this.getIconsForOrderStatus(validStatusNames.find(sn => sn === orderStatusName) || 'open')

      const orderSKUs = this.getSKUs()

      return (
        <div>
          {(isDragging && (checkedOrders && checkedOrders.size > 1)) && <Notification> {checkedOrders.size} </Notification>}
          <OrderCardContainer hide={hide ? 1 : 0} className={className} isDisabled={isDisabled} status={orderStatusName}>
            {isCreateAppointment
              ? <OrderCardHeader status={orderStatusName}>
                <CheckboxContainer status={orderStatusName}>
                  <CustomCheckbox
                    checked
                    onChange={this.handleSelectedChange}
                    value='orderCheckbox'
                    color='default'
                    status={orderStatusName}
                  />
                </CheckboxContainer>
                <OrderCardLabel status={orderStatusName}>{primaryRefValue}</OrderCardLabel>
                <OrderCardButtonContainer>
                  <img
                    src={deleteIcon}
                    alt='Delete order'
                    onClick={onDeleteOrder}
                  />
                  <img
                    src={hide ? arrowUpIcon : arrowDownIcon}
                    alt='Details'
                    onClick={() => this.toggleHide()}
                  />
                </OrderCardButtonContainer>
              </OrderCardHeader>
              : <OrderCardHeader status={orderStatusName}>
                {!isScheduled && <CheckboxContainer status={orderStatusName}>
                  <CustomCheckbox
                    disabled={isScheduled || false}
                    checked={selectedLocalState || isSelectAllOrders}
                    onChange={this.handleSelectedChange}
                    value='orderCheckbox'
                    color='default'
                    status={orderStatusName}
                  />
                </CheckboxContainer>}
                <OrderCardLabel status={orderStatusName}>{primaryRefValue}</OrderCardLabel>
                <OrderCardButtonContainer>
                  <img src={createIcon} alt='Create Appointment'
                    onClick={() => isScheduled ? onEditAppointment() : onCreateAppointment()}/>
                  <img src={infoIcon} alt='Details'
                    onClick={onOpenOrderDetailsModal} />
                </OrderCardButtonContainer>
              </OrderCardHeader>
            }
            {hide
              ? null
              : <OrderCardContent>
                <Section>
                  <Field>
                    <Label>Customer</Label>
                    <BigValue>{order.getIn(['customer', 'name'])}</BigValue>
                  </Field>
                  <Field>
                    <Label>Destination</Label>
                    <BigValue>{destination && destination.get('city')}, {destination && destination.get('state')}</BigValue>
                  </Field>
                  <Field>
                    <Label>Delivery</Label>
                    <Value>{moment.utc(order.get('deliveryDate')).format('MM-DD-YYYY')}</Value>
                  </Field>
                </Section>
                <SectionBottom>
                  <Field>
                    <Label>QTY</Label>
                    <Value>{orderCases ? orderCases.value : 'N/A'}</Value>
                  </Field>
                  <Field>
                    <Label>Weight</Label>
                    <Value>{orderWeight ? `${orderWeight.value.toFixed(1)} ${orderWeight.UOM || ''}` : 'N/A'}</Value>
                  </Field>
                </SectionBottom>
                <SectionBottom>
                  <Field>
                    <Label>SKUs</Label>
                    <Value>{orderSKUs && orderSKUs.value.length}</Value>
                  </Field>
                  <Field>
                    <Label>Pallets</Label>
                    <Value>{(orderPallets && orderPallets.toFixed(2)) || 'N/A'}</Value>
                  </Field>
                </SectionBottom>
              </OrderCardContent>
            }
          </OrderCardContainer>
        </div>
      )
    }
}

OrderCard.propTypes = {
  checkedOrders: PropTypes.object,
  isDragging: PropTypes.bool,
  isDisabled: PropTypes.bool,
  order: PropTypes.object,
  selected: PropTypes.bool,
  onSelectedChange: PropTypes.func,
  onCreateAppointment: PropTypes.func,
  onOpenOrderDetailsModal: PropTypes.func,
  className: PropTypes.string,
  isCreateAppointment: PropTypes.bool,
  onDeleteOrder: PropTypes.func,
  collapseAll: PropTypes.bool,
  expandAll: PropTypes.bool,
  resetCollapseAndExpand: PropTypes.func,
  onEditAppointment: PropTypes.func,
  orderStatuses: PropTypes.object,
  orderCases: PropTypes.object,
  orderWeight: PropTypes.object,
  orderSKUs: PropTypes.object,
  orderPallets: PropTypes.object,
  isSelectAllOrders: PropTypes.bool
}

const mapStateToProps = state => ({
  orderStatuses: getAllOrderStatuses(state),
  orderItems: getOrderItems(state),
  items: getItems(state)
})

export default connect(
  mapStateToProps,
  {}
)(OrderCard)
