import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const Container = styled.div`
  display: flex;
  margin-right: 30px;
`

const Button = styled.div`
  border: 1px solid #aab0b0;
  background-color: white;
  color: #aab0b0;
  display: flex;
  align-items: center;
  height: 30px;
  padding: 0px 10px;
  font-weight: bold;
  font-size: 12px;
  cursor: pointer;

  ${props => props.active && css`
    background-color: #aab0c0;
    color: white;
  `}

  ${props => props.left && css`
    border-radius: 2px 0px 0px 2px;
  `}

  ${props => props.right && css`
    border-radius: 0px 2px 2px 0px;
  `}
`

class HourFormatSwitch extends Component {
  render () {
    const { is24Format, onChange } = this.props
    return (
      <Container>
        <Button active={!is24Format} left onClick={() => onChange(false)}>12hr</Button>
        <Button active={is24Format} right onClick={() => onChange(true)}>24hr</Button>
      </Container>
    )
  }
}

HourFormatSwitch.propTypes = {
  is24Format: PropTypes.object,
  onChange: PropTypes.func
}

export default HourFormatSwitch
