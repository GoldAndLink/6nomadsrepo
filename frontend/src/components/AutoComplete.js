import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import CreatableSelect from 'react-select/lib/Creatable'

const selectStyles = error => ({
  container: base => ({ ...base, border: 'none', boxShadow: 'none', outline: 'none' }),
  control: (base, { isFocused }) => ({
    ...base,
    borderRadius: 2,
    borderWidth: '0px 0px 1px 0px',
    borderColor: error ? '#d9534f' : (isFocused ? '#0eb48f' : '#bbb'),
    backgroundColor: 'transparent',
    boxShadow: 'none',
    outline: 'none'
  }),
  valueContainer: base => ({
    ...base,
    padding: '2px 0px'
  }),
  option: (base, { isSelected, isFocused }) => ({
    ...base,
    backgroundColor: isSelected || isFocused ? '#d2f8d8' : null,
    color: isSelected || isFocused ? '#61c9b5' : null
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: base => ({
    ...base,
    transform: 'scale(0.6)',
    padding: '8px 0px'
  })
})

const Container = styled.div`
  position: relative;
  width: 100%;
  margin-top: -7px;
`

const Label = styled.label`
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 600;
  font-size: 10px;

  ${props => props.error && css`
    color: red;
  `}
`

const HelpText = styled.label`
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 300;
  font-size: 12px;

  ${props => props.error && css`
    color: red;
  `}
`

class AutoComplete extends Component {
  onChangeAutoComplete = (selection, input) => {
    const { onChange } = this.props
    if (!selection) {
      // nothing selected
      onChange({ label: null, value: null })
    } else if (selection.value === selection.label) {
      // creating a new one
      onChange({ ...selection, value: null })
    } else {
      // updating existing
      onChange({ ...selection })
    }
  }

  render () {
    const { label, error, options, value } = this.props
    const selectedValue = value && options.find(option => option.value === value.value)

    return (
      <Container>
        <Label error={error}>{label}</Label>
        {error &&
          <HelpText error={error}>{error}</HelpText>
        }
        <CreatableSelect {...this.props} value={selectedValue} styles={selectStyles(error)} onChange={this.onChangeAutoComplete}/>
      </Container>
    )
  }
}

export default AutoComplete
