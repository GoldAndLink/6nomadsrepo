import React, { PureComponent } from 'react'
import moment from 'moment'
import 'moment-timezone'
import { Droppable } from 'react-beautiful-dnd'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { generateDropppableId } from '../utils/utils'
import { isRequestLate } from '../utils/time'

import Event from './Event'
import Slider from './Slider'
import DeleteAppointmentModal from './DeleteAppointmentModal'

import { appointmentStatuses } from '../containers/Appointments'
import { requestStatuses } from '../containers/Requests'

const DEFAULT_APPOINTMENT_STATUS = appointmentStatuses.draft
const DEFAULT_REQUEST_STATUS = requestStatuses.pending

const Table = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  z-index: 0;
  max-height: 100%;
`

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  min-height: 42px;
  color: #898989;
  font-size: 12px;
  background-color: #e7eaee;
  border-width: 1px 0px;
  border-style: solid;
  border-color: #aab0c0;
  overflow-x: hidden;
`

const HeaderTime = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-width: 60px;
  height: 42px;
  font-weight: bold;
  border-width: 0px 1px;
  border-style: solid;
  border-color: #aab0c0;
`

const HeaderDoor = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 42px;
  min-width: ${props => props.width}px;
  font-weight: 300;
  border-right: solid 1px #aab0c0;

  ${props => props.small && css`
    font-size: 9px;
  `}
`

const Scrollable = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  overflow-y: scroll;
  overflow-x: hidden;
  position: relative;
  height: 0;
`

const TimeColumn = styled.div`
  min-width: 61px;
  position: relative;
  height: ${props => props.height}px;
  border-right: solid 1px #ebebeb;
  background-color: #ffffff;
`

const TimeLabel = styled.div`
  margin-top: -5px;
  text-align: center;
  width: 60px;
  position: absolute;
  font-size: 14px;
  font-weight: 300;
  color: #898989;
  top: ${props => props.topPosition}px;
`

const HorizontalLine = styled.div`
  position: absolute;
  left: 61px;
  right: 0px;
  border-bottom: solid 1px #efefef;
  top: ${props => props.topPosition}px;
  z-index: 1;
`

const DoorColumn = styled.div`
  position: relative;
  min-width: ${props => props.width}px;
  height: ${props => props.height}px;
  overflow: hidden;
  border-right: solid 1px #efefef;
  top: ${props => props.topPosition}px;
  background-color: #ffffff;
`

const TableEvent = styled(Event)`
  position: absolute;
  left: 0px;
  top: ${props => props.topPosition}px;
  width: ${props => props.width}px;
  overflow: hidden;
  z-index: 2;
`

const DoorSlider = styled.div`
  min-height: 70px;
  background-color: #f4f6f9;
  box-shadow: 0 -1px 5px 0 rgba(0, 0, 0, 0.14);
  margin: 0px -10px;
  padding: 15px;
`

const DroppableContainer = styled.div`
  position: absolute;
  top: ${props => props.topPosition}px;
  width: ${props => props.width}px;
  height: ${props => props.height}px;
  z-index: 2;

  ${props => props.isDraggingOver && css`
    border: 1px solid #0eb48f;
  `}
`

const DEFAULT_COLUMN_SIZE = 140

class TimeTable extends PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      isDeleteAppointmentOpen: false,
      appointmentForDelete: null,
      firstDoor: 0,
      visibleColumns: 1
    }

    this.scrollable = React.createRef()
  }

  componentDidMount () {
    window.addEventListener('resize', this.onResize)
    this.onResize()
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.onResize)
  }

  componentDidUpdate (prevProps) {
    if (this.props.zoomLevel !== prevProps.zoomLevel) {
      this.setState({ firstDoor: 0 })
    }
    const { focusAppointment, timezone } = this.props
    if (focusAppointment && focusAppointment !== prevProps.focusAppointment) {
      const initTime = moment(focusAppointment.get('date')).tz(timezone || 'UTC')
      const startOfDay = moment.tz(`${this.props.startDate.format('YYYY-MM-DD')}`, timezone || 'UTC')
      const topPosition = this.getTimePosition(initTime, startOfDay)
      if (this.scrollable.current) {
        this.scrollable.current.scrollTop = topPosition
      }
    }
  }

  onResize = () => {
    const scrollable = this.scrollable.current
    const { width } = scrollable ? scrollable.getBoundingClientRect() : 0
    const columnSize = this.getColumnSize()

    const visibleColumns = Math.floor((width - 60) / columnSize)

    let { firstDoor } = this.state
    const prevVisibleColumns = this.state.visibleColumns
    if (visibleColumns > prevVisibleColumns) {
      firstDoor = Math.max(firstDoor - (visibleColumns - prevVisibleColumns), 0)
    }

    this.setState({
      visibleColumns,
      firstDoor
    })
  }

  getColumnSize () {
    const { zoomLevel } = this.props
    return DEFAULT_COLUMN_SIZE * zoomLevel / 100
  }

  getTimePosition (time, startOfDay) {
    const hour = time.diff(startOfDay, 'hour')
    return this.getColumnSize() * hour
  }

  disableDrop (startTimeContainer, doorAppointments) {
    const { isUpsertAppointmentVisible, timezone } = this.props
    const endTimeContainer = startTimeContainer.clone().add(60, 'minute')

    const intersection = doorAppointments.some(da => {
      const startTimeEvent = moment.tz(da.get('date'), timezone || 'UTC')
      const endTimeEvent = startTimeEvent.clone().add(da.get('duration'), 'minute')

      return startTimeEvent.isSame(startTimeContainer) ||
        (endTimeEvent.isAfter(startTimeContainer) &&
        endTimeEvent.isSameOrBefore(endTimeContainer))
    })

    return isUpsertAppointmentVisible || intersection
  }

  renderDroppableContainers (door, doorAppointments) {
    const { startDate, timezone, doorsSchedule } = this.props
    const columnSize = this.getColumnSize()
    const doorId = door.get('id')

    let startHour = 0
    let endHour = 23
    if (doorsSchedule && doorsSchedule[doorId]) {
      const s = doorsSchedule[doorId]
      startHour = s.startHour || 0
      if (s.endHour < s.startHour) {
        endHour = s.endHour + 24
      } else {
        endHour = s.endHour || 23
      }
    }

    return [...Array(endHour - startHour + 1).keys()].map(i => {
      const hour = startHour + i
      const startTime = startDate.clone().hour(hour).minute(0)
      const startTimeWithTimeZone = moment.tz(startDate.format('YYYY-MM-DDTHH:mm:ss'), timezone || 'UTC').hour(hour % 24).minute(0).second(0)
      if (hour >= 24) {
        startTimeWithTimeZone.add(1, 'day')
      }

      return (
        <Droppable
          key={doorId + 'droppable' + hour}
          droppableId={generateDropppableId('timeline', startTime, doorId)}
          isDropDisabled={this.disableDrop(startTimeWithTimeZone, doorAppointments)}
        >
          {(provided, snapshot) => (
            <DroppableContainer
              innerRef={provided.innerRef}
              {...provided.droppableProps}
              topPosition={i * columnSize}
              width={columnSize - 2}
              height={columnSize}
              isDraggingOver={snapshot.isDraggingOver}
            >
              {provided.placeholder}
            </DroppableContainer>
          )}
        </Droppable>
      )
    })
  }

  getStatus = (carrierRequest, appointmentStatus) => {
    const carrierRequestStatus = carrierRequest ? carrierRequest.get('status') : null
    if (carrierRequestStatus === requestStatuses.canceled && appointmentStatus && (appointmentStatus.get('name') === appointmentStatuses.draft || appointmentStatus.get('name') === appointmentStatuses.scheduled)) {
      return requestStatuses.canceled
    } else if (carrierRequestStatus === requestStatuses.reschedule && appointmentStatus && (appointmentStatus.get('name') === appointmentStatuses.draft || appointmentStatus.get('name') === appointmentStatuses.scheduled)) {
      return requestStatuses.reschedule
    } else {
      return carrierRequestStatus || DEFAULT_REQUEST_STATUS
    }
  }

  renderDoorColumn (door, minHour) {
    const {
      isUpsertAppointmentVisible,
      appointments,
      appointmentStatuses,
      zoomLevel,
      onEditAppointment,
      onAppointmentDurationChange,
      onAppointmentMessage,
      doorsSchedule,
      startDate,
      timezone
    } = this.props

    if (!appointments) return

    const doorId = door.get('id')
    let startHour = 0
    let endHour = 23
    if (doorsSchedule && doorsSchedule[doorId]) {
      const s = doorsSchedule[doorId]
      startHour = s.startHour || 0
      if (s.endHour < s.startHour) {
        endHour = s.endHour + 24
      } else {
        endHour = s.endHour || 23
      }
    }

    const doorAppointments = appointments.filter(appointment =>
      appointment.get('doorId') === doorId
    )

    const columnSize = this.getColumnSize()
    const startOfDay = moment.tz(`${startDate.format('YYYY-MM-DD')}`, timezone || 'UTC').hour(startHour)

    return (
      <DoorColumn
        key={doorId + 'column'}
        topPosition={(startHour - minHour) * columnSize}
        width={columnSize}
        height={columnSize * (endHour - startHour + 1)}
      >
        {this.renderDroppableContainers(door, doorAppointments)}
        { doorAppointments.map(appointment => {
          const initTime = moment(appointment.get('date')).tz(timezone || 'UTC')
          const duration = appointment.get('duration')
          const endTime = initTime.clone().add(duration, 'minute')
          const topPosition = this.getTimePosition(initTime, startOfDay)
          const bottomPosition = this.getTimePosition(endTime, startOfDay)

          const appointmentStatus = appointmentStatuses ? appointmentStatuses.find(as =>
            as.get('id') === appointment.get('appointmentStatusId')
          ) : null

          const carrierRequest = appointment.get('carrierRequests').valueSeq().first()

          return (
            <TableEvent
              size={zoomLevel}
              key={appointment.get('id')}
              id={appointment.get('id')}
              appointment={appointment}
              initTime={initTime}
              endTime={endTime}
              duration={duration}
              onDurationChange={duration => onAppointmentDurationChange(appointment, duration)}
              primaryRefValue={appointment.get('primaryRefValue') || ''}
              PO={appointment.get('PO') || ''}
              onEdit={() => onEditAppointment(appointment)}
              onMessage={() => onAppointmentMessage(appointment)}
              onDelete={() => this.onAppointmentDelete(appointment)}
              totalOrders={appointment.get('orders').size}
              isUpsertAppointmentVisible={isUpsertAppointmentVisible}
              topPosition={topPosition}
              width={columnSize}
              height={bottomPosition - topPosition}
              inProgress={appointment.get('inProgress')}
              isRequestLate={isRequestLate(carrierRequest, appointment, appointmentStatus)}
              requestStatus={this.getStatus(carrierRequest, appointmentStatus)}
              carrierRequest={carrierRequest}
              appointmentStatuses={appointmentStatus}
              appointmentStatus={appointmentStatus ? appointmentStatus.get('name') : DEFAULT_APPOINTMENT_STATUS}
            />
          )
        })}
      </DoorColumn>
    )
  }

  onAppointmentDelete = appointment => {
    this.setState({
      isDeleteAppointmentOpen: true,
      appointmentForDelete: appointment
    })
  }

  onCloseDeleteAppointment = () => {
    this.setState({
      isDeleteAppointmentOpen: false,
      appointmentForDelete: null
    })
  }

  onConfirmAppointmentDelete = () => {
    const { deleteAppointment } = this.props
    deleteAppointment(this.state.appointmentForDelete)
    this.onCloseDeleteAppointment()
  }

  render () {
    const {
      doors,
      doorsSchedule,
      zoomLevel,
      is24Format
    } = this.props
    const { isDeleteAppointmentOpen, firstDoor, visibleColumns } = this.state

    const columnSize = this.getColumnSize()

    let minHour = 0
    let maxHour = 23
    if (doors && doorsSchedule) {
      minHour = 24
      maxHour = 0
      doors.map(door => {
        const schedule = doorsSchedule[door.get('id')]
        if (schedule) {
          minHour = Math.min(minHour, schedule.startHour)
          if (schedule.endHour < schedule.startHour) {
            maxHour = Math.max(maxHour, schedule.endHour + 24)
          } else {
            maxHour = Math.max(maxHour, schedule.endHour)
          }
        } else {
          minHour = Math.min(minHour, 0)
          maxHour = Math.max(maxHour, 23)
        }
      })
    }

    const numHours = maxHour - minHour + 1

    return (
      <Table>
        <HeaderContainer>
          <HeaderTime>
            Time
          </HeaderTime>
          {
            doors && doors.skip(firstDoor).map(door => (
              <HeaderDoor
                key={door.get('id') + 'event'}
                width={columnSize}
                small={zoomLevel <= 25}
              >
                {door.get('name')}
              </HeaderDoor>
            ))
          }
        </HeaderContainer>
        <Scrollable innerRef={this.scrollable}>
          <TimeColumn height={columnSize * numHours}>
            {[...Array(numHours).keys()].map(i => i > 0 &&
              <TimeLabel topPosition={i * columnSize} key={i}>
                {moment().hour(minHour + i).format(is24Format ? 'HH:00' : 'hh:00a')}
              </TimeLabel>
            )}
          </TimeColumn>
          <div>
            {[...Array(numHours).keys()].map(i =>
              <HorizontalLine topPosition={i * columnSize} key={i} />
            )}
          </div>
          { doors && doors.skip(firstDoor).map(door =>
            this.renderDoorColumn(door, minHour))
          }
        </Scrollable>
        <DoorSlider>
          { doors && (doors.size > visibleColumns) &&
            <Slider
              min={0}
              max={doors.size - visibleColumns}
              value={firstDoor}
              onChange={firstDoor => this.setState({ firstDoor })}
            />
          }
        </DoorSlider>
        <DeleteAppointmentModal
          isOpen={isDeleteAppointmentOpen}
          onConfirmDelete={this.onConfirmAppointmentDelete}
          onClose={this.onCloseDeleteAppointment}
        />
      </Table>
    )
  }
}

TimeTable.propTypes = {
  appointments: PropTypes.any,
  isUpsertAppointmentVisible: PropTypes.bool,
  deleteAppointment: PropTypes.func,
  appointmentStatuses: PropTypes.any,
  doors: PropTypes.object,
  doorsSchedule: PropTypes.object,
  zoomLevel: PropTypes.number
}

TimeTable.defaultProps = {
  appointments: [],
  doors: []
}

export default TimeTable
