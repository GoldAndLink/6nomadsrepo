import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment-timezone'
import { connect } from 'react-redux'
import styled, { css } from 'styled-components'
import Modal from './Modal'
import UpsertAppointmentForm from '../forms/UpsertAppointmentForm'
import {
  getIsUpsertAppointmentVisible,
  getEditingAppointment
} from '../modules/appointments/selectors'
import {
  getAllDoors,
  getAllBuildings,
  getAllAreas,
  getAllSites
} from '../modules/entities/selectors'
import {
  getStartDate,
  getWarehouse,
  getSite
} from '../modules/app/selectors'
import AppointmentsActions from '../modules/appointments/actions'
import closeIcon from '../assets/images/close.svg'

const APPOINTMENTS_TAB = 0
const INVENTORY_TAB = 1
const EMAIL_HIDDEN_TAB = 2
const SUMMON_SMS_TAB = 3

const customModalStyles = {
  content: {
    top: '60px',
    left: '350px',
    right: 'auto',
    bottom: 'auto'
  },
  overlay: {
    backgroundColor: 'rgba(60, 65, 78, 0.6)'
  }
}

const Header = styled.div`
  display: flex;
  align-items: center;
  height: 45px;
  padding: 0px 10px;
  border-bottom: 1px solid #e7eaee;
`

const Tabs = styled.div`
  flex: 1;
  display: flex;
`

const Tab = styled.div`
  cursor: pointer;
  font-size: 10px;
  font-weight: bold;
  letter-spacing: 1.1px;
  color: #bbbbbb;
  text-transform: uppercase;
  margin-right: 30px;

  ${props => props.selected && css`
    color: #61c9b5;
    text-decoration: underline;
  `}
`

const Close = styled.div`
  cursor: pointer;
`

class UpsertAppointmentModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      initialValues: {},
      tab: 0,
      editingAppointment: null,
      isDeleteAppointmentOpen: false,
      appointmentForDelete: null,
      formChanged: false
    }
  }

  componentDidMount () {
    document.addEventListener('keydown', this.onEscClose)
  }

  componentWillUnmount () {
    document.removeEventListener('keydown', this.onEscClose)
  }

  componentDidUpdate (prevProps) {
    const { isOpen } = this.props

    if (!isOpen && prevProps.isOpen) {
      this.setState({ tab: APPOINTMENTS_TAB })
    }
  }

  onEscClose = event => {
    if (event.keyCode === 27) {
      // if needed when this.state.formChanged is true, show warning modal
      this.handleClose()
    }
  }

  hasFormChanged = formChanged => {
    this.setState({ formChanged })
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    const { editingAppointment, doors, buildings, areas, sites } = nextProps

    if (editingAppointment !== prevState.editingAppointment) {
      if (!editingAppointment) {
        return { initialValues: {}, editingAppointment: null }
      } else {
        const object = editingAppointment.toJS()

        const doorId = editingAppointment.get('doorId')
        const door = doorId && doors.get(doorId + '')
        const area = door && areas.get(door.get('areaId') + '')
        const building = (area && buildings.get(area.get('buildingId') + '')) || editingAppointment.get('building')
        const site = (building && sites.get(building.get('siteId') + '')) || editingAppointment.get('site')
        const timezone = (building && building.get('timezone')) || 'UTC'

        const datetime = object.date
        object.date = moment(datetime).tz(timezone)
        object.time = moment(datetime).tz(timezone)
        object.building = building && building.get('id')
        object.carrier = editingAppointment.get('carrierId') && { value: editingAppointment.get('carrierId').toString() }
        object.driver = editingAppointment.get('driverId') && { value: editingAppointment.get('driverId').toString() }
        object.site = site && site.get('id')
        object.area = area && area.get('id')
        object.inProgress = editingAppointment.get('inProgress')
        object.inventoryReviewUserId = editingAppointment.get('inventoryReviewUserId')

        return { initialValues: object, editingAppointment: editingAppointment }
      }
    }
    return null
  }

  handleClose = () => {
    const { deleteAppointment, editingAppointment, close } = this.props

    if (editingAppointment && editingAppointment.get('inProgress')) {
      close()
      deleteAppointment(editingAppointment.get('id'))
    } else {
      close()
    }
  }

  onDeleteAppointment = id => {
    this.setState({
      isDeleteAppointmentOpen: true,
      appointmentForDelete: id
    })
  }

  onCloseDeleteAppointment = () => {
    this.setState({
      isDeleteAppointmentOpen: false,
      appointmentForDelete: null
    })
  }

  switchToAppointmentsTab = () => this.setState({ tab: APPOINTMENTS_TAB })

  switchToInventoryTab = () => this.setState({ tab: INVENTORY_TAB })

  switchToEmailHiddenTab = () => this.setState({ tab: EMAIL_HIDDEN_TAB })

  switchToSummonSMSTab = () => this.setState({ tab: SUMMON_SMS_TAB })

  render () {
    const { isOpen, startDate, editingAppointment, warehouse, site } = this.props
    const {
      initialValues,
      tab,
      isDeleteAppointmentOpen,
      appointmentForDelete } = this.state

    return (
      <Modal
        hideHeader={true}
        isOpen={isOpen}
        styles={customModalStyles}
      >
        <div>
          <Header>
            <Tabs>
              <Tab onClick={this.switchToAppointmentsTab} selected={(tab === APPOINTMENTS_TAB) || (tab === EMAIL_HIDDEN_TAB)}>Appointment</Tab>
              <Tab onClick={this.switchToInventoryTab} selected={tab === INVENTORY_TAB}>Inventory</Tab>
              <Tab onClick={this.switchToSummonSMSTab} selected={tab === SUMMON_SMS_TAB}>Summon Driver</Tab>
            </Tabs>
            <Close onClick={this.handleClose}>
              <img src={closeIcon} alt="close" />
            </Close>
          </Header>
          <UpsertAppointmentForm
            editingAppointment={editingAppointment}
            initialValues={{
              ...initialValues,
              site: initialValues.site || site,
              building: initialValues.building || warehouse,
              date: initialValues.date || startDate
            }}
            tab={tab}
            switchToEmailHiddenTab={this.switchToEmailHiddenTab}
            switchToAppointmentsTab={this.switchToAppointmentsTab}
            switchToInventoryTab={this.switchToInventoryTab}
            subscription={{ submitting: true, pristine: true }}
            isDeleteAppointmentOpen={isDeleteAppointmentOpen}
            appointmentForDelete={appointmentForDelete}
            onDeleteAppointment={this.onDeleteAppointment}
            onCloseDeleteAppointment={this.onCloseDeleteAppointment}
            hasFormChanged={this.hasFormChanged}
          />
        </div>
      </Modal>
    )
  }
}

UpsertAppointmentModal.propTypes = {
  site: PropTypes.object,
  warehouse: PropTypes.object,
  startDate: PropTypes.any,
  deleteAppointment: PropTypes.func,
  isOpen: PropTypes.bool,
  getDrivers: PropTypes.func,
  getCarriers: PropTypes.func,
  editingAppointment: PropTypes.any,
  close: PropTypes.func,
  buildings: PropTypes.object
}

const mapStateToProps = state => ({
  site: getSite(state),
  startDate: getStartDate(state),
  isOpen: getIsUpsertAppointmentVisible(state),
  editingAppointment: getEditingAppointment(state),
  doors: getAllDoors(state),
  warehouse: getWarehouse(state),
  buildings: getAllBuildings(state),
  areas: getAllAreas(state),
  sites: getAllSites(state)
})

const mapDispatchToProps = dispatch => ({
  close: () => dispatch(AppointmentsActions.closeUpsertAppointment()),
  deleteAppointment: id => dispatch(AppointmentsActions.deleteAppointment(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true }
)(UpsertAppointmentModal)
