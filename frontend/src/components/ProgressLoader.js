import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { keyframes } from 'styled-components'
import loadingIcon from '../assets/images/loading.svg'

const Loading = styled.div`
  position: fixed;
  z-index: 4;
  top: 0;
  left: 0;
  width:100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  background-color: rgba(126, 129, 138, 0.75);
  display: ${props => props.isOpen ? 'flex' : 'none'}
`

const Title = styled.div`
  font-size: 24px;
  color: white;
  margin-top: 10px;
`
const LoadingIconContainer = styled.div`
  display: flex;
  position: absolute;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const rotation = keyframes`
  from {-webkit-transform: rotate(0deg);}
  to   {-webkit-transform: rotate(359deg);}
`

const LoadingIcon = styled.img`
  -webkit-animation: ${rotation} 2s infinite linear;
`

class ProgressLoader extends Component {
  render () {
    const { isOpen } = this.props
    return (
      <Loading isOpen={isOpen}>
        <LoadingIconContainer>
          <LoadingIcon src={loadingIcon} alt='loadingIcon' />
          <Title>LOADING...</Title>
        </LoadingIconContainer>
      </Loading>
    )
  }
}

ProgressLoader.propTypes = {
  isOpen: PropTypes.bool
}

export default ProgressLoader
