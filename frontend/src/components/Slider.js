import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const Container = styled.div`
  position: relative;
  height: 10px;
  width: 100%;

  ${props => props.dragging && css`
    cursor: grabbing;
  `}
`

const Handler = styled.div`
  user-select: none;
  position: absolute;
  cursor: pointer;
  width: 50px;
  border-radius: 7px;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.23);
  background-color: #ffffff;
  height: 10px;
  left: ${props => props.position}px;
`

const HorizonalLine = styled.div`
  position: absolute;
  top: 5px;
  left: 0px;
  right: 0px;
  height: 1px;
  border-bottom: solid 1px #e5e2e5;
`

class Slider extends Component {
  state = {
    dragging: false,
    draggingX: 0,
    draggingStartX: 0,
    draggingStartValue: 0,
    width: 0,
    left: 0
  }

  container = React.createRef()

  componentDidMount () {
    window.addEventListener('mouseup', this.onMouseUp)
    window.addEventListener('resize', this.onResize)
    this.onResize()
  }

  componentWillUnmount () {
    window.removeEventListener('mouseup', this.onMouseUp)
    window.removeEventListener('resize', this.onResize)
  }

  onResize = () => {
    const container = this.container.current
    const { left, width } = container ? container.getBoundingClientRect() : 0
    this.setState({ left, width })
  }

  onMouseDown = e => {
    window.addEventListener('mousemove', this.onMouseMove)

    this.setState({
      dragging: true,
      draggingX: e.clientX,
      draggingStartX: e.clientX,
      draggingStartValue: this.props.value
    })
  }

  onMouseMove = e => {
    const { draggingStartX, draggingStartValue, width } = this.state
    const { min, max, value, onChange } = this.props

    if (width === 0) return

    const valueDiff = Math.round((max - min) * (e.clientX - draggingStartX) / (width - 50))

    const newValue = Math.max(Math.min(draggingStartValue + valueDiff, max), 0)

    this.setState({ draggingX: e.clientX })
    if (newValue !== value) {
      onChange(newValue)
    }
  }

  onMouseUp = () => {
    window.removeEventListener('mousemove', this.onMouseMove)
    this.setState({ dragging: false })
  }

  render () {
    const { min, max, value } = this.props
    const { dragging, draggingX, left, width } = this.state

    let position = 0
    if (max > min && !dragging) {
      position = (width - 50) * (value - min) / (max - min)
    } else {
      position = Math.min(Math.max(draggingX - left - 25, 0), width - 25)
    }

    return (
      <Container innerRef={this.container} dragging={dragging}>
        <HorizonalLine />
        <Handler position={position} onMouseDown={this.onMouseDown} />
      </Container>
    )
  }
}

Slider.propTypes = {
  value: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  onChange: PropTypes.func
}

export default Slider
