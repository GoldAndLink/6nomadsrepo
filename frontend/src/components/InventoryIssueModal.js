import React, { Component } from 'react'
import Modal from './Modal'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Button from './Button'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

const Container = styled.div`
  width: 400px;
`

const Header = styled.div`
  height: 40px;
  border-bottom: 1px solid #e9edf3;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #444a59;
  font-weight: bold;
  font-size: 14px;
  position: relative;
  text-transform: uppercase;
  letter-spacing: 1.1;
`

const Content = styled.div`
  padding: 8px 0px 20px 0px;
  color: #898989;
  display: flex;
  flex-direction: column;
  align-items: center;

  p {
    margin: 30px;
  }
`

const Buttons = styled.div`
  display: flex;
  align-content: center;
`

const PrimaryButton = styled(Button)`
  width: 110px;
  height: 30px;
  background-color: #61c9b5;
  border: 1px solid #0eb48f;
  font-size: 12px;
  color: white;
  margin: 0px 12px;
`

const CancelButton = styled(Button)`
  width: 90px;
  height: 30px;
  background-color: #f3f6f6;
  border: 1px solid #aab0c0;
  font-size: 12px;
  color: #aab0c0;
  margin: 0px 12px;
`

class InventoryIssueModal extends Component {
  render () {
    const { isOpen, onSave, onCancel } = this.props

    return (
      <Modal
        isOpen={isOpen}
        onRequestClose={onCancel}
        styles={customStyles}
        hideHeader={true}
      >
        <Container>
          <Header>
            Inventory Issue
          </Header>
          <Content>
            <p>There is a potential inventory shortage on this appointment. Are you sure you want to save it?</p>
            <Buttons>
              <PrimaryButton onClick={onSave}>
                Save
              </PrimaryButton>
              <CancelButton onClick={onCancel}>
                Cancel
              </CancelButton>
            </Buttons>
          </Content>
        </Container>
      </Modal>
    )
  }
}

InventoryIssueModal.propTypes = {
  isOpen: PropTypes.bool,
  onSave: PropTypes.func,
  onCancel: PropTypes.func
}

export default InventoryIssueModal
