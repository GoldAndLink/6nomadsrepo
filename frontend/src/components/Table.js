import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const TableElement = styled.table`
  border-collapse: collapse;
`

const Tr = styled.tr`
  background-color: ${props => props.issue && !props.noIssues ? '#FDEDEF' : 'none'};
`

const Th = styled.th`
  text-align: left;
  font-weight: 300;
  padding: 10px 24px;
  border-bottom: 1px solid ${props => props.theme.lighterGray};
`

const Td = styled.td`
  padding: 10px 24px;
  border-bottom: 1px solid ${props => props.theme.lighterGray};
`

class Table extends Component {
  render () {
    const { data, children, className, noIssues } = this.props
    const columns = React.Children.map(children, ({ props }) => props)
    return (
      <TableElement className={className}>
        <thead>
          <Tr>
            {columns.map((column, i) => (
              <Th key={i}>{column.title}</Th>
            ))}
          </Tr>
        </thead>
        <tbody>
          {data && data.map((item, i) =>
            <Tr key={i} noIssues={noIssues} issue={item.get('issue')}>
              {columns.map((column, j) => (
                <Td key={j}>{column.content && column.content(item)}</Td>
              ))}
            </Tr>
          )}
        </tbody>
      </TableElement>
    )
  }
}

Table.propTypes = {
  data: PropTypes.object,
  children: PropTypes.node,
  className: PropTypes.object,
  noIssues: PropTypes.bool
}

export const Column = () => null

export default Table
