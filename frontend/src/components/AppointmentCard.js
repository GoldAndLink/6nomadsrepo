import React, { Component } from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { convertToCamelCase, convertToKebabCase, formatPhoneNumber } from '../utils/utils'
import { requestStatuses } from '../containers/Requests'
import { appointmentStatuses } from '../containers/Appointments'
import { isRequestLate } from '../utils/time'

const images = require.context('../assets/images/', true)

const Container = styled.div`
  border: 1px solid ${props => props.status ? props.theme.appointmentStatuses[props.status].text : '#aab0c0'};
  border-radius: 2px;
  display: flex;
  flex-direction: column;
`

const Header = styled.div`
  display: flex;
  background-color: ${props => props.status ? props.theme.appointmentStatuses[props.status].background : '#f3f6f6'};
  border-bottom: 1px solid ${props => props.status ? props.theme.appointmentStatuses[props.status].text : '#aab0c0'};
  padding: 10px;
  align-items: center;
`

const Title = styled.div`
  text-transform: uppercase;
  font-size: 12px;
  color: ${props => props.status ? props.theme.appointmentStatuses[props.status].text : '#aab0c1'};
  flex: 1;
`

const Buttons = styled.div`
  img {
    margin-left: 5px;
    cursor: pointer;
  }
`

const Content = styled.div`
  background-color: white;
  padding: 10px;
`
const Section = styled.div`
  display: flex;
  flex-direction: column;
`

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 2.5px;
  margin-top: 2.5px;
  flex-wrap: nowrap;
  flex-basis: 40%;
`

const SectionTitle = styled.h3`
  width: 100%;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #aab0c0;
  margin: 5px 0px 5px 0px;
`

const Label = styled.span`
  font-weight: bold;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;

  &::after {
    content: ':';
  }
`

const Value = styled.span`
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const getDate = (appointment, doors, areas, buildings) => {
  const date = moment.utc(appointment.get('date'))
  if (date) {
    const door = doors.find(d => d.get('id') === appointment.get('doorId'))
    const area = areas.find(d => d.get('id') === door.get('areaId'))
    const building = buildings.find(d => d.get('id') === area.get('buildingId'))

    return date.tz(building.get('timezone', 'UTC')).format('L')
  }
  return ''
}

const getDoor = appointment => appointment.getIn(['door', 'name'])

const getBuilding = appointment => appointment.getIn(['door', 'area', 'building', 'name'])

const getLocation = appointment => appointment.getIn(['door', 'area', 'building', 'location', 'name'])

const getCarrier = appointment => appointment.getIn(['carrier', 'name'])

const getDriver = appointment => {
  const firstName = appointment.getIn(['driver', 'firstName'])
  const lastName = appointment.getIn(['driver', 'lastName'])
  return `${firstName || ''} ${lastName || ''}`
}

const getIcons = (requestStatus, appointmentStatus, isRequestLate, appointment) => {
  const requestException = [requestStatuses.canceled, requestStatuses.reschedule, requestStatuses.carrierLate].includes(requestStatus)
  const appointmentStatusWithPossibleException = [appointmentStatuses.draft, appointmentStatuses.scheduled].includes(appointmentStatus)
  const appointmentStatusValue = isRequestLate ? convertToKebabCase(requestStatuses.carrierLate) : convertToKebabCase(appointmentStatus)
  const inventoryIssues = appointment.get('inventoryIssues', null)
  const inventoryInvalid = inventoryIssues && inventoryIssues.size
  const inventoryReviewUserId = appointment.get('inventoryReviewUserId')

  let infoIcon = ''
  if (appointmentStatusWithPossibleException && (inventoryInvalid && !inventoryReviewUserId)) {
    infoIcon = images(`./info-icon-list-inventory-issue-${appointmentStatusValue}.svg`)
  } else if (requestException && appointmentStatusWithPossibleException) {
    infoIcon = images(`./info-icon-list-${appointmentStatusValue}-${requestStatus}.svg`)
  } else {
    infoIcon = images(`./info-icon-list-${appointmentStatusValue}.svg`)
  }

  const deleteIcon = images(`./delete-icon-${appointmentStatusValue}.svg`)

  return {
    deleteIcon,
    infoIcon
  }
}

class AppointmentCard extends Component {
  state = { isRequestLate: false }

  calculateLateness = () => {
    const { appointmentStatuses, appointment } = this.props
    const appointmentStatus = appointmentStatuses ? appointmentStatuses.find(as =>
      as.get('id') === appointment.get('appointmentStatusId')
    ) : null
    const carrierRequest = appointment.get('carrierRequests').valueSeq().first()
    if (appointmentStatus && appointment) {
      this.setState({
        isRequestLate: isRequestLate(carrierRequest, appointment, appointmentStatus)
      })
    }
  }

  componentDidMount () {
    this.calculateLateness()
    this.interval = setInterval(() => {
      this.calculateLateness()
    }, 4000)
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }
  render () {
    const { appointment, onClick, onEdit, onDelete, className, appointmentStatus, requestStatus, doors, areas, buildings } = this.props
    const { isRequestLate } = this.state
    return (
      <Container className={className} status={convertToCamelCase(isRequestLate ? requestStatuses.carrierLate : appointmentStatus)}>
        <Header status={convertToCamelCase(isRequestLate ? requestStatuses.carrierLate : appointmentStatus)}>
          <Title status={convertToCamelCase(isRequestLate ? requestStatuses.carrierLate : appointmentStatus)}>Appt #{appointment.get('id')}</Title>
          <Buttons>
            <img src={getIcons(requestStatus, appointmentStatus, isRequestLate, appointment).infoIcon} onClick={onEdit} alt='infoIcon' />
            <img src={getIcons(requestStatus, appointmentStatus, isRequestLate, appointment).deleteIcon} onClick={onDelete} alt='deleteIcon' />
          </Buttons>
        </Header>
        <Content onClick={onClick}>
          <Section>
            <Field col={7}>
              <Label>Status</Label>
              <Value>{appointmentStatus}</Value>
            </Field>
          </Section>
          <Section>
            <SectionTitle>General</SectionTitle>
            <Field col={7}>
              <Label>Date</Label>
              <Value>{getDate(appointment, doors, areas, buildings)}</Value>
            </Field>
            <Field col={5}>
              <Label>Duration</Label>
              <Value>{appointment.get('duration')}</Value>
            </Field>
            <Field col={7}>
              <Label>Notes</Label>
              <Value>{appointment.get('notes')}</Value>
            </Field>
          </Section>
          <Section>
            <SectionTitle>Location</SectionTitle>
            <Field col={7}>
              <Label>Location</Label>
              <Value>{getLocation(appointment)}</Value>
            </Field>
            <Field col={5}>
              <Label>Door</Label>
              <Value>{getDoor(appointment)}</Value>
            </Field>
            <Field col={7}>
              <Label>Destination</Label>
              <Value>{appointment.get('destination')}</Value>
            </Field>
            <Field col={5}>
              <Label>Building</Label>
              <Value>{getBuilding(appointment)}</Value>
            </Field>
          </Section>
          <Section>
            <SectionTitle>Carrier</SectionTitle>
            <Field col={7}>
              <Label>Carrier Name</Label>
              <Value>{getCarrier(appointment)}</Value>
            </Field>
            <Field col={5}>
              <Label>Load Type</Label>
              <Value>{''}</Value>
            </Field>
            <Field col={7}>
              <Label>Trailer</Label>
              <Value>{appointment.get('trailer')}</Value>
            </Field>
            <Field col={5}>
              <Label>Seal</Label>
              <Value>{''}</Value>
            </Field>
          </Section>
          <Section>
            <SectionTitle>Driver</SectionTitle>
            <Field col={12}>
              <Label>Driver Name</Label>
              <Value>{getDriver(appointment)}</Value>
            </Field>
            <Field col={12}>
              <Label>Driver Phone Number</Label>
              <Value>{formatPhoneNumber(appointment.get('contactPhone'))}</Value>
            </Field>
          </Section>
        </Content>
      </Container>
    )
  }
}

AppointmentCard.propTypes = {
  appointmentStatuses: PropTypes.any,
  doors: PropTypes.object,
  areas: PropTypes.object,
  buildings: PropTypes.object,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  className: PropTypes.string,
  appointment: PropTypes.object,
  onClick: PropTypes.func,
  appointmentStatus: PropTypes.string,
  requestStatus: PropTypes.string,
  isRequestLate: PropTypes.bool,

  orders: PropTypes.object
}

export default AppointmentCard
