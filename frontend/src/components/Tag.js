import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div`
  margin: 0px 5px;
  background-color: #f3f6f6;
  border: 1px solid #aab0c0;
  padding: 6px 10px;
  border-radius: 2px;
`

const CloseIcon = styled.span`
  cursor: pointer;
  font-weight: 700;
  margin-left: 10px;
`

const Tag = ({ label, onClose, className }) => (
  <Container className={className}>
    {label}
    <CloseIcon onClick={onClose}>x</CloseIcon>
  </Container>
)

Tag.propTypes = {
  label: PropTypes.string,
  onClose: PropTypes.func,
  className: PropTypes.object
}

export default Tag
