import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { requestStatuses } from '../containers/Requests'
const images = require.context('../assets/images/', true)

const DEFAULT_STATUS = 'pending'

const Container = styled.div`
  border: solid 1px #AAB0C0;
  border-radius: 2px;
  display: flex;
  flex-direction: column;
`

const Header = styled.div`
  display: flex;
  background-color: #fff;
  border-bottom: ${props => props.hide ? 'none' : '1px solid #aab0c0'};
  background-color: ${props => props.theme.requestStatuses[props.status] ? props.theme.requestStatuses[props.status].background : '#F3F6F6'};
  padding: 10px 3px 10px 10px;
  align-items: center;
  min-height: 24px;
`

const Title = styled.div`
  text-transform: uppercase;
  font-size: 11px;
  color: ${props => props.theme.requestStatuses[props.status] ? props.theme.requestStatuses[props.status].text : '#AAB0C0'};
  flex: 1;
  margin-right: 10px;
`

const Buttons = styled.div`
  img {
    cursor: pointer;
  }
`

const Content = styled.div`
  background-color: white;
  padding: 10px;
  cursor: pointer;
  display: ${props => props.hide ? 'none' : 'initial'};
`

const Section = styled.div`
  display: flex;
  flex-wrap: wrap;
  font-size: 10px;
`

const Field = styled.div`
  display: flex;
  flex-direction: row;
  color: #b6b6b6;
  margin-bottom: 5px;
`

const Label = styled.span`
  width: 81px;
  font-weight: bold;
  margin-right: 5px;

  &::after {
    content: ':';
  }
`

const Value = styled.span`
  width: 100px;
  font-weight: 300;
`

const HorizontalSpace = styled.div`
  margin-bottom: 10px;
`

const AppointmentIcon = styled.img`
  height: 24px;
`

const getDate = appointment => {
  const date = appointment.get('date')
  if (date) {
    return moment(date).format('dddd, MMMM D')
  }
}

const getIconForRequestStatus = name => {
  return {
    appointmentIcon: images(`./request-icon-${name}.svg`)
  }
}

const getHeader = carrierRequest => {
  if (carrierRequest.get('status') === requestStatuses.canceled) {
    return 'Cancellation request'
  } else if (carrierRequest.get('status') === requestStatuses.reschedule) {
    return 'Reschedule requested '
  } else {
    return getDate(carrierRequest)
  }
}

const getRescheduleTime = (carrierRequest, timezone) => {
  if (carrierRequest.get('rescheduleTimeSuggestion') && carrierRequest.get('status') === requestStatuses.reschedule) {
    return carrierRequest && moment(carrierRequest.get('rescheduleTimeSuggestion')).tz(timezone).format('MM.DD.YYYY - LT')
  }
  if (!carrierRequest.get('rescheduleTimeSuggestion')) {
    return <div />
  }
}

const getRequestStatus = request => {
  const validRequestStatuses = [requestStatuses.pending, requestStatuses.scheduled, requestStatuses.reschedule, requestStatuses.canceled]
  return validRequestStatuses.find(as => as === request.get('status')) || DEFAULT_STATUS
}
const RequestCard = ({ carrierRequest, onClick, onCreate, className, hide, timezone }) => {
  const carrierRequestOrders = carrierRequest.getIn(['carrierRequestOrders'], null)
  const carrierRequestOrder = carrierRequestOrders ? carrierRequestOrders.first() : null
  return (
    <Container className={className}>
      <Header hide={hide}>
        <Title status={getRequestStatus(carrierRequest)}>
          {getHeader(carrierRequest)}
          {getRescheduleTime(carrierRequest, timezone)}
        </Title>
        <Buttons>
          <AppointmentIcon src={getIconForRequestStatus(getRequestStatus(carrierRequest)).appointmentIcon} onClick={onCreate} alt='editIcon' />
        </Buttons>
      </Header>
      <Content onClick={onClick} hide={hide}>
        <Section>
          <Field>
            <Label>Pickup Date</Label>
            <Value>{moment(carrierRequest.get('date')).format('MM-DD-YYYY')}</Value>
          </Field>
          <Field>
            <Label>Pickup Time</Label>
            <Value>{moment(carrierRequest.get('timeStart')).utc().format('HH:mm') + '-' + moment(carrierRequest.get('timeEnd')).utc().format('HH:mm')}</Value>
          </Field>
          <HorizontalSpace>
            <Field>
              <Label>Carrier Name</Label>
              <Value>{carrierRequest.get('carrier') ? carrierRequest.get('carrier').get('name') : ''}</Value>
            </Field>
            <Field>
              <Label>PO</Label>
              <Value>{carrierRequestOrder ? carrierRequestOrder.get('poNumber') : 'N/A'}</Value>
            </Field>
          </HorizontalSpace>
        </Section>
      </Content>
    </Container>
  )
}

RequestCard.propTypes = {
  onCreate: PropTypes.func,
  className: PropTypes.string,
  timezone: PropTypes.string,
  carrierRequest: PropTypes.object,
  carrierRequestOrders: PropTypes.object,
  hide: PropTypes.boolean,
  building: PropTypes.object,
  onClick: PropTypes.func
}

export default RequestCard
