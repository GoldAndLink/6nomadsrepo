import React from 'react'
import styled, { css } from 'styled-components'

const Container = styled.div`
  position: relative;

  ${props => props.fullwidth && css`
    width: 100%;
  `}
`

const Label = styled.label`
  position: absolute;
  top: 18px;
  left: 0px;
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 600;
  font-size: 10px;
  transition: top 150ms ease-in-out;

  ${props => props.error && css`
    color: red;
  `}
`

const HelpText = styled.label`
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 300;
  font-size: 12px;

  ${props => props.error && css`
    color: red;
  `}
`

const Input = styled.textarea`
  width: 100%;
  font-size: 18px;
  margin-top: 9px;
  border-style: none none solid none;
  border-width: 1px;
  border-color: ${props => props.theme.lightGray};
  margin-bottom: 2px;
  resize: none;
  
  &:active {
    outline: none;
  }

  &::-webkit-input-placeholder {
    opacity: 0;
    color: ${props => props.theme.lightGray};
    transition: opacity 150ms ease-in-out;
  }

  &:focus {
    outline: none;
    border-color: ${props => props.theme.brandDarker};
    &::-webkit-input-placeholder {
      opacity: 1;
    }
  }

  &:focus + label {
    top: 0px;
  }
  ${props => props.value && css`
    & + label {
      top: 0px;
    }
  `}

  ${props => props.isBox && css`
    margin-top: 16px;
    border-style: solid;
    & + label {
      top: 0px;
    }
  `}
`

const TextArea = ({ label, fullwidth, ...props }) => (
  <Container fullwidth={fullwidth}>
    <Input {...props} />
    {label &&
      <Label theme={props.theme} error={props.error}>{label}</Label>
    }
    {props.error &&
      <HelpText theme={props.theme} error={props.error}>{props.error}</HelpText>
    }
  </Container>
)

export default TextArea
