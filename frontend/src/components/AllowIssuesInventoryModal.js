import ReactModal from 'react-modal'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

function ReactModalAdapter ({ className, ...props }) {
  const contentClassName = `${className}__content`
  const overlayClassName = `${className}__overlay`
  return (
    <ReactModal
      portalClassName={className}
      className={contentClassName}
      overlayClassName={overlayClassName}
      {...props}
    />
  )
}

const StyledModal = styled(ReactModalAdapter)`
  &__overlay {
    position: fixed;
    z-index: 2;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(255, 255, 255, 0.75);
  }

  &__content {
    padding: 0;
    position: absolute;
    top: 40px;
    left: 40px;
    right: 40px;
    bottom: 40px;
    border: 1px solid #ccc;
    background: #fff;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    borderRadius: 4px;
    outline: none;
    .header {
      display: flex;
      flex-direction: row;
      justify-content: center;
      background-color: #white;
      font-size: 16px;
      font-weight: 600;
      font-style: normal;
      font-stretch: normal;
      text-transform: uppercase;
      line-height: normal;
      letter-spacing: 2px;
      text-align: center;
      color: #3c414e;
      padding: 20px 0 20px 0;
      border-bottom: 1px solid #3c414e;

      .title {
        flex: 1;
        align-self: center;
      }
    }
  }
`
const Container = styled.div`
  margin: 30px 40px;
  max-width: 330px;
`
const Text = styled.p`
  font-size: 18px;
  font-style: normal;
  line-height: normal;
  font-weight: 300;
  letter-spacing: 1px;
  text-align: center;
  color: #3c414e;
  margin: 0 20px 30px;
`
const Buttons = styled.div`
  display: flex;
  justify-content: space-between;
`

const Cancel = styled.button`
  font-size: 18px;
  width: 140px;
  border-radius: 3px;
  border: 1px solid #AAB1C0;
  background-color: #F6F6F6;
  color: #AAB1C0;
  text-align: center;
  font-weight: normal;
  height: 50px;
  padding: 10px 15px;
  cursor: pointer;
  letter-spacing: 1px;

  &:focus {
    outline: none;
  }
`

const YesButton = styled.button`
  font-size: 18px;
  width: 140px;
  border-radius: 3px;
  border: 1px solid ${props => props.theme.brandDarker};
  background-color: ${props => props.theme.brand};
  color: white;
  text-align: center;
  font-weight: normal;
  height: 50px;
  padding: 10px 15px;
  cursor: pointer;
  letter-spacing: 1px;

  &:focus {
    outline: none;
  }
`

class AllowIssuesInventoryModal extends Component {
  render () {
    const { isOpen = false, onDisallowIssues, onAllowIssues } = this.props
    return (
      <div>
        <StyledModal
          isOpen={isOpen}
          onAfterOpen={this.afterOpenModal}
          style={customStyles}
          contentLabel='Create User'
        >
          <div className='header'>
            <span className='title'>Allow issues</span>
          </div>
          <div>
            <Container>
              <Text>Are you sure you want to continue the scheduled appointment with the shortage of inventory ?</Text>
              <Buttons>
                <Cancel onClick={onDisallowIssues}>
                        Cancel
                </Cancel>
                <YesButton onClick={onAllowIssues}>
                        Yes
                </YesButton>
              </Buttons>
            </Container>
          </div>
        </StyledModal>
      </div>
    )
  }
}
AllowIssuesInventoryModal.propTypes = {
  isOpen: PropTypes.bool,
  onDisallowIssues: PropTypes.func,
  onAllowIssues: PropTypes.func
}
export default AllowIssuesInventoryModal
