import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const Container = styled.div`
  display: flex;
  align-items: center;
  font-size: 12px;
  color: #aaafc1;

  span {
    margin-right: 4px;
  }
`

const Button = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  color: #aab0c0;
  border: 1px solid #aab0c0;
  border-radius: 2px;
  width: 20px;
  height: 20px;
  font-weight: bold;
  margin: 0px 1px;
  cursor: pointer;
  user-select: none;

  ${props => props.disabled && css`
    pointer-events: none;
    opacity: 0.5;
  `}
`

class ZoomSelector extends Component {
  onMinusClick = () => {
    const { onChange, level, step } = this.props
    if (onChange) {
      onChange(level - step)
    }
  }

  onPlusClick = () => {
    const { onChange, level, step } = this.props
    if (onChange) {
      onChange(level + step)
    }
  }

  render () {
    const { level, minLevel, maxLevel, step } = this.props

    return (
      <Container>
        <span>ZOOM: {level}%</span>
        <Button
          onClick={this.onMinusClick}
          disabled={level - step < minLevel}
        >
          ‒
        </Button>
        <Button
          onClick={this.onPlusClick}
          disabled={level + step > maxLevel}
        >
          +
        </Button>
      </Container>
    )
  }
}

ZoomSelector.propTypes = {
  onChange: PropTypes.func,
  minLevel: PropTypes.number,
  maxLevel: PropTypes.number,
  level: PropTypes.number,
  step: PropTypes.number
}

export default ZoomSelector
