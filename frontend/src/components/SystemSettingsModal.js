import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import Modal from './Modal'
import Users from '../containers/Users'

import closeIcon from '../assets/images/close-green.svg'
import { isSystemSettingsOpen } from '../modules/users/selectors'
import UsersActions from '../modules/users/actions'

const customModalStyles = {
  content: {
    top: '30px',
    left: '30px',
    right: '30px',
    bottom: '30px',
    overflow: 'hidden'
  },
  overlay: {
    backgroundColor: 'rgba(60, 65, 78, 0.6)',
    zIndex: 1000
  }
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  position: absolute;
  top: 0px;
  width: 100%;
  bottom: 0px;
`

const Header = styled.div`
  display: flex;
  align-items: center;
  height: 60px;
  padding: 0px 20px;
`

const Content = styled.div`
  flex: 1;
  overflow-y: auto;
`

const Title = styled.div`
  flex: 1;
  font-size: 18px;
  font-weight: 300;
  color: #bbbbbb;
`

const Close = styled.div`
  cursor: pointer;

  img {
    width: 20px;
    height: 20px;
  }
`

class SystemSettingsModal extends Component {
  render () {
    const { isOpen, hideSystemSettings } = this.props

    return (
      <Modal
        hideHeader={true}
        isOpen={isOpen}
        contentLabel='System Settings'
        onRequestClose={hideSystemSettings}
        styles={customModalStyles}
      >
        <Container>
          <Header>
            <Title>System Settings</Title>
            <Close onClick={hideSystemSettings}>
              <img src={closeIcon} alt="close" />
            </Close>
          </Header>
          <Content>
            <Users />
          </Content>
        </Container>
      </Modal>
    )
  }
}

SystemSettingsModal.propTypes = {
  isOpen: PropTypes.bool,
  hideSystemSettings: PropTypes.func
}

const mapStateToProps = state => ({
  isOpen: isSystemSettingsOpen(state)
})

const mapDispatchToProps = dispatch => ({
  hideSystemSettings: () => dispatch(UsersActions.hideSystemSettings())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SystemSettingsModal)
