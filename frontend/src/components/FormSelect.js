import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import Select from './Select'

const selectStyles = error => ({
  container: base => ({ ...base, border: 'none', boxShadow: 'none', outline: 'none' }),
  control: base => ({
    ...base,
    borderRadius: 2,
    borderWidth: '0px 0px 1px 0px',
    borderColor: error ? '#d9534f' : '#ebebeb',
    boxShadow: 'none',
    outline: 'none'
  }),
  valueContainer: base => ({
    ...base,
    padding: '2px 0px'
  }),
  option: (base, { isSelected, isFocused }) => ({
    ...base,
    backgroundColor: isSelected || isFocused ? '#d2f8d8' : null,
    color: isSelected || isFocused ? '#61c9b5' : null
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: base => ({
    ...base,
    transform: 'scale(0.6)',
    padding: '8px 0px'
  })
})

const Container = styled.div`
  position: relative;
`

const Label = styled.label`
  position: absolute;
  top: -8px;
  left: 0px;
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 600;
  font-size: 10px;

  ${props => props.error && css`
    color: red;
  `}
`

const HelpText = styled.label`
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 300;
  font-size: 12px;

  ${props => props.error && css`
    color: red;
  `}
`

const SelectWrap = styled.div`
  margin-top: 18px;
`

class FormSelect extends Component {
  render () {
    const { label, error } = this.props

    return (
      <Container>
        <SelectWrap>
          <Select {...this.props} styles={selectStyles} />
        </SelectWrap>
        <Label error={error}>{label}</Label>
        {error &&
          <HelpText error={error}>{error}</HelpText>
        }
      </Container>
    )
  }
}

export default FormSelect
