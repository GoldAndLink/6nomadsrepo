import styled from 'styled-components'
import {
  Tab as UnstyledTab,
  TabList as UnstyledTabList,
  Tabs as UnstyledTabs,
  TabPanel as UnstyledTabPanel
} from 'react-tabs'

const Tabs = styled(UnstyledTabs)`
  margin-top: 0px;
  flex-direction: row;
`

const TabList = styled(UnstyledTabList)`
  width: 110px;
  padding: 0;
  margin: 0;
  background-color: ${props => props.theme.darkBackground};
`

const Tab = styled(UnstyledTab).attrs({
  selectedClassName: 'selected',
  disabledClassName: 'disabled'
})`
  display: flex;
  justify-content: center;
  font-size: 12px;
  font-weight: bold;
  padding: 12px 13px 12px 13px;
  list-style: none;
  cursor: pointer;
  color: white;
  background-color: transparent;

  .notifications {
    margin-left: 10px;
  }

  &.selected {
    color: ${props => props.theme.brandDarker};
    border-left: 3px solid ${props => props.theme.brandDarker};
    border-top: 1px solid #aab0c0;
    border-bottom: 1px solid #aab0c0;
    padding-left: 10px;
    padding-right: 13px;

    .notifications {
      margin-left: 10px;
      color: #61c9b5;
    }
  }

  &.disabled {
    color: #e0e0e0;
    cursor: not-allowed;
  }
`

const TabPanel = styled(UnstyledTabPanel).attrs({ selectedClassName: 'selected' })`
  display: none;
  background-color: #e6e9ee;
  flex: 1;
  width: 240px;
  &.selected {
    display: flex;
  }
`

Tab.tabsRole = 'Tab'
Tabs.tabsRole = 'Tabs'
TabPanel.tabsRole = 'TabPanel'
TabList.tabsRole = 'TabList'

export { Tab, TabList, Tabs, TabPanel }
