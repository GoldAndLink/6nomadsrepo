import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import styled from 'styled-components'
import Login from '../containers/login/Login'
import Home from '../containers/Home'
import Users from '../containers/Users'
import CarrierApp from '../containers/carrier/CarrierApp'
import { NotificationContainer } from 'react-notifications'
import 'react-notifications/lib/notifications.css'

const Container = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: #f4f6f9;
`

const App = () => (
  <Router>
    <Container>
      <Route exact path='/' component={Login} />
      <Route exact path='/home' component={Home} />
      <Route path='/carrier' component={CarrierApp} />
      {/* <Route path='/login' component={Login} /> */}
      <Route path='/users' component={Users} />
      <NotificationContainer/>
    </Container>
  </Router>
)

export default App
