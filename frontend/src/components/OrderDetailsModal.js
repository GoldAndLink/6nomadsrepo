import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled, { css } from 'styled-components'
import Modal from './Modal'
import OrderItemsTab from '../components/OrderItemsTab'
import {
  getIsOrderDetailsModalVisible,
  getOrderDetails
} from '../modules/orders/selectors'
import { getUserName } from '../modules/users/selectors'
import OrdersActions from '../modules/orders/actions'
import closeIcon from '../assets/images/close.svg'

const INVENTORY_TAB = 1

const customModalStyles = {
  content: {
    top: '60px',
    left: '350px',
    right: 'auto',
    bottom: 'auto'
  },
  overlay: {
    backgroundColor: 'rgba(60, 65, 78, 0.6)'
  }
}

const Header = styled.div`
  display: flex;
  align-items: center;
  height: 45px;
  padding: 0px 10px;
  border-bottom: 1px solid #e7eaee;
`

const Tabs = styled.div`
  flex: 1;
  display: flex;
`

const Tab = styled.div`
  cursor: pointer;
  font-size: 10px;
  font-weight: bold;
  letter-spacing: 1.1px;
  color: #bbbbbb;
  text-transform: uppercase;
  margin-right: 30px;

  ${props => props.selected && css`
    color: #61c9b5;
    text-decoration: underline;
  `}
`

const Close = styled.div`
  cursor: pointer;
`

class OrderDetailsModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      initialValues: {},
      tab: 1
    }
  }

  componentDidMount () {
    document.addEventListener('keydown', this.onEscClose)
  }

  componentWillUnmount () {
    document.removeEventListener('keydown', this.onEscClose)
  }

  onEscClose = event => {
    if (event.keyCode === 27) this.props.close()
  }

  /* componentDidUpdate (prevProps) {
    const { isOpen, editingAppointment } = this.props
    if (isOpen && !prevProps.isOpen) {
      if (!editingAppointment) {
        this.setState({ initialValues: {} })
      } else {
        const object = {}
        editingAppointment.forEach((value, key) => {
          const keys = key.split('.')
          const last = keys.pop()
          keys.reduce((r, a) => { const result = r[a] = r[a] || {}; return result }, object)[last] = value
        })

        const datetime = object.date
        object.date = moment(datetime)
        object.time = moment(datetime)

        object.site = editingAppointment.getIn(['door', 'area', 'building', 'siteId'])
        object.building = editingAppointment.getIn(['door', 'area', 'buildingId'])
        object.area = editingAppointment.getIn(['door', 'areaId'])

        this.setState({ initialValues: object })
      }
    }
  } */

  render () {
    const { isOpen, close, orderDetails, userName } = this.props
    const order = orderDetails ? orderDetails.get('order') : null
    const pallets = order ? order.get('pallets') : null
    const primaryRefValue = order ? order.get('primaryRefValue') && order.get('primaryRefValue').length > 12 ? order.get('primaryRefValue').substring(0, 12) + '...' : order.get('primaryRefValue') : null
    const { tab } = this.state

    return (
      <Modal
        hideHeader={true}
        isOpen={isOpen}
        styles={customModalStyles}
      >
        <div>
          <Header>
            <Tabs>
              <Tab onClick={this.switchToInventoryTab} selected={tab === INVENTORY_TAB}>Orders details: {primaryRefValue}</Tab>
            </Tabs>
            <Close onClick={close}>
              <img src={closeIcon} alt="close" />
            </Close>
          </Header>
          <OrderItemsTab
            orderItems={orderDetails ? orderDetails.get('orderItems') : null}
            items={orderDetails ? orderDetails.get('items') : null}
            order={orderDetails ? orderDetails.get('order') : null}
            pallets={pallets}
            userName={userName}
            onClose={close}
            noIssues={true}
          />
        </div>
      </Modal>
    )
  }
}

OrderDetailsModal.propTypes = {
  isOpen: PropTypes.bool,
  editingAppointment: PropTypes.bool,
  close: PropTypes.func,
  orderDetails: PropTypes.object,
  userName: PropTypes.string
}

const mapStateToProps = state => ({
  isOpen: getIsOrderDetailsModalVisible(state),
  orderDetails: getOrderDetails(state),
  userName: getUserName(state)
})

const mapDispatchToProps = dispatch => ({
  close: () => dispatch(OrdersActions.closeOrderDetailsModal())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderDetailsModal)
