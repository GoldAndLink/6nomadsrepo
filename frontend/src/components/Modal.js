import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactModal from 'react-modal'
import styled from 'styled-components'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

function ReactModalAdapter ({ className, ...props }) {
  const contentClassName = `${className}__content`
  const overlayClassName = `${className}__overlay`
  return (
    <ReactModal
      portalClassName={className}
      className={contentClassName}
      overlayClassName={overlayClassName}
      {...props}
    />
  )
}

const StyledModal = styled(ReactModalAdapter)`
  

  &__overlay {
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(255, 255, 255, 0.75);
  }

  &__content {
    padding: 0;
    position: absolute;
    top: 40px;
    left: 40px;
    right: 40px;
    bottom: 40px;
    border: 1px solid #ccc;
    background: #fff;
    -webkit-overflow-scrolling: touch;
    borderRadius: 4px;
    outline: none;
    .header {
      display: flex;
      flex-direction: row;
      justify-content: center;
      background-color: #61c9b5;
      font-size: 18px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
      color: #ffffff;
      padding: 14px 0 14px 0;

      .title {
        flex: 1;
        align-self: center;
      }

      .close {
        cursor: pointer;
        border: 0;
        color: #ffffff;
        background-color: #61c9b5;
        align-self: flex-end;
        margin-right: 18px;
      }
    }
  }
`

class Modal extends Component {
  render () {
    const { children, title = '', isOpen = false, onRequestClose, hideHeader, styles } = this.props
    return (
      <div>
        <StyledModal
          isOpen={isOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={onRequestClose}
          style={{ ...customStyles, ...styles }}
          contentLabel='Create User'
        >
          {!hideHeader &&
            <div className='header'>
              <span className='title'>{title}</span> <button onClick={onRequestClose} className='close'>x</button>
            </div>
          }
          <div>
            {children}
          </div>
        </StyledModal>
      </div>
    )
  }
}

Modal.propTypes = {
  children: PropTypes.object,
  title: PropTypes.string,
  isOpen: PropTypes.bool,
  onRequestClose: PropTypes.func,
  hideHeader: PropTypes.bool,
  styles: PropTypes.object
}

export default Modal
