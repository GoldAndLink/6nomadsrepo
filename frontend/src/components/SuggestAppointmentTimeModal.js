import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment-timezone'
import Modal from './Modal'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Button from './Button'

import InventoryItemsActions from '../modules/inventoryItems/actions'
import {
  getSuggestAppointmentTimes,
  isSuggestAppointmentLoading
} from '../modules/inventoryItems/selectors'
import {
  getDoors
} from '../modules/doors/selectors'

import closeIcon from '../assets/images/close.svg'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

const Container = styled.div`
  width: 400px;
`

const Header = styled.div`
  height: 40px;
  border-bottom: 1px solid #e9edf3;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #444a59;
  font-weight: bold;
  font-size: 10px;
  position: relative;
  text-transform: uppercase;
  letter-spacing: 1.1;
`

const CloseIcon = styled.div`
  position: absolute;
  cursor: pointer;
  top: 10px;
  right: 10px;
  img {
    width: 16px;
    height: 16px;
  }
`

const Content = styled.div`
  padding: 8px 0px 20px 0px;
  color: #898989;
  display: flex;
  flex-direction: column;
  align-items: center;

  p {
    margin: 30px;
  }
`

const ContentTitle = styled.div`
  font-size: 10px;
  font-weight: 300;
  letter-spacing: 1.1;
  text-transform: uppercase;
  text-align: center;
`

const Options = styled.div`
  width: 100%;
  margin: 20px 0px;
`

const Option = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 30px;
  border-bottom: 1px solid #f3f6f6;
  width: 100%;
  cursor: pointer;
  font-size: 10px;
  letter-spacing: 1.1px;
  font-weight: 300;
  background-color: ${props => props.selected ? '#d2f8d8' : 'white'};

  &:hover {
    background-color: ${props => props.selected ? '#d2f8d8' : '#f3f6f6'};
  }
`

const Buttons = styled.div`
  display: flex;
  align-content: center;
`

const PrimaryButton = styled(Button)`
  width: 110px;
  height: 30px;
  background-color: #61c9b5;
  border: 1px solid #0eb48f;
  font-size: 12px;
  color: white;
  margin: 0px 12px;
`

const CancelButton = styled(Button)`
  width: 90px;
  height: 30px;
  background-color: #f3f6f6;
  border: 1px solid #aab0c0;
  font-size: 12px;
  color: #aab0c0;
  margin: 0px 12px;
`

class SuggestAppointmentTimeModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      selectingDoor: false,
      selectedTime: null,
      selectedDoor: null
    }
  }

  componentDidUpdate (prevProps) {
    const { isOpen, suggestAppointmentTime } = this.props
    if (isOpen && !prevProps.isOpen) {
      suggestAppointmentTime()
    }
  }

  renderSelectTime () {
    const { selectedTime } = this.state
    const { times, onClose, timezone } = this.props

    return (
      <Content>
        <ContentTitle>Select a date from the list bellow</ContentTitle>
        <Options>
          {times && times.slice(0, 10).map((time, i) => {
            const newTime = moment.tz(time.get('time'), timezone)
            return (
              <Option
                key={i}
                selected={time === selectedTime}
                onClick={() => this.setState({ selectedTime: time })}
              >
                {newTime.format('MM . DD . YYYY')}
                {' | '}
                {newTime.format('HH:mm')}
              </Option>
            )
          })}
        </Options>
        <Buttons>
          <CancelButton onClick={onClose}>Cancel</CancelButton>
          <PrimaryButton
            disabled={!selectedTime}
            onClick={() => this.setState({ selectingDoor: true })}
          >
            Select a door
          </PrimaryButton>
        </Buttons>
      </Content>
    )
  }

  renderSelectDoor () {
    const { selectedTime, selectedDoor } = this.state
    const { onClose, onNewTimeSelected, doors, timezone } = this.props

    return (
      <Content>
        <ContentTitle>Select a door</ContentTitle>
        <Options>
          {selectedTime && selectedTime.get('doors').map(doorId => {
            const door = doors && doors.find(d => d.get('id') === doorId)
            return (
              <Option
                key={doorId}
                selected={doorId === selectedDoor}
                onClick={() => this.setState({ selectedDoor: doorId })}
              >
                {door && door.get('name')}
              </Option>
            )
          })}
        </Options>
        <Buttons>
          <CancelButton onClick={() => this.setState({ selectingDoor: false, selectedDoor: null })}>
            Back
          </CancelButton>
          <PrimaryButton
            disabled={!selectedDoor}
            onClick={() => {
              const newTime = moment.tz(selectedTime.get('time'), timezone)
              onClose()
              onNewTimeSelected({
                time: newTime,
                door: selectedDoor
              })
            }}
          >
            Select a door
          </PrimaryButton>
        </Buttons>
      </Content>
    )
  }

  renderNoTimes () {
    const { onClose } = this.props

    return (
      <Content>
        <p>Sorry, there are no dates/times available. Please contact production.</p>
        <Buttons>
          <PrimaryButton onClick={onClose}>
            Ok
          </PrimaryButton>
        </Buttons>
      </Content>
    )
  }

  render () {
    const { selectingDoor } = this.state
    const { isOpen, onClose, loading, times } = this.props

    let content
    if (loading) {
      content = (
        <Content>Loading...</Content>
      )
    } else {
      if (times && times.size) {
        if (selectingDoor) {
          content = this.renderSelectDoor()
        } else {
          content = this.renderSelectTime()
        }
      } else {
        content = this.renderNoTimes()
      }
    }

    let title = 'Next available loading date(s)'
    if (!loading && (!times || !times.size)) {
      title = 'No dates available'
    }

    return (
      <Modal
        isOpen={isOpen}
        onRequestClose={onClose}
        styles={customStyles}
        hideHeader={true}
      >
        <Container>
          <Header>
            {title}
            <CloseIcon onClick={onClose}>
              <img src={closeIcon} alt="close" />
            </CloseIcon>
          </Header>
          {content}
        </Container>
      </Modal>
    )
  }
}

SuggestAppointmentTimeModal.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  suggestAppointmentTime: PropTypes.func,
  times: PropTypes.array,
  loading: PropTypes.bool,
  doors: PropTypes.object,
  timezone: PropTypes.string,
  onNewTimeSelected: PropTypes.func
}

const mapStateToProps = state => ({
  times: getSuggestAppointmentTimes(state),
  loading: isSuggestAppointmentLoading(state),
  doors: getDoors(state)
})

const mapDispatchToProps = dispatch => ({
  suggestAppointmentTime: () => dispatch(InventoryItemsActions.suggestAppointmentTime())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SuggestAppointmentTimeModal)
