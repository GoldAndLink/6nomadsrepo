import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactSelect, { components } from 'react-select'
import styled from 'styled-components'

const customStyles = error => ({
  container: base => ({ ...base, border: 'none', boxShadow: 'none', outline: 'none' }),
  control: base => ({
    ...base,
    borderRadius: 2,
    borderColor: error ? '#d9534f' : '#ebebeb',
    boxShadow: 'none',
    outline: 'none'
  }),
  option: (base, { isSelected, isFocused }) => ({
    ...base,
    backgroundColor: isSelected || isFocused ? '#d2f8d8' : null,
    color: isSelected || isFocused ? '#61c9b5' : null
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: base => ({ ...base, transform: 'scale(0.6)' })
})

class Option extends Component {
  render () {
    return (
      <div>
        <components.Option {...this.props}>
          <input
            type="checkbox"
            checked={this.props.isSelected}
            onChange={e => null}
          />{' '}
          <label>{this.props.data.label} </label>
        </components.Option>
      </div>
    )
  }
}

const ValueContainer = ({ children, innerRef, innerProps, isDisabled }) => {
  const Container = styled.div`
    position: absolute;
    top: 12px;
  `
  const assignedLocations = (children && children[0].length) || 0
  const placeholders = children && children.find(c => !Array.isArray(c))

  return !isDisabled ? (
    <div ref={innerRef} {...innerProps} style={{ margin: '5px 20px' }}>
      { assignedLocations > 0 && <Container>Assigned Locations ({assignedLocations})</Container> }
      {placeholders}
    </div>
  ) : null
}

ValueContainer.propTypes = {
  children: PropTypes.any,
  innerProps: PropTypes.any,
  isDisabled: PropTypes.bool
}

class AccountSelect extends Component {
  onChangeAccount = (selections, input) => {
    const { options, onChange } = this.props

    // TODO unify data structure on both list and form so we avoid using both id and value
    const selected = options
      .filter(option => selections.some(selection => option.value === selection.value))
      .map(option => ({ ...option, id: option.value }))

    onChange(selected)
  }

  render () {
    const { options, value, className, error, styles, isClearable } = this.props
    const selectedValue = value && options.filter(option => value.some(val => (val.id || val.value) === option.value))

    return (
      <ReactSelect
        closeMenuOnSelect={false}
        hideSelectedOptions={false}
        className={className}
        isMulti
        components={{ Option, ValueContainer }}
        isClearable={!!isClearable}
        value={selectedValue}
        placeholder={'Assigned Locations (0)'}
        styles={styles ? styles(error) : customStyles(error)}
        onChange={this.onChangeAccount}
        options={options}
      />
    )
  }
}

AccountSelect.propTypes = {
  options: PropTypes.any,
  value: PropTypes.any,
  onChange: PropTypes.func,
  className: PropTypes.object,
  error: PropTypes.any,
  styles: PropTypes.any,
  isClearable: PropTypes.bool
}

export default AccountSelect
