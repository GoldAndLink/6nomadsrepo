import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import styled from 'styled-components'
import Table, { Column } from './Table'
import Button from './Button'
import { Checkbox } from '@material-ui/core'

import AllowIssuesInventoryModal from '../components/AllowIssuesInventoryModal'
import NoDatesInventoryModal from '../components/NoDatesInventoryModal'
import SuggestAppointmentTimeModal from '../components/SuggestAppointmentTimeModal'
import NextAvailableLoadingDatesModal from '../components/NextAvailableLoadingDatesModal'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 620px;
  height: 530px;

  div.row {
    display: flex;
    flex-direction: row;
  }

  .pb15 {
    padding-bottom: 15px;
  }

  .pt25 {
    padding-top: 25px;
  }

  .pt15 {
    padding-top: 15px;
  }

  .pb10 {
    padding-bottom: 10px;
  }

  .ml20 {
    margin-left: 24px;
  }

  .mr20 {
    margin-right: 24px;
  }
`

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  div.column {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;

    p.content {
      letter-spacing: 1.1px;
      color: #43485c;
      margin-top: 0;
      margin-bottom: 3px;
      font-size: 10px;
      align-self: flex-start;

      &.gray {
        color: #43485c;
      }
      &.dark-gray {
        color: #43475d;
      }
      &.darker-gray {
        color: #3c414e;
      }
    }
  }
`

const UppercaseLabels = styled.h2`
  color: #61c9b5;
  text-transform: uppercase;
  letter-spacing: 1.1px;
  font-size: 12px;
  margin-bottom: 0;

  &.pt15 {
    padding-top: 15px;
  }

  &.pb10 {
    padding-bottom: 10px;
  }

  &.pdl4 {
    padding-left: 4px;
  }
  &.pdr4 {
    padding-right: 4px;
  }
`

const OrderDetailsLabel = styled.h2`
  height: 12px;
  align-self: flex-start;
  font-size: 10px;
  font-weight: bold;
  letter-spacing: 1.1px;
  color: #43485c;
`

const ItemsTableContainer = styled.div`
  height: 250px;
  overflow-y:visible;
  overflow-x:scroll;
`

const ItemsTable = styled(Table)`
  width: 100%;
  font-size: 14px;

  th {
    font-weight: 400;
    color: '#3c414e';
  }
  td {
    font-weight: 300;
    color: '#898989';
  }
`

const Footer = styled.div`
  height: 30px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 20px;
  padding-right: 20px;
  display: flex;
  flex: 1;
  justify-content: space-between;
  flex-direction: row;
`

const AllowIssuesContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
`

const AllowIssuesButton = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
`

const AllowIssuesUser = styled.div`
  margin-top: 10px;
  font-size: 8px;
  font-weight: 300;
  text-transform: uppercase;
`

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-self: center;
`

const CreateAppointmentButton = styled(Button)`
  width: 70px;
  height: 30px;
  border-radius: 2px;
  background-color: #61c9b5;
  text-align: center;
  font-size: 12px;
`

const CancelButton = styled.div`
  height: 14px;
  font-size: 12px;
  font-weight: bold;
  color: #3c414e;
  cursor: pointer;
  margin-left: auto;
  margin-right: 11px;
  text-align: center;
  align-self: center;
  margin-left: 20px;
`

const Label = styled.div`
  height: 15px;
  align-self: center;
  font-size: 12px;
  font-weight: bold;
  color: #aab0c0;
`

const ReviewLabel = styled.div`
  height: 12px;
  font-size: 8px;
  font-weight: 300;
  line-height: 1.5;
  letter-spacing: 0.9px;
  text-align: right;
  color: #4a4a4a;
  margin-right: 20px;
  margin-bottom: 16px;
  text-transform: uppercase;
`

const TimeSlotsButton = styled.button`
  font-size: 12px;
  margin-left: 6px;
  border-radius: 2px;
  border: 1px solid #AAB1C0;
  background-color: #F6F6F6;
  color: #AAB1C0;
  text-align: center;
  height: 30px;
  padding: 10px 15px;
  cursor: pointer;
  letter-spacing: 1px;

  &:focus {
    outline: none;
  }
`

const CustomCheckbox = styled(Checkbox)`
  padding: 0 !important;
  margin-left: 7px;
`

class OrderItemsTab extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isSuggestAppointmentTimeOpen: false,
      isNoDatesInventoryModalOpen: false,
      isAllowIssuesInventoryModalOpen: false,
      isNextAvailableLoadingDatesModalOpen: false,
      allowIssuesCheckbox: false
    }
  }

  componentDidMount () {
    if (this.props.inventoryReviewUserName) {
      this.setState({ allowIssuesCheckbox: true })
    }
  }

  componentDidUpdate (prevProps, prevState) {
    const { inventoryReviewUserName } = this.props
    if (inventoryReviewUserName !== prevProps.inventoryReviewUserName) {
      this.setState({ allowIssuesCheckbox: !!inventoryReviewUserName })
    }
  }

  onChangeAllowIssuesCheckbox = event => {
    const { onAllowIssuesChange } = this.props

    onAllowIssuesChange(event.target.checked)
  }

  onAllowIssues = () => {
    const { user, appointment, updateAppointment } = this.props

    if (appointment && appointment.inventoryReviewUserId) {
      updateAppointment({ id: appointment.id, inventoryReviewUserId: null })
    } else {
      updateAppointment({ id: appointment.id, inventoryReviewUserId: user && user.get('id') })
    }

    this.setState({ isAllowIssuesInventoryModalOpen: false })
    // this.props.onUpdateAppointmentInventoryReviewUserId(true)
  }

  onCloseSuggestAppointmentTime = () => {
    this.setState({
      isSuggestAppointmentTimeOpen: false
    })
  }

  onCloseNoDatesInventoryModal = () => {
    this.setState({
      isNoDatesInventoryModalOpen: false
    })
  }

  onCloseNextAvailableLoadingDatesModal = () => {
    this.setState({
      isNextAvailableLoadingDatesModalOpen: false
    })
  }

  onDisallowIssuesInventoryModal = () => {
    this.setState({
      isAllowIssuesInventoryModalOpen: false
    })
    // this.props.onUpdateAppointmentInventoryReviewUserId(false)
  }

  onTimeSlotsClick = () => {
    this.setState({
      isSuggestAppointmentTimeOpen: true
    })
  }

  onDisplayNoDatesInventoryModal = () => {
    this.setState({
      isNoDatesInventoryModalOpen: true
    })
  }

  onDisplayNextAvailableLoadingDatesModal = () => {
    this.setState({
      isNextAvailableLoadingDatesModalOpen: true
    })
  }

  onDisplayAllowIssuesInventoryModal = () => {
    this.setState({
      isAllowIssuesInventoryModalOpen: true
    })
  }

  getUserName = userId => {
    const { users } = this.props

    if (users && userId) {
      const user = users.find(user => user.get('id') === userId)
      return user && `${user.get('firstName')} ${user.get('lastName')}`
    }
  }

  getQuantity = sku => {
    const { orderItems, items } = this.props
    if (sku) {
      const skuItemsId = items.filter(item => item.get('sku') === sku).map(item => item.get('id'))
      return orderItems
        .filter(orderItem => skuItemsId.indexOf(orderItem.get('itemId')) !== -1)
        .reduce((quantity, orderItem) => quantity + orderItem.get('quantity'), 0)
    }
  }

  getQuantityUOM = itemId => {
    if (itemId) {
      const orderItem = this.props.orderItems.find(orderItem => orderItem.get('itemId') === itemId)
      return orderItem.get('quantityUOM')
    }
  }

  render () {
    const {
      orderItems,
      items,
      pallets,
      order,
      createAppointment,
      inventoryItemsProjection,
      appointmentDate,
      /* switchToAppointmentsTab, */
      inventoryReviewUserName,
      onClose,
      onSave,
      onNewTimeSelected,
      timezone,
      pickupLocation,
      noIssues,
      appointment
    } = this.props

    const {
      allowIssuesCheckbox,
      isSuggestAppointmentTimeOpen,
      isNoDatesInventoryModalOpen,
      isAllowIssuesInventoryModalOpen,
      isNextAvailableLoadingDatesModalOpen
    } = this.state

    /* const orderItems = {
      pickup: {
        address: 'Deleted Retail Inc.',
        city: 'Gonzales, CA'
      },
      deliver: {
        address: 'Wegmans - Rochester',
        city: 'Rochester, NY'
      },
      pallets: 3,
      weight: '2,226lbs'
    } */

    let uniqueItems = []
    const uniqueItemsSku = items ? [...new Set(items.map(item => item.get('sku')))] : []
    uniqueItemsSku.map(uniqueItemSku => uniqueItems.push(items.find(item => item.get('sku') === uniqueItemSku)))
    uniqueItems = uniqueItems.map(ui => {
      const sku = ui.get('sku')
      const projection = inventoryItemsProjection ? inventoryItemsProjection.get(sku) : null
      ui.set('issue', false)
      if (projection && projection.get('deltas')) {
        const deltas = projection.get('deltas')
        let summary = 0
        let index = 0

        while (index < deltas.size) {
          const projection = deltas.get(index)
          const appointmentId = projection.get('appointmentId')
          const isSameAppointment = appointment && appointment.id &&
            appointmentId && appointment.id === appointmentId

          // If we reach current appointment or future, stop looking for more deltas
          if (moment(projection.get('time')).isAfter(moment(appointmentDate)) ||
            (isSameAppointment && moment(projection.get('time')).isSame(moment(appointmentDate)))) {
            break
          }

          summary += projection.get('quantity')
          index++
        }

        const required = this.getQuantity(sku)
        if (summary < required) {
          return ui.set('issue', true)
        }
      } else {
        return ui.set('issue', true)
      }
      return ui
    })
    const displayedWeight = orderItems ? orderItems.reduce((weightAccu, orderItem) => {
      const total = weightAccu + orderItem.get('weight')
      return total
    }, 0) : 'N/A'
    const displayedItemsLength = uniqueItems.length // items ? items.size : 0

    const displayedPallets = pallets || 'N/A'

    const otherRefs = order && order.getIn(['data', 'otherRefs'])
    // const PO = otherRefs && otherRefs.find(or => or.get('type') === 'PO')
    const location = order && order.get('destination')
    const customer = order && order.get('customer')

    let issuesUserName
    if (appointment) {
      issuesUserName = this.getUserName(appointment.inventoryReviewUserId)
    }

    return (
      <Container>
        <AllowIssuesInventoryModal
          isOpen={isAllowIssuesInventoryModalOpen}
          onAllowIssues={this.onAllowIssues}
          onDisallowIssues={this.onDisallowIssuesInventoryModal}
        />
        <SuggestAppointmentTimeModal
          isOpen={isSuggestAppointmentTimeOpen}
          onClose={this.onCloseSuggestAppointmentTime}
          onNewTimeSelected={onNewTimeSelected}
          timezone={timezone}
        />
        <NoDatesInventoryModal
          isOpen={isNoDatesInventoryModalOpen}
          onClose={this.onCloseNoDatesInventoryModal}
        />
        <NextAvailableLoadingDatesModal
          isOpen={isNextAvailableLoadingDatesModalOpen}
          onClose={this.onCloseNextAvailableLoadingDatesModal}
        />
        {order ? (<HeaderContainer>
          <div className={'column ml20'}>
            <OrderDetailsLabel className={'pt15'}>Customer Name</OrderDetailsLabel>
            {!!customer && <p className={'content gray'}>{customer.get('name')}</p>}
            {!customer && <p className={'content gray'}>N/A</p>}
          </div>
          <div className={'column ml20'}>
            <OrderDetailsLabel className={'pt15'}>Destination</OrderDetailsLabel>
            {!!location && <p className={'content gray'}>{location.get('name')}</p>}
            {!!location && <p className={'content gray'}>{location.get('address1')}</p>}
            {!!location && <p className={'content gray'}>{location.get('city')}, {location.get('state')}</p>}
            {!location && <p className={'content gray'}>N/A</p>}
          </div>
          <div className={'column ml20 mr20'}>
            <OrderDetailsLabel className={'pt15'}>References</OrderDetailsLabel>
            {/* {!!PO && <p className={'content gray'}>{PO && PO.get('val')}</p>} */}
            {!!otherRefs && otherRefs.map((otherRef, index) => (<p key={index} className={'content gray'}>{otherRef.get('type')}: {otherRef.get('val')}</p>))}
            {!otherRefs && <p className={'content gray'}>N/A</p>}
          </div>
        </HeaderContainer>)
          : (
            <HeaderContainer>
              <div className={'column ml20'}>
                <UppercaseLabels className={'pt15 pb10'}>Pickup</UppercaseLabels>
                {!!pickupLocation && <p className={'content gray'}>{pickupLocation.get('name')}</p>}
                {!!pickupLocation && <p className={'content gray'}>{pickupLocation.get('address1')}</p>}
                {!!pickupLocation && <p className={'content gray'}>{pickupLocation.get('city')}, {pickupLocation.get('state')}</p>}
                {!pickupLocation && <p className={'content gray'}>N/A</p>}
              </div>
            </HeaderContainer>
          )}
        <div className={'row ml20 pt15 pb10'}>
          <UppercaseLabels>Items ({displayedItemsLength})</UppercaseLabels>
          <UppercaseLabels className={'pdl4 pdr4'}> | </UppercaseLabels>
          <UppercaseLabels>Pallets ({displayedPallets && displayedPallets.toFixed && displayedPallets.toFixed(2)})</UppercaseLabels>
          <UppercaseLabels className={'pdl4 pdr4'}> | </UppercaseLabels>
          <UppercaseLabels>Weight ({displayedWeight && displayedWeight.toFixed && displayedWeight.toFixed(1)} lbs)</UppercaseLabels>
        </div>
        <ItemsTableContainer>
          <ItemsTable noIssues={noIssues} data={uniqueItems}>
            <Column
              title='SKU'
              content={item => item.get('sku')}
            />
            <Column title='Quantity' content={item => this.getQuantity(item.get('sku')) || 'N/A'} />
            <Column title='Unit' content={item => this.getQuantityUOM(item.get('id')) || 'N/A'} />
            <Column
              title='Item Name'
              content={item => item.get('name') || 'N/A'}
            />
          </ItemsTable>
        </ItemsTableContainer>
        {createAppointment &&
          <Footer>
            <ButtonsContainer>
              <CreateAppointmentButton primary onClick={onSave}>
                Save
              </CreateAppointmentButton>
              <CancelButton onClick={onClose}>Cancel</CancelButton>
            </ButtonsContainer>
            <ButtonsContainer>
              <Label>Other appointments</Label>
              <TimeSlotsButton onClick={this.onTimeSlotsClick}>
                Time slots
              </TimeSlotsButton>
            </ButtonsContainer>
            <AllowIssuesContainer>
              <AllowIssuesButton>
                <Label>Allow issues</Label>
                <CustomCheckbox
                  checked={appointment.allowIssues}
                  onChange={this.onChangeAllowIssuesCheckbox}
                  color='default'
                />
              </AllowIssuesButton>
              {issuesUserName &&
                <AllowIssuesUser>
                  Reviewed by {issuesUserName}
                </AllowIssuesUser>
              }
            </AllowIssuesContainer>
          </Footer>
        }
        {createAppointment && !!allowIssuesCheckbox && <ReviewLabel>Reviewed by {inventoryReviewUserName}</ReviewLabel>}
      </Container>
    )
  }
}

OrderItemsTab.propTypes = {
  user: PropTypes.object,
  users: PropTypes.object,
  appointment: PropTypes.object,
  updateAppointment: PropTypes.func,
  appointmentDate: PropTypes.object,
  orderItems: PropTypes.object,
  items: PropTypes.array,
  pallets: PropTypes.number,
  order: PropTypes.object,
  createAppointment: PropTypes.bool,
  inventoryItemsProjection: PropTypes.object,
  onSubmit: PropTypes.func,
  switchToAppointmentsTab: PropTypes.func,
  onClose: PropTypes.func,
  onSave: PropTypes.func,
  onUpdateAppointmentInventoryReviewUserId: PropTypes.func,
  inventoryReviewUserName: PropTypes.string,
  onNewTimeSelected: PropTypes.func,
  onAllowIssuesChange: PropTypes.func,
  timezone: PropTypes.string,
  pickupLocation: PropTypes.object,
  noIssues: PropTypes.bool
}

export default OrderItemsTab
