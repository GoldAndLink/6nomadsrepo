import moment from 'moment'
import 'moment-timezone'

const appointmentStatuses = {
  draft: 'Draft',
  scheduled: 'Scheduled',
  checkedIn: 'Checked In',
  loading: 'Loading',
  checkedOut: 'Checket Out'
}

const requestStatuses = {
  pending: 'pending',
  scheduled: 'scheduled',
  reschedule: 'reschedule',
  canceled: 'canceled',
  carrierLate: 'carrier Late'
}

function isCarrierRequestLate (carrierRequest) {
  const timezone = 'UTC'
  const now = moment.tz(timezone)

  const timeStart = moment.tz(carrierRequest.get('timeStart'), timezone)
  const requestDate = moment.tz(carrierRequest.get('date'), timezone)
  requestDate.hour(timeStart.hour())
  requestDate.minutes(timeStart.minutes())

  return now.isSameOrAfter(requestDate)
}

function isAppointmentLate (appointment) {
  const building = appointment.getIn(['door', 'area', 'building'])
  const timezone = (building && building.get('timezone')) || 'UTC'

  const now = moment.tz(timezone)
  const initTime = moment.tz(appointment.get('date'), timezone)
  // const duration = appointment.get('duration')
  // const endTime = initTime.clone().add(duration, 'minute')

  return now.isSameOrAfter(initTime)
}

export function isRequestLate (carrierRequest, appointment, appointmentStatus) {
  const carrierRequestStatus = carrierRequest ? carrierRequest.get('status') : null
  const carrierRequestStatusOK = (carrierRequestStatus === requestStatuses.scheduled || carrierRequestStatus === requestStatuses.reschedule || carrierRequestStatus === requestStatuses.canceled)
  const appointmentStatusOK = appointmentStatus && (appointmentStatus.get('name') === appointmentStatuses.draft || appointmentStatus.get('name') === appointmentStatuses.scheduled)

  return (appointmentStatusOK && isAppointmentLate(appointment)) || (carrierRequestStatusOK && appointmentStatusOK && isCarrierRequestLate(carrierRequest))
}
