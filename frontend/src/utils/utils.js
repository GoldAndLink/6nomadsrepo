export const maskPhoneNumber = phoneNumber => {
  const numbers = phoneNumber.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/)
  return !numbers[2] ? numbers[1] : '' + numbers[1] + '-' + numbers[2] + (numbers[3] ? '-' + numbers[3] : '')
}

// this function return phone number in format (xxx) xxx-xxxx
export const formatPhoneNumber = phoneNumberString => {
  const cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3]
  }
  return null
}

export function mapToObject (m) {
  const o = {}
  for (const [k, v] of m) { o[k] = v }
  return o
}

export function mapToObjectRec (m) {
  const lo = {}
  for (const [k, v] of m) {
    if (v instanceof Map) {
      lo[k] = mapToObjectRec(v)
    } else {
      lo[k] = v
    }
  }
  return lo
}

export function parseDroppableType (destination) {
  return destination.droppableId.split('-')[0]
}

export function parseDroppableId (destination) {
  return parseInt(destination.droppableId.split('-')[1])
}

export function parseDoorId (destination) {
  return parseInt(destination.droppableId.split('-')[2])
}

export function parseDraggableType (draggableId) {
  return draggableId.split('-')[0]
}

export function parseDraggableId (draggableId) {
  return parseInt(draggableId.split('-')[1])
}

export function generateDropppableId (type, id, doorId) {
  return type + '-' + id + '-' + doorId
}

export function generateDraggableId (type, id) {
  return type + '-' + id
}

export function convertToCamelCase (name) {
  const status = name.charAt(0).toLowerCase() + name.slice(1)
  return status.replace(/ /g, '')
}

export function convertToKebabCase (name) {
  return name.toLowerCase().replace(/ /g, '-')
}
