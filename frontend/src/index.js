import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'
import { ThemeProvider } from 'styled-components'
import 'react-tabs/style/react-tabs.css'

import createStore from './store'
import App from './components/App'
import registerServiceWorker from './registerServiceWorker'
import { theme } from './styles'

const { store, persistor } = createStore()

const router = (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </PersistGate>
  </Provider>
)

ReactDOM.render(router, document.getElementById('root'))
registerServiceWorker()
