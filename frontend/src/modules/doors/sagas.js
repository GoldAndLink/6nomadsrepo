import {
  call,
  put,
  takeLatest,
  fork,
  select
} from 'redux-saga/effects'
import { normalize } from 'normalizr'
import { AppointmentSchema } from '../appointments/sagas'

import axios from '../../utils/axios'
import DoorsActions, {
  DoorsTypes
} from './actions'
import {
  token as getToken
} from '../users/selectors'
import {
  getAllOrders,
  getAllAreas,
  getAllDoors
} from '../entities/selectors'
import EntitiesActions from '../entities/actions'
import { OrderSchema } from '../orders/sagas'

// Sagas
function * getDoors ({
  payload
}) {
  yield put(DoorsActions.getDoorsLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      areaId
    } = payload
    const url = areaId ? `/doors?areaId=${areaId}` : `/doors`
    const {
      data
    } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(DoorsActions.getDoorsSuccess(data))
  } catch (e) {
    yield put(DoorsActions.getDoorsFailure(e.message || e))
  }
}

function * getAppointmentsForDoor ({
  doorId,
  doorName
}) {
  yield put(DoorsActions.getAppointmentsForDoorLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `/appointments?doorId=${doorId}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    const doorObject = {
      id: doorId,
      name: doorName,
      appointments: data
    }

    yield put(DoorsActions.getAppointmentsForDoorSuccess(doorObject))
  } catch (e) {
    yield put(DoorsActions.getAppointmentsForDoorFailure(e.message || e))
  }
}

function * getAppointmentsForDoors ({ payload }) {
  const { buildingId,
    startTime,
    endTime,
    startDate,
    endDate } = payload
  yield put(DoorsActions.getAppointmentsForDoorsLoading())

  try {
    if (buildingId) {
      const token = yield select(state => getToken(state))
      const areas = yield select(state => getAllAreas(state))
      const doors = yield select(state => getAllDoors(state))

      const areaBuilding = areas.find(a => a.get('buildingId') === buildingId)
      const areaDoors = doors.filter(d => d.get('areaId') === areaBuilding.get('id'))

      let attributes = ''
      areaDoors.map((door, index) => { index > 0 ? attributes += `&doorId=${door.get('id')}` : attributes += `doorId=${door.get('id')}` })
      const {
        data
      } = yield call(axios.get, `/appointments?${attributes}&timeFrom=${startTime}&timeTo=${endTime}&dateFrom=${startDate}&dateTo=${endDate}`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })

      // update normalized entities appointments
      const normalizedData = normalize(data, [AppointmentSchema])
      yield put(EntitiesActions.mergeAppointments(normalizedData.entities))

      let orders = yield select(state => getAllOrders(state))

      for (const appt of data) {
        if (appt.orders) {
          const normalizedOrdersData = normalize(appt.orders, [OrderSchema])
          orders = orders.merge(normalizedOrdersData.entities.orders)
        }
      }

      yield put(EntitiesActions.mergeOrders({ orders }))
      yield put(DoorsActions.setAppointments(data))
    }

    yield put(DoorsActions.getAppointmentsForDoorsSuccess())
  } catch (e) {
    yield put(DoorsActions.getAppointmentsForDoorsFailure(e.message || e))
  }
}

function * getDoorDuration ({ payload }) {
  yield put(DoorsActions.getDoorDurationLoading())
  try {
    const token = yield select(state => getToken(state))
    const { id } = payload
    const { data } = yield call(axios.get, `/doorDurations?doorId=${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    yield put(DoorsActions.getDoorDurationSuccess(data))
  } catch (e) {
    yield put(DoorsActions.getDoorDurationFailure(e.message || e))
  }
}

// Watchers
function * getDoorsWatcher () {
  yield takeLatest(DoorsTypes.GET_DOORS, getDoors)
}

function * getAppointmentsForDoorWatcher () {
  yield takeLatest(DoorsTypes.GET_APPOINTMENTS_FOR_DOOR, getAppointmentsForDoor)
}

function * getAppointmentsForDoorsWatcher () {
  yield takeLatest(DoorsTypes.GET_APPOINTMENTS_FOR_DOORS, getAppointmentsForDoors)
}

function * getDoorDurationWatcher () {
  yield takeLatest(DoorsTypes.GET_DOOR_DURATION, getDoorDuration)
}

export default function * root () {
  yield fork(getDoorsWatcher)
  yield fork(getAppointmentsForDoorWatcher)
  yield fork(getAppointmentsForDoorsWatcher)
  yield fork(getDoorDurationWatcher)
}
