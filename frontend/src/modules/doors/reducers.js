import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { DoorsTypes } from './actions'
import { SitesTypes } from '../sites/actions'
import { AppointmentsTypes } from '../appointments/actions'

// getDoors
export const getDoorsLoading = state =>
  state.merge({
    getDoorsIsLoading: true,
    getDoorsErrorMessage: ''
  })

export const getDoorsSuccess = (state, { doors }) =>
  state.merge({
    doors: [...state.doors, ...doors],
    getDoorsIsLoading: false,
    getDoorsErrorMessage: null
  })

export const getDoorsFailure = (state, { errorMessage }) =>
  state.merge({
    doors: null,
    getDoorsIsLoading: false,
    getDoorsErrorMessage: errorMessage
  })

// clearDoors
export const clearDoors = state =>
  state.merge({
    doors: []
  })

// clearAppointments
export const clearAppointments = state =>
  state.merge({
    appointments: []
  })

// setAppointments
export const setAppointments = (state, { appointments }) =>
  state.merge({
    appointments: appointments
  })

// editAppointments
export const editAppointment = (state, { appointment }) => {
  return state.merge({
    appointments: state.get('appointments').map(
      appt => (appt.get('id') !== appointment.id ? appt : appt.merge({
        'duration': appointment.duration,
        'doorId': appointment.doorId,
        'date': appointment.date }))
    )
  })
}

// getAppointmentsForDoor
export const getAppointmentsForDoorLoading = state =>
  state.merge({
    getAppointmentsForDoorIsLoading: true,
    getAppointmentsForDoorErrorMessage: ''
  })

export const getAppointmentsForDoorSuccess = (state, { appointments }) =>
  state.merge({
    appointments: [...state.appointments, appointments],
    getAppointmentsForDoorIsLoading: false,
    getAppointmentsForDoorErrorMessage: null
  })

export const getAppointmentsForDoorFailure = (state, { errorMessage }) =>
  state.merge({
    appointments: null,
    getAppointmentsForDoorIsLoading: false,
    getAppointmentsForDoorErrorMessage: errorMessage
  })

// getAppointmentsForDoors
export const getAppointmentsForDoorsLoading = state =>
  state.merge({
    getAppointmentsForDoorsIsLoading: true,
    getAppointmentsForDoorsErrorMessage: ''
  })

export const getAppointmentsForDoorsSuccess = state =>
  state.merge({
    getAppointmentsForDoorsIsLoading: false,
    getAppointmentsForDoorsErrorMessage: null
  })

export const getAppointmentsForDoorsFailure = (state, { errorMessage }) =>
  state.merge({
    getAppointmentsForDoorsIsLoading: false,
    getAppointmentsForDoorsErrorMessage: errorMessage
  })

export const getAllSitesSuccess = (state, { entities: { doors } }) =>
  state.merge({ doors })

// getDurationRulesForDoor
export const getDoorDurationLoading = state =>
  state.merge({
    getDoorDurationIsLoading: true,
    getDoorDurationErrorMessage: ''
  })

export const getDoorDurationSuccess = (state, { doorDuration }) =>
  state.merge({
    doorDuration,
    getDoorDurationIsLoading: false,
    getDoorDurationErrorMessage: null
  })

export const getDoorDurationFailure = (state, { errorMessage }) =>
  state.merge({
    doorDuration: null,
    getDoorDurationIsLoading: false,
    getDoorDurationErrorMessage: errorMessage
  })

// appointments socket
export const updateAppointmentForDoorWithSocketAppointment = (state, { appointment }) => {
  return state.merge({
    appointments: state.get('appointments').map(
      appt => (appt.get('id') !== appointment.id ? appt : appt.merge({ ...appointment }))
    )
  })
}

export const updateAppointment = (state, { payload }) =>
  state.merge({
    appointments: state.get('appointments').map(
      appt => (appt.get('id') !== payload.id ? appt : appt.merge({ ...payload }))
    )
  })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DoorsTypes.GET_DOORS_LOADING]: getDoorsLoading,
  [DoorsTypes.GET_DOORS_SUCCESS]: getDoorsSuccess,
  [DoorsTypes.GET_DOORS_FAILURE]: getDoorsFailure,

  [DoorsTypes.CLEAR_DOORS]: clearDoors,

  [DoorsTypes.CLEAR_APPOINTMENTS]: clearAppointments,

  [DoorsTypes.SET_APPOINTMENTS]: setAppointments,

  [DoorsTypes.EDIT_APPOINTMENT]: editAppointment,

  [DoorsTypes.GET_APPOINTMENTS_FOR_DOOR_LOADING]: getAppointmentsForDoorLoading,
  [DoorsTypes.GET_APPOINTMENTS_FOR_DOOR_SUCCESS]: getAppointmentsForDoorSuccess,
  [DoorsTypes.GET_APPOINTMENTS_FOR_DOOR_FAILURE]: getAppointmentsForDoorFailure,

  [DoorsTypes.GET_APPOINTMENTS_FOR_DOORS_LOADING]: getAppointmentsForDoorsLoading,
  [DoorsTypes.GET_APPOINTMENTS_FOR_DOORS_SUCCESS]: getAppointmentsForDoorsSuccess,
  [DoorsTypes.GET_APPOINTMENTS_FOR_DOORS_FAILURE]: getAppointmentsForDoorsFailure,
  [SitesTypes.GET_ALL_SITES_SUCCESS]: getAllSitesSuccess,

  [DoorsTypes.GET_DOOR_DURATION_LOADING]: getDoorDurationLoading,
  [DoorsTypes.GET_DOOR_DURATION_SUCCESS]: getDoorDurationSuccess,
  [DoorsTypes.GET_DOOR_DURATION_FAILURE]: getDoorDurationFailure,

  [DoorsTypes.UPDATE_APPOINTMENT_FOR_DOOR_WITH_SOCKET_APPOINTMENT]: updateAppointmentForDoorWithSocketAppointment,

  [AppointmentsTypes.UPDATE_APPOINTMENT]: updateAppointment
})
