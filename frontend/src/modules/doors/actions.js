import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getDoors
  getDoors: ['payload'],
  // The operation has started and is loading
  getDoorsLoading: null,
  // The doors was successfully fetched
  getDoorsSuccess: ['doors'],
  // An error occurred
  getDoorsFailure: ['errorMessage'],

  clearDoors: null,

  clearAppointments: null,

  setAppointments: ['appointments'],

  editAppointment: ['appointment'],

  updateAppointmentForDoorWithSocketAppointment: ['appointment'],

  // tmp hack
  getAppointmentsForDoor: ['doorId'],
  getAppointmentsForDoorLoading: null,
  getAppointmentsForDoorSuccess: ['appointments'],
  getAppointmentsForDoorFailure: ['errorMessage'],

  getAppointmentsForDoors: ['payload'],
  getAppointmentsForDoorsLoading: null,
  getAppointmentsForDoorsSuccess: null,
  getAppointmentsForDoorsFailure: ['errorMessage'],

  getDoorDuration: ['payload'],
  getDoorDurationLoading: null,
  getDoorDurationSuccess: ['doorDuration'],
  getDoorDurationFailure: ['errorMessage']
})

export const DoorsTypes = Types
export default Creators
