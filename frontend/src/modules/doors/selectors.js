/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import { getAreaById } from '../areas/selectors'

export const getDoors = state => {
  return state.doors ? state.doors.get('doors') : new Map()
}

export const getAppointmentsForDoors = state => {
  return state.doors ? state.doors.get('appointments') : new Map()
}

export const getDoorDuration = state => {
  return state.doors ? state.doors.get('doorDuration') : new Map()
}

export const getDoorById = (state, id) => {
  const door = id && state.entities && state.entities.get('doors') ? state.entities.get('doors').get(id.toString()) : null
  if (door) return door.set('area', getAreaById(state, door.get('areaId')))
  return door
}

export const getAppointmentsForDoorsIsLoading = state =>
  state.doors.get('getAppointmentsForDoorsIsLoading')
