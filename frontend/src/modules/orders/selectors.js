/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import { List } from 'immutable'

export const injectPO = orders => {
  if (orders) {
    orders = orders.map(o => {
      const data = o.get('data')
      let PO = data ? data.get('otherRefs', []).find(ref => ref.get('type') === 'PO') : null
      if (!PO) {
        PO = data ? data.get('otherRefs', []).first() : null
      }
      return PO ? o.set('PO', PO.get('val')) : o
    })
  }
  return orders
}

export const getOrders = state => {
  const orders = state.orders ? state.orders.get('orders') : new List()
  return injectPO(orders)
}

export const getPages = state => state.orders.get('pages')

export const getAllOrderStatuses = state => {
  return state.orders.get('orderStatuses')
}

export const getOrdersForAppointment = (state, appointmentId) => {
  const appointment = state.entities && state.entities.get('appointments').find(appt => appt.get('id') === appointmentId)
  return appointment.get('orders')
}

export const getSelectedOrders = state => {
  return state.orders.get('selectedOrders')
}

export const getCheckedOrders = state => {
  return state.orders.get('checkedOrders')
}

export const getIsOrderDetailsModalVisible = state => {
  return state.orders.get('isOrderDetailsModalVisible')
}

export const getOrderDetails = state => {
  return state.orders.get('orderDetails')
}

export const getOrdersIsLoading = state =>
  state.orders.get('getOrdersIsLoading')

export const getSearchAttributes = state => {
  return {
    searchText: state.orders.get('searchText'),
    attributesSelect: state.orders.get('attributesSelect'),
    customerSelect: state.orders.get('customerSelect'),
    isScheduledSelect: state.orders.get('isScheduledSelect'),
    ordersStatusSelect: state.orders.get('ordersStatusSelect'),
    deliveryDateSelect: state.orders.get('deliveryDateSelect'),
    destinationSelect: state.orders.get('destinationSelect'),
    currentPage: state.orders.get('currentPage')
  }
}
