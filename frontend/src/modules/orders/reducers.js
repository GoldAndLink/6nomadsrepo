import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { OrdersTypes } from './actions'
import { AppointmentsTypes } from '../appointments/actions'

// getOrders
export const getOrdersLoading = state =>
  state.merge({
    getOrdersIsLoading: true,
    getOrdersErrorMessage: ''
  })

export const getOrdersSuccess = (state, { orders, pages }) =>
  state.merge({
    orders,
    pages,
    getOrdersIsLoading: false,
    getOrdersErrorMessage: null
  })

export const getOrdersFailure = (state, { errorMessage }) =>
  state.merge({
    getOrdersIsLoading: false,
    getOrdersErrorMessage: errorMessage
  })

// getOrder
export const getOrderLoading = state =>
  state.merge({
    getOrderIsLoading: true,
    getOrderErrorMessage: null
  })

export const getOrderSuccess = (state, { order }) =>
  state.merge({
    order,
    getOrderIsLoading: false,
    getOrderErrorMessage: null
  })

export const getOrderFailure = (state, { errorMessage }) =>
  state.merge({
    order: null,
    getOrderIsLoading: false,
    getOrderErrorMessage: errorMessage
  })

// getAllOrderStatuses
export const getAllOrderStatusesLoading = state =>
  state.merge({
    getAllOrderStatusesIsLoading: true,
    getAllOrderStatusesErrorMessage: ''
  })

export const getAllOrderStatusesSuccess = (state, { orderStatuses }) => {
  const orderStatusesWithoutCancelled = orderStatuses ? orderStatuses.filter(orderStatus => orderStatus.name.toLowerCase() !== 'cancelled') : orderStatuses
  return state.merge({
    orderStatuses: orderStatusesWithoutCancelled,
    getAllOrderStatusesIsLoading: false,
    getAllOrderStatusesErrorMessage: null
  })
}

export const getAllOrderStatusesFailure = (state, { errorMessage }) =>
  state.merge({
    orderStatuses: null,
    getAllOrderStatusesIsLoading: false,
    getAllOrderStatusesErrorMessage: errorMessage
  })

export const setSelectedOrders = (state, { orders }) =>
  state.merge({
    selectedOrders: orders
  })

export const setCheckedOrders = (state, { orders }) =>
  state.merge({
    checkedOrders: orders
  })

export const selectOrder = (state, { order }) =>
  state.merge({
    selectedOrders: [...state.get('selectedOrders'), order]
  })

export const deselectOrder = (state, { order }) =>
  state.merge({
    selectedOrders: state.get('selectedOrders').filter(o => o.get('id') !== order.get('id'))
  })

export const openOrderDetailsModal = state =>
  state.merge({
    isOrderDetailsModalVisible: true
  })

export const closeOrderDetailsModal = state =>
  state.merge({
    isOrderDetailsModalVisible: false,
    orderDetails: null
  })

export const setOrderDetails = (state, { orderDetails }) =>
  state.merge({
    orderDetails
  })

// Orders search
export const onSearchChange = (state, { payload }) => {
  return state.merge({
    ...state,
    ...payload
  })
}

// Orders socket
export const updateOrdersWithSocketOrder = (state, { payload }) => {
  const { socketOrder } = payload

  return state.merge({
    orders: state.get('orders').map(
      order => order.get('id') !== socketOrder.id ? order : order.merge(socketOrder)
    )
  })
}

export const createAppointmentSuccess = (state, { appointment }) => {
  return state.merge({
    checkedOrders: [],
    selectOrders: []
  })
}

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [OrdersTypes.GET_ORDERS_LOADING]: getOrdersLoading,
  [OrdersTypes.GET_ORDERS_SUCCESS]: getOrdersSuccess,
  [OrdersTypes.GET_ORDERS_FAILURE]: getOrdersFailure,

  [OrdersTypes.GET_ORDER_LOADING]: getOrderLoading,
  [OrdersTypes.GET_ORDER_SUCCESS]: getOrderSuccess,
  [OrdersTypes.GET_ORDER_FAILURE]: getOrderFailure,

  [OrdersTypes.GET_ALL_ORDER_STATUSES_LOADING]: getAllOrderStatusesLoading,
  [OrdersTypes.GET_ALL_ORDER_STATUSES_SUCCESS]: getAllOrderStatusesSuccess,
  [OrdersTypes.GET_ALL_ORDER_STATUSES_FAILURE]: getAllOrderStatusesFailure,

  [OrdersTypes.SET_SELECTED_ORDERS]: setSelectedOrders,
  [OrdersTypes.SET_CHECKED_ORDERS]: setCheckedOrders,
  [OrdersTypes.SELECT_ORDER]: selectOrder,
  [OrdersTypes.DESELECT_ORDER]: deselectOrder,
  [OrdersTypes.OPEN_ORDER_DETAILS_MODAL]: openOrderDetailsModal,
  [OrdersTypes.CLOSE_ORDER_DETAILS_MODAL]: closeOrderDetailsModal,
  [OrdersTypes.SET_ORDER_DETAILS]: setOrderDetails,

  [OrdersTypes.ON_SEARCH_CHANGE]: onSearchChange,

  [OrdersTypes.UPDATE_ORDERS_WITH_SOCKET_ORDER]: updateOrdersWithSocketOrder,

  [AppointmentsTypes.CREATE_APPOINTMENT_SUCCESS]: createAppointmentSuccess
})
