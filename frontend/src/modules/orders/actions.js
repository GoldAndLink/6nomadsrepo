import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getOrders
  getOrders: ['payload'],
  getOrdersDelayed: ['payload'],
  // The operation has started and is loading
  getOrdersLoading: null,
  // The users was successfully fetched
  getOrdersSuccess: ['orders', 'pages'],
  // An error occurred
  getOrdersFailure: ['errorMessage'],

  setSelectedOrders: ['orders'],
  setCheckedOrders: ['orders'],
  selectOrder: ['order'],
  deselectOrder: ['order'],

  // getOrder
  getOrder: ['payload'],
  getOrderLoading: null,
  getOrderSuccess: ['order'],
  getOrderFailure: ['errorMessage'],

  // getAllOrderStatuses
  getAllOrderStatuses: null,
  getAllOrderStatusesLoading: null,
  getAllOrderStatusesSuccess: ['orderStatuses'],
  getAllOrderStatusesFailure: ['errorMessage'],

  openOrderDetailsModal: null,
  setOrderDetails: ['orderDetails'],
  closeOrderDetailsModal: null,

  // orders search
  searchOrders: ['payload'],
  onSearchChange: ['payload'],

  // orders socket
  updateOrdersWithSocketOrder: ['payload']

})

export const OrdersTypes = Types
export default Creators
