import {
  call,
  put,
  takeLatest,
  fork,
  select
} from 'redux-saga/effects'
import moment from 'moment'
import { delay } from 'redux-saga'
import { normalize, schema } from 'normalizr'
import EntitiesActions from '../entities/actions'

import axios from '../../utils/axios'
import OrdersActions, {
  OrdersTypes
} from './actions'
import AppointmentActions from '../appointments/actions'
import {
  token as getToken,
  getUserAccounts
} from '../users/selectors'
import {
  getAppointmentOrders
} from '../appointmentOrders/selectors'
import {
  getSearchAttributes
} from './selectors'
import config from '../../config'

const baseUrl = '/orders'

export const OrderSchema = new schema.Entity('orders')

/* Sagas */

function * getOrders ({
  payload
}) {
  yield put(OrdersActions.getOrdersLoading())
  try {
    const token = yield select(state => getToken(state))
    const accounts = yield select(state => getUserAccounts(state))

    const {
      searchText,
      attributesSelect,
      isScheduledSelect,
      isOpen,
      deliveryDate,
      deliveryDateFrom,
      deliveryDateTo,
      customerId,
      orderStatusId,
      destinationId,
      page,
      include
    } = payload

    const urlPrefix = isOpen ? `/open${baseUrl}` : baseUrl
    let url = searchText && attributesSelect ? `${urlPrefix}?${attributesSelect}=${searchText}` : `${urlPrefix}?`

    if (deliveryDate) {
      url += `&deliveryDate=${deliveryDate}`
    }
    if (deliveryDateFrom && deliveryDateTo) {
      url += `&deliveryDateFrom=${deliveryDateFrom}&deliveryDateTo=${deliveryDateTo}`
    }
    if (customerId) {
      url += `&customerId=${customerId}`
    }
    if (orderStatusId) {
      url += `&orderStatusId=${orderStatusId}`
    }
    if (destinationId) {
      url += `&destinationId=${destinationId}`
    }
    if (page) {
      url += `&page=${page}`
    }
    if (!isOpen && accounts && accounts.size) {
      accounts.forEach(account => {
        url += `&accountId=${account.get('id')}`
      })
    }
    if (include) {
      url += `&_include=${include}`
    }

    const {
      data
    } = yield call(axios.get, url, {
      headers: {
        Authorization: isOpen ? `Key ${config.API_KEY}` : `Bearer ${token}`
      }
    })

    const normalizedData = normalize(data.orders, [OrderSchema])
    yield put(EntitiesActions.mergeOrders(normalizedData.entities))

    if (isScheduledSelect === true) {
      const appointmentOrders = yield select(state => getAppointmentOrders(state))
      yield put(OrdersActions.getOrdersSuccess(data.orders.filter(order =>
        appointmentOrders.findIndex(appointmentOrder =>
          appointmentOrder.get('orderId') === order.get('id')
        ) !== -1)))
    } else if (isScheduledSelect === false) {
      const appointmentOrders = yield select(state => getAppointmentOrders(state))
      yield put(OrdersActions.getOrdersSuccess(data.orders.filter(order =>
        appointmentOrders.findIndex(appointmentOrder =>
          appointmentOrder.get('orderId') === order.get('id')
        ) === -1)))
    } else {
      if (page && page > 1) {
        const currentOrders = yield select(state => state.orders.get('orders'))
        if (currentOrders) {
          yield put(
            OrdersActions.getOrdersSuccess(
              [...currentOrders.toArray(), ...data.orders],
              data.pages
            )
          )
        } else {
          yield put(OrdersActions.getOrdersSuccess(data.orders, data.pages))
        }
      } else {
        yield put(OrdersActions.getOrdersSuccess(data.orders, data.pages))
      }
    }
    if (include && include === 'appointments') {
      let appointments = []
      data.orders.forEach(o => {
        appointments = appointments.concat(o.appointments)
      })
      yield put(AppointmentActions.updateAppointments(appointments))
    }
  } catch (e) {
    yield put(OrdersActions.getOrdersFailure(e.message || e))
  }
}

function * getOrder ({
  payload
}) {
  yield put(OrdersActions.getOrderLoading())
  try {
    const {
      id,
      token
    } = payload
    const {
      data
    } = yield call(axios.get, `${baseUrl}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(OrdersActions.getOrderSuccess(data.order))
  } catch (e) {
    yield put(OrdersActions.getOrderFailure(e.message || e))
  }
}

function * searchOrders ({
  payload
}) {
  yield put(OrdersActions.onSearchChange(payload))
  const attributes = yield select(state => getSearchAttributes(state))

  const data = {
    ...attributes,
    orderStatusId: attributes.ordersStatusSelect,
    customerId: attributes.customerSelect,
    deliveryDate: attributes.deliveryDateSelect ? attributes.deliveryDateSelect.format('YYYY-MM-DD') : null,
    destinationId: attributes.destinationSelect,
    page: attributes.currentPage,
    include: 'appointments'
  }

  if (data.deliveryDate) {
    const deliveryDate = moment(data.deliveryDate)
    data.deliveryDateFrom = deliveryDate.subtract(1, 'day').format('YYYY-MM-DD')
    data.deliveryDateTo = deliveryDate.add(2, 'days').format('YYYY-MM-DD')
    // change is done inplace so we need to normalize
    deliveryDate.subtract(1, 'day')
  }

  yield put(OrdersActions.getOrdersDelayed(data))
}

function * getOrdersDelayed ({
  payload
}) {
  yield delay(1000)
  yield put(OrdersActions.getOrders(payload))
}

function * getAllOrderStatuses () {
  yield put(OrdersActions.getAllOrderStatusesLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, '/order_statuses', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(OrdersActions.getAllOrderStatusesSuccess(data))
  } catch (e) {
    yield put(OrdersActions.getAllOrderStatusesFailure(e.message || e))
  }
}

// Watchers
function * getOrdersWatcher () {
  yield takeLatest(OrdersTypes.GET_ORDERS, getOrders)
}

function * searchOrdersWatcher () {
  yield takeLatest(OrdersTypes.SEARCH_ORDERS, searchOrders)
}

function * getOrdersDelayedWatcher () {
  yield takeLatest(OrdersTypes.GET_ORDERS_DELAYED, getOrdersDelayed)
}

function * getOrderWatcher () {
  yield takeLatest(OrdersTypes.GET_ORDER, getOrder)
}

function * getAllOrderStatusesWatcher () {
  yield takeLatest(OrdersTypes.GET_ALL_ORDER_STATUSES, getAllOrderStatuses)
}

export default function * root () {
  yield fork(getOrdersWatcher)
  yield fork(getOrderWatcher)
  yield fork(getAllOrderStatusesWatcher)
  yield fork(searchOrdersWatcher)
  yield fork(getOrdersDelayedWatcher)
}
