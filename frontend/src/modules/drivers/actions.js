import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getAllDrivers
  getAllDrivers: null,
  // The operation has started and is loading
  getAllDriversLoading: null,
  // The drivers was successfully fetched
  getAllDriversSuccess: ['entities'],
  // An error occurred
  getAllDriversFailure: ['errorMessage'],

  // socket
  updateDriverWithSocketDriver: ['payload']
})

export const DriversTypes = Types
export default Creators
