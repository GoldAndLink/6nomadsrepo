import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { DriversTypes } from './actions'

// getAllDrivers
export const getAllDriversLoading = state =>
  state.merge({
    getAllDriversIsLoading: true,
    getAllDriversErrorMessage: ''
  })

export const getAllDriversSuccess = (state, { entities: { drivers } }) =>
  state.mergeIn(['drivers'], drivers).merge({
    getAllDriversIsLoading: false,
    getAllDriversErrorMessage: null
  })

export const getAllDriversFailure = (state, { errorMessage }) =>
  state.merge({
    drivers: null,
    getAllDriversIsLoading: false,
    getAllDriversErrorMessage: errorMessage
  })

export const updateDriverWithSocketDriver = (state, { payload: { driver } }) =>
  state.mergeIn(['drivers', driver && driver.id.toString()], driver)

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DriversTypes.GET_ALL_DRIVERS_LOADING]: getAllDriversLoading,
  [DriversTypes.GET_ALL_DRIVERS_SUCCESS]: getAllDriversSuccess,
  [DriversTypes.GET_ALL_DRIVERS_FAILURE]: getAllDriversFailure,
  [DriversTypes.UPDATE_DRIVER_WITH_SOCKET_DRIVER]: updateDriverWithSocketDriver
})
