import { call, put, takeLatest, fork } from 'redux-saga/effects'

import axios from '../../utils/axios'
import DriversActions, { DriversTypes } from './actions'
import config from '../../config'
import { normalize, schema } from 'normalizr'

export const DriverSchema = new schema.Entity('drivers')

// Sagas
function * getAllDrivers ({ payload }) {
  yield put(DriversActions.getAllDriversLoading())
  try {
    const { data } = yield call(axios.get, `/open/drivers`, {
      headers: {
        Authorization: `Key ${config.API_KEY}`
      }
    })
    const normalizedData = normalize(data, [DriverSchema])
    yield put(DriversActions.getAllDriversSuccess(normalizedData.entities))
  } catch (e) {
    yield put(DriversActions.getAllDriversFailure(e.message || e))
  }
}

// Watchers
function * getAllDriversWatcher () {
  yield takeLatest(DriversTypes.GET_ALL_DRIVERS, getAllDrivers)
}

export default function * root () {
  yield fork(getAllDriversWatcher)
}
