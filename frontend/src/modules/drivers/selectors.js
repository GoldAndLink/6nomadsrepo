/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
export const getAllDrivers = state => {
  return state.entities && state.entities.get('drivers') ? [...state.entities.get('drivers').values()] : []
}

export const getDriverById = (state, id) => {
  const door = id && state.entities && state.entities.get('drivers') ? state.entities.get('drivers').get(id.toString()) : null
  return door
}
