/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
export const getItems = state => {
  return state.items ? state.items.get('items') : new Map()
}
