import { call, put, takeLatest, fork, select } from 'redux-saga/effects'
import {
  token as getToken
} from '../users/selectors'
import axios from '../../utils/axios'
import FeedsActions, { FeedsTypes } from './actions'
import SitesActions from '../sites/actions'
import DriversActions from '../drivers/actions'
import CarriersActions from '../carriers/actions'
import AppointmentActions from '../appointments/actions'
import OrdersAction from '../orders/actions'
import UsersActions from '../users/actions'
import DoorsActions from '../doors/actions'
import SchedulesActions from '../schedules/actions'
import ReportsActions from '../reports/actions'

import { normalize, schema } from 'normalizr'

const CarrierSchema = new schema.Entity('carriers')

const DoorDurationSchema = new schema.Entity('doorDurations')

const DriverSchema = new schema.Entity('drivers')

const DoorSchema = new schema.Entity('doors', {
  doorDurations: [DoorDurationSchema]
})

const AreaSchema = new schema.Entity('areas', {
  doors: [DoorSchema]
})

const BuildingSchema = new schema.Entity('buildings', {
  areas: [AreaSchema]
})

const SiteSchema = new schema.Entity('sites', {
  buildings: [BuildingSchema]
})

// Sagas
function * getBootstrapData ({ payload }) {
  yield put(FeedsActions.getBootstrapDataLoading())
  try {
    const {
      carrierPortal
    } = payload

    let url = '/feeds/bootstrap'
    const token = yield select(state => getToken(state))

    if (carrierPortal) {
      url += `?carrierPortal=${carrierPortal}`
    }

    const { data } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    if (carrierPortal) {
      const normalizedSites = normalize(data.sites, [SiteSchema])
      yield put(SitesActions.getAllSitesSuccess(normalizedSites.entities))
    } else {
      const normalizedSites = normalize(data.sites, [SiteSchema])
      const normalizedCarriers = normalize(data.carriers, [CarrierSchema])
      const normalizedDrivers = normalize(data.drivers, [DriverSchema])

      yield put(SitesActions.getAllSitesSuccess(normalizedSites.entities))
      yield put(DriversActions.getAllDriversSuccess(normalizedDrivers.entities))
      yield put(CarriersActions.getAllCarriersSuccess(normalizedCarriers.entities))
      yield put(AppointmentActions.getAllAppointmentStatusesSuccess(data.appointmentStatuses))
      yield put(OrdersAction.getAllOrderStatusesSuccess(data.orderStatuses))
      yield put(UsersActions.getRolesSuccess(data.roles))
      yield put(DoorsActions.getDoorDurationSuccess(normalizedSites.entities.doorDurations))
      yield put(FeedsActions.getBootstrapDataSuccess(data))
      yield put(SchedulesActions.setSchedules(data.schedules))
      yield put(SchedulesActions.setDoorSchedules(data.doorSchedules))

      // let's update the bubbles count with the new info
      yield put(ReportsActions.updateWarnings())
    }
  } catch (e) {
    yield put(FeedsActions.getBootstrapDataFailure(e.message || e))
  }
}

// Watchers
function * getBootstrapDataWatcher () {
  yield takeLatest(FeedsTypes.GET_BOOTSTRAP_DATA, getBootstrapData)
}

export default function * root () {
  yield fork(getBootstrapDataWatcher)
}
