export const getBootstrapDataIsLoading = state => {
  return state.feeds.get('getBootstrapDataIsLoading')
}
