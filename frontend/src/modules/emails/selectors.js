export const getSendEmailIsLoading = state =>
  state.emails.get('sendEmailIsLoading')

export const getSendEmailErrorMessage = state =>
  state.emails.get('sendEmailErrorMessage')
