import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getAllSites
  getAllSites: null,
  // The operation has started and is loading
  getAllSitesLoading: null,
  // The sites was successfully fetched
  getAllSitesSuccess: ['entities'],
  // An error occurred
  getAllSitesFailure: ['errorMessage'],

  // getBuildingsForSite
  getBuildingsForSite: ['payload'],
  // The operation has started and is loading
  getBuildingsForSiteLoading: null,
  // The buildings was successfully fetched
  getBuildingsForSiteSuccess: ['buildings'],
  // An error occurred
  getBuildingsForSiteFailure: ['errorMessage']
})

export const SitesTypes = Types
export default Creators
