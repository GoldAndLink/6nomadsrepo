/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
export const getAllSites = state => {
  return state.sites ? state.sites.get('sites') : new Map()
}

export const getBuildingsForSite = state => {
  return state.sites ? state.sites.get('buildings') : new Map()
}

export const getSiteById = (state, id) => {
  return id && state.entities && state.entities.get('sites') ? state.entities.get('sites').get(id.toString()) : null
}
