import { Map } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = Map({
  sites: null,
  buildings: null,
  getAllSitesIsLoading: false,
  getAllSitesErrorMessage: null,
  getBuildingsForSiteIsLoading: false,
  getBuildingsForSiteErrorMessage: null
})
