import { call, put, takeLatest, fork, select } from 'redux-saga/effects'
import { normalize, schema } from 'normalizr'
import axios from '../../utils/axios'
import SitesActions, { SitesTypes } from './actions'
import { token as getToken } from '../users/selectors'

const DoorSchema = new schema.Entity('doors')

const AreaSchema = new schema.Entity('areas', {
  doors: [DoorSchema]
})

const BuildingSchema = new schema.Entity('buildings', {
  areas: [AreaSchema]
})

const SiteSchema = new schema.Entity('sites', {
  buildings: [BuildingSchema]
})

// Sagas
function * getAllSites () {
  yield put(SitesActions.getAllSitesLoading())
  try {
    const token = yield select(state => getToken(state))
    const { data } = yield call(axios.get, `/sites`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    const normalizedData = normalize(data, [SiteSchema])

    yield put(SitesActions.getAllSitesSuccess(normalizedData.entities))
  } catch (e) {
    yield put(SitesActions.getAllSitesFailure(e.message || e))
  }
}

function * getBuildingsForSite ({ payload }) {
  yield put(SitesActions.getBuildingsForSiteLoading())
  try {
    const token = yield select(state => getToken(state))
    const { id } = payload
    const { data } = yield call(axios.get, `/sites/${id}/buildings`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(SitesActions.getBuildingsForSiteSuccess(data))
  } catch (e) {
    yield put(SitesActions.getBuildingsForSiteFailure(e.message || e))
  }
}

// Watchers
function * getAllSitesWatcher () {
  yield takeLatest(SitesTypes.GET_ALL_SITES, getAllSites)
}

function * getBuildingsForSiteWatcher () {
  yield takeLatest(SitesTypes.GET_BUILDINGS_FOR_SITE, getBuildingsForSite)
}

export default function * root () {
  yield fork(getAllSitesWatcher)
  yield fork(getBuildingsForSiteWatcher)
}
