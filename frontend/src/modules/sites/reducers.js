import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { SitesTypes } from './actions'

// getAllSites
export const getAllSitesLoading = state =>
  state.merge({
    getAllSitesIsLoading: true,
    getAllSitesErrorMessage: ''
  })

export const getAllSitesSuccess = (state, { entities: { sites } }) =>
  state.merge({
    sites,
    getAllSitesIsLoading: false,
    getAllSitesErrorMessage: null
  })

export const getAllSitesFailure = (state, { errorMessage }) =>
  state.merge({
    sites: null,
    getAllSitesIsLoading: false,
    getAllSitesErrorMessage: errorMessage
  })

// getBuildingsForSite
export const getBuildingsForSiteLoading = state =>
  state.merge({
    getBuildingsForSiteIsLoading: true,
    getBuildingsForSiteErrorMessage: ''
  })

export const getBuildingsForSiteSuccess = (state, { buildings }) =>
  state.merge({
    buildings,
    getBuildingsForSiteIsLoading: false,
    getBuildingsForSiteErrorMessage: null
  })

export const getBuildingsForSiteFailure = (state, { errorMessage }) =>
  state.merge({
    buildings: null,
    getBuildingsForSiteIsLoading: false,
    getBuildingsForSiteErrorMessage: errorMessage
  })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [SitesTypes.GET_ALL_SITES_LOADING]: getAllSitesLoading,
  [SitesTypes.GET_ALL_SITES_SUCCESS]: getAllSitesSuccess,
  [SitesTypes.GET_ALL_SITES_FAILURE]: getAllSitesFailure,
  [SitesTypes.GET_BUILDINGS_FOR_SITE_LOADING]: getBuildingsForSiteLoading,
  [SitesTypes.GET_BUILDINGS_FOR_SITE_SUCCESS]: getBuildingsForSiteSuccess,
  [SitesTypes.GET_BUILDINGS_FOR_SITE_FAILURE]: getBuildingsForSiteFailure
})
