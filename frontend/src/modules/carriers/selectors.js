/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
export const getAllCarriers = state => {
  return state.entities && state.entities.get('carriers') ? [...state.entities.get('carriers').values()] : []
}

export const getCarrierById = (state, id) => {
  return id && state.entities && state.entities.get('carriers') ? state.entities.get('carriers').get(id.toString()) : null
}

export const getCarrierLoginOpen = state => {
  return state.carriers.get('isCarrierLoginOpen')
}

export const isLoggedIn = state => {
  return !!state.carriers.get('token')
}

export const getCarrierUser = state => {
  return state.carriers.get('user')
}
