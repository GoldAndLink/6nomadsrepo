import { call, put, takeLatest, fork } from 'redux-saga/effects'
import { normalize, schema } from 'normalizr'

import axios from '../../utils/axios'
import CarriersActions, { CarriersTypes } from './actions'
import CarrierRequestsActions from '../carrierRequests/actions'
import config from '../../config'

export const CarrierSchema = new schema.Entity('carriers')

// Sagas
function * getAllCarriers ({ payload }) {
  yield put(CarriersActions.getAllCarriersLoading())
  try {
    const { data } = yield call(axios.get, `/open/carriers`, {
      headers: {
        Authorization: `Key ${config.API_KEY}`
      }
    })
    const normalizedData = normalize(data, [CarrierSchema])

    yield put(CarriersActions.getAllCarriersSuccess(normalizedData.entities))
  } catch (e) {
    yield put(CarriersActions.getAllCarriersFailure(e.message || e))
  }
}

function * login ({ payload }) {
  const { username, password } = payload
  yield put(CarriersActions.loginLoading())
  try {
    const { data } = yield call(axios.post, `/users/login`, {
      email: username,
      password
    })
    yield put(CarriersActions.loginSuccess(data.token, data.user))
  } catch (e) {
    yield put(CarriersActions.loginFailure(e.message || e))
  }
}

function * logout () {
  yield put(CarriersActions.logoutLoading())
  try {
    yield put(CarrierRequestsActions.getAllCarrierRequestsSuccess([]))
    yield put(CarriersActions.logoutSuccess())
  } catch (e) {
    yield put(CarriersActions.logoutFailure(e.message || e))
  }
}

// Watchers
function * getAllCarriersWatcher () {
  yield takeLatest(CarriersTypes.GET_ALL_CARRIERS, getAllCarriers)
}

function * loginWatcher () {
  yield takeLatest(CarriersTypes.LOGIN, login)
}

function * logoutWatcher () {
  yield takeLatest(CarriersTypes.LOGOUT, logout)
}

export default function * root () {
  yield fork(getAllCarriersWatcher)
  yield fork(loginWatcher)
  yield fork(logoutWatcher)
}
