import { Map } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = Map({
  carriers: Map({}),
  token: null,
  user: null,
  getAllCarriersIsLoading: false,
  getAllCarriersErrorMessage: null,
  isCarrierLoginOpen: false,
  loginIsLoading: false,
  loginErrorMessage: null
})
