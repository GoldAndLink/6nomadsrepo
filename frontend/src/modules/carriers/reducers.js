import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { CarriersTypes } from './actions'

// getAllCarriers
export const getAllCarriersLoading = state =>
  state.merge({
    getAllCarriersIsLoading: true,
    getAllCarriersErrorMessage: ''
  })

export const getAllCarriersSuccess = (state, { entities: { carriers } }) =>
  state.mergeIn(['carriers'], carriers).merge({
    getAllCarriersIsLoading: false,
    getAllCarriersErrorMessage: null
  })

export const updateCarrierWithSocketCarrier = (state, { payload: { carrier } }) =>
  state.mergeIn(['carriers', carrier && carrier.id.toString()], carrier)

export const getAllCarriersFailure = (state, { errorMessage }) =>
  state.merge({
    carriers: null,
    getAllCarriersIsLoading: false,
    getAllCarriersErrorMessage: errorMessage
  })

export const showCarrierLogin = state => {
  return state.merge({
    isCarrierLoginOpen: true
  })
}

export const hideCarrierLogin = state => {
  return state.merge({
    isCarrierLoginOpen: false
  })
}

// login
export const loginLoading = state =>
  state.merge({
    loginIsLoading: true,
    loginErrorMessage: null
  })

export const loginSuccess = (state, { token, user }) =>
  state.merge({
    token,
    user,
    loginIsLoading: false,
    loginErrorMessage: null
  })

export const loginFailure = (state, { errorMessage }) =>
  state.merge({
    token: null,
    user: null,
    loginIsLoading: false,
    loginErrorMessage: errorMessage
  })

// logout
export const logoutLoading = state =>
  state.merge({
    logoutIsLoading: true,
    logoutErrorMessage: null
  })

export const logoutSuccess = state =>
  state.merge({
    token: null,
    user: null,
    logoutIsLoading: false,
    logoutErrorMessage: null
  })

export const logoutFailure = (state, { errorMessage }) =>
  state.merge({
    token: null,
    user: null,
    logoutIsLoading: false,
    logoutErrorMessage: errorMessage
  })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CarriersTypes.GET_ALL_CARRIERS_LOADING]: getAllCarriersLoading,
  [CarriersTypes.GET_ALL_CARRIERS_SUCCESS]: getAllCarriersSuccess,
  [CarriersTypes.GET_ALL_CARRIERS_FAILURE]: getAllCarriersFailure,
  [CarriersTypes.UPDATE_CARRIER_WITH_SOCKET_CARRIER]: updateCarrierWithSocketCarrier,
  [CarriersTypes.SHOW_CARRIER_LOGIN]: showCarrierLogin,
  [CarriersTypes.HIDE_CARRIER_LOGIN]: hideCarrierLogin,
  [CarriersTypes.LOGIN_LOADING]: loginLoading,
  [CarriersTypes.LOGIN_SUCCESS]: loginSuccess,
  [CarriersTypes.LOGIN_FAILURE]: loginFailure,
  [CarriersTypes.LOGOUT_LOADING]: logoutLoading,
  [CarriersTypes.LOGOUT_SUCCESS]: logoutSuccess,
  [CarriersTypes.LOGOUT_FAILURE]: logoutFailure
})
