import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getAllCarriers
  getAllCarriers: null,
  // The operation has started and is loading
  getAllCarriersLoading: null,
  // The carriers was successfully fetched
  getAllCarriersSuccess: ['entities'],
  // An error occurred
  getAllCarriersFailure: ['errorMessage'],

  // socket
  updateCarrierWithSocketCarrier: ['payload'],

  // carrierLogin
  showCarrierLogin: null,
  hideCarrierLogin: null,

  // login
  login: ['payload'],
  loginLoading: null,
  loginSuccess: ['token', 'user'],
  loginFailure: ['errorMessage'],

  // logout
  logout: null,
  logoutLoading: null,
  logoutSuccess: null,
  logoutFailure: null
})

export const CarriersTypes = Types
export default Creators
