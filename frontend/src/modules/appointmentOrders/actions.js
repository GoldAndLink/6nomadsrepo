import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getAppointmentOrders
  getAppointmentOrders: null,
  // The operation has started and is loading
  getAppointmentOrdersLoading: null,
  // The appointmentOrders was successfully fetched
  getAppointmentOrdersSuccess: ['appointmentOrders'],
  // An error occurred
  getAppointmentOrdersFailure: ['errorMessage']
})

export const AppointmentOrdersTypes = Types
export default Creators
