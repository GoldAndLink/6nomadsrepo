import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { AppointmentOrdersTypes } from './actions'

// getAppointmentOrders
export const getAppointmentOrdersLoading = state =>
  state.merge({
    getAppointmentOrdersIsLoading: true,
    getAppointmentOrdersErrorMessage: ''
  })

export const getAppointmentOrdersSuccess = (state, { appointmentOrders }) =>
  state.merge({
    appointmentOrders,
    getAppointmentOrdersIsLoading: false,
    getAppointmentOrdersErrorMessage: null
  })

export const getAppointmentOrdersFailure = (state, { errorMessage }) =>
  state.merge({
    appointmentOrders: null,
    getAppointmentOrdersIsLoading: false,
    getAppointmentOrdersErrorMessage: errorMessage
  })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AppointmentOrdersTypes.GET_APPOINTMENT_ORDERS_LOADING]: getAppointmentOrdersLoading,
  [AppointmentOrdersTypes.GET_APPOINTMENT_ORDERS_SUCCESS]: getAppointmentOrdersSuccess,
  [AppointmentOrdersTypes.GET_APPOINTMENT_ORDERS_FAILURE]: getAppointmentOrdersFailure
})
