import { call, put, takeLatest, fork, select } from 'redux-saga/effects'

import axios from '../../utils/axios'
import AppointmentOrdersActions, { AppointmentOrdersTypes } from './actions'
import { token as getToken } from '../users/selectors'

// Sagas
function * getAppointmentOrders ({ payload }) {
  yield put(AppointmentOrdersActions.getAppointmentOrdersLoading())
  try {
    const token = yield select(state => getToken(state))
    const { data } = yield call(axios.get, `/appointment_orders`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentOrdersActions.getAppointmentOrdersSuccess(data))
  } catch (e) {
    yield put(AppointmentOrdersActions.getAppointmentOrdersFailure(e.message || e))
  }
}

// Watchers
function * getAppointmentOrdersWatcher () {
  yield takeLatest(AppointmentOrdersTypes.GET_APPOINTMENT_ORDERS, getAppointmentOrders)
}

export default function * root () {
  yield fork(getAppointmentOrdersWatcher)
}
