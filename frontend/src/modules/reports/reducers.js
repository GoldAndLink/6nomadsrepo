import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { ReportsTypes } from './actions'

// getSummary
export const getSummaryLoading = state =>
  state.merge({
    getSummaryIsLoading: true,
    getSummaryErrorMessage: ''
  })

export const getSummarySuccess = (state, { summary }) =>
  state.merge({
    summary,
    getSummaryIsLoading: false,
    getSummaryErrorMessage: null
  })

export const getSummaryFailure = (state, { errorMessage }) =>
  state.merge({
    summary: null,
    getSummaryIsLoading: false,
    getSummaryErrorMessage: errorMessage
  })

// getSummaryYesterday
export const getSummaryYesterdayLoading = state =>
  state.merge({
    getSummaryYesterdayIsLoading: true,
    getSummaryYesterdayErrorMessage: ''
  })

export const getSummaryYesterdaySuccess = (state, { summaryYesterday }) =>
  state.merge({
    summaryYesterday,
    getSummaryYesterdayIsLoading: false,
    getSummaryYesterdayErrorMessage: null
  })

export const getSummaryYesterdayFailure = (state, { errorMessage }) =>
  state.merge({
    summaryYesterday: null,
    getSummaryYesterdayIsLoading: false,
    getSummaryYesterdayErrorMessage: errorMessage
  })

// getSummaryYesterday
export const getTodayAppointmentsSummaryLoading = state =>
  state.merge({
    getTodayAppointmentsSummaryIsLoading: true,
    getTodayAppointmentsSummaryErrorMessage: ''
  })

export const getTodayAppointmentsSummarySuccess = (state, { todayAppointmentsSummary }) =>
  state.merge({
    todayAppointmentsSummary,
    getTodayAppointmentsSummaryIsLoading: false,
    getTodayAppointmentsSummaryErrorMessage: null
  })

export const getTodayAppointmentsSummaryFailure = (state, { errorMessage }) =>
  state.merge({
    todayAppointmentsSummary: null,
    getTodayAppointmentsSummaryIsLoading: false,
    getTodayAppointmentsSummaryErrorMessage: errorMessage
  })

// getTodayAppointments
export const getTodayAppointmentsLoading = state =>
  state.merge({
    getTodayAppointmentsIsLoading: true,
    getTodayAppointmentsErrorMessage: ''
  })

export const getTodayAppointmentsSuccess = (state, { todayAppointments }) =>
  state.merge({
    todayAppointments,
    getTodayAppointmentsIsLoading: false,
    getTodayAppointmentsErrorMessage: null
  })

export const getTodayAppointmentsFailure = (state, { errorMessage }) =>
  state.merge({
    todayAppointments: null,
    getTodayAppointmentsIsLoading: false,
    getTodayAppointmentsErrorMessage: errorMessage
  })

// getYesterdayAppointments
export const getYesterdayAppointmentsLoading = state =>
  state.merge({
    getYesterdayAppointmentsIsLoading: true,
    getYesterdayAppointmentsErrorMessage: ''
  })

export const getYesterdayAppointmentsSuccess = (state, { yesterdayAppointments }) =>
  state.merge({
    yesterdayAppointments,
    getYesterdayAppointmentsIsLoading: false,
    getYesterdayAppointmentsErrorMessage: null
  })

export const getYesterdayAppointmentsFailure = (state, { errorMessage }) =>
  state.merge({
    yesterdayAppointments: null,
    getYesterdayAppointmentsIsLoading: false,
    getYesterdayAppointmentsErrorMessage: errorMessage
  })

// getLastWeekAppointments
export const getLastWeekAppointmentsLoading = state =>
  state.merge({
    getLastWeekAppointmentsIsLoading: true,
    getLastWeekAppointmentsErrorMessage: ''
  })

export const getLastWeekAppointmentsSuccess = (state, { lastWeekAppointments }) =>
  state.merge({
    lastWeekAppointments,
    getLastWeekAppointmentsIsLoading: false,
    getLastWeekAppointmentsErrorMessage: null
  })

export const getLastWeekAppointmentsFailure = (state, { errorMessage }) =>
  state.merge({
    lastWeekAppointments: null,
    getLastWeekAppointmentsIsLoading: false,
    getLastWeekAppointmentsErrorMessage: errorMessage
  })

// getLastWeekAppointments
export const getSummaryCustomerYesterdayLoading = state =>
  state.merge({
    getSummaryCustomerYesterdayIsLoading: true,
    getSummaryCustomerYesterdayErrorMessage: ''
  })

export const getSummaryCustomerYesterdaySuccess = (state, { summaryCustomerYesterday }) =>
  state.merge({
    summaryCustomerYesterday,
    getSummaryCustomerYesterdayIsLoading: false,
    getSummaryCustomerYesterdayErrorMessage: null
  })

export const getSummaryCustomerYesterdayFailure = (state, { errorMessage }) =>
  state.merge({
    summaryCustomerYesterday: null,
    getSummaryCustomerYesterdayIsLoading: false,
    getSummaryCustomerYesterdayErrorMessage: errorMessage
  })

// getLastWeekAppointments
export const getSummaryCustomerLastWeekLoading = state =>
  state.merge({
    getSummaryCustomerLastWeekIsLoading: true,
    getSummaryCustomerLastWeekErrorMessage: ''
  })

export const getSummaryCustomerLastWeekSuccess = (state, { summaryCustomerLastWeek }) =>
  state.merge({
    summaryCustomerLastWeek,
    getSummaryCustomerLastWeekIsLoading: false,
    getSummaryCustomerLastWeekErrorMessage: null
  })

export const getSummaryCustomerLastWeekFailure = (state, { errorMessage }) =>
  state.merge({
    summaryCustomerLastWeek: null,
    getSummaryCustomerLastWeekIsLoading: false,
    getSummaryCustomerLastWeekErrorMessage: errorMessage
  })

export const reducer = createReducer(INITIAL_STATE, {
  [ReportsTypes.GET_SUMMARY_LOADING]: getSummaryLoading,
  [ReportsTypes.GET_SUMMARY_SUCCESS]: getSummarySuccess,
  [ReportsTypes.GET_SUMMARY_FAILURE]: getSummaryFailure,
  [ReportsTypes.GET_SUMMARY_YESTERDAY_LOADING]: getSummaryYesterdayLoading,
  [ReportsTypes.GET_SUMMARY_YESTERDAY_SUCCESS]: getSummaryYesterdaySuccess,
  [ReportsTypes.GET_SUMMARY_YESTERDAY_FAILURE]: getSummaryYesterdayFailure,
  [ReportsTypes.GET_TODAY_APPOINTMENTS_SUMMARY_LOADING]: getTodayAppointmentsSummaryLoading,
  [ReportsTypes.GET_TODAY_APPOINTMENTS_SUMMARY_SUCCESS]: getTodayAppointmentsSummarySuccess,
  [ReportsTypes.GET_TODAY_APPOINTMENTS_SUMMARY_FAILURE]: getTodayAppointmentsSummaryFailure,
  [ReportsTypes.GET_TODAY_APPOINTMENTS_LOADING]: getTodayAppointmentsLoading,
  [ReportsTypes.GET_TODAY_APPOINTMENTS_SUCCESS]: getTodayAppointmentsSuccess,
  [ReportsTypes.GET_TODAY_APPOINTMENTS_FAILURE]: getTodayAppointmentsFailure,
  [ReportsTypes.GET_YESTERDAY_APPOINTMENTS_LOADING]: getYesterdayAppointmentsLoading,
  [ReportsTypes.GET_YESTERDAY_APPOINTMENTS_SUCCESS]: getYesterdayAppointmentsSuccess,
  [ReportsTypes.GET_YESTERDAY_APPOINTMENTS_FAILURE]: getYesterdayAppointmentsFailure,
  [ReportsTypes.GET_LAST_WEEK_APPOINTMENTS_LOADING]: getLastWeekAppointmentsLoading,
  [ReportsTypes.GET_LAST_WEEK_APPOINTMENTS_SUCCESS]: getLastWeekAppointmentsSuccess,
  [ReportsTypes.GET_LAST_WEEK_APPOINTMENTS_FAILURE]: getLastWeekAppointmentsFailure,
  [ReportsTypes.GET_SUMMARY_CUSTOMER_YESTERDAY_LOADING]: getSummaryCustomerYesterdayLoading,
  [ReportsTypes.GET_SUMMARY_CUSTOMER_YESTERDAY_SUCCESS]: getSummaryCustomerYesterdaySuccess,
  [ReportsTypes.GET_SUMMARY_CUSTOMER_YESTERDAY_FAILURE]: getSummaryCustomerYesterdayFailure,
  [ReportsTypes.GET_SUMMARY_CUSTOMER_LAST_WEEK_LOADING]: getSummaryCustomerLastWeekLoading,
  [ReportsTypes.GET_SUMMARY_CUSTOMER_LAST_WEEK_SUCCESS]: getSummaryCustomerLastWeekSuccess,
  [ReportsTypes.GET_SUMMARY_CUSTOMER_LAST_WEEK_FAILURE]: getSummaryCustomerLastWeekFailure
})
