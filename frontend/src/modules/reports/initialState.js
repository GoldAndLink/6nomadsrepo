import { Map } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = Map({
  summary: null,
  summaryYesterday: null,
  todayAppointmentsSummary: null,
  todayAppointments: null,
  yesterdayAppointments: null,
  lastWeekAppointments: null,
  summaryCustomerYesterday: null,
  summaryCustomerLastWeek: null
})
