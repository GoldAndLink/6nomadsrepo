import {
  call,
  put,
  takeLatest,
  fork,
  select
} from 'redux-saga/effects'

import axios from '../../utils/axios'
import ReportsActions, {
  ReportsTypes
} from './actions'
import AppointmentActions from '../appointments/actions'

import {
  token as getToken
} from '../users/selectors'
import {
  getWarehouse
} from '../app/selectors'
import {
  getAllBuildings,
  getAllAreas
} from '../entities/selectors'

const baseUrl = '/reports'

/* Sagas */

function * getSummary () {
  yield put(ReportsActions.getSummaryLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/summary`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getSummarySuccess(data))
  } catch (e) {
    yield put(ReportsActions.getSummaryFailure(e.message || e))
  }
}

function * getSummaryYesterday () {
  yield put(ReportsActions.getSummaryYesterdayLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/summaryYesterday`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getSummaryYesterdaySuccess(data))
  } catch (e) {
    yield put(ReportsActions.getSummaryYesterdayFailure(e.message || e))
  }
}

function * getTodayAppointmentsSummary () {
  yield put(ReportsActions.getTodayAppointmentsSummaryLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/todayAppointmentsSummary`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getTodayAppointmentsSummarySuccess(data))
  } catch (e) {
    yield put(ReportsActions.getTodayAppointmentsSummaryFailure(e.message || e))
  }
}

function * getTodayAppointments () {
  yield put(ReportsActions.getTodayAppointmentsLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/todayAppointments`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getTodayAppointmentsSuccess(data))
  } catch (e) {
    yield put(ReportsActions.getTodayAppointmentsFailure(e.message || e))
  }
}

function * getYesterdayAppointments () {
  yield put(ReportsActions.getYesterdayAppointmentsLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/yesterdayAppointments`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getYesterdayAppointmentsSuccess(data))
  } catch (e) {
    yield put(ReportsActions.getYesterdayAppointmentsFailure(e.message || e))
  }
}

function * getLastWeekAppointments () {
  yield put(ReportsActions.getLastWeekAppointmentsLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/lastWeekAppointments`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getLastWeekAppointmentsSuccess(data))
  } catch (e) {
    yield put(ReportsActions.getLastWeekAppointmentsFailure(e.message || e))
  }
}

function * getSummaryCustomerYesterday () {
  yield put(ReportsActions.getSummaryCustomerYesterdayLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/summaryCustomerYesterday`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getSummaryCustomerYesterdaySuccess(data))
  } catch (e) {
    yield put(ReportsActions.getSummaryCustomerYesterdayFailure(e.message || e))
  }
}

function * getSummaryCustomerLastWeek () {
  yield put(ReportsActions.getSummaryCustomerLastWeekLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/summaryCustomerLastWeek`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(ReportsActions.getSummaryCustomerLastWeekSuccess(data))
  } catch (e) {
    yield put(ReportsActions.getSummaryCustomerLastWeekFailure(e.message || e))
  }
}

function * updateWarnings () {
  try {
    const token = yield select(state => getToken(state))
    // let's update the bubbles count with the new info
    const warehouse = yield select(state => getWarehouse(state))
    const areas = yield select(state => getAllAreas(state))
    const buildings = yield select(state => getAllBuildings(state))

    const building = buildings.find(a => a.get('id') === warehouse)
    const area = areas.find(a => a.get('buildingId') === building.get('id'))
    const doors = area.get('doors', [])

    const r = yield call(axios.get, `/reports/warnings?timezone=${building.get('timezone')}&doors=${doors.toJS()}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentActions.mergeAppointmentCounts({
      appointmentYesterdayCount: r.data.appointmentYesterdayCount,
      appointmentTodayCount: r.data.appointmentTodayCount,
      appointmentNext1Count: r.data.appointmentNext1Count,
      appointmentNext2Count: r.data.appointmentNext2Count,
      appointmentNext3Count: r.data.appointmentNext3Count
    }))
  } catch (e) {
    console.log('ERROR:', e)
  }
}

/* Watchers */

function * updateWarningsWatcher () {
  yield takeLatest(ReportsTypes.UPDATE_WARNINGS, updateWarnings)
}

function * getSummaryWatcher () {
  yield takeLatest(ReportsTypes.GET_SUMMARY, getSummary)
}

function * getSummaryYesterdayWatcher () {
  yield takeLatest(ReportsTypes.GET_SUMMARY_YESTERDAY, getSummaryYesterday)
}

function * getTodayAppointmentsSummaryWatcher () {
  yield takeLatest(ReportsTypes.GET_SUMMARY_YESTERDAY, getTodayAppointmentsSummary)
}

function * getTodayAppointmentsWatcher () {
  yield takeLatest(ReportsTypes.GET_TODAY_APPOINTMENTS, getTodayAppointments)
}

function * getYesterdayAppointmentsWatcher () {
  yield takeLatest(ReportsTypes.GET_YESTERDAY_APPOINTMENTS, getYesterdayAppointments)
}

function * getLastWeekAppointmentsWatcher () {
  yield takeLatest(ReportsTypes.GET_LAST_WEEK_APPOINTMENTS, getLastWeekAppointments)
}

function * getSummaryCustomerYesterdayWatcher () {
  yield takeLatest(ReportsTypes.GET_SUMMARY_CUSTOMER_YESTERDAY, getSummaryCustomerYesterday)
}

function * getSummaryCustomerLastWeekWatcher () {
  yield takeLatest(ReportsTypes.GET_SUMMARY_CUSTOMER_LAST_WEEK, getSummaryCustomerLastWeek)
}

export default function * root () {
  yield fork(updateWarningsWatcher)
  yield fork(getSummaryWatcher)
  yield fork(getSummaryYesterdayWatcher)
  yield fork(getTodayAppointmentsSummaryWatcher)
  yield fork(getTodayAppointmentsWatcher)
  yield fork(getYesterdayAppointmentsWatcher)
  yield fork(getLastWeekAppointmentsWatcher)
  yield fork(getSummaryCustomerYesterdayWatcher)
  yield fork(getSummaryCustomerLastWeekWatcher)
}
