import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getSummary
  getSummary: null,
  getSummaryLoading: null,
  getSummarySuccess: ['summary'],
  getSummaryFailure: ['errorMessage'],

  // getSummaryYesterday
  getSummaryYesterday: null,
  getSummaryYesterdayLoading: null,
  getSummaryYesterdaySuccess: ['summaryYesterday'],
  getSummaryYesterdayFailure: ['errorMessage'],

  // getTodayAppointmentsSummary
  getTodayAppointmentsSummary: null,
  getTodayAppointmentsSummaryLoading: null,
  getTodayAppointmentsSummarySuccess: ['todayAppointmentsSummary'],
  getTodayAppointmentsSummaryFailure: ['errorMessage'],

  // getTodayAppointments
  getTodayAppointments: null,
  getTodayAppointmentsLoading: null,
  getTodayAppointmentsSuccess: ['todayAppointments'],
  getTodayAppointmentsFailure: ['errorMessage'],

  // getYesterdayAppointments
  getYesterdayAppointments: null,
  getYesterdayAppointmentsLoading: null,
  getYesterdayAppointmentsSuccess: ['yesterdayAppointments'],
  getYesterdayAppointmentsFailure: ['errorMessage'],

  // getLastWeekAppointments
  getLastWeekAppointments: null,
  getLastWeekAppointmentsLoading: null,
  getLastWeekAppointmentsSuccess: ['lastWeekAppointments'],
  getLastWeekAppointmentsFailure: ['errorMessage'],

  // getSummaryCustomerYesterday
  getSummaryCustomerYesterday: null,
  getSummaryCustomerYesterdayLoading: null,
  getSummaryCustomerYesterdaySuccess: ['summaryCustomerYesterday'],
  getSummaryCustomerYesterdayFailure: ['errorMessage'],

  // getSummaryCustomerLastWeek
  getSummaryCustomerLastWeek: null,
  getSummaryCustomerLastWeekLoading: null,
  getSummaryCustomerLastWeekSuccess: ['summaryCustomerLastWeek'],
  getSummaryCustomerLastWeekFailure: ['errorMessage'],

  // warning bubbles
  updateWarnings: null
})

export const ReportsTypes = Types
export default Creators
