export const getSummary = state => {
  return state.reports.get('summary')
}

export const getSummaryYesterday = state => {
  return state.reports.get('summaryYesterday')
}

export const getTodayAppointmentsSummary = state => {
  return state.reports.get('todayAppointmentsSummary')
}

export const getTodayAppointments = state => {
  return state.reports.get('todayAppointments')
}

export const getYesterdayAppointments = state => {
  return state.reports.get('yesterdayAppointments')
}

export const getLastWeekAppointments = state => {
  return state.reports.get('lastWeekAppointments')
}

export const getSummaryCustomerYesterday = state => {
  return state.reports.get('summaryCustomerYesterday')
}

export const getSummaryCustomerLastWeek = state => {
  return state.reports.get('summaryCustomerLastWeek')
}
