import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  setSchedules: ['schedules'],
  setDoorSchedules: ['doorSchedules']
})

export const SchedulesTypes = Types
export default Creators
