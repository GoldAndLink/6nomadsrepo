import moment from 'moment'
import { createSelector } from 'reselect'
import { getDoorsForBuilding } from '../buildings/selectors'
import { getStartDate } from '../app/selectors'

export const getSchedules = state => state.schedules.get('schedules')
export const getDoorSchedules = state => state.schedules.get('doorSchedules')

export const getScheduleForDoors = createSelector(
  getSchedules,
  getDoorSchedules,
  getDoorsForBuilding,
  getStartDate,
  (schedules, doorSchedules, doors, date) => {
    if (!schedules || !schedules.size ||
      !doorSchedules || !doorSchedules.size ||
      !doors || !doors.size) {
      return {}
    }

    const schedule = schedules.find(s => (
      date.isAfter(moment(s.get('startDate'))) &&
      date.isBefore(moment(s.get('endDate')))
    ))

    if (!schedule) {
      return {}
    }

    const dayOfWeek = date.day()

    return doors.reduce((doorsObj, door) => {
      const doorSchedule = doorSchedules.find(s => (
        s.get('active') &&
        s.get('doorId') === door.get('id') &&
        s.get('scheduleId') === schedule.get('id') &&
        s.get('dayOfWeek') === dayOfWeek
      ))

      if (!doorSchedule) {
        return doorsObj
      }

      doorsObj[door.get('id')] = {
        startHour: moment(doorSchedule.get('open'), 'HH:mm:ss').hour(),
        endHour: moment(doorSchedule.get('close'), 'HH:mm:ss').hour()
      }

      return doorsObj
    }, {})
  }
)
