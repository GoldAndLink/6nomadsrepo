import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { SchedulesTypes } from './actions'

export const setSchedules = (state, { schedules }) =>
  state.merge({
    schedules
  })

export const setDoorSchedules = (state, { doorSchedules }) =>
  state.merge({
    doorSchedules
  })

export const reducer = createReducer(INITIAL_STATE, {
  [SchedulesTypes.SET_SCHEDULES]: setSchedules,
  [SchedulesTypes.SET_DOOR_SCHEDULES]: setDoorSchedules
})
