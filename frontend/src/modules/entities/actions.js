import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  mergeAppointments: ['entities'],
  mergeOrders: ['entities']
})

export const EntitiesTypes = Types
export default Creators
