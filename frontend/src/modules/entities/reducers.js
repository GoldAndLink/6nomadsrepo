import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { EntitiesTypes } from './actions'
import { SitesTypes } from '../sites/actions'
import { DriversTypes } from '../drivers/actions'
import { CarriersTypes } from '../carriers/actions'

export const getAllSitesSuccess = (state, { entities }) => {
  const {
    sites,
    buildings,
    areas,
    doors
  } = entities

  return state.merge({
    sites,
    buildings,
    areas,
    doors
  })
}

export const getAllDriversSuccess = (state, { entities }) => {
  const {
    drivers
  } = entities

  return state.mergeIn(['drivers'], drivers)
}

export const getAllCarriersSuccess = (state, { entities }) => {
  const {
    carriers
  } = entities

  return state.mergeIn(['carriers'], carriers)
}

export const updateCarrierWithSocketCarrier = (state, { payload: { carrier } }) =>
  state.mergeIn(['carriers', carrier && carrier.id.toString()], carrier)

export const updateDriverWithSocketDriver = (state, { payload: { driver } }) =>
  state.mergeIn(['drivers', driver && driver.id.toString()], driver)

export const mergeAppointments = (state, { entities }) => {
  const {
    appointments
  } = entities

  return state.set('appointments', state.get('appointments').merge({ ...appointments }))
}

export const mergeOrders = (state, { entities }) => {
  const {
    orders
  } = entities

  return state.set('orders', state.get('orders').merge(orders))
}

export const reducer = createReducer(INITIAL_STATE, {
  [SitesTypes.GET_ALL_SITES_SUCCESS]: getAllSitesSuccess,
  [DriversTypes.GET_ALL_DRIVERS_SUCCESS]: getAllDriversSuccess,
  [CarriersTypes.GET_ALL_CARRIERS_SUCCESS]: getAllCarriersSuccess,
  [CarriersTypes.UPDATE_CARRIER_WITH_SOCKET_CARRIER]: updateCarrierWithSocketCarrier,
  [DriversTypes.UPDATE_DRIVER_WITH_SOCKET_DRIVER]: updateDriverWithSocketDriver,
  [EntitiesTypes.MERGE_APPOINTMENTS]: mergeAppointments,
  [EntitiesTypes.MERGE_ORDERS]: mergeOrders
})
