import { Map } from 'immutable'

export const INITIAL_STATE = Map({
  appointments: Map(),
  sites: Map(),
  buildings: Map(),
  areas: Map(),
  doors: Map(),
  orders: Map()
})
