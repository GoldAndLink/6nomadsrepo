import { getDoorById } from '../doors/selectors'
import { getDriverById } from '../drivers/selectors'
import { getCarrierById } from '../carriers/selectors'

export const getAllSites = state => {
  return state.entities.get('sites')
}

export const getAllBuildings = state => {
  return state.entities.get('buildings')
}

export const getAllAreas = state => {
  return state.entities.get('areas')
}

export const getAllDoors = state => {
  return state.entities.get('doors')
}

export const getAppointments = state => {
  if (state.entities && state.entities.get('appointments')) {
    return state.entities.get('appointments')
      .map(appt => appt.set('door', getDoorById(state, appt.get('doorId')))
        .set('driver', getDriverById(state, appt.get('driverId')))
        .set('carrier', getCarrierById(state, appt.get('carrierId'))))
  }
  return state.entities ? state.entities.get('appointments') : new Map()
}

export const getAllOrders = state => {
  return state.entities.get('orders') && state.entities.get('orders')
}
