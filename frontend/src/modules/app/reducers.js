import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { AppTypes } from './actions'
import { UsersTypes } from '../users/actions'

export const setSite = (state, { HomeView: { site } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      site
    }
  })

export const setSiteFailure = (state, { HomeView: { errorMessage } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      errorMessage
    }
  })

export const setWarehouse = (state, { HomeView: { warehouse } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      warehouse
    }
  })

export const setWarehouseFailure = (state, { HomeView: { errorMessage } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      errorMessage
    }
  })

export const setStartDate = (state, { HomeView: { startDate } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      startDate
    }
  })
export const setEndDate = (state, { HomeView: { endDate } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      endDate
    }
  })
export const setStartTime = (state, { HomeView: { startTime } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      startTime
    }
  })
export const setEndTime = (state, { HomeView: { endTime } }) =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      endTime
    }
  })
export const loginSuccess = state =>
  state.merge({
    HomeView: {
      ...state.get('HomeView').toJS(),
      site: null,
      warehouse: null
    }
  })

export const setFocusAppointment = (state, { appointment }) =>
  state.merge({
    focusAppointment: appointment
  })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AppTypes.SET_SITE]: setSite,
  [AppTypes.SET_SITE_FAILURE]: setSiteFailure,
  [AppTypes.SET_WAREHOUSE]: setWarehouse,
  [AppTypes.SET_WAREHOUSE_FAILURE]: setWarehouseFailure,
  [AppTypes.SET_START_DATE]: setStartDate,
  [AppTypes.SET_END_DATE]: setEndDate,
  [AppTypes.SET_START_TIME]: setStartTime,
  [AppTypes.SET_END_TIME]: setEndTime,
  [AppTypes.SET_FOCUS_APPOINTMENT]: setFocusAppointment,
  [UsersTypes.LOGIN_SUCCESS]: loginSuccess
})
