/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import moment from 'moment'
import { getBuildingById } from '../buildings/selectors'

export const getSite = state => {
  return state.app.get('HomeView').get('site')
}

export const getWarehouse = state => {
  return state.app.get('HomeView').get('warehouse')
}

export const getWarehouseTimezone = state => {
  const buildingId = state.app.get('HomeView').get('warehouse')
  const selectedBuilding = getBuildingById(state, buildingId)
  return (selectedBuilding && selectedBuilding.get('timezone')) || 'UTC'
}

export const getStartDate = state => {
  let startDate = state.app.get('HomeView').get('startDate')
  if (!moment.isMoment(startDate)) startDate = moment(startDate)
  return startDate
}

export const getEndDate = state => {
  let endDate = state.app.get('HomeView').get('endDate')
  if (!moment.isMoment(endDate)) endDate = moment(endDate)
  return endDate
}

export const getStartTime = state => {
  let startTime = state.app.get('HomeView').get('startTime')
  if (!moment.isMoment(startTime)) startTime = moment(startTime)
  return startTime
}

export const getEndTime = state => {
  let endTime = state.app.get('HomeView').get('endTime')
  if (!moment.isMoment(endTime)) endTime = moment(endTime)
  return endTime
}

export const getFocusAppointment = state =>
  state.app.get('focusAppointment')
