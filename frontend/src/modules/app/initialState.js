import { Map } from 'immutable'
import moment from 'moment'

/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = Map({
  HomeView: Map({
    startDate: moment(),
    endDate: moment().add(7, 'day'),
    startTime: moment().startOf('day'),
    endTime: moment().endOf('day')
  })
})
