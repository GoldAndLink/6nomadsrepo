import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // The appointmentOrders was successfully fetched
  setSite: ['HomeView'],
  setSiteFailure: ['HomeView']['errorMessage'],
  setWarehouse: ['HomeView'],
  setWarehouseFailure: ['HomeView']['errorMessage'],
  setStartDate: ['HomeView'],
  setEndDate: ['HomeView'],
  setStartTime: ['HomeView'],
  setEndTime: ['HomeView'],
  setFocusAppointment: ['appointment']
})

export const AppTypes = Types
export default Creators
