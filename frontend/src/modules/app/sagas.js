import { put, takeLatest, fork } from 'redux-saga/effects'
import AppActions, { AppTypes } from './actions'
import SitesActions from '../sites/actions'
import ReportsActions from '../reports/actions'

// Sagas
function * setSite ({ HomeView: { id } }) {
  try {
    yield put(SitesActions.getBuildingsForSite({ id }))
  } catch (e) {
    yield put(AppActions.setSiteFailure(e.message || e))
  }
}

function * setWarehouse ({ HomeView: { warehouse } }) {
  try {
    // let's update the bubbles count with the new info
    yield put(ReportsActions.updateWarnings())
  } catch (e) {
    yield put(AppActions.setSiteFailure(e.message || e))
  }
}

// Watchers
function * setWarehouseWatcher () {
  yield takeLatest(AppTypes.SET_WAREHOUSE, setWarehouse)
}

function * setSiteWatcher () {
  yield takeLatest(AppTypes.SET_SITE, setSite)
}

export default function * root () {
  yield fork(setWarehouseWatcher)
  yield fork(setSiteWatcher)
}
