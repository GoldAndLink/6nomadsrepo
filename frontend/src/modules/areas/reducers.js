import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { AreasTypes } from './actions'
import { SitesTypes } from '../sites/actions'

// getAreas
export const getAreasLoading = state =>
  state.merge({
    getAreasIsLoading: true,
    getAreasErrorMessage: ''
  })

export const getAreasSuccess = (state, { areas }) =>
  state.merge({
    areas,
    getAreasIsLoading: false,
    getAreasErrorMessage: null
  })

export const getAreasFailure = (state, { errorMessage }) =>
  state.merge({
    areas: null,
    getAreasIsLoading: false,
    getAreasErrorMessage: errorMessage
  })

export const getAllSitesSuccess = (state, { entities: { areas } }) =>
  state.merge({ areas })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AreasTypes.GET_AREAS_LOADING]: getAreasLoading,
  [AreasTypes.GET_AREAS_SUCCESS]: getAreasSuccess,
  [AreasTypes.GET_AREAS_FAILURE]: getAreasFailure,
  [SitesTypes.GET_ALL_SITES_SUCCESS]: getAllSitesSuccess
})
