import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getAreas
  getAreas: ['payload'],
  // The operation has started and is loading
  getAreasLoading: null,
  // The areas was successfully fetched
  getAreasSuccess: ['areas'],
  // An error occurred
  getAreasFailure: ['errorMessage']
})

export const AreasTypes = Types
export default Creators
