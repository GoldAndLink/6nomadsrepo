import { call, put, takeLatest, fork, select } from 'redux-saga/effects'

import axios from '../../utils/axios'
import AreasActions, { AreasTypes } from './actions'
import { token as getToken } from '../users/selectors'

// Sagas
function * getAreas ({ payload }) {
  yield put(AreasActions.getAreasLoading())
  try {
    const token = yield select(state => getToken(state))
    const { buildingId } = payload
    const url = buildingId ? `/areas?buildingId=${buildingId}` : `/areas`
    const { data } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AreasActions.getAreasSuccess(data))
  } catch (e) {
    yield put(AreasActions.getAreasFailure(e.message || e))
  }
}

// Watchers
function * getAreasWatcher () {
  yield takeLatest(AreasTypes.GET_AREAS, getAreas)
}

export default function * root () {
  yield fork(getAreasWatcher)
}
