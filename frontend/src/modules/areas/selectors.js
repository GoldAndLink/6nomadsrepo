/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import { getBuildingById } from '../buildings/selectors'

export const getAreas = state => {
  return state.areas ? state.areas.get('areas') : new Map()
}

export const getAreaById = (state, id) => {
  const area = id && state.entities && state.entities.get('areas') ? state.entities.get('areas').get(id.toString()) : null
  if (area) return area.set('building', getBuildingById(state, area.get('buildingId')))
  return area
}
