import { combineReducers } from 'redux'
import { all, fork } from 'redux-saga/effects'

import { reducer as appReducer } from './app/reducers'
import appSagas from './app/sagas'

import { reducer as usersReducer } from './users/reducers'
import usersSagas from './users/sagas'

import { reducer as ordersReducer } from './orders/reducers'
import ordersSagas from './orders/sagas'

import { reducer as appointmentsReducer } from './appointments/reducers'
import appointmentsSagas from './appointments/sagas'

import { reducer as buildingsReducer } from './buildings/reducers'
import buildingsSagas from './buildings/sagas'

import { reducer as carriersReducer } from './carriers/reducers'
import carriersSagas from './carriers/sagas'

import { reducer as customersReducer } from './customers/reducers'
import customersSagas from './customers/sagas'

import { reducer as driversReducer } from './drivers/reducers'
import driversSagas from './drivers/sagas'

import { reducer as locationsReducer } from './locations/reducers'
import locationsSagas from './locations/sagas'

import { reducer as sitesReducer } from './sites/reducers'
import sitesSagas from './sites/sagas'

import { reducer as areasReducer } from './areas/reducers'
import areasSagas from './areas/sagas'

import { reducer as doorsReducer } from './doors/reducers'
import doorsSagas from './doors/sagas'

import { reducer as appointmentOrdersReducer } from './appointmentOrders/reducers'
import appointmentOrdersSagas from './appointmentOrders/sagas'

import { reducer as orderItemsReducer } from './orderItems/reducers'
import orderItemsSagas from './orderItems/sagas'

import { reducer as itemsReducer } from './items/reducers'
import itemsSagas from './items/sagas'

import { reducer as emailsReducer } from './emails/reducers'
import emailsSagas from './emails/sagas'

import { reducer as carrierRequestsReducer } from './carrierRequests/reducers'
import carrierRequestsSagas from './carrierRequests/sagas'

import { reducer as entitiesReducer } from './entities/reducers'

import { reducer as feedsReducer } from './feeds/reducers'
import feedsSagas from './feeds/sagas'

import { reducer as smsReducer } from './sms/reducers'
import smsSagas from './sms/sagas'

import { reducer as reportsReducer } from './reports/reducers'
import reportsSagas from './reports/sagas'

import { reducer as inventoryItemsReducer } from './inventoryItems/reducers'
import inventoryItemsSagas from './inventoryItems/sagas'

import { reducer as schedulesReducer } from './schedules/reducers'

/* Reducers */
export const rootReducer = combineReducers({
  /**
   * Register your reducers here.
   * @see https://redux.js.org/api-reference/combinereducers
   */
  app: appReducer,
  users: usersReducer,
  orders: ordersReducer,
  appointments: appointmentsReducer,
  buildings: buildingsReducer,
  carriers: carriersReducer,
  customers: customersReducer,
  drivers: driversReducer,
  locations: locationsReducer,
  sites: sitesReducer,
  areas: areasReducer,
  doors: doorsReducer,
  appointmentOrders: appointmentOrdersReducer,
  orderItems: orderItemsReducer,
  items: itemsReducer,
  emails: emailsReducer,
  entities: entitiesReducer,
  carrierRequests: carrierRequestsReducer,
  feeds: feedsReducer,
  sms: smsReducer,
  reports: reportsReducer,
  inventoryItems: inventoryItemsReducer,
  schedules: schedulesReducer
})

/* Sagas */
export function * rootSaga () {
  yield all([
    fork(appSagas),
    fork(usersSagas),
    fork(ordersSagas),
    fork(appointmentsSagas),
    fork(buildingsSagas),
    fork(carriersSagas),
    fork(customersSagas),
    fork(driversSagas),
    fork(locationsSagas),
    fork(sitesSagas),
    fork(areasSagas),
    fork(doorsSagas),
    fork(appointmentOrdersSagas),
    fork(orderItemsSagas),
    fork(itemsSagas),
    fork(emailsSagas),
    fork(carrierRequestsSagas),
    fork(feedsSagas),
    fork(smsSagas),
    fork(reportsSagas),
    fork(inventoryItemsSagas)
  ])
}
