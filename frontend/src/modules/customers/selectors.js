/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import { List } from 'immutable'

export const getAllCustomers = state => {
  return state.customers ? state.customers.get('customers') : List([])
}
