import { call, put, takeLatest, fork, select } from 'redux-saga/effects'
import moment from 'moment'

import axios from '../../utils/axios'
import InventoryItemsActions, { InventoryItemsTypes } from './actions'
import { token as getToken } from '../users/selectors'
import { getEditingAppointment } from '../appointments/selectors'
import { getItems } from '../items/selectors'
import { getDoorById } from '../doors/selectors'
import { getAreaById } from '../areas/selectors'
import { getBuildingById, getDoorsForBuilding } from '../buildings/selectors'

const baseUrl = '/inventoryItems'

// Sagas
function * getInventoryItems ({ payload }) {
  yield put(InventoryItemsActions.getInventoryItemsLoading())
  try {
    const token = yield select(state => getToken(state))
    const { data } = yield call(axios.get, baseUrl, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(InventoryItemsActions.getInventoryItemsSuccess(data))
  } catch (e) {
    yield put(InventoryItemsActions.getInventoryItemsFailure(e.message || e))
  }
}

function * getInventoryItemsProjection ({ payload }) {
  yield put(InventoryItemsActions.getInventoryItemsProjectionLoading()) // findsecrets-ignore-line
  try {
    const { skus, doorId } = payload
    let url = `${baseUrl}/projection`

    const door = yield select(state => getDoorById(state, doorId))
    const area = yield select(state => getAreaById(state, door.get('areaId')))
    const building = yield select(state => getBuildingById(state, area.get('buildingId')))
    const location = building.get('location')

    url += `?locationCode=${location.get('code')}`

    if (skus && skus.length > 0) {
      skus.forEach((sku, index) => {
        url += `&sku=${sku}`
      })
    }

    const token = yield select(state => getToken(state))
    const { data } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(InventoryItemsActions.getInventoryItemsProjectionSuccess(data))
  } catch (e) {
    yield put(InventoryItemsActions.getInventoryItemsProjectionFailure(e.message || e))
  }
}

function * getInventoryItemsProjections ({ payload }) {
  yield put(InventoryItemsActions.getInventoryItemsLoading())
  try {
    const token = yield select(state => getToken(state))
    const { buildingId } = payload
    const url = buildingId ? `/areas?buildingId=${buildingId}` : `/areas`
    const { data } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(InventoryItemsActions.getInventoryItemsSuccess(data))
  } catch (e) {
    yield put(InventoryItemsActions.getInventoryItemsFailure(e.message || e))
  }
}

async function getAppointmentItems (appointment, token) {
  const orders = appointment.get('orders') || []
  const ordersItems = await Promise.all(
    orders.map(async order => {
      const result = await axios.get(`/orderItems?orderId=${order.get('id')}`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      return result.data
    })
  )

  return ordersItems.reduce((acc, val) => acc.concat(val), [])
}

function getAvailableDoors (appointments, doors, time, duration) {
  const end = time.clone().add(duration, 'minutes')

  return doors
    .filter(door => {
      const overlapingAppointment = appointments.some(appointment => {
        if (appointment.doorId === door.get('id')) {
          const apptStart = moment(appointment.date)
          const apptEnd = apptStart.clone().add(appointment.duration, 'minutes')

          // Is overlapping
          return end.isAfter(apptStart) && time.isBefore(apptEnd)
        } else {
          return false
        }
      })

      return !overlapingAppointment
    })
    .map(door => door.get('id'))
    .toJSON()
}

function * suggestAppointmentTime () {
  yield put(InventoryItemsActions.suggestAppointmentTimeLoading())
  try {
    const token = yield select(state => getToken(state))
    const appointment = yield select(state => getEditingAppointment(state))

    const items = yield select(state => getItems(state))

    // Get all the items for appointment orders
    const ordersItems = yield call(getAppointmentItems, appointment, token)

    // Calculate total quantity of each item
    const itemQuantities = ordersItems
      .reduce((quantities, orderItem) => {
        const item = items.find(i => i.get('id') === orderItem.itemId)
        const sku = item.get('sku')
        if (!quantities[sku]) {
          quantities[sku] = orderItem.quantity || 0
        } else {
          quantities[sku] += orderItem.quantity || 0
        }

        return quantities
      }, {})

    const door = yield select(state => getDoorById(state, appointment.get('doorId')))
    const area = yield select(state => getAreaById(state, door.get('areaId')))
    const building = yield select(state => getBuildingById(state, area.get('buildingId')))
    const location = building.get('location')

    const doors = yield select(state => getDoorsForBuilding(state))

    // Get inventory projection for all the items
    let url = `${baseUrl}/projection?locationCode=${location.get('code')}`

    Object.keys(itemQuantities).forEach((sku, index) => {
      url += `&sku=${sku}`
    })

    const { data: projectionData } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    // Start looking for a suitable time from appointment
    // currenttime until 3 days in the future
    const time = moment(appointment.get('date')).startOf('hour').add(1, 'hour')
    const endTime = moment(appointment.get('date')).add(3, 'days')

    // Get all appointments in this time period to check for overlaping
    url = '/appointments?'
    url += doors.map(d => `doorId=${d.get('id')}`).join('&')
    url += `&timeFrom=${time.format('HH:mm')}`
    url += `&timeTo=${endTime.format('HH:mm')}`
    url += `&dateFrom=${time.format('MM/DD/YYYY')}`
    url += `&dateTo=${endTime.format('MM/DD/YYYY')}`

    const { data: appointments } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    // Build an object that specifies for each item what is
    // the total quantity at each time
    const inventoryProjection = Object.keys(projectionData).reduce((obj, sku) => {
      obj[sku] = []
      const itemData = projectionData[sku]

      let acc = 0

      obj[sku].push({ quantity: 0 })

      const deltas = itemData.deltas || []
      deltas.forEach(delta => {
        // Don't take into account the appointment we are about to move
        if (delta.appointmentId !== appointment.get('id')) {
          acc += delta.quantity
          obj[sku].push({
            quantity: acc,
            time: moment(delta.time)
          })
        }
      })

      return obj
    }, {})

    // Start checking for possible available times
    const availableTimes = []

    while (time.isBefore(endTime)) {
      const missingItem = Object.keys(itemQuantities).some(sku => {
        // Shift inventory projection values if they are older than the time we are checking
        while (inventoryProjection[sku].length > 1 &&
          inventoryProjection[sku][1].time.isBefore(time)) {
          inventoryProjection[sku].shift()
        }

        return itemQuantities[sku] > inventoryProjection[sku][0].quantity
      })

      if (!missingItem) {
        const availableDoors = getAvailableDoors(appointments, doors, time, 60)

        if (availableDoors.length > 0) {
          availableTimes.push({
            time: time.clone(),
            doors: availableDoors
          })
        }
      }

      time.add(1, 'hour')
    }

    yield put(InventoryItemsActions.suggestAppointmentTimeSuccess(availableTimes))
  } catch (e) {
    yield put(InventoryItemsActions.suggestAppointmentTimeFailure(e.message || e))
  }
}

// Watchers
function * getInventoryItemsWatcher () {
  yield takeLatest(InventoryItemsTypes.GET_INVENTORY_ITEMS, getInventoryItems)
}

function * getInventoryItemsProjectionWatcher () {
  yield takeLatest(InventoryItemsTypes.GET_INVENTORY_ITEMS_PROJECTION, getInventoryItemsProjection)
}
function * getInventoryItemsProjectionsWatcher () {
  yield takeLatest(InventoryItemsTypes.GET_INVENTORY_ITEMS_PROJECTION, getInventoryItemsProjections)
}

function * suggestAppointmentTimeWatcher () {
  yield takeLatest(InventoryItemsTypes.SUGGEST_APPOINTMENT_TIME, suggestAppointmentTime)
}

export default function * root () {
  yield fork(getInventoryItemsWatcher)
  yield fork(getInventoryItemsProjectionWatcher)
  yield fork(getInventoryItemsProjectionsWatcher)
  yield fork(suggestAppointmentTimeWatcher)
}
