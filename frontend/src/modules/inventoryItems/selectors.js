/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
export const getInventoryItems = state => {
  return state.inventoryItems ? state.inventoryItems.get('inventoryItems') : new Map()
}

export const getInventoryItemsProjection = state => {
  return state.inventoryItems ? state.inventoryItems.get('inventoryItemsProjection') : new Map()
}

export const isSuggestAppointmentLoading = state => {
  return state.inventoryItems ? state.inventoryItems.get('suggestAppointmentTimeIsLoading') : new Map()
}

export const getSuggestAppointmentTimes = state => {
  return state.inventoryItems ? state.inventoryItems.get('suggestAppointmentTimeTimes') : new Map()
}
