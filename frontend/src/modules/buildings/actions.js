import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getDoorsForBuilding
  getDoorsForBuilding: ['payload'],
  // The operation has started and is loading
  getDoorsForBuildingLoading: null,
  // The buildings was successfully fetched
  getDoorsForBuildingSuccess: ['doors'],
  // An error occurred
  getDoorsForBuildingFailure: ['errorMessage'],

  // getAreasForBuilding
  getAreasForBuilding: ['payload'],
  // The operation has started and is loading
  getAreasForBuildingLoading: null,
  // The areas was successfully fetched
  getAreasForBuildingSuccess: ['areas'],
  // An error occurred
  getAreasForBuildingFailure: ['errorMessage']
})

export const BuildingsTypes = Types
export default Creators
