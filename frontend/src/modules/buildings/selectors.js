/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import { getSiteById } from '../sites/selectors'
import { getLocationById } from '../locations/selectors'

export const getDoorsForBuilding = state => {
  return state.buildings ? state.buildings.get('doors') : new Map()
}

export const getAreasForBuilding = state => {
  return state.buildings ? state.buildings.get('areas') : new Map()
}

export const getBuildingById = (state, id) => {
  const building = id && state.entities && state.entities.get('buildings') ? state.entities.get('buildings').get(id.toString()) : null
  if (building) return building.set('site', getSiteById(state, building.get('siteId')))
  return building
}
