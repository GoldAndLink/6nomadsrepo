import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { BuildingsTypes } from './actions'
import { SitesTypes } from '../sites/actions'

// getDoorsForBuilding
export const getDoorsForBuildingLoading = state =>
  state.merge({
    getDoorsForBuildingIsLoading: true,
    getDoorsForBuildingErrorMessage: ''
  })

export const getDoorsForBuildingSuccess = (state, { doors }) =>
  state.merge({
    doors,
    getDoorsForBuildingIsLoading: false,
    getDoorsForBuildingErrorMessage: null
  })

export const getDoorsForBuildingFailure = (state, { errorMessage }) =>
  state.merge({
    doors: null,
    getDoorsForBuildingIsLoading: false,
    getDoorsForBuildingErrorMessage: errorMessage
  })

// getAreasForBuilding
export const getAreasForBuildingLoading = state =>
  state.merge({
    getAreasForBuildingIsLoading: true,
    getAreasForBuildingErrorMessage: ''
  })

export const getAreasForBuildingSuccess = (state, { areas }) =>
  state.merge({
    areas,
    getAreasForBuildingIsLoading: false,
    getAreasForBuildingErrorMessage: null
  })

export const getAreasForBuildingFailure = (state, { errorMessage }) =>
  state.merge({
    areas: null,
    getAreasForBuildingIsLoading: false,
    getAreasForBuildingErrorMessage: errorMessage
  })

export const getAllSitesSuccess = (state, { entities: { buildings } }) =>
  state.merge({ buildings })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [BuildingsTypes.GET_DOORS_FOR_BUILDING_LOADING]: getDoorsForBuildingLoading,
  [BuildingsTypes.GET_DOORS_FOR_BUILDING_SUCCESS]: getDoorsForBuildingSuccess,
  [BuildingsTypes.GET_DOORS_FOR_BUILDING_FAILURE]: getDoorsForBuildingFailure,
  [BuildingsTypes.GET_AREAS_FOR_BUILDING_LOADING]: getAreasForBuildingLoading,
  [BuildingsTypes.GET_AREAS_FOR_BUILDING_SUCCESS]: getAreasForBuildingSuccess,
  [BuildingsTypes.GET_AREAS_FOR_BUILDING_FAILURE]: getAreasForBuildingFailure,
  [SitesTypes.GET_ALL_SITES_SUCCESS]: getAllSitesSuccess
})
