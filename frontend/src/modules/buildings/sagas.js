import { call, put, takeLatest, fork, select } from 'redux-saga/effects'

import axios from '../../utils/axios'
import BuildingsActions, { BuildingsTypes } from './actions'
import { token as getToken } from '../users/selectors'

// Sagas
function * getDoorsForBuilding ({ payload }) {
  yield put(BuildingsActions.getDoorsForBuildingLoading())
  try {
    const token = yield select(state => getToken(state))
    const { id } = payload
    const { data } = yield call(axios.get, `/buildings/${id}/doors`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(BuildingsActions.getDoorsForBuildingSuccess(data))
  } catch (e) {
    yield put(BuildingsActions.getDoorsForBuildingFailure(e.message || e))
  }
}

function * getAreasForBuilding ({ payload }) {
  yield put(BuildingsActions.getAreasForBuildingLoading())
  try {
    const token = yield select(state => getToken(state))
    const { id } = payload
    const { data } = yield call(axios.get, `/buildings/${id}/areas`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(BuildingsActions.getAreasForBuildingSuccess(data))
  } catch (e) {
    yield put(BuildingsActions.getAreasForBuildingFailure(e.message || e))
  }
}

// Watchers
function * getDoorsForBuildingWatcher () {
  yield takeLatest(BuildingsTypes.GET_DOORS_FOR_BUILDING, getDoorsForBuilding)
}

function * getAreasForBuildingWatcher () {
  yield takeLatest(BuildingsTypes.GET_AREAS_FOR_BUILDING, getAreasForBuilding)
}

export default function * root () {
  yield fork(getDoorsForBuildingWatcher)
  yield fork(getAreasForBuildingWatcher)
}
