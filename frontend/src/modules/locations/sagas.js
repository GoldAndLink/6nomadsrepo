import { call, put, takeLatest, fork, select } from 'redux-saga/effects'

import axios from '../../utils/axios'
import LocationsActions, { LocationsTypes } from './actions'
import { token as getToken } from '../users/selectors'

// Sagas
function * getAllLocations ({ payload }) {
  yield put(LocationsActions.getAllLocationsLoading())

  try {
    let params = ''

    const {
      name
    } = payload

    if (name) params = params + `name=${name}`

    const token = yield select(state => getToken(state))
    const { data } = yield call(axios.get, `/locations?${params}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    yield put(LocationsActions.getAllLocationsSuccess(data))
  } catch (e) {
    yield put(LocationsActions.getAllLocationsFailure(e.message || e))
  }
}

// Watchers
function * getAllLocationsWatcher () {
  yield takeLatest(LocationsTypes.GET_ALL_LOCATIONS, getAllLocations)
}

export default function * root () {
  yield fork(getAllLocationsWatcher)
}
