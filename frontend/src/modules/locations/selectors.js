/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import { List } from 'immutable'

export const getAllLocations = state => {
  return state.locations.get('locations') || List([])
}

export const getLocationById = (state, id) => {
  return id && state.entities && state.entities.get('locations') ? state.entities.get('locations').get(id.toString()) : null
}
