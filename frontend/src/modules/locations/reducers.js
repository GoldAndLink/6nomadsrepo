import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { LocationsTypes } from './actions'

// getAllLocations
export const getAllLocationsLoading = state =>
  state.merge({
    getAllLocationsIsLoading: true,
    getAllLocationsErrorMessage: ''
  })

export const getAllLocationsSuccess = (state, { locations }) =>
  state.merge({
    locations,
    getAllLocationsIsLoading: false,
    getAllLocationsErrorMessage: null
  })

export const getAllLocationsFailure = (state, { errorMessage }) =>
  state.merge({
    locations: null,
    getAllLocationsIsLoading: false,
    getAllLocationsErrorMessage: errorMessage
  })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [LocationsTypes.GET_ALL_LOCATIONS_LOADING]: getAllLocationsLoading,
  [LocationsTypes.GET_ALL_LOCATIONS_SUCCESS]: getAllLocationsSuccess,
  [LocationsTypes.GET_ALL_LOCATIONS_FAILURE]: getAllLocationsFailure
})
