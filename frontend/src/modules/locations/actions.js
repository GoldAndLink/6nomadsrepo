import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getAllLocations
  getAllLocations: ['payload'],
  // The operation has started and is loading
  getAllLocationsLoading: null,
  // The locations was successfully fetched
  getAllLocationsSuccess: ['locations'],
  // An error occurred
  getAllLocationsFailure: ['errorMessage']
})

export const LocationsTypes = Types
export default Creators
