import { Map } from 'immutable'

export const INITIAL_STATE = Map({
  sendSMSIsLoading: false,
  sendSMSErrorMessage: null
})
