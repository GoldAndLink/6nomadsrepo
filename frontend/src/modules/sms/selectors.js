export const getSendSMSIsLoading = state =>
  state.sms.get('sendSMSIsLoading')

export const getSendSMSErrorMessage = state =>
  state.sms.get('sendSMSErrorMessage')
