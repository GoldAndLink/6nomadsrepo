/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */

export const isLoggedIn = state => {
  return !!state.users.get('token')
}
export const isAdmin = state => {
  const user = state.users.get('user')
  const roles = user.get('roles')
  if (!roles) return false
  return roles.find(role => role.get('name') === 'admin') !== undefined
}
export const getDefaultBuilding = state => {
  const user = state.users.get('user')
  return user && user.get('defaultBuildingId')
}
export const getLoggedUser = state => state.users.get('user')

export const token = state => {
  return state.users.get('token')
}
export const getUsers = state => {
  return state.users.get('users')
}

export const getRoles = state => {
  return state.users.get('roles')
}

export const getAccounts = state => {
  return state.users.get('accounts')
}

export const authorize = (state, roles) => {
  const user = state.users.get('user')
  const userRoles = user.get('roles')

  const existRole = roles.find(role => {
    return userRoles.find(ur => {
      return ur.get('name') === role
    })
  })
  return !!existRole
}

export const isSystemSettingsOpen = state =>
  state.users.get('isSystemSettingsOpen')

export const getUserEmail = state =>
  state.users.getIn(['user', 'email'])

export const getUserName = state => {
  const firstName = state.users.getIn(['user', 'firstName'])
  const lastName = state.users.getIn(['user', 'lastName'])
  return `${firstName} ${lastName}`
}

export const getUserId = state => state.users.getIn(['user', 'id'])

export const getAllowIssuesUser = state => state.users.get('allowIssuesUser')

export const getUserAccounts = state => state.users.getIn(['user', 'accounts'])
