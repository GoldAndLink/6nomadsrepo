import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  // getAppointments
  getAppointments: ['payload'],
  // The operation has started and is loading
  getAppointmentsLoading: null,
  // The appointments was successfully fetched
  getAppointmentsSuccess: ['appointments'],
  // An error occurred
  getAppointmentsFailure: ['errorMessage'],
  // update appointments
  updateAppointments: ['appointments'],

  // getAppointment
  getAppointment: ['id'],
  getAppointmentLoading: null,
  getAppointmentSuccess: ['appointment'],
  getAppointmentFailure: ['errorMessage'],

  // getAppointmentOrders
  getAppointmentOrders: ['payload'],
  getAppointmentOrdersLoading: null,
  getAppointmentOrdersSuccess: ['appointmentOrders'],
  getAppointmentOrdersFailure: ['errorMessage'],

  // createAppointment
  createAppointment: ['payload'],
  createAppointmentLoading: null,
  createAppointmentSuccess: ['appointment'],
  createAppointmentFailure: ['errorMessage'],

  // removeOrderFromAppointment
  removeOrderFromAppointment: ['appointmentId', 'orderId'],
  removeOrderFromAppointmentLoading: null,
  removeOrderFromAppointmentSuccess: null,
  removeOrderFromAppointmentFailure: ['errorMessage'],

  // deleteAppointment
  deleteAppointment: ['id'],
  deleteAppointmentLoading: null,
  deleteAppointmentSuccess: null,
  deleteAppointmentFailure: ['errorMessage'],

  // updateAppointment
  updateAppointment: ['payload'],
  updateAppointmentLoading: null,
  updateAppointmentSuccess: ['appointment'],
  updateAppointmentFailure: ['errorMessage'],

  // getAllAppointmentStatuses
  getAllAppointmentStatuses: null,
  getAllAppointmentStatusesLoading: null,
  getAllAppointmentStatusesSuccess: ['appointmentStatuses'],
  getAllAppointmentStatusesFailure: ['errorMessage'],

  // moveAppointment
  moveAppointment: ['payload'],
  moveAppointmentLoading: null,
  moveAppointmentSuccess: ['appointment'],
  moveAppointmentFailure: ['errorMessage'],

  openCreateAppointment: null,
  openEditAppointment: ['appointment'],
  createFromRequest: ['request', 'apptData'],
  closeUpsertAppointment: null,
  updateAppointmentWithSocketAppointment: ['data'],

  getAppointmentsForWarehouse: null,
  getAppointmentsForWarehouseLoading: null,
  getAppointmentsForWarehouseSuccess: null,
  getAppointmentsForWarehouseFailure: ['errorMessage'],
  setAppointments: ['appointments'],

  // clearRequest
  clearRequest: ['id'],
  clearRequestLoading: null,
  clearRequestSuccess: ['appointment'],
  clearRequestFailure: ['errorMessage'],

  // counts
  mergeAppointmentCounts: ['payload'],

  // flag recalculate duration
  setRecalculateDurationFlag: ['flag']
})

export const AppointmentsTypes = Types
export default Creators
