import { getDoorById } from '../doors/selectors'
import { getDriverById } from '../drivers/selectors'
import { getCarrierById } from '../carriers/selectors'

export const getAppointments = state => {
  if (state.appointments && state.appointments.get('appointments')) {
    return state.appointments.get('appointments')
      .map(appt => appt.set('door', getDoorById(state, appt.get('doorId')))
        .set('driver', getDriverById(state, appt.get('driverId')))
        .set('carrier', getCarrierById(state, appt.get('carrierId'))))
  }
  return state.appointments ? state.appointments.get('appointments') : new Map()
}

export const getAppointment = state => {
  return state.appointments.get('appointment')
}

export const getAppointmentOrders = state => {
  return state.appointments.get('appointmentOrders')
}

export const getAllAppointmentStatuses = state => {
  return state.appointments.get('appointmentStatuses')
}

export const getIsUpsertAppointmentVisible = state => {
  return state.appointments.get('isUpsertAppointmentVisible')
}

export const getEditingAppointment = state => {
  return state.appointments.get('editingAppointment')
}

export const getAppointmentCounts = state => {
  return state.appointments.get('appointmentCounts')
}

export const getRecalculateDurationFlag = state => {
  return state.appointments.get('recalculateDurationFlag')
}

export const getAppointmentById = (state, id) => {
  return id && state.entities && state.entities.get('appointments') ? state.entities.get('appointments').get(id.toString()) : null
}

export const getClearRequestIsLoading = state =>
  state.appointments.get('clearRequestIsLoading')

export const getCreateAppointmentIsLoading = state =>
  state.appointments.get('createAppointmentIsLoading')

export const getUpdateAppointmentIsLoading = state =>
  state.appointments.get('updateAppointmentIsLoading')

export const getAppointmentsIsLoading = state =>
  state.appointments.get('getAppointmentsIsLoading')

export default { getAppointments, getAppointmentById }
