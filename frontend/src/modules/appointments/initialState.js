import { Map } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = Map({
  appointments: [],
  appointment: null,
  appointmentOrders: null,
  appointmentStatuses: null,

  getAppointmentsIsLoading: false,
  getAppointmentsErrorMessage: null,

  getAppointmentIsLoading: false,
  getAppointmentErrorMessage: null,

  getAppointmentOrdersIsLoading: false,
  getAppointmentOrdersErrorMessage: null,

  createAppointmentIsLoading: false,
  createAppointmentErrorMessage: null,

  removeOrderFromAppointmentIsLoading: false,
  removeOrderFromAppointmentErrorMesage: null,

  deleteAppointmentIsLoading: false,
  deleteAppointmentErrorMesage: null,

  updateAppointmentIsLoading: false,
  updateAppointmentErrorMessage: null,

  getAllAppointmentStatusesIsLoading: false,
  getAllAppointmentStatusesErrorMessage: null,

  moveAppointmentIsLoading: false,
  moveAppointmentErrorMessage: null,

  getAppointmentsForWarehouseIsLoading: false,
  getAppointmentsForWarehouseErrorMessage: null,

  isUpsertAppointmentVisible: false,
  editingAppointment: null,

  clearRequestIsLoading: false,
  clearRequestErrorMessage: null,

  appointmentCounts: null,

  recalculateDurationFlag: false
})
