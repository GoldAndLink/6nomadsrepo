import {
  call,
  put,
  takeLatest,
  fork,
  select
} from 'redux-saga/effects'
import { normalize, schema } from 'normalizr'
import moment from 'moment'
import { Map, List, fromJS } from 'immutable'

import axios from '../../utils/axios'
import AppointmentsActions, {
  AppointmentsTypes
} from './actions'
import OrdersActions from '../orders/actions'
import DoorsActions from '../doors/actions'
import EntitiesActions from '../entities/actions'
import CarriersActions from '../carriers/actions'
import DriversActions from '../drivers/actions'
import {
  token as getToken
} from '../users/selectors'
import { getAppointmentsForDoors } from '../doors/selectors'
import {
  getWarehouse
} from '../app/selectors'

import {
  getAllBuildings,
  getAllAreas,
  getAllDoors,
  getAllOrders
} from '../entities/selectors'
import appointmentSelectors from './selectors'
import { NotificationManager } from 'react-notifications'

// getAppointmentById

import { OrderSchema } from '../orders/sagas'
import { CarrierSchema } from '../carriers/sagas'
import { DriverSchema } from '../drivers/sagas'

const baseUrl = '/appointments'

export const AppointmentSchema = new schema.Entity('appointments')

const updateAppointmentOnList = (newAppt, apptList, upsert = false) => {
  // check if appt already exist in the list
  const appt = apptList.find(ap => ap.get('id') === newAppt.get('id'))

  // if it was deleted remove from grid
  if (newAppt.get('deletedAt')) {
    const index = apptList.findIndex(ap => ap.get('id') === newAppt.get('id'))
    if (index >= 0) apptList = apptList.delete(index)
  } else {
    // if exist just update
    if (appt) {
      apptList = apptList.map(ap => {
        if (ap.get('id') === newAppt.get('id')) {
          return newAppt
        }
        return ap
      })
    } else {
      // if doesnt push to the list of appointments for doors
      if (upsert) apptList = apptList.push(newAppt)
    }
  }
  return apptList
}

/* Sagas */

function * getAppointments ({
  payload
}) {
  yield put(AppointmentsActions.getAppointmentsLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      searchText,
      attributesSelect,
      dateFrom,
      dateTo,
      customerId,
      appointmentStatusId,
      destinationId
    } = payload
    let url = searchText
      ? `${baseUrl}?${attributesSelect}=${searchText}`
      : `${baseUrl}?`

    if (dateFrom) {
      url += `&dateFrom=${dateFrom}&dateTo=${dateTo || dateFrom}`
    }
    if (customerId) {
      url += `&customerId=${customerId}`
    }
    if (appointmentStatusId) {
      url += `&appointmentStatusId=${appointmentStatusId}`
    }
    if (destinationId) {
      url += `&destinationId=${destinationId}`
    }

    const {
      data
    } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    const normalizedData = normalize(data, [AppointmentSchema])
    yield put(EntitiesActions.mergeAppointments(normalizedData.entities))

    let orders = yield select(state => getAllOrders(state))

    for (const appt of data) {
      if (appt.orders) {
        const normalizedOrdersData = normalize(appt.orders, [OrderSchema])
        orders = orders.merge(normalizedOrdersData.entities)
      }
    }
    yield put(EntitiesActions.mergeOrders(orders))

    yield put(AppointmentsActions.getAppointmentsSuccess(data))
  } catch (e) {
    yield put(AppointmentsActions.getAppointmentsFailure(e.message || e))
  }
}

function * getAppointment ({
  id
}) {
  yield put(AppointmentsActions.getAppointmentLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentsActions.getAppointmentSuccess(data))
  } catch (e) {
    yield put(AppointmentsActions.getAppointmentFailure(e.message || e))
  }
}

function * getAppointmentOrders ({
  payload
}) {
  yield put(AppointmentsActions.getAppointmentOrdersLoading())
  try {
    const {
      id
    } = payload
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, `${baseUrl}/${id}/orders`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentsActions.getAppointmentOrdersSuccess(data))
  } catch (e) {
    yield put(AppointmentsActions.getAppointmentOrdersFailure(e.message || e))
  }
}

function * createAppointment ({
  payload
}) {
  yield put(AppointmentsActions.createAppointmentLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.post, baseUrl, payload, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    if (data.carrier) {
      const normalizedData = normalize([data.carrier], [CarrierSchema])
      yield put(CarriersActions.getAllCarriersSuccess(normalizedData.entities))
    }

    if (data.driver) {
      const normalizedData = normalize([data.driver], [DriverSchema])
      yield put(DriversActions.getAllDriversSuccess(normalizedData.entities))
    }

    yield put(AppointmentsActions.createAppointmentSuccess(data))
  } catch (e) {
    if (e.response.status === 400) {
      NotificationManager.error("Appointment couldn't be created because it was overlapping another one.")
    } else if (e.response.status === 422) {
      NotificationManager.error('Appointment missing required fields.')
    } else if (e.response.status === 412) {
      NotificationManager.error(e.response.data ? e.response.data.error : 'One or more orders are already scheduled.')
    } else {
      NotificationManager.error('Appointment could not be created.')
    }

    yield put(AppointmentsActions.createAppointmentFailure(e.message || e))
  }
}

function * updateAppointment ({
  payload
}) {
  yield put(AppointmentsActions.updateAppointmentLoading())
  try {
    payload.inProgress = false
    const preserveUndefinedPayload = JSON.stringify(payload, (k, v) => v === undefined ? null : v)
    const token = yield select(state => getToken(state))

    const {
      data
    } = yield call(axios.put, `${baseUrl}/${payload.id}`, preserveUndefinedPayload, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    if (data.carrier) {
      const normalizedData = normalize([data.carrier], [CarrierSchema])
      yield put(CarriersActions.getAllCarriersSuccess(normalizedData.entities))
    }

    if (data.driver) {
      const normalizedData = normalize([data.driver], [DriverSchema])
      yield put(DriversActions.getAllDriversSuccess(normalizedData.entities))
    }

    yield put(AppointmentsActions.updateAppointmentSuccess(data))
  } catch (e) {
    console.log('ERROR:', e)
    if (e.response) {
      if ([400, 409].includes(e.response.status)) {
        NotificationManager.error("Appointment couldn't be modified because it was overlapping another one.")
      } else if (e.response.status === 422) {
        NotificationManager.error('Appointment missing required fields.')
      }
    } else {
      NotificationManager.error('Appointment could not be modified.')
    }

    yield put(AppointmentsActions.updateAppointmentFailure(e.message || e))
  }
}

function * removeOrderFromAppointment ({
  appointmentId,
  orderId
}) {
  yield put(AppointmentsActions.removeOrderFromAppointmentLoading())
  try {
    const token = yield select(state => getToken(state))
    yield call(axios.delete, `${baseUrl}/${appointmentId}/orders/${orderId}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentsActions.removeOrderFromAppointmentSuccess())
  } catch (e) {
    yield put(AppointmentsActions.removeOrderFromAppointmentFailure(e.message || e))
  }
}

function * deleteAppointment ({
  id
}) {
  yield put(AppointmentsActions.deleteAppointmentLoading())
  try {
    const token = yield select(state => getToken(state))
    yield call(axios.delete, `${baseUrl}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentsActions.deleteAppointmentSuccess())
  } catch (e) {
    yield put(AppointmentsActions.deleteAppointmentFailure(e.message || e))
  }
}

function * getAllAppointmentStatuses () {
  yield put(AppointmentsActions.getAllAppointmentStatusesLoading())
  try {
    const token = yield select(state => getToken(state))
    const {
      data
    } = yield call(axios.get, '/appointment_statuses', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentsActions.getAllAppointmentStatusesSuccess(data))
  } catch (e) {
    yield put(AppointmentsActions.getAllAppointmentStatusesFailure(e.message || e))
  }
}

function * moveAppointment ({
  payload
}) {
  const oldAppointment = yield select(state => appointmentSelectors.getAppointmentById(state, payload.id))
  yield put(AppointmentsActions.moveAppointmentLoading())
  try {
    const token = yield select(state => getToken(state))

    yield put(DoorsActions.editAppointment(payload))
    const {
      data
    } = yield call(axios.put, `${baseUrl}/${payload.id}`, payload, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentsActions.moveAppointmentSuccess(data))
  } catch (e) {
    if (e.response.status === 400) {
      NotificationManager.error("Appointment couldn't be created because it was overlapping another one.")
    } else if (e.response.status === 422) {
      NotificationManager.error('Appointment missing required fields.')
    } else {
      NotificationManager.error('Appointment could not be created.')
    }

    yield put(DoorsActions.editAppointment(oldAppointment.toJS()))
    yield put(AppointmentsActions.moveAppointmentFailure(e.message || e))
  }
}

function * getAppointmentsForWarehouse () {
  yield put(AppointmentsActions.getAppointmentsLoading())
  try {
    const buildingId = yield select(state => getWarehouse(state))
    const token = yield select(state => getToken(state))
    const doors = yield select(state => getAllDoors(state))
    const areas = yield select(state => getAllAreas(state))

    const buildingArea = areas.find(a => a.get('buildingId') === buildingId) || new Map()
    const buildingDoors = (doors.filter(d => d.get('areaId') === buildingArea.get('id')) || new Map()).valueSeq().toJS()

    let attributes = ''

    buildingDoors.map((door, index) => { index > 0 ? attributes += `&doorId=${door.id}` : attributes += `doorId=${door.id}` })

    const {
      data
    } = yield call(axios.get, `/appointments?${attributes}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    const normalizedData = normalize(data, [AppointmentSchema])
    yield put(EntitiesActions.mergeAppointments(normalizedData.entities))

    yield put(AppointmentsActions.getAppointmentsSuccess(data))
    yield put(DoorsActions.setAppointments(data))
  } catch (e) {
    yield put(AppointmentsActions.getAppointmentsFailure(e.message || e))
  }
}

function * clearRequest ({
  id
}) {
  yield put(AppointmentsActions.clearRequestLoading())
  try {
    const token = yield select(state => getToken(state))

    const {
      data
    } = yield call(axios.post, `${baseUrl}/${id}/clear-request`, null, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(AppointmentsActions.clearRequestSuccess(data))
  } catch (e) {
    yield put(AppointmentsActions.clearRequestFailure(e.message || e))
  }
}

function * createFromRequest ({ request, apptData = {} }) {
  if (!request) return

  try {
    const token = yield select(state => getToken(state))
    const requestOrders = request.get('carrierRequestOrders')

    if (requestOrders && requestOrders.size) {
      const poNumbers = requestOrders.map(ro => ro.get('poNumber'))
      const url = `/orders?PO=${poNumbers.join('&PO=')}`
      const { data } = yield call(axios.get, url, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      yield put(OrdersActions.setSelectedOrders(data.orders))
    } else {
      yield put(OrdersActions.setSelectedOrders([]))
    }

    let date = request.get('date') && moment(request.get('date'))
    if (date && request.get('timeStart')) {
      date = moment.tz(
        `${date.format('YYYY-MM-DD')} ${moment.utc(request.get('timeStart')).format('HH:mm')}`,
        request.get('building', new Map()).get('timezone', 'UTC')
      )
    }

    const appt = {
      recalculateDuration: true,
      carrierRequestId: request.get('id'),
      date,
      duration: 60,
      carrierId: request.get('carrierId'),
      driverId: request.get('driverId'),
      contactPhone: request.get('phone'),
      email: request.get('email'),
      trailer: request.get('trailerLicense'),
      tractor: request.get('tractorNumber'),
      site: request.get('site'),
      siteId: request.get('site', new Map()).get('id', null),
      building: request.get('building'),
      buildingId: request.get('building', new Map()).get('id', null),
      ...apptData
    }

    yield put(AppointmentsActions.openEditAppointment(appt))
  } catch (e) {
    console.log(e)
  }
}

function * updateAppointmentWithSocketAppointment ({ data }) {
  try {
    let normalizedData = {}
    if (data instanceof Array) {
      normalizedData = normalize(data, [AppointmentSchema])
    } else {
      normalizedData = normalize([data], [AppointmentSchema])
      data = [data]
    }
    yield put(EntitiesActions.mergeAppointments(normalizedData.entities))

    // update appointments on the grid
    const warehouse = yield select(state => getWarehouse(state))
    const doors = yield select(state => getAllDoors(state))
    const areas = yield select(state => getAllAreas(state))
    const buildings = yield select(state => getAllBuildings(state))
    let appointmentsForDoors = yield select(state => getAppointmentsForDoors(state))
    let appointmentsSideBar = yield select(state => appointmentSelectors.getAppointments(state))

    for (const appointment of data) {
      const appointmentDoor = doors.find(d => d.get('id') === appointment.doorId) || Map()
      const appointmentArea = areas.find(a => a.get('id') === appointmentDoor.get('areaId')) || Map()
      const appointmentBuilding = buildings.find(a => a.get('id') === appointmentArea.get('buildingId')) || Map()

      const newAppt = new Map({
        ...appointment,
        carrierRequests: List((appointment.carrierRequests && appointment.carrierRequests.map(c => Map(c)))) || List([]),
        orders: List((appointment.orders && appointment.orders.map(o => fromJS(o)))) || List([]),
        inventoryIssues: List((appointment.inventoryIssues || []))
      })

      if (appointmentBuilding.get('id') === warehouse) {
        appointmentsForDoors = updateAppointmentOnList(newAppt, appointmentsForDoors, true)
      }

      // update appointments on appointments side bar
      appointmentsSideBar = updateAppointmentOnList(newAppt, appointmentsSideBar, false)
    }
    yield put(DoorsActions.setAppointments(appointmentsForDoors))
    yield put(AppointmentsActions.getAppointmentsSuccess(appointmentsSideBar))
  } catch (e) {
    console.log('ERROR:', e)
  }
}

/* Watchers */

function * updateAppointmentWithSocketAppointmentWatcher () {
  yield takeLatest(AppointmentsTypes.UPDATE_APPOINTMENT_WITH_SOCKET_APPOINTMENT, updateAppointmentWithSocketAppointment)
}

function * getAppointmentsWatcher () {
  yield takeLatest(AppointmentsTypes.GET_APPOINTMENTS, getAppointments)
}

function * getAppointmentWatcher () {
  yield takeLatest(AppointmentsTypes.GET_APPOINTMENT, getAppointment)
}

function * getAppointmentOrdersWatcher () {
  yield takeLatest(AppointmentsTypes.GET_APPOINTMENT_ORDERS, getAppointmentOrders)
}

function * createAppointmentWatcher () {
  yield takeLatest(AppointmentsTypes.CREATE_APPOINTMENT, createAppointment)
}

function * updateAppointmentWatcher () {
  yield takeLatest(AppointmentsTypes.UPDATE_APPOINTMENT, updateAppointment)
}

function * removeOrderFromAppointmentWatcher () {
  yield takeLatest(AppointmentsTypes.REMOVE_ORDER_FROM_APPOINTMENT, removeOrderFromAppointment)
}

function * deleteAppointmentWatcher () {
  yield takeLatest(AppointmentsTypes.DELETE_APPOINTMENT, deleteAppointment)
}

function * getAllAppointmentStatusesWatcher () {
  yield takeLatest(AppointmentsTypes.GET_ALL_APPOINTMENT_STATUSES, getAllAppointmentStatuses)
}

function * moveAppointmentWatcher () {
  yield takeLatest(AppointmentsTypes.MOVE_APPOINTMENT, moveAppointment)
}

function * getAppointmentsForWarehouseWatcher () {
  yield takeLatest(AppointmentsTypes.GET_APPOINTMENTS_FOR_WAREHOUSE, getAppointmentsForWarehouse)
}

function * clearRequestWatcher () {
  yield takeLatest(AppointmentsTypes.CLEAR_REQUEST, clearRequest)
}

function * createFromRequestWatcher () {
  yield takeLatest(AppointmentsTypes.CREATE_FROM_REQUEST, createFromRequest)
}

export default function * root () {
  yield fork(updateAppointmentWithSocketAppointmentWatcher)
  yield fork(getAppointmentsWatcher)
  yield fork(getAppointmentWatcher)
  yield fork(getAppointmentOrdersWatcher)
  yield fork(createAppointmentWatcher)
  yield fork(updateAppointmentWatcher)
  yield fork(removeOrderFromAppointmentWatcher)
  yield fork(deleteAppointmentWatcher)
  yield fork(getAllAppointmentStatusesWatcher)
  yield fork(moveAppointmentWatcher)
  yield fork(getAppointmentsForWarehouseWatcher)
  yield fork(clearRequestWatcher)
  yield fork(createFromRequestWatcher)
}
