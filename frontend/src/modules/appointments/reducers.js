import { createReducer } from 'reduxsauce'
import { fromJS } from 'immutable'
import uniqWith from 'lodash.uniqwith'
import { INITIAL_STATE } from './initialState'
import { AppointmentsTypes } from './actions'

// getAppointments
export const getAppointmentsLoading = state =>
  state.merge({
    getAppointmentsIsLoading: true,
    getAppointmentsErrorMessage: ''
  })

export const getAppointmentsSuccess = (state, { appointments }) =>
  state.merge({
    appointments,
    getAppointmentsIsLoading: false,
    getAppointmentsErrorMessage: null
  })

export const getAppointmentsFailure = (state, { errorMessage }) =>
  state.merge({
    appointments: null,
    getAppointmentsIsLoading: false,
    getAppointmentsErrorMessage: errorMessage
  })

// getAppointment
export const getAppointmentLoading = state =>
  state.merge({
    getAppointmentIsLoading: true,
    getAppointmentErrorMessage: ''
  })

export const getAppointmentSuccess = (state, { appointment }) =>
  state.merge({
    appointment,
    getAppointmentIsLoading: false,
    getAppointmentErrorMessage: null
  })

export const getAppointmentFailure = (state, { errorMessage }) =>
  state.merge({
    appointment: null,
    getAppointmentIsLoading: false,
    getAppointmentErrorMessage: errorMessage
  })

// getAppointmentOrders
export const getAppointmentOrdersLoading = state =>
  state.merge({
    getAppointmentOrdersIsLoading: true,
    getAppointmentOrdersErrorMessage: null
  })

export const getAppointmentOrdersSuccess = (state, { appointmentOrders }) =>
  state.merge({
    appointmentOrders,
    getAppointmentOrdersIsLoading: false,
    getAppointmentOrdersErrorMessage: null
  })

export const getAppointmentOrdersFailure = (state, { errorMessage }) =>
  state.merge({
    order: null,
    getAppointmentOrdersIsLoading: false,
    getAppointmentOrdersErrorMessage: errorMessage
  })

// createAppointment
export const createAppointmentLoading = state =>
  state.merge({
    createAppointmentIsLoading: true,
    createAppointmentErrorMessage: null
  })

export const createAppointmentSuccess = (state, { appointment }) =>
  state.merge({
    createAppointmentIsLoading: false,
    createAppointmentErrorMessage: null,
    isUpsertAppointmentVisible: !appointment.appointmentStatusId,
    editingAppointment: state.get('editingAppointment') ? state.get('editingAppointment').set('id', appointment.id) : { id: appointment.id }
  })

export const createAppointmentFailure = (state, { errorMessage }) =>
  state.merge({
    createAppointmentIsLoading: false,
    createAppointmentErrorMessage: errorMessage
  })

// removeOrderFromAppointment
export const removeOrderFromAppointmentLoading = state =>
  state.merge({
    removeOrderFromAppointmentIsLoading: true,
    removeOrderFromAppointmentErrorMesage: null
  })

export const removeOrderFromAppointmentSuccess = state =>
  state.merge({
    removeOrderFromAppointmentIsLoading: false,
    removeOrderFromAppointmentErrorMesage: null
  })

export const removeOrderFromAppointmentFailure = (state, { errorMessage }) =>
  state.merge({
    removeOrderFromAppointmentIsLoading: false,
    removeOrderFromAppointmentErrorMesage: errorMessage
  })

// deleteAppointment
export const deleteAppointmentLoading = state =>
  state.merge({
    deleteAppointmentIsLoading: true,
    deleteAppointmentErrorMesage: null
  })

export const deleteAppointmentSuccess = state =>
  state.merge({
    deleteAppointmentIsLoading: false,
    deleteAppointmentErrorMesage: null
  })

export const deleteAppointmentFailure = (state, { errorMessage }) =>
  state.merge({
    deleteAppointmentIsLoading: false,
    deleteAppointmentErrorMesage: errorMessage
  })

// updateAppointment
export const updateAppointmentLoading = state =>
  state.merge({
    updateAppointmentIsLoading: true,
    updateAppointmentErrorMessage: null
  })

export const updateAppointmentSuccess = state =>
  state.merge({
    updateAppointmentIsLoading: false,
    updateAppointmentErrorMessage: null,
    isUpsertAppointmentVisible: false,
    editingAppointment: null
  })

export const updateAppointmentFailure = (state, { errorMessage }) =>
  state.merge({
    updateAppointmentIsLoading: false,
    updateAppointmentErrorMessage: errorMessage
  })

export const updateAppointments = (state, { appointments }) => {
  const uniqueAppointments = uniqWith([...state.get('appointments'), ...fromJS(appointments)], (a, b) => a.get('id') === b.get('id'))
  return state.merge({
    appointments: uniqueAppointments,
    getAppointmentsIsLoading: false,
    getAppointmentsErrorMessage: null
  })
}

// getAllAppointmentStatuses
export const getAllAppointmentStatusesLoading = state =>
  state.merge({
    getAllAppointmentStatusesIsLoading: true,
    getAllAppointmentStatusesErrorMessage: ''
  })

export const getAllAppointmentStatusesSuccess = (state, { appointmentStatuses }) =>
  state.merge({
    appointmentStatuses,
    getAllAppointmentStatusesIsLoading: false,
    getAllAppointmentStatusesErrorMessage: null
  })

export const getAllAppointmentStatusesFailure = (state, { errorMessage }) =>
  state.merge({
    appointmentStatuses: null,
    getAllAppointmentStatusesIsLoading: false,
    getAllAppointmentStatusesErrorMessage: errorMessage
  })

// moveAppointment
export const moveAppointmentLoading = state =>
  state.merge({
    moveAppointmentIsLoading: true,
    moveAppointmentErrorMessage: null
  })

export const moveAppointmentSuccess = state =>
  state.merge({
    moveAppointmentIsLoading: false,
    moveAppointmentErrorMessage: null,
    isUpsertAppointmentVisible: false,
    editingAppointment: null
  })

export const moveAppointmentFailure = (state, { errorMessage }) =>
  state.merge({
    moveAppointmentIsLoading: true,
    moveAppointmentErrorMessage: errorMessage
  })

export const openCreateAppointment = state =>
  state.merge({
    isUpsertAppointmentVisible: true,
    editingAppointment: null
  })

export const openEditAppointment = (state, { appointment }) =>
  state.merge({
    isUpsertAppointmentVisible: true,
    editingAppointment: appointment
  })

export const closeUpsertAppointment = state =>
  state.merge({
    isUpsertAppointmentVisible: false,
    editingAppointment: null
  })

// getAppointmentsForWarehouse
export const getAppointmentsForWarehouseLoading = state =>
  state.merge({
    getAppointmentsForWarehouseIsLoading: true,
    getAppointmentsForWarehouseErrorMessage: ''
  })

export const getAppointmentsForWarehouseSuccess = state =>
  state.merge({
    getAppointmentsForWarehouseIsLoading: false,
    getAppointmentsForWarehouseErrorMessage: null
  })

export const getAppointmentsForWarehouseFailure = (state, { errorMessage }) =>
  state.merge({
    getAppointmentsForWarehouseIsLoading: false,
    getAppointmentsForWarehouseErrorMessage: errorMessage
  })

// setAppointments
export const setAppointments = (state, { appointments }) =>
  state.merge({
    appointments: appointments
  })

// clearRequest
export const clearRequestLoading = state =>
  state.merge({
    clearRequestIsLoading: true,
    clearRequestErrorMessage: null
  })

export const clearRequestSuccess = (state, { appointment }) =>
  state.merge({
    clearRequestIsLoading: false,
    clearRequestErrorMessage: null,
    editingAppointment: appointment
  })

export const clearRequestFailure = (state, { errorMessage }) =>
  state.merge({
    clearRequestIsLoading: false,
    clearRequestErrorMessage: errorMessage
  })

export const mergeAppointmentCounts = (state, { payload }) =>
  state.merge({
    appointmentCounts: {
      appointmentYesterdayCount: payload.appointmentYesterdayCount,
      appointmentTodayCount: payload.appointmentTodayCount,
      appointmentNext1Count: payload.appointmentNext1Count,
      appointmentNext2Count: payload.appointmentNext2Count,
      appointmentNext3Count: payload.appointmentNext3Count
    }
  })

export const setRecalculateDurationFlag = (state, { flag }) =>
  state.merge({
    recalculateDurationFlag: flag
  })

export const reducer = createReducer(INITIAL_STATE, {
  [AppointmentsTypes.GET_APPOINTMENTS_LOADING]: getAppointmentsLoading,
  [AppointmentsTypes.GET_APPOINTMENTS_SUCCESS]: getAppointmentsSuccess,
  [AppointmentsTypes.GET_APPOINTMENTS_FAILURE]: getAppointmentsFailure,
  [AppointmentsTypes.GET_APPOINTMENT_LOADING]: getAppointmentLoading,
  [AppointmentsTypes.GET_APPOINTMENT_SUCCESS]: getAppointmentSuccess,
  [AppointmentsTypes.GET_APPOINTMENT_FAILURE]: getAppointmentFailure,

  [AppointmentsTypes.GET_ALL_APPOINTMENT_STATUSES_LOADING]: getAllAppointmentStatusesLoading,
  [AppointmentsTypes.GET_ALL_APPOINTMENT_STATUSES_SUCCESS]: getAllAppointmentStatusesSuccess,
  [AppointmentsTypes.GET_ALL_APPOINTMENT_STATUSES_FAILURE]: getAllAppointmentStatusesFailure,

  [AppointmentsTypes.CREATE_APPOINTMENT_LOADING]: createAppointmentLoading,
  [AppointmentsTypes.CREATE_APPOINTMENT_SUCCESS]: createAppointmentSuccess,
  [AppointmentsTypes.CREATE_APPOINTMENT_FAILURE]: createAppointmentFailure,

  [AppointmentsTypes.UPDATE_APPOINTMENT_LOADING]: updateAppointmentLoading,
  [AppointmentsTypes.UPDATE_APPOINTMENT_SUCCESS]: updateAppointmentSuccess,
  [AppointmentsTypes.UPDATE_APPOINTMENT_FAILURE]: updateAppointmentFailure,
  [AppointmentsTypes.UPDATE_APPOINTMENTS]: updateAppointments,

  [AppointmentsTypes.DELETE_APPOINTMENT_LOADING]: deleteAppointmentLoading,
  [AppointmentsTypes.DELETE_APPOINTMENT_SUCCESS]: deleteAppointmentSuccess,
  [AppointmentsTypes.DELETE_APPOINTMENT_FAILURE]: deleteAppointmentFailure,
  [AppointmentsTypes.OPEN_CREATE_APPOINTMENT]: openCreateAppointment,
  [AppointmentsTypes.OPEN_EDIT_APPOINTMENT]: openEditAppointment,
  [AppointmentsTypes.CLOSE_UPSERT_APPOINTMENT]: closeUpsertAppointment,

  [AppointmentsTypes.GET_APPOINTMENTS_FOR_WAREHOUSE_LOADING]: getAppointmentsForWarehouseLoading,
  [AppointmentsTypes.GET_APPOINTMENTS_FOR_WAREHOUSE_SUCCESS]: getAppointmentsForWarehouseSuccess,
  [AppointmentsTypes.GET_APPOINTMENTS_FOR_WAREHOUSE_FAILURE]: getAppointmentsForWarehouseFailure,

  [AppointmentsTypes.CLEAR_REQUEST_LOADING]: clearRequestLoading,
  [AppointmentsTypes.CLEAR_REQUEST_SUCCESS]: clearRequestSuccess,
  [AppointmentsTypes.CLEAR_REQUEST_FAILURE]: clearRequestFailure,

  [AppointmentsTypes.MERGE_APPOINTMENT_COUNTS]: mergeAppointmentCounts,

  [AppointmentsTypes.SET_RECALCULATE_DURATION_FLAG]: setRecalculateDurationFlag
})
