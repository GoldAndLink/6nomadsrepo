import { call, put, takeLatest, fork, select } from 'redux-saga/effects'

import axios from '../../utils/axios'
import CarrierRequestsActions, { CarrierRequests } from './actions'
import { normalize } from 'normalizr'
import EntitiesActions from '../entities/actions'
import { AppointmentSchema } from '../appointments/sagas'
import { token as getToken } from '../users/selectors'
import config from '../../config'

const baseUrl = '/carrierRequests'
const openBaseUrl = '/open/carrierRequests'

// Sagas
function * getAllCarrierRequests ({ payload }) {
  yield put(CarrierRequestsActions.getAllCarrierRequestsLoading())
  try {
    const {
      searchText,
      attributesSelect,
      carrierRequestStatus,
      carrierPortal,
      carrierSelect,
      sortByDate,
      includes,
      guid
    } = payload

    let url = searchText && attributesSelect ? `${baseUrl}?${attributesSelect}=${searchText}` : `${baseUrl}?`
    if (sortByDate) {
      url += `&sort=updatedAt:${sortByDate}`
    }

    if (searchText && attributesSelect) {
      url += `&${attributesSelect}=${searchText}`
    }

    if (carrierRequestStatus) {
      url += `&status=${carrierRequestStatus}`
    }

    if (carrierSelect) {
      url += `&carrierId=${carrierSelect}`
    }

    if (carrierPortal) {
      url += `&carrierPortal=${carrierPortal}`
    }

    if (includes && includes.length > 0) {
      includes.forEach(include => {
        url += `&_include=${include}`
      })
    }

    if (guid) {
      url += `&guid=${guid}`
    }

    const token = yield select(state => getToken(state))

    const {
      data
    } = yield call(axios.get, url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    if (carrierPortal) {
      const appointments = data.map(cr => cr.appointment)
      const normalizedData = normalize(appointments, [AppointmentSchema])
      yield put(EntitiesActions.mergeAppointments(normalizedData.entities))
    }

    yield put(CarrierRequestsActions.getAllCarrierRequestsSuccess(data))
  } catch (e) {
    console.log('ERROR:', e)
    yield put(CarrierRequestsActions.getAllCarrierRequestsFailure(e.message || e))
  }
}

function * getNumberOfCarrierRequests ({ payload }) {
  yield put(CarrierRequestsActions.getNumberOfCarrierRequestsLoading()) // findsecrets-ignore-line
  try {
    const token = yield select(state => getToken(state))
    const { data } = yield call(axios.get, `${baseUrl}/count`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    yield put(CarrierRequestsActions.getNumberOfCarrierRequestsSuccess(data.numberOfCarrierRequests))
  } catch (e) {
    yield put(CarrierRequestsActions.getNumberOfCarrierRequestsFailure(e.message || e))
  }
}

function * getCarrierRequestPickUpTimes ({ payload }) {
  yield put(CarrierRequestsActions.getCarrierRequestPickUpTimesLoading()) // findsecrets-ignore-line
  try {
    const { data } = yield call(axios.get, `open/carrierRequestTimeRanges`, {
      headers: {
        Authorization: `Key ${config.API_KEY}`
      }
    })
    yield put(CarrierRequestsActions.getCarrierRequestPickUpTimesSuccess(data))
  } catch (e) {
    yield put(CarrierRequestsActions.getCarrierRequestPickUpTimesFailure(e.message || e)) // findsecrets-ignore-line
  }
}

function * createCarrierRequest ({ payload }) {
  yield put(CarrierRequestsActions.createCarrierRequestLoading())
  try {
    const {
      data
    } = yield call(axios.post, openBaseUrl, payload, {
      headers: {
        Authorization: `Key ${config.API_KEY}`
      }
    })
    yield put(CarrierRequestsActions.createCarrierRequestSuccess(data))
    yield put(CarrierRequestsActions.getAllCarrierRequests({}))
  } catch (e) {
    yield put(CarrierRequestsActions.createCarrierRequestFailure(e.message || e))
  }
}

function * updateCarrierRequest ({ payload }) {
  yield put(CarrierRequestsActions.updateCarrierRequestLoading())
  const { id } = payload
  try {
    const {
      data
    } = yield call(axios.put, `${openBaseUrl}/${id}`, payload, {
      headers: {
        Authorization: `Key ${config.API_KEY}`
      }
    })
    yield put(CarrierRequestsActions.updateCarrierRequestSuccess(data))
  } catch (e) {
    yield put(CarrierRequestsActions.updateCarrierRequestFailure(e.message || e))
  }
}

// Watchers
function * getAllCarrierRequestsWatcher () {
  yield takeLatest(CarrierRequests.GET_ALL_CARRIER_REQUESTS, getAllCarrierRequests)
}

function * getNumberOfCarrierRequestsWatcher () {
  yield takeLatest(CarrierRequests.GET_NUMBER_OF_CARRIER_REQUESTS, getNumberOfCarrierRequests)
}

function * getCarrierRequestPickUpTimesWatcher () { // findsecrets-ignore-line
  yield takeLatest(CarrierRequests.GET_CARRIER_REQUEST_PICK_UP_TIMES, getCarrierRequestPickUpTimes)
}

function * createCarrierRequestWatcher () {
  yield takeLatest(CarrierRequests.CREATE_CARRIER_REQUEST, createCarrierRequest)
}

function * updateCarrierRequestWatcher () {
  yield takeLatest(CarrierRequests.UPDATE_CARRIER_REQUEST, updateCarrierRequest)
}

export default function * root () {
  yield fork(getAllCarrierRequestsWatcher)
  yield fork(getNumberOfCarrierRequestsWatcher)
  yield fork(getCarrierRequestPickUpTimesWatcher) // findsecrets-ignore-line
  yield fork(createCarrierRequestWatcher)
  yield fork(updateCarrierRequestWatcher)
}
