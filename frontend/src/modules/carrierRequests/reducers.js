// findsecrets-ignore-file
import { fromJS } from 'immutable'
import { createReducer } from 'reduxsauce'

import { INITIAL_STATE } from './initialState'
import { CarrierRequests } from './actions'
import { CarriersTypes } from '../carriers/actions'

// getAllCarrierRequests
export const getAllCarrierRequestsLoading = state =>
  state.merge({
    getAllCarrierRequestsIsLoading: true,
    getAllCarrierRequestsErrorMessage: ''
  })

export const getAllCarrierRequestsSuccess = (state, { carrierRequests }) =>
  state.merge({
    carrierRequests,
    getAllCarrierRequestsIsLoading: false,
    getAllCarrierRequestsErrorMessage: null
  })

export const getAllCarrierRequestsFailure = (state, { errorMessage }) =>
  state.merge({
    carrierRequests: null,
    getAllCarrierRequestsIsLoading: false,
    getAllCarrierRequestsErrorMessage: errorMessage
  })

// getNumberOfCarrierRequests
export const getNumberOfCarrierRequestsLoading = state =>
  state.merge({
    getNumberOfCarrierRequestsIsLoading: true,
    getNumberOfCarrierRequestsErrorMessage: ''
  })

export const getNumberOfCarrierRequestsSuccess = (state, { numberOfCarrierRequests }) =>
  state.merge({
    numberOfCarrierRequests: numberOfCarrierRequests,
    getNumberOfCarrierRequestsIsLoading: false,
    getNumberOfCarrierRequestsErrorMessage: null
  })

export const getNumberOfCarrierRequestsFailure = (state, { errorMessage }) =>
  state.merge({
    numberOfCarrierRequests: null,
    getNumberOfCarrierRequestsIsLoading: false,
    getNumberOfCarrierRequestsErrorMessage: errorMessage
  })

// getCarrierRequestPickUpTimes
export const getCarrierRequestPickUpTimesLoading = state =>
  state.merge({
    getCarrierRequestPickUpTimesIsLoading: true,
    getCarrierRequestPickUpTimesErrorMessage: ''
  })

export const getCarrierRequestPickUpTimesSuccess = (state, { carrierRequestsPickUpTimes }) =>
  state.merge({
    carrierRequestsPickUpTimes,
    getCarrierRequestPickUpTimesIsLoading: false,
    getCarrierRequestPickUpTimesErrorMessage: null
  })

export const getCarrierRequestPickUpTimesFailure = (state, { errorMessage }) =>
  state.merge({
    carrierRequestsPickUpTimes: null,
    getCarrierRequestPickUpTimesIsLoading: false,
    getCarrierRequestPickUpTimesErrorMessage: errorMessage
  })

// createCarrierRequest

export const createCarrierRequestLoading = state =>
  state.merge({
    createCarrierRequestIsLoading: true,
    createCarrierRequestErrorMessage: null
  })

export const createCarrierRequestSuccess = (state, { carrierRequest }) =>
  state.merge({
    creatingCarrierRequest: null,
    createCarrierRequestIsLoading: false,
    createCarrierRequestErrorMessage: null
  })

export const createCarrierRequestFailure = (state, { errorMessage }) =>
  state.merge({
    creatingCarrierRequest: null,
    createCarrierRequestIsLoading: false,
    createCarrierRequestErrorMessage: errorMessage
  })

// carrierRequests socket
export const updateCarrierRequestsWithSocketCarrierRequest = (state, { payload }) => {
  const { socketCarrierRequest } = payload
  if (socketCarrierRequest.deletedAt) {
    return state.merge({
      carrierRequests: state.get('carrierRequests').filter(carrierRequest => carrierRequest.get('id') !== socketCarrierRequest.id)
    })
  }

  if (socketCarrierRequest.status && socketCarrierRequest.status.toUpperCase() === 'SCHEDULED') {
    return state.merge({
      carrierRequests: state.get('carrierRequests').filter(carrierRequest => carrierRequest.get('id') !== socketCarrierRequest.id)
    })
  }

  const carrierRequestExists = state.get('carrierRequests').find(carrierRequest => carrierRequest.get('id') === socketCarrierRequest.id)
  if (!carrierRequestExists) {
    return state.merge({
      carrierRequests: state.get('carrierRequests').push(fromJS(socketCarrierRequest))
    })
  } else {
    return state.merge({
      carrierRequests: state.get('carrierRequests').map(
        carrierRequest => carrierRequest.get('id') !== socketCarrierRequest.id ? carrierRequest : carrierRequest.merge(socketCarrierRequest)
      )
    })
  }
}

export const updateCarrierRequestsWithSocketCarrierRequestOnCarrierSide = (state, { payload }) => {
  const { socketCarrierRequest } = payload
  if (socketCarrierRequest.deletedAt) {
    return state.merge({
      carrierRequests: state.get('carrierRequests').filter(carrierRequest => carrierRequest.get('id') !== socketCarrierRequest.id)
    })
  }

  const carrierRequestExists = state.get('carrierRequests').find(carrierRequest => carrierRequest.get('id') === socketCarrierRequest.id)
  if (carrierRequestExists) {
    return state.merge({
      carrierRequests: state.get('carrierRequests').map(
        carrierRequest => carrierRequest.get('id') !== socketCarrierRequest.id ? carrierRequest : carrierRequest.merge(socketCarrierRequest)
      )
    })
  }

  return state
}

export const updateCountCarrierRequestsWithSocketCountCarrierRequests = (state, { payload: { numberOfCarrierRequests } }) => {
  return state.merge({
    numberOfCarrierRequests: numberOfCarrierRequests
  })
}

export const updateCreatingCarrierRequest = (state, { payload }) => {
  return state.merge({
    creatingCarrierRequest: state.get('creatingCarrierRequest')
      ? state.get('creatingCarrierRequest').merge({
        ...payload
      }) : payload
  })
}

export const resetCreatingCarrierRequest = state => {
  return state.merge({
    creatingCarrierRequest: null
  })
}

export const loginSuccess = (state, { user }) => {
  let creatingCarrierRequest = state.get('creatingCarrierRequest')
  if (creatingCarrierRequest) {
    creatingCarrierRequest = creatingCarrierRequest.merge({
      carrierId: user.carrierId,
      driverId: user.driverId,
      phone: user.phone,
      email: user.email,
      prefContactMethod: user.prefContactMethod
    })
  }

  return state.merge({ creatingCarrierRequest })
}

// update carrierRequest
export const updateCarrierRequestLoading = state =>
  state.merge({
    updateCarrierRequestIsLoading: true,
    updateCarrierRequestErrorMessage: null
  })

export const updateCarrierRequestSuccess = (state, { updatedCarrierRequest }) => {
  if (updatedCarrierRequest.status.toUpperCase() === 'SCHEDULED') {
    return state.merge({
      carrierRequests: state.get('carrierRequests').filter(carrierRequest => carrierRequest.get('id') !== updatedCarrierRequest.id),
      updateCarrierRequestIsLoading: false,
      updateCarrierRequestErrorMessage: null
    })
  }

  return state.merge({
    carrierRequests: state.get('carrierRequests').map(
      carrierRequest => carrierRequest.get('id') !== updatedCarrierRequest.id ? carrierRequest : carrierRequest.merge(updatedCarrierRequest)
    ),
    updateCarrierRequestIsLoading: false,
    updateCarrierRequestErrorMessage: null
  })
}

export const updateCarrierRequestFailure = (state, { errorMessage }) =>
  state.merge({
    updateCarrierRequestIsLoading: false,
    updateCarrierRequestErrorMessage: errorMessage
  })

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CarrierRequests.GET_ALL_CARRIER_REQUESTS_LOADING]: getAllCarrierRequestsLoading,
  [CarrierRequests.GET_ALL_CARRIER_REQUESTS_SUCCESS]: getAllCarrierRequestsSuccess,
  [CarrierRequests.GET_ALL_CARRIER_REQUESTS_FAILURE]: getAllCarrierRequestsFailure,
  [CarrierRequests.GET_NUMBER_OF_CARRIER_REQUESTS_LOADING]: getNumberOfCarrierRequestsLoading,
  [CarrierRequests.GET_NUMBER_OF_CARRIER_REQUESTS_SUCCESS]: getNumberOfCarrierRequestsSuccess,
  [CarrierRequests.GET_NUMBER_OF_CARRIER_REQUESTS_FAILURE]: getNumberOfCarrierRequestsFailure,
  [CarrierRequests.GET_CARRIER_REQUEST_PICK_UP_TIMES_LOADING]: getCarrierRequestPickUpTimesLoading,
  [CarrierRequests.GET_CARRIER_REQUEST_PICK_UP_TIMES_SUCCESS]: getCarrierRequestPickUpTimesSuccess,
  [CarrierRequests.GET_CARRIER_REQUEST_PICK_UP_TIMES_FAILURE]: getCarrierRequestPickUpTimesFailure,
  [CarrierRequests.CREATE_CARRIER_REQUEST_LOADING]: createCarrierRequestLoading,
  [CarrierRequests.CREATE_CARRIER_REQUEST_SUCCESS]: createCarrierRequestSuccess,
  [CarrierRequests.CREATE_CARRIER_REQUEST_FAILURE]: createCarrierRequestFailure,
  [CarrierRequests.UPDATE_CARRIER_REQUESTS_WITH_SOCKET_CARRIER_REQUEST]: updateCarrierRequestsWithSocketCarrierRequest,
  [CarrierRequests.UPDATE_CARRIER_REQUESTS_WITH_SOCKET_CARRIER_REQUEST_ON_CARRIER_SIDE]: updateCarrierRequestsWithSocketCarrierRequestOnCarrierSide,
  [CarrierRequests.UPDATE_COUNT_CARRIER_REQUESTS_WITH_SOCKET_COUNT_CARRIER_REQUESTS]: updateCountCarrierRequestsWithSocketCountCarrierRequests,
  [CarrierRequests.UPDATE_CREATING_CARRIER_REQUEST]: updateCreatingCarrierRequest,
  [CarrierRequests.RESET_CREATING_CARRIER_REQUEST]: resetCreatingCarrierRequest,
  [CarriersTypes.LOGIN_SUCCESS]: loginSuccess,
  [CarrierRequests.UPDATE_CARRIER_REQUEST_LOADING]: updateCarrierRequestLoading,
  [CarrierRequests.UPDATE_CARRIER_REQUEST_SUCCESS]: updateCarrierRequestSuccess,
  [CarrierRequests.UPDATE_CARRIER_REQUEST_FAILURE]: updateCarrierRequestFailure
})
