/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 */
import { getAppointmentById } from '../appointments/selectors'
import { getDriverById } from '../drivers/selectors'
import { getCarrierById } from '../carriers/selectors'
import { List } from 'immutable'

export const getAllCarrierRequests = state => {
  if (state.carrierRequests && state.carrierRequests.get('carrierRequests')) {
    return state.carrierRequests.get('carrierRequests')
      .map(request => request.set('appointment', getAppointmentById(state, request.get('appointmentId')))
        .set('driver', getDriverById(state, request.get('driverId')))
        .set('carrier', getCarrierById(state, request.get('carrierId'))))
  }
  return state.carrierRequests && (state.carrierRequests.get('carrierRequests', new List()) || new List())
}

export const numberOfCarrierRequests = state => {
  return state.carrierRequests.get('numberOfCarrierRequests')
}

export const getCreatingCarrierRequest = state => {
  return state.carrierRequests.get('creatingCarrierRequest')
}

export const getCarrierRequestPickUpTimes = state => {
  return state.carrierRequests.get('carrierRequestsPickUpTimes')
}

export const getAllCarrierRequestsIsLoading = state =>
  state.carrierRequests.get('getAllCarrierRequestsIsLoading')
