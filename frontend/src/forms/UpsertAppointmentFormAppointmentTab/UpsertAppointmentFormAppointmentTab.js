import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'react-datepicker/dist/react-datepicker.css'
import styled from 'styled-components'
import isEmpty from 'lodash.isempty'
import isEqual from 'lodash.isequal'
import { List } from 'immutable'
import Button from '../../components/Button'
import FormSelect from '../../components/FormSelect'
import OrderCard from '../../components/OrderCard'
import TextInput from '../../components/TextInput'
import TextArea from '../../components/TextArea'
import FormDatePicker from '../../components/FormDatePicker'
import AutoComplete from '../../components/AutoComplete'
import { Form, Field, FormSpy } from 'react-final-form'
import moment from 'moment'
import { Droppable } from 'react-beautiful-dnd'
import { generateDropppableId, maskPhoneNumber } from '../../utils/utils'

const Container = styled.div`
  display: flex;
  width: 620px;
  height: 530px;
  overflow: hidden;
`

const OrdersContainer = styled.div`
  width: 250px;
  background-color: #3C414E;
  padding: 10px;
`

const OrdersHeader = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const OrdersTitle = styled.h2`
  margin: 0px 0px 10px 0px;
  color: #dbdee7;
  text-transform: uppercase;
  letter-spacing: 1.1px;
  font-size: 12px;
`

const AppointmentsContainer = styled.div`
  width: 370px;
  padding: 10px;
  padding-top: 0;
  overflow: hidden;
`

const FormRow = styled.div`
  display: flex;
  margin: 0px -18px 10px -18px;
  box-sizing: border-box;
`

const FormColumn = styled.div`
  flex: 1;
  padding: 0px 18px;
  font-size: 12px;
  ${({ alignRight }) => alignRight && `
      display: flex;
      justify-content: flex-end;
  `}
`

const StyledOrderCard = styled(OrderCard)`
  margin: 0px 0px 10px 0px;
`

const CollapseCommand = styled.div`
  height: 14px;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  color: #aab0c0;
  cursor: pointer;
  margin: 0 5px;
`

const Separator = styled.span`
  height: 14px;
  font-size: 12px;
  color: #aab0c0;
`

const CollapseCommandContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: 5px;
`

const TimeContainer = styled.div`
  flex: 1;
  display: flex;
  margin: 0px -6px 0px -6px;
`

const TimeColumn = styled.div`
  flex: 1;
  padding: 0px 6px;
`

const EmailButton = styled(Button)`
  width: 105px;
  height: 46px;
  margin-top: 9px;
  ${({ disabled }) => disabled && `
      color: #b3b9c7;
      border-color: #b3b9c7;
  `}
`

const Buttons = styled.div`
  display: flex;
  justify-content: space-between;
`

const SaveButton = styled(Button)`
  width: 60px;
  height: 30px;
  border-radius: 2px;
  background-color: #61c9b5;
  font-size: 12px;
  text-align: center;
  color: #f3f6f6;
`

const CancelButton = styled(Button)`
  width: 150px;
  height: 30px;
  border-radius: 2px;
  background-color: #fdedef;
  font-size: 12px;
  text-align: center;
  color: #ff0d29;
`

const ErrorText = styled.div`
  color: red;
`

const TotalsContainer = styled.div`
  display: flex;
  flex-direction: column;
`

const LabelContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const Label = styled.h3`
  height: 15px;
  font-size: 12px;
  font-weight: bold;
  color: #b3b9c7;
  span {
    height: 11px;
    font-size: 12px;
    font-weight: 300;
    color: #aab0c0;
  }
`

const Title = styled.h2`
  width: 50px;
  height: 18px;
  font-size: 19px;
  font-weight: 300;
  color: #d0d4e0;
`

const durationOptions = [
  { label: '60 min', value: 60 },
  { label: '120 min', value: 120 },
  { label: '180 min', value: 180 },
  { label: '240 min', value: 240 }
]

class UpsertAppointmentFormAppointmentTab extends Component {
  constructor (props) {
    super(props)

    this.state = {
      collapseAll: false,
      expandAll: false,
      isEmailDisable: true
    }
  }

  componentDidMount () {
    const { initialValues } = this.props
    if (initialValues && initialValues.id) {
      this.setState({ isEmailDisable: false })
    }
  }

  /* componentDidUpdate(prevProps, prevState) {
    const { values } = state
    if (values && !!values.id && prevState.values.id !==) {

    }
  } */

  collapseAll = () => this.setState(state => ({
    collapseAll: true,
    expandAll: false
  }))

  expandAll = () => this.setState(state => ({
    expandAll: true,
    collapseAll: false
  }))

  resetCollapseAndExpand = () => this.setState(state => ({
    collapseAll: false,
    expandAll: false
  }))

  onFieldChange = state => {
    const { values } = state
    const { openEditAppointment, initialValues, onSubmit, getDoorDuration } = this.props
    const { doorId, date, time, building } = values

    if (building) this.props.onSetBuilding(building)

    // initial request to create "ghost" appointment
    // later requests are processed via standard update
    if (doorId && date && time && isEmpty(initialValues)) {
      getDoorDuration({ id: doorId })
      openEditAppointment(values)
      this.setState({ isEmailDisable: false })
      onSubmit({ duration: 60, inProgress: true, ...values })
    }

    this.checkChanges(values, initialValues)
  }

  checkChanges = (newValues, oldValues) => {
    const newValuesCleaned = { ...newValues }

    if (newValuesCleaned.driver && newValuesCleaned.driver.value === null && newValuesCleaned.driver.label === null) newValuesCleaned.driver = null
    if (newValuesCleaned.carrier && newValuesCleaned.carrier.value === null && newValuesCleaned.carrier.label === null) newValuesCleaned.carrier = null

    if (!newValues.hasOwnProperty('trailer')) { newValuesCleaned.trailer = null }
    if (!newValues.hasOwnProperty('tractor')) { newValuesCleaned.tractor = null }
    if (!newValues.hasOwnProperty('contactPhone')) { newValuesCleaned.contactPhone = null }

    const equal = isEqual(newValuesCleaned, oldValues)
    this.props.hasFormChanged(!equal)
  }

  onEmail = e => {
    e.preventDefault()
    this.props.switchToEmailHiddenTab()
  }

  onDeleteClick = e => {
    e.preventDefault()
    this.props.onDelete(this.props.initialValues.id)
  }

  getCases = order => {
    const selectedItems = order.get('items', List([]))

    let orderItems = List([])
    selectedItems.map(si => {
      orderItems = orderItems.concat(List([si.get('orderItem')]))
    })

    const cases = orderItems.reduce((sumQuantity, oi) => sumQuantity + oi.get('quantity') || 0, 0)
    const UOM = orderItems && !!orderItems.size ? orderItems.first().get('quantityUOM') : ''
    return { value: cases, UOM }
  }

  getWeight = order => {
    const selectedItems = order.get('items', List([]))

    let orderItems = List([])
    selectedItems.map(si => {
      orderItems = orderItems.concat(List([si.get('orderItem')]))
    })

    const weight = orderItems.reduce((sumWeight, orderOrderItem) => sumWeight + orderOrderItem.get('weight') || 0, 0)
    const UOM = orderItems && !!orderItems.size ? orderItems.first().get('weightUOM') : ''
    return { value: weight, UOM }
  }

  getSKUs = order => {
    const items = order.get('items', List([]))
    const uniqSKUs = Array.from(new Set(items.map(s => s.get('sku'))))
    return { value: uniqSKUs }
  }

  getPallets = order => order ? order.get('pallets') : null

  render () {
    const {
      orders,
      subscription,
      initialValues,
      submitButtonText,
      statusOptions,
      siteOptions,
      carrierOptions,
      driverOptions,
      onDeleteOrder,
      getBuildingOptions,
      getDoorOptions,
      validate,
      isSaving,
      onSubmit,
      canDelete,
      onChange,
      onOrdersChange
    } = this.props

    const { collapseAll, expandAll, isEmailDisable } = this.state

    const totalsOrderCases = orders.reduce((acc, order) => {
      const cases = this.getCases(order)
      return cases ? acc + cases.value : acc
    }, 0)
    const totalsOrderWeight = orders.reduce((acc, order) => {
      const weight = this.getWeight(order)
      return weight ? acc + weight.value : acc
    }, 0)
    const totalsOrderSKUs = new Set(orders.reduce((acc, order) => {
      const SKUs = this.getSKUs(order)
      return SKUs ? [...acc, ...SKUs.value] : acc
    }, []))

    const totalsOrderPallets = orders.reduce((acc, order) => {
      const pallets = this.getPallets(order)
      return pallets ? acc + pallets : acc
    }, 0)

    return (
      <Container>
        <OrdersContainer>
          <OrdersHeader>
            <OrdersTitle>
              Orders
            </OrdersTitle>
            <CollapseCommandContainer>
              <CollapseCommand onClick={this.expandAll}>
                Expand all
              </CollapseCommand>
              <Separator>|</Separator>
              <CollapseCommand onClick={this.collapseAll}>
                Collapse all
              </CollapseCommand>
            </CollapseCommandContainer>
          </OrdersHeader>
          <Droppable droppableId={generateDropppableId('appointment')}>
            {(provided, snapshot) => {
              return (
                <div
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                  style={{ height: '370px', overflowX: 'auto' }}>
                  {(!orders || !orders.size) &&
                    <ErrorText>At least 1 order needs to be associated with the appointment</ErrorText>
                  }
                  {orders.map(order => {
                    const orderCases = this.getCases(order)
                    const orderWeight = this.getWeight(order)
                    const orderSKUs = this.getSKUs(order)
                    const orderPallets = this.getPallets(order)
                    return (
                      <StyledOrderCard
                        key={order.get('id')}
                        order={order}
                        isCreateAppointment
                        onDeleteOrder={() => {
                          onOrdersChange(orders.filter(o => o.get('id') !== order.get('id')))
                          onDeleteOrder(order)
                        }}
                        collapseAll={collapseAll}
                        expandAll={expandAll}
                        resetCollapseAndExpand={this.resetCollapseAndExpand}
                        orderCases={orderCases}
                        orderWeight={orderWeight}
                        orderSKUs={orderSKUs}
                        orderPallets={orderPallets}
                      />
                    )
                  })}
                  {provided.placeholder}
                </div>)
            }}
          </Droppable>
          <TotalsContainer>
            <Title>Totals</Title>
            <LabelContainer>
              <Label>Weight: <span>{totalsOrderWeight.toFixed(1)}</span></Label>
              <Label>SKUs: <span>{totalsOrderSKUs.size}</span></Label>
            </LabelContainer>
            <LabelContainer>
              <Label>Cases: <span>{totalsOrderCases}</span></Label>
              <Label>Pallets: <span>{totalsOrderPallets.toFixed(2)}</span></Label>
            </LabelContainer>
          </TotalsContainer>
        </OrdersContainer>
        <AppointmentsContainer>
          <Form
            onSubmit={data => onSubmit(data)}
            initialValues={initialValues}
            validate={validate}
            subscription={subscription}
            render={({ handleSubmit, submitting }) => (
              <form onSubmit={handleSubmit}>
                <FormRow>
                  <FormSpy
                    onChange={state => onChange && onChange(state.values)}
                  />
                  <FormColumn>
                    <Field name='site'>
                      {({ input, meta }) => (
                        <FormSelect
                          {...input}
                          error={meta.touched && meta.error}
                          options={siteOptions}
                          label='Site'
                        />
                      )}
                    </Field>
                  </FormColumn>
                  <FormColumn>
                    <FormSpy>
                      {({ form }) =>
                        (
                          <Field name='building'>
                            {({ input, meta }) => (
                              <FormSelect
                                {...input}
                                error={meta.touched && meta.error}
                                options={getBuildingOptions(form.getState().values.site)}
                                label='Building'
                              />
                            )}
                          </Field>
                        )
                      }
                    </FormSpy>
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <Field name='date' component='{DatePicker}'>
                      {({ input, meta }) => (
                        <FormDatePicker
                          {...input}
                          showIcon
                          label="Date"
                          dateFormat='MM-DD-YYYY'
                          selected={input.value ? moment(input.value) : null}
                          placeholderText='Date'
                          error={meta.touched && meta.error}
                          autoComplete="off"
                        />
                      )}
                    </Field>
                  </FormColumn>
                  <FormColumn>
                    <TimeContainer>
                      <TimeColumn>
                        <Field name='time' component='{DatePicker}'>
                          {({ input, meta }) => (
                            <FormDatePicker
                              {...input}
                              label="Time"
                              showTimeSelect
                              showTimeSelectOnly
                              timeIntervals={60}
                              dateFormat='LT'
                              timeCaption='time'
                              placeholderText='Time'
                              timeFormat='HH:mm'
                              selected={input.value ? moment(input.value) : null}
                              error={meta.touched && meta.error}
                              autoComplete="off"
                            />
                          )}
                        </Field>
                      </TimeColumn>
                      <TimeColumn>
                        <Field name='duration'>
                          {({ input, meta }) => (
                            <FormSelect
                              {...input}
                              error={meta.touched && meta.error}
                              options={durationOptions}
                              label='Duration'
                            />
                          )}
                        </Field>
                      </TimeColumn>
                    </TimeContainer>
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <FormSpy>
                      {({ form }) =>
                        <Field name='doorId'>
                          {({ input, meta }) => (
                            <FormSelect
                              {...input}
                              error={meta.touched && meta.error}
                              options={getDoorOptions(form.getState().values.building)}
                              label='Door'
                            />
                          )}
                        </Field>
                      }
                    </FormSpy>
                  </FormColumn>
                  <FormColumn>
                    <Field name='appointmentStatusId'>
                      {({ input, meta }) => (
                        <FormSelect
                          {...input}
                          error={meta.touched && meta.error}
                          options={statusOptions}
                          label='Status'
                        />
                      )}
                    </Field>
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <Field name={'carrier'}>
                      {({ input, meta }) => (
                        <AutoComplete {...input}
                          label="Carrier"
                          error={meta.touched && meta.error}
                          options={carrierOptions}
                          isClearable={true}
                        />
                      )}
                    </Field>
                  </FormColumn>
                  <FormColumn alignRight>
                    <EmailButton disabled={isEmailDisable} onClick={this.onEmail}>
                      Email
                    </EmailButton>
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <Field name='contactPhone'>
                      {({ input, meta }) => (
                        <TextInput
                          {...input}
                          error={meta.touched && meta.error}
                          value={maskPhoneNumber(input.value)}
                          fullwidth
                          maxLength='12'
                          placeholder='xxx-xxx-xxxx'
                          pattern='(\d{0,3})-(\d{0,3})-(\d{0,4})'
                          label='Contact Phone'
                          type='tel'
                        />
                      )}
                    </Field>
                  </FormColumn>
                  <FormColumn>
                    <Field name='driver'>
                      {({ input, meta }) => (
                        <AutoComplete {...input}
                          label="Driver"
                          error={meta.touched && meta.error}
                          options={driverOptions}
                          isClearable={true}
                        />
                      )}
                    </Field>
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <Field name='trailer'>
                      {({ input, meta }) => (
                        <TextInput
                          {...input}
                          error={meta.touched && meta.error}
                          fullwidth
                          label='Trailer Number'
                          type='text'
                        />
                      )}
                    </Field>
                  </FormColumn>
                  <FormColumn>
                    <Field name='tractor'>
                      {({ input, meta }) => (
                        <TextInput
                          {...input}
                          error={meta.touched && meta.error}
                          fullwidth
                          label='Tractor Number'
                          type='text'
                        />
                      )}
                    </Field>
                  </FormColumn>
                </FormRow>
                <Field name='notes'>
                  {({ input, meta }) => (
                    <TextArea
                      isBox
                      {...input}
                      error={meta.touched && meta.error}
                      fullwidth
                      label='Notes'
                      rows="4"
                    />
                  )}
                </Field>
                <Buttons>
                  <SaveButton primary type='submit' disabled={submitting || isSaving || (!orders || !orders.size)}>
                    {submitButtonText}
                  </SaveButton>
                  {canDelete &&
                    <CancelButton danger onClick={this.onDeleteClick}>
                      Cancel Appointment
                    </CancelButton>
                  }
                </Buttons>
                <FormSpy subscription={{ values: true }} onChange={state => this.onFieldChange(state)}>
                  {({ form, initialValues }) => {
                    return null
                  }}
                </FormSpy>
              </form>
            )}
          />
        </AppointmentsContainer>
      </Container>
    )
  }
}

UpsertAppointmentFormAppointmentTab.propTypes = {
  orders: PropTypes.object,
  subscription: PropTypes.object,
  initialValues: PropTypes.object,
  onChange: PropTypes.func,
  submitButtonText: PropTypes.string,
  statusOptions: PropTypes.any,
  siteOptions: PropTypes.array,
  carrierOptions: PropTypes.any,
  driverOptions: PropTypes.object,
  onDeleteOrder: PropTypes.func,
  onSubmit: PropTypes.func,
  getBuildingOptions: PropTypes.func,
  getDoorOptions: PropTypes.func,
  validate: PropTypes.func,
  isSaving: PropTypes.bool,
  switchToEmailHiddenTab: PropTypes.func,
  setPreFilledData: PropTypes.func,
  onSetBuilding: PropTypes.func,
  hasFormChanged: PropTypes.func
  // ToDo: send ordersItems and items as props to Upsert => call func in OrderCard as prop => reduce func to get totals
}

export default UpsertAppointmentFormAppointmentTab
