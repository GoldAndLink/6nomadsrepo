// findsecrets-ignore-file
import React, { Component } from 'react'
import { Map, List } from 'immutable'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import moment from 'moment-timezone'
import 'react-datepicker/dist/react-datepicker.css'
import styled, { css } from 'styled-components'
import isEmpty from 'lodash.isempty'
import UpsertAppointmentFormAppointmentTab from './UpsertAppointmentFormAppointmentTab/UpsertAppointmentFormAppointmentTab'
import UpsertAppointmentFormEmailHiddenTab from './UpsertAppointmentFormEmailHiddenTab/UpsertAppointmentFormEmailHiddenTab'
import SummonSMSForm from './SummonSMSForm'
import OrderItemsTab from '../components/OrderItemsTab'
import DeleteAppointmentModal from '../components/DeleteAppointmentModal'
import InventoryIssueModal from '../components/InventoryIssueModal'

import SitesActions from '../modules/sites/actions'
import {
  getAllSites,
  getAllBuildings,
  getAllAreas,
  getAllDoors
} from '../modules/entities/selectors'
import CarriersActions from '../modules/carriers/actions'
import { getAllCarriers } from '../modules/carriers/selectors'
import DriversActions from '../modules/drivers/actions'
import BuildingsActions from '../modules/buildings/actions'
import InventoryItemsActions from '../modules/inventoryItems/actions'
import AppActions from '../modules/app/actions'
import SMSActions from '../modules/sms/actions'
import { getAllDrivers } from '../modules/drivers/selectors'
import { getAllCarrierRequests } from '../modules/carrierRequests/selectors'
import { getSendSMSIsLoading, getSendSMSErrorMessage } from '../modules/sms/selectors'
import { getSendEmailIsLoading, getSendEmailErrorMessage } from '../modules/emails/selectors'
import AppointmentsActions from '../modules/appointments/actions'
import UsersActions from '../modules/users/actions'
import {
  getAllAppointmentStatuses,
  getEditingAppointment,
  getClearRequestIsLoading,
  getCreateAppointmentIsLoading,
  getUpdateAppointmentIsLoading,
  getRecalculateDurationFlag
} from '../modules/appointments/selectors'
import OrdersActions from '../modules/orders/actions'
import { getSelectedOrders } from '../modules/orders/selectors'
import DoorsActions from '../modules/doors/actions'
import { getDoorDuration } from '../modules/doors/selectors'
import EmailsActions from '../modules/emails/actions'
import { getInventoryItemsProjection } from '../modules/inventoryItems/selectors'
import { getUserName, getUsers, getUserId, getAllowIssuesUser, getLoggedUser } from '../modules/users/selectors'
import { getWarehouse } from '../modules/app/selectors'

import { requestStatuses } from '../containers/Requests'
import WarningIconCanceled from '../assets/images/warning-icon-canceled.svg'
import WarningIconReschedule from '../assets/images/warning-icon-reschedule.svg'

const APPOINTMENTS_TAB = 0
const INVENTORY_TAB = 1
const EMAIL_HIDDEN_TAB = 2
const SUMMON_SMS_TAB = 3

const SUMMON_SMS_TEMPLATE_ID = 3

const CancelRequestAlert = styled.div`
  display: flex;
  align-items: center;
  color: #ff1e3a;
  border-color: #ff1e3a;
  background-color: #fdedef;
  margin: 6px 10px;
  font-size: 12px;
  height: 38px;
  border-style: solid;
  border-width: 1px;
  padding: 0px 10px;

  span {
    margin-left: 10px;
    flex: 1
  }
`

const RescheduleRequestAlert = styled(CancelRequestAlert)`
  color: #ff8100;
  border-color: #ff8100;
  background-color: #fff4c1;
`

const ClearRequestButton = styled.div`
  font-size: 12px;
  font-weight: bold;
  text-transform: uppercase;
  cursor: pointer;
  display: flex;
  align-items: center;

  img {
    margin-left: 6px;
  }

  ${props => props.disabled && css`
    opacity: 0.5;
    pointer-events: none;
  `}
`

class UpsertAppointmentForm extends Component {
  constructor (props) {
    super(props)

    const { orders, initialValues } = props
    const doorDuration = this.calculateDuration(orders, initialValues.duration)

    this.state = {
      initialValues: {
        ...initialValues,
        allowIssues: !!initialValues.inventoryReviewUserId,
        duration: initialValues.recalculateDuration ? doorDuration.get('duration') : initialValues.duration
      },
      sendSMSError: false,
      inventoryReviewUserName: null,
      pickupLocation: null,
      isInventoryIssueOpen: false,
      dataBeforeInventoryIssues: null
    }
  }

  componentDidMount () {
    this.getInventoryProjection()
    // Do we really need to get ALL??
    // Why don't filter by something
    // We already get some of that info on bootstrap endpoint
    // this.props.getAllCarriers()
    // this.props.getAllDrivers()
    // this.props.getAllCarrierRequests()
  }

  componentDidUpdate (prevProps, prevState) {
    const {
      sendSMSIsLoading,
      sendSMSErrorMessage,
      sendEmailIsLoading,
      sendEmailErrorMessage,
      closeUpsertAppointment
    } = this.props

    if (!sendSMSIsLoading && prevProps.sendSMSIsLoading) {
      if (!sendSMSErrorMessage) {
        closeUpsertAppointment()
      } else {
        this.setState({ sendSMSError: sendSMSErrorMessage })
      }
    }
    if (!sendEmailIsLoading && prevProps.sendEmailIsLoading) {
      if (!sendEmailErrorMessage) {
        closeUpsertAppointment()
      } else {
        this.setState({ sendEmailError: sendEmailErrorMessage })
      }
    }

    if (this.props.orders !== prevProps.orders) {
      this.getInventoryProjection()
    }
  }

  // That is a really bad way to update the duration!
  // It needs to be event-driven
  // getDerivedStateFromProps it's an anti-pattern and must be used
  // only in specifc cases
  static getDerivedStateFromProps (nextProps, prevState) {
    const { orders, doorDurations, recalculateDurationFlag, setRecalculateDurationFlag } = nextProps
    const { initialValues } = prevState

    if (recalculateDurationFlag) {
      let items = List([])
      orders.map(o => {
        items = items.concat(o.get('items', List([])))
      })

      const skus = Array.from(new Set(items.map(item => item.get('sku'))))
      const numberOfUniqItems = skus.length
      const doorDuration = doorDurations.sort((a, b) => b.get('quantityFrom') - a.get('quantityFrom')).find(bdr => bdr.get('quantityFrom') < numberOfUniqItems)

      if (initialValues.inventoryReviewUserId) {
        // this.setInventoryReviewUserName(initialValues.inventoryReviewUserId)
      }

      if (doorDuration) {
        setTimeout(() => {
          setRecalculateDurationFlag(false)
        }, 100)

        const duration = doorDuration.get('duration')
        return {
          initialValues: {
            ...initialValues,
            duration
          }
        }
      } else {
        return {
          initialValues
        }
      }
    }
    return null
  }

  getInventoryProjection () {
    const { orders } = this.props

    let selectedItems = List([])
    orders.map(o => {
      selectedItems = selectedItems.concat(o.get('items', List([])))
    })

    const skus = Array.from(new Set(selectedItems.map(item => item.get('sku'))))
    if (this.props.editingAppointment && this.props.editingAppointment.get('doorId')) {
      this.props.getInventoryItemsProjection({
        skus,
        doorId: this.props.editingAppointment.get('doorId')
      })
    }
  }

  validate = values => {
    const errors = {}

    if (!values.site) {
      errors.site = 'Required'
    }
    if (!values.building) {
      errors.building = 'Required'
    }
    if (!values.doorId) {
      errors.doorId = 'Required'
    }
    if (!values.date) {
      errors.date = 'Required'
    }
    if (!values.time) {
      errors.time = 'Required'
    }

    return errors
  }

  checkInventory (appointmentDate) {
    const { inventoryItemsProjection, orders } = this.props

    if (!inventoryItemsProjection) return true

    let items = List([])
    orders.map(o => {
      items = items.concat(o.get('items', List([])))
    })

    let orderItems = List([])
    items.map(si => {
      orderItems = orderItems.concat(List([si.get('orderItem')]))
    })

    const quantities = orderItems
      .reduce((quantities, orderItem) => {
        const item = items.find(i => i.get('id') === orderItem.get('itemId'))
        const sku = item.get('sku')
        if (!quantities[sku]) {
          quantities[sku] = orderItem.get('quantity') || 0
        } else {
          quantities[sku] += orderItem.get('quantity') || 0
        }

        return quantities
      }, {})

    const inventoryIssue = Object.keys(quantities).some(sku => {
      const projection = inventoryItemsProjection.get(sku)
      if (projection && projection.get('deltas')) {
        const available = projection.get('deltas').reduce((available, delta) => {
          if (!moment(delta.get('time')).isAfter(appointmentDate)) {
            return available + delta.get('quantity')
          } else {
            return available
          }
        }, 0)
        if (quantities[sku] > available) {
          return true
        } else {
          return false
        }
      } else {
        return true
      }
    })

    return !inventoryIssue
  }

  onSubmit = (data, forceInventory) => {
    const {
      user,
      orders,
      initialValues,
      editingAppointment,
      createAppointment,
      updateAppointment,
      buildings,
      warehouse
    } = this.props

    const building = buildings.find(b => b.get('id') === data.building)
    const timezone = building.get('timezone') || 'UTC'

    const {
      doorId,
      driver,
      carrier,
      contactPhone,
      appointmentStatusId,
      date,
      time,
      duration,
      notes,
      trailer,
      tractor,
      inProgress = false,
      allowIssues
    } = data

    const prevReviewUserId = editingAppointment && editingAppointment.get('inventoryReviewUserId')

    const newDate = moment.tz(`${date instanceof moment ? date.format('YYYY-MM-DD') : date} ${time.format('HH:mm')}`, timezone).utc()

    if (!allowIssues && !forceInventory && !this.checkInventory(newDate)) {
      this.setState({
        isInventoryIssueOpen: true,
        initialValues: data,
        dataBeforeInventoryIssues: data
      })
      return
    }

    let inventoryReviewUserId
    if (!allowIssues) {
      inventoryReviewUserId = null
    } else {
      inventoryReviewUserId = prevReviewUserId || (user && user.get('id'))
    }

    const appointment = {
      date: newDate.toISOString(),
      duration,
      trailer: trailer,
      tractor,
      doorId,
      driverName: driver && driver.label,
      driverId: driver && driver.value,
      carrierId: carrier && carrier.value,
      carrierName: carrier && carrier.label,
      appointmentStatusId,
      notes,
      contactPhone,
      orderIds: orders.map(order => order.get('id')),
      inProgress: initialValues.inProgress ? false : inProgress,
      inventoryReviewUserId
    }

    if (initialValues && initialValues.id) {
      updateAppointment({ id: initialValues.id, ...appointment })
      if (warehouse !== building.get('id')) {
        this.showAppointment(appointment, building)
      }
    } else {
      if (initialValues.carrierRequestId) appointment.carrierRequestId = initialValues.carrierRequestId
      if (initialValues.inventoryReviewUserId) appointment.inventoryReviewUserId = initialValues.inventoryReviewUserId
      createAppointment(appointment)
      if (initialValues.carrierRequestId) this.props.closeUpsertAppointment()
    }
  }

  showAppointment (appointment, building) {
    const { setStartDate, setEndDate, setStartTime, setSite, setWarehouse, getAppointmentsForDoors } = this.props
    const date = moment(appointment.date)

    const site = building.get('siteId')
    const warehouse = building.get('id')
    const startDate = date.format('L')
    const endDate = date.add(7, 'days').format('L')
    const startTime = date.startOf('day').format('HH:mm:ss')
    const endTime = date.endOf('day').format('HH:mm:ss')

    setStartTime({ startTime: moment(startDate).startOf('day') })
    setStartDate({ startDate: moment(startDate) })
    setEndDate({ endDate: moment(endDate) })
    setSite({ site, id: site })
    setWarehouse({ warehouse: warehouse })

    getAppointmentsForDoors({
      buildingId: warehouse,
      startDate,
      endDate,
      startTime,
      endTime
    })
  }

  onDeleteOrder = order => {
    this.props.deselectOrder(order)
  }

  getBuildingOptions = site => {
    const { sites, buildings } = this.props

    if (!site) return []

    const siteBuildings =
      sites.get(site + '').get('buildings').map(id => buildings.get(id + ''))

    return siteBuildings.map(building => ({
      label: building.get('name'),
      value: building.get('id')
    }))
  }

  getDoorOptions = building => {
    const { buildings, areas, doors } = this.props

    if (!building) return []

    const area =
      buildings
        .get(building + '')
        .get('areas').first()

    const buildingDoors =
      areas
        .get(area + '')
        .get('doors')
        .map(id => doors.get(id + ''))

    return buildingDoors.map(door => ({
      label: door.get('name'),
      value: door.get('id')
    }))
  }

  getAggregatedOrderDetails = () => {
    const { orders } = this.props

    const ordersPalletsAcc = orders ? orders.reduce((acc, order) => {
      const pallets = order.get('pallets') || null
      return pallets ? acc + pallets : acc
    }, 0) : null

    let selectedItems = List([])
    orders.map(o => {
      selectedItems = selectedItems.concat(o.get('items', List([])))
    })

    let selectedOrderItems = List([])
    selectedItems.map(si => {
      selectedOrderItems = selectedOrderItems.concat(List([si.get('orderItem')]))
    })

    const ordersDetails = {
      orderItems: selectedOrderItems,
      items: selectedItems,
      ordersPalletsAcc
    }

    return ordersDetails
  }

  getRescheduleTime = (carrierRequest, timezone) => {
    if (!carrierRequest.get('rescheduleTimeSuggestion')) return 'UNKNOWN'

    return moment.tz(carrierRequest.get('rescheduleTimeSuggestion'), timezone).format('HH:mm')
  }

  onSendEmail = emailFormValues => {
    const { sendEmail, switchToAppointmentsTab, initialValues } = this.props
    const { id } = initialValues
    sendEmail({
      htmlTemplate: 'basic',
      emailTemplateId: null,
      subject: `DO NOT REPLY: Pickup Appointment Confirmation #${id}`,
      ...emailFormValues
    })
    switchToAppointmentsTab()
  }

  onSendSMS = ({ to }) => {
    this.props.sendSMS(to, { ...this.getSummonSMSData(), to })
  }

  onConfirmAppointmentDelete = () => {
    const { deleteAppointment, closeUpsertAppointment, appointmentForDelete, onCloseDeleteAppointment } = this.props
    deleteAppointment(appointmentForDelete)
    onCloseDeleteAppointment()
    closeUpsertAppointment()
  }

  clearRequest = () => {
    const { clearRequest, initialValues } = this.props
    clearRequest(initialValues.id)
  }

  generateEmailContent = () => {
    const { orders, sites, initialValues, switchToAppointmentsTab, buildings } = this.props

    if (isEmpty(initialValues)) {
      switchToAppointmentsTab()
      return ''
    }

    const { id, date, time, site } = initialValues
    const SOs = orders.map(order => order.get('primaryRefValue')).toJS()
    const ordersOtherRefs = orders.map(order => order.getIn(['data', 'otherRefs'])).toJS()
    const POs = ordersOtherRefs.map(orderOtherRefs => orderOtherRefs.find(otherRef => otherRef.type === 'PO'))
    const formatedDate = date ? date.format('MM/DD/YYYY') : null
    const formatedTime = time ? time.format('hh:mm') : null
    const formatedSite = sites ? sites.find(siteItem => siteItem.get('id') === site) : null

    const buildingId = formatedSite.get('buildings').first()
    const building = buildings.find(building => building.get('id') === buildingId)
    const location = building.get('location')

    let formatedLocation = ''
    if (!location) {
      formatedLocation = '<Address-from-DB>'
    } else {
      formatedLocation += `${location.get('name')}`
      formatedLocation += '\n'
      formatedLocation += `${location.get('address1')}`
      formatedLocation += '\n'
      formatedLocation += `${location.get('address2')}`
      formatedLocation += '\n'
      formatedLocation += `${location.get('city')}`
      formatedLocation += '\n'
      formatedLocation += `${location.get('state')}, ${location.get('postalCode')}`
      formatedLocation += '\n'
    }

    let content = ''
    content += `This email is to confirm your pickup appointment scheduled: ${formatedDate || '<Appt-DATE>'} at ${formatedTime || '<Appt-TIME>'}.`
    content += '\n'
    content += `Must check in within an hour of the appointment.`
    content += '\n'
    content += '\n'
    content += `Location: ${formatedSite.get('name') || '<Location-from-DB>'}`
    content += '\n'
    content += '\n'
    content += `Address:`
    content += '\n'
    content += formatedLocation
    content += '\n'
    content += `Appointment Number: #${id || '<Appt-Number>'}`
    content += '\n'
    content += '\n'
    content += `SO Number(s):`
    content += `${SOs.map(SO => '\n' + SO) || '<List-SO-Numbers>'}`
    content += '\n'
    content += '\n'
    content += `PO Numbers(s):`
    content += `${POs.map(PO => '\n' + (PO ? PO.val : '')) || '<List-PO-Numbers>'}`
    content += '\n'
    content += '\n'
    content += `Trailer must be:`
    content += '\n'
    content += '\n'
    content += `- Clean and Sanitized`
    content += '\n'
    content += `- Precooled to 34 degrees and set at 35 continuous`
    content += '\n'
    content += `- Air chute must be in good repair`
    content += '\n'
    content += `- Please remove all load locks`

    return content
  }

  getEmailPreFilledData = () => {
    return {
      data: this.generateEmailContent()
    }
  }

  getSummonSMSData = () => {
    const {
      editingAppointment: appt,
      carriers,
      sites,
      doors,
      areas,
      buildings,
      carrierRequests,
      drivers
    } = this.props

    if (!appt) return ({})

    const customer = appt.getIn(['orders', 0, 'customer', 'name'])
    const destination = appt.getIn(['orders', 0, 'destination'])

    const apptContactPhone = appt.get('contactPhone')
    const apptCR = carrierRequests.find(cr => cr.get('appointmentId') === appt.get('id'))
    const crPhone = apptCR && apptCR.get('phone')
    const apptDriver = drivers.find(driver => driver.get('id') === appt.get('driverId'))
    const driverPhone = apptDriver && apptDriver.get('phone')
    const to = apptContactPhone || crPhone || driverPhone || ''

    const door = doors.find(d => d.get('id') === appt.get('doorId'))
    const area = door && areas.find(a => a.get('id') === door.get('areaId'))
    const building = area && buildings.find(b => b.get('id') === area.get('buildingId'))
    const site = building && sites.find(s => s.get('id') === building.get('siteId'))
    const carrier = carriers.find(c => c.get('id') === appt.get('carrierId'))

    let time = appt.get('date')
    const timezone = building && building.get('timezone')
    if (time && timezone) {
      time = moment.tz(time, timezone).format('YYYY-MM-DD HH:mm')
    }

    return {
      customer,
      to,
      destination: destination ? destination.get('name') : '',
      carrier: carrier ? carrier.get('name') : '',
      site: site ? site.get('name') : '',
      door: door ? door.get('name') : '',
      time
    }
  }

  onUpdateAppointmentInventoryReviewUserId = isYes => {
    const { initialValues } = this.state
    const updatedInitialValues = initialValues
    updatedInitialValues.inventoryReviewUserId = isYes ? this.props.userId : null
    this.setState({ initialValues: updatedInitialValues })
    this.setInventoryReviewUserName(updatedInitialValues.inventoryReviewUserId)
  }

  onNewTimeSelected = ({ time, door }) => {
    const { initialValues } = this.state
    const { switchToAppointmentsTab } = this.props
    this.setState({ initialValues: {
      ...initialValues,
      date: moment(time),
      time: moment(time),
      doorId: door
    } })
    switchToAppointmentsTab()
  }

  onAllowIssuesChange = allowIssues => {
    const { initialValues } = this.state
    this.setState({ initialValues: {
      ...initialValues,
      allowIssues
    } })
  }

  setInventoryReviewUserName = async id => {
    await this.props.getAllowIssuesUser({ id })
    const { allowIssuesUser } = this.props
    this.setState({
      inventoryReviewUserName: id
        ? allowIssuesUser
          ? `${allowIssuesUser.get('firstName')} ${allowIssuesUser.get('lastName')}`
          : 'N/A'
        : null
    })
  }

  getLocation = buildingId => {
    const { buildings } = this.props
    const building = buildings && buildings.find(building => building.get('id') === buildingId)
    const location = building.get('location')
    return location
  }

  onSetBuilding = building => {
    const location = this.getLocation(building)
    this.setState({ pickupLocation: location })
  }

  onSaveInventoryIssue = () => {
    this.setState({ isInventoryIssueOpen: false })
    this.onSubmit(this.state.dataBeforeInventoryIssues, true)
  }

  onCancelInventoryIssue = () => {
    this.setState({ isInventoryIssueOpen: false })
    this.props.switchToInventoryTab()
  }

  calculateDuration = (orders, defaultDuration = 60) => {
    const { doorDurations } = this.props
    let items = List([])
    orders.map(o => {
      items = items.concat(o.get('items', List([])))
    })

    const skus = Array.from(new Set(items.map(item => item.get('sku'))))
    const numberOfUniqSkus = skus.length
    const doorDuration = doorDurations.sort((a, b) => b.get('quantityFrom') - a.get('quantityFrom')).find(bdr => bdr.get('quantityFrom') < numberOfUniqSkus)
    return doorDuration || Map({ duration: defaultDuration })
  }

  onOrdersChange = orders => {
    const { initialValues } = this.state

    if (orders) {
      const doorDuration = this.calculateDuration(orders, initialValues.duration)

      if (doorDuration) {
        this.setState({
          initialValues: {
            ...initialValues,
            duration: doorDuration.get('duration')
          }
        })
      }
    }
  }

  render () {
    const {
      orders,
      subscription,
      sites,
      doors,
      areas,
      buildings,
      carriers,
      drivers,
      inventoryItemsProjection,
      appointmentStatuses,
      tab,
      openEditAppointment,
      getDoorDuration,
      switchToEmailHiddenTab,
      switchToAppointmentsTab,
      editingAppointment,
      clearRequestIsLoading,
      createAppointmentIsLoading,
      updateAppointmentIsLoading,
      sendSMSIsLoading,
      closeUpsertAppointment,
      isDeleteAppointmentOpen,
      onDeleteAppointment,
      onCloseDeleteAppointment,
      hasFormChanged,
      updateAppointment,
      user,
      users
    } = this.props
    const {
      initialValues,
      sendSMSError,
      inventoryReviewUserName,
      pickupLocation,
      isInventoryIssueOpen
    } = this.state

    const door = doors && editingAppointment && doors.find(d => d.get('id') === editingAppointment.get('doorId'))
    const area = door && areas && areas.find(a => a.get('id') === door.get('areaId'))
    const building = area && buildings && buildings.find(b => b.get('id') === area.get('buildingId'))
    const timezone = building && building.get('timezone')

    const statusOptions = appointmentStatuses
      ? appointmentStatuses.map(appointmentStatus => ({
        label: appointmentStatus.get('name'),
        value: appointmentStatus.get('id')
      }))
      : []

    const siteOptions = sites
      ? sites.valueSeq().map(site => ({
        label: site.get('name'), value: site.get('id')
      })).toArray()
      : []

    const carrierOptions = carriers
      ? carriers.map(carrier => ({
        label: carrier.get('name'),
        value: carrier.get('id').toString()
      }))
      : []

    const driverOptions = drivers
      ? drivers.map(driver => ({
        label: `${driver.get('firstName') || ''} ${driver.get('lastName') || ''}`,
        value: driver.get('id') && driver.get('id').toString()
      }))
      : []

    const ordersDetails = this.getAggregatedOrderDetails()

    const canDelete = initialValues && initialValues.id

    if (tab === APPOINTMENTS_TAB) {
      const carrierRequests =
        (editingAppointment && editingAppointment.get('carrierRequests')) || []
      const cancelRequested = carrierRequests.some(c => c.get('status') === requestStatuses.canceled)
      const rescheduleRequested = carrierRequests.find(c => c.get('status') === requestStatuses.reschedule)

      if (initialValues && !initialValues.hasOwnProperty('appointmentStatusId')) {
        const scheduledAppointmentStatus = appointmentStatuses.find(apptStatus => apptStatus.get('name').toLowerCase() === 'scheduled')
        const appointmentStatusId = scheduledAppointmentStatus.get('id')
        initialValues.appointmentStatusId = appointmentStatusId
      }

      const isSaving = createAppointmentIsLoading || updateAppointmentIsLoading
      return (
        <div>
          {initialValues && initialValues.id && cancelRequested &&
            <CancelRequestAlert>
              <img src={WarningIconCanceled} />
              <span>A cancellation has been requested for this appointment.</span>
              <ClearRequestButton disabled={clearRequestIsLoading} onClick={this.clearRequest}>
                Clear request
              </ClearRequestButton>
            </CancelRequestAlert>
          }
          {initialValues && initialValues.id && rescheduleRequested &&
            <RescheduleRequestAlert>
              <img src={WarningIconReschedule} />
              <span>Request to reschedule this appointment to {this.getRescheduleTime(rescheduleRequested, timezone)}</span>
              <ClearRequestButton disabled={clearRequestIsLoading} onClick={this.clearRequest}>
                Clear request
              </ClearRequestButton>
            </RescheduleRequestAlert>
          }
          <UpsertAppointmentFormAppointmentTab
            orders={orders}
            subscription={subscription}
            initialValues={initialValues}
            onChange={initialValues => this.setState({ initialValues })}
            submitButtonText={isSaving ? 'Saving...' : 'Save'}
            statusOptions={statusOptions}
            siteOptions={siteOptions}
            carrierOptions={carrierOptions}
            driverOptions={driverOptions}
            onDeleteOrder={this.onDeleteOrder}
            onSubmit={this.onSubmit}
            getBuildingOptions={this.getBuildingOptions}
            getDoorOptions={this.getDoorOptions}
            validate={this.validate}
            openEditAppointment={openEditAppointment}
            getDoorDuration={getDoorDuration}
            switchToEmailHiddenTab={switchToEmailHiddenTab}
            canDelete={canDelete}
            onDelete={onDeleteAppointment}
            onSetBuilding={this.onSetBuilding}
            hasFormChanged={hasFormChanged}
            isSaving={isSaving}
            onOrdersChange={this.onOrdersChange}
          />
          <DeleteAppointmentModal
            isOpen={isDeleteAppointmentOpen}
            onConfirmDelete={this.onConfirmAppointmentDelete}
            onClose={onCloseDeleteAppointment}
          />
          <InventoryIssueModal
            isOpen={isInventoryIssueOpen}
            onSave={this.onSaveInventoryIssue}
            onCancel={this.onCancelInventoryIssue}
          />
        </div>
      )
    }
    if (tab === INVENTORY_TAB) {
      return (
        <div>
          <OrderItemsTab
            user={user}
            users={users}
            appointment={initialValues}
            updateAppointment={updateAppointment}
            appointmentDate={initialValues && initialValues.date}
            inventoryItemsProjection={inventoryItemsProjection}
            orderItems={ordersDetails.orderItems}
            items={ordersDetails.items}
            pallets={ordersDetails.ordersPalletsAcc}
            onClose={closeUpsertAppointment}
            onSave={() => this.onSubmit(initialValues)}
            createAppointment={true}
            switchToAppointmentsTab={switchToAppointmentsTab}
            onUpdateAppointmentInventoryReviewUserId={this.onUpdateAppointmentInventoryReviewUserId}
            inventoryReviewUserName={inventoryReviewUserName}
            pickupLocation={pickupLocation}
            onNewTimeSelected={this.onNewTimeSelected}
            onAllowIssuesChange={this.onAllowIssuesChange}
            timezone={timezone}
          />
          <InventoryIssueModal
            isOpen={isInventoryIssueOpen}
            onSave={this.onSaveInventoryIssue}
            onCancel={this.onCancelInventoryIssue}
          />
        </div>
      )
    }
    if (tab === EMAIL_HIDDEN_TAB) {
      return <UpsertAppointmentFormEmailHiddenTab
        onSendEmail={this.onSendEmail}
        switchToAppointmentsTab={switchToAppointmentsTab}
        preFilledData={this.getEmailPreFilledData()}
      />
    }
    if (tab === SUMMON_SMS_TAB) {
      return <SummonSMSForm
        onSendSMS={this.onSendSMS}
        onCancel={switchToAppointmentsTab}
        data={this.getSummonSMSData()}
        loading={sendSMSIsLoading}
        error={sendSMSError}
      />
    }
  }
}

UpsertAppointmentForm.propTypes = {
  warehouse: PropTypes.any,
  doorDurations: PropTypes.any,
  user: PropTypes.object,
  users: PropTypes.object,
  editingAppointment: PropTypes.object,
  onDeleteAppointment: PropTypes.func,
  onCloseDeleteAppointment: PropTypes.func,
  isDeleteAppointmentOpen: PropTypes.bool,
  appointmentForDelete: PropTypes.number,
  switchToAppointmentsTab: PropTypes.func,
  switchToInventoryTab: PropTypes.func,
  switchToEmailHiddenTab: PropTypes.func,
  getAllSites: PropTypes.func,
  getAllBuildings: PropTypes.func,
  getAllAreas: PropTypes.func,
  getAllDoors: PropTypes.func,
  getAllCarriers: PropTypes.func,
  getAllDrivers: PropTypes.func,
  getAllAppointmentStatuses: PropTypes.func,
  getSelectedOrders: PropTypes.func,
  createAppointment: PropTypes.func,
  getInventoryItemsProjection: PropTypes.func,
  updateAppointment: PropTypes.func,
  deselectOrder: PropTypes.func,
  openEditAppointment: PropTypes.func,
  getDoorDuration: PropTypes.func,
  sendEmail: PropTypes.func,
  getAreasForBuilding: PropTypes.func,
  getDoorsForBuilding: PropTypes.func,
  setStartDate: PropTypes.func,
  setStartTime: PropTypes.func,
  setEndDate: PropTypes.func,
  setWarehouse: PropTypes.func,
  setSite: PropTypes.func,
  getAppointmentsForDoors: PropTypes.func,
  showAppointment: PropTypes.func,
  sendSMSIsLoading: PropTypes.bool,
  sendSMSErrorMessage: PropTypes.string,
  sendEmailIsLoading: PropTypes.bool,
  sendEmailErrorMessage: PropTypes.string,
  sendSMS: PropTypes.func,
  hasFormChanged: PropTypes.func,

  orders: PropTypes.object,
  tab: PropTypes.number,
  initialValues: PropTypes.object,
  subscription: PropTypes.object,
  sites: PropTypes.object,
  carriers: PropTypes.object,
  drivers: PropTypes.object,
  doors: PropTypes.object,
  areas: PropTypes.object,
  buildings: PropTypes.object,
  appointmentStatuses: PropTypes.object,
  inventoryItemsProjection: PropTypes.object,
  doorDurationRules: PropTypes.object,

  closeUpsertAppointment: PropTypes.func,
  clearRequestIsLoading: PropTypes.object,
  createAppointmentIsLoading: PropTypes.bool,
  updateAppointmentIsLoading: PropTypes.bool,
  deleteAppointment: PropTypes.func,
  clearRequest: PropTypes.func,

  userId: PropTypes.number,
  serNAme: PropTypes.string,
  allowIssuesUser: PropTypes.object,
  getAllowIssuesUser: PropTypes.func,

  carrierRequests: PropTypes.object,
  getAllCarrierRequests: PropTypes.func
}

const mapStateToProps = state => ({
  warehouse: getWarehouse(state),
  recalculateDurationFlag: getRecalculateDurationFlag(state),
  user: getLoggedUser(state),
  users: getUsers(state),
  sites: getAllSites(state),
  carriers: getAllCarriers(state),
  drivers: getAllDrivers(state),
  appointmentStatuses: getAllAppointmentStatuses(state),
  editingAppointment: getEditingAppointment(state),
  clearRequestIsLoading: getClearRequestIsLoading(state),
  createAppointmentIsLoading: getCreateAppointmentIsLoading(state),
  updateAppointmentIsLoading: getUpdateAppointmentIsLoading(state),
  buildings: getAllBuildings(state),
  areas: getAllAreas(state),
  doors: getAllDoors(state),
  orders: getSelectedOrders(state) || [],
  inventoryItemsProjection: getInventoryItemsProjection(state),
  doorDurations: getDoorDuration(state),
  sendSMSIsLoading: getSendSMSIsLoading(state),
  sendSMSErrorMessage: getSendSMSErrorMessage(state),
  sendEmailIsLoading: getSendEmailIsLoading(state),
  sendEmailErrorMessage: getSendEmailErrorMessage(state),
  userId: getUserId(state),
  serName: getUserName(state),
  allowIssuesUser: getAllowIssuesUser(state),
  carrierRequests: getAllCarrierRequests(state)
})

const mapDispatchToProps = dispatch => ({
  setRecalculateDurationFlag: flag => dispatch(AppointmentsActions.setRecalculateDurationFlag(flag)),
  getAllowIssuesUser: payload => dispatch(UsersActions.getAllowIssuesUser(payload)),
  getAllSites: () => dispatch(SitesActions.getAllSites()),
  getAllCarriers: () => dispatch(CarriersActions.getAllCarriers()),
  getAllDrivers: () => dispatch(DriversActions.getAllDrivers()),
  getInventoryItemsProjection: payload => dispatch(InventoryItemsActions.getInventoryItemsProjection(payload)),
  setStartDate: payload => dispatch(AppActions.setStartDate(payload)),
  setStartTime: payload => dispatch(AppActions.setStartTime(payload)),
  setEndDate: payload => dispatch(AppActions.setEndDate(payload)),
  setWarehouse: payload => dispatch(AppActions.setWarehouse(payload)),
  setSite: payload => dispatch(AppActions.setSite(payload)),
  getDoorsForBuilding: payload => dispatch(BuildingsActions.getDoorsForBuilding(payload)),
  getAreasForBuilding: payload => dispatch(BuildingsActions.getAreasForBuilding(payload)),
  getAppointmentsForDoors: payload => dispatch(DoorsActions.getAppointmentsForDoors(payload)),
  getAllAppointmentStatuses: () => dispatch(AppointmentsActions.getAllAppointmentStatuses()),
  deselectOrder: order => dispatch(OrdersActions.deselectOrder(order)),
  createAppointment: payload => dispatch(AppointmentsActions.createAppointment(payload)),
  updateAppointment: payload => dispatch(AppointmentsActions.updateAppointment(payload)),
  openEditAppointment: appointment => dispatch(AppointmentsActions.openEditAppointment(appointment)),
  closeUpsertAppointment: appointment => dispatch(AppointmentsActions.closeUpsertAppointment()),
  getDoorDuration: payload => dispatch(DoorsActions.getDoorDuration(payload)),
  sendEmail: payload => dispatch(EmailsActions.sendEmail(payload)),
  deleteAppointment: id => dispatch(AppointmentsActions.deleteAppointment(id)),
  clearRequest: id => dispatch(AppointmentsActions.clearRequest(id)),
  sendSMS: (to, content) => dispatch(SMSActions.sendSMS(to, SUMMON_SMS_TEMPLATE_ID, content)),
  getAllCarrierRequests: () => dispatch(DriversActions.getAllDrivers())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpsertAppointmentForm)
