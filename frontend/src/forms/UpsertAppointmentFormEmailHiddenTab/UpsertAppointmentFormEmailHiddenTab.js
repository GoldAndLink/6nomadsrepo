// findsecrets-ignore-file
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'react-datepicker/dist/react-datepicker.css'
import styled from 'styled-components'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import TextArea from '../../components/TextArea'
import { Form, Field } from 'react-final-form'

const Container = styled.div`
    display: flex;
    justify-content: center;
    width: 620px;
`

const EmailFormContainer = styled.div`
    width: 500px;
    padding: 20px;
    overflow: hidden;
`

const StyledTextInput = styled(TextInput)`
    margin-bottom: 20px;
`

const TextAreaPadded = styled(TextArea)`
    padding-top: 10px;
    border-style: solid;
    border-width: 1px;
    margin-top: 15px;
    padding-right: 0;
    padding-left: 5px;
    line-height: 1.4;
    color: #3c414e;
    font-size: 12px;
`

const ButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    flex: 1;
    padding-top: 10px;

    div.button {
        display: flex;
        align-items: flex-end;
        align-self: center;
    }
`

const CancelButton = styled(Button)`
    margin-right: 10px;
    border-style: none;
    color: #b3b9c7;
`

const SendButton = styled(Button)`

`

class UpsertAppointmentFormEmailHiddenTab extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isErrors: true
    }
  }

  validate = values => {
    const errors = {}
    if (!values.to) {
      errors.to = 'Required'
    }
    /* if (!values.from) {
      errors.from = 'Required'
    } */
    if (!values.data) {
      errors.data = 'Required'
    }

    const isErrors = !!Object.entries(errors).length

    this.setState({ isErrors })
    return errors
  }

  onCancel = e => {
    e.preventDefault()
    this.props.switchToAppointmentsTab()
  }

  render () {
    const {
      preFilledData,
      onSendEmail
    } = this.props
    return (
      <Container>
        <EmailFormContainer>
          <Form
            onSubmit={onSendEmail}
            initialValues={preFilledData}
            validate={this.validate}
            render={({ handleSubmit, submitting }) => (
              <form onSubmit={handleSubmit}>
                <Field name='to'>
                  {({ input, meta }) => (
                    <StyledTextInput
                      {...input}
                      error={meta.touched && meta.error}
                      fullwidth
                      placeholder='Email Address'
                      label='To'
                      type='email'
                    />
                  )}
                </Field>
                <Field name='data'>
                  {({ input, meta }) => (
                    <TextAreaPadded
                      {...input}
                      error={meta.touched && meta.error}
                      fullwidth
                      label='Message* (Click field to edit email)'
                      rows='15'
                    />
                  )}
                </Field>
                <ButtonContainer>
                  <div className={'button'}>
                    <CancelButton onClick={this.onCancel}>
                            Cancel
                    </CancelButton>
                  </div>
                  <div className={'button'}>
                    <SendButton primary type='submit'>
                            Send
                    </SendButton>
                  </div>
                </ButtonContainer>
              </form>
            )}
          />
        </EmailFormContainer>
      </Container>
    )
  }
}

UpsertAppointmentFormEmailHiddenTab.propTypes = {
  preFilledData: PropTypes.object,
  onSendEmail: PropTypes.func,
  switchToEmailHiddenTab: PropTypes.func
}

export default UpsertAppointmentFormEmailHiddenTab
