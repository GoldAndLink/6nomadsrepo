/* eslint-disable react/jsx-key */
import React from 'react'
import * as TestRenderer from 'react-test-renderer'

import UpsertAppointmentFormEmailHiddenTab from '../UpsertAppointmentFormEmailHiddenTab'

let renderer: TestRenderer.ReactTestRenderer
let instance: TestRenderer.ReactTestInstance

beforeAll(() => {
  renderer = TestRenderer.create(
    <UpsertAppointmentFormEmailHiddenTab
      preFilledData={{}}
      onSendEmail={() => {}}
      switchToEmailHiddenTab={() => {}}
    />
  )
  instance = renderer.root
})

describe('UpsertAppointmentFormEmailHiddenTab container', async () => {
  test('Component should render without error', () => {
    const component = TestRenderer.create(
      <UpsertAppointmentFormEmailHiddenTab
        preFilledData={{}}
        onSendEmail={() => {}}
        switchToEmailHiddenTab={() => {}}
      />
    )

    expect(component.toJSON()).toMatchSnapshot()
  })
  test('Component should render 2 TextInput', () => {
    const textInputComponents = instance.findAll(
      el => el.type === 'input'
    )

    expect(textInputComponents).toHaveLength(2)
  })
  test('Component should render 1 TextArea', () => {
    const textAreaComponents = instance.findAll(
      el => el.type === 'textarea'
    )

    expect(textAreaComponents).toHaveLength(1)
  })
})
