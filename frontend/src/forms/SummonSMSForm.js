import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Form, Field } from 'react-final-form'
import TextInput from '../components/TextInput'
import TextArea from '../components/TextArea'
import Button from '../components/Button'

const Container = styled.div`
  width: 620px;
  padding: 20px;
  overflow: hidden;
`

const StyledTextInput = styled(TextInput)`
  margin-bottom: 20px;
`

const TextAreaPadded = styled(TextArea)`
  padding-top: 10px;
  border-style: solid;
  border-width: 1px;
  margin-top: 15px;
  padding-right: 0;
  padding-left: 5px;
`

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  flex: 1;
  padding-top: 10px;
`

const CancelButton = styled(Button)`
  margin-right: 10px;
  border-style: none;
  color: #b3b9c7;
`

const Error = styled.div`
  color: red;
`

class SummonSMSForm extends React.Component {
  onCancel = e => {
    const { onCancel } = this.props
    e.preventDefault()
    onCancel && onCancel()
  }

  render () {
    const { data, onSendSMS, loading, error } = this.props

    const content =
      `Customer: ${data.customer || ''}\n` +
      `Destination: ${data.destination || ''}\n` +
      `Carrier: ${data.carrier || ''}\n` +
      `Site: ${data.site || ''}\n` +
      `Door: ${data.door || ''}\n` +
      `Time: ${data.time || ''}\n` +
      `To: ${data.to}`

    return (
      <Container>
        {error &&
          <Error>{error}</Error>
        }
        <Form
          onSubmit={onSendSMS}
          initialValues={{ to: data.to }}
          render={({ handleSubmit, submitting }) => (
            <form onSubmit={handleSubmit}>
              <Field name='to'>
                {({ input, meta }) => (
                  <StyledTextInput
                    {...input}
                    error={meta.touched && meta.error}
                    fullwidth
                    placeholder='Phone number'
                    label='To'
                  />
                )}
              </Field>
              <TextAreaPadded
                value={content}
                readOnly
                fullwidth
                rows='15'
              />
              <ButtonContainer>
                <div className={'button'}>
                  <CancelButton onClick={this.onCancel}>
                    Cancel
                  </CancelButton>
                </div>
                <div className={'button'}>
                  <Button primary type='submit' disabled={loading}>
                    Send
                  </Button>
                </div>
              </ButtonContainer>
            </form>
          )}
        />
      </Container>
    )
  }
}

SummonSMSForm.propTypes = {
  initialValues: PropTypes.object,
  onSendSMS: PropTypes.func
}

export default SummonSMSForm
