import React from 'react'
import styled from 'styled-components'
import Button from '../components/Button'
import Select from '../components/Select'
import AccountSelect from '../components/AccountSelect'
import TextInput from '../components/TextInput'
import { Form, Field } from 'react-final-form'

const validate = (values, isEditUser) => {
  const errors = {}
  if (!values.roles) {
    errors.roles = 'Required'
  }
  if (!values.firstName) {
    errors.firstName = 'Required'
  }
  if (!values.lastName) {
    errors.lastName = 'Required'
  }
  if (!values.email) {
    errors.email = 'Required'
  }
  if (!isEditUser && !values.password) {
    errors.password = 'Required'
  }

  if (values.password) {
    if (!values.password.match(/^.*[a-z].*$/)) {
      errors.password = 'must contain a lower case'
    } else if (!values.password.match(/^.*[A-Z].*$/)) {
      errors.password = 'must contain a upper case'
    } else if (!values.password.match(/^.*[!@#$%^&*()_+\-=[\]{};':"\\].*$/)) {
      errors.password = 'must contain a special character'
    } else if (values.password.length < 6) {
      errors.password = 'must be at least 6 chars long'
    }
  }

  return errors
}

const FormSelect = styled(Select)`
  margin-bottom: 20px;
`

const FormContent = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding: 18px 41px 0 41px;
  height: 400px;
  width: 300px;
`

const StyledButton = styled(Button)`
  height: 36px;
  margin-top: 20px;
`

const UpsertUserForm = ({ subscription, onSubmit, roleOptions, initialValues, label, accountOptions }) => (
  <Form
    initialValues={initialValues}
    onSubmit={onSubmit}
    validate={values => validate(values, !!initialValues)}
    subscription={subscription}
    render={({ handleSubmit, submitting }) => (
      <FormContent onSubmit={handleSubmit}>
        <Field name='roles' component='select'>
          {({ input, meta }) => (
            <FormSelect {...input}
              error={meta.touched && meta.error}
              options={roleOptions}
              placeholder="Role..."
            />
          )}
        </Field>
        <Field name='accounts' component='select'>
          {({ input, meta }) => <AccountSelect {...input}
            options={accountOptions}
            error={meta.touched && meta.error}
            placeholder="Accounts..."
          />}
        </Field>
        <Field name='firstName'>
          {({ input, meta }) => (
            <TextInput
              {...input}
              error={meta.touched && meta.error}
              fullwidth
              label='First Name'
              type='text'
              placeholder='Derik'
            />
          )}
        </Field>
        <Field name='lastName'>
          {({ input, meta }) => (
            <TextInput
              {...input}
              error={meta.touched && meta.error}
              fullwidth
              label='Last Name'
              type='text'
              placeholder='Kodama'
            />
          )}
        </Field>
        <Field name='email'>
          {({ input, meta }) => (
            <TextInput
              {...input}
              error={meta.touched && meta.error}
              fullwidth
              label='Email Address'
              type='text'
              placeholder='unknown@gmail.com'
            />
          )}
        </Field>
        <Field name='password'>
          {({ input, meta }) => (
            <TextInput
              {...input}
              error={meta.touched && meta.error}
              fullwidth
              label='Password'
              type='password'
              placeholder='Password'
            />
          )}
        </Field>
        <StyledButton primary type='submit' disabled={submitting}>{label}</StyledButton>
      </FormContent>
    )}
  />
)

export default UpsertUserForm
