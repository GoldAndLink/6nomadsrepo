import Raven from 'raven-js'
const envs = {
  local: {
    ENV: 'local',
    API_BASE: 'http://localhost:8080',
    API_KEY: '003026bbc133714df1834b8638bb496e-8f4b3d9a-e931-478d-a994-28a725159ab9',
    SENTRY_URL: ''
  },
  dev: {
    ENV: 'development',
    API_BASE: 'deleted',
    API_KEY: 'deleted',
    SENTRY_URL: 'deleted'
  },
  staging: {
    ENV: 'staging',
    API_BASE: 'deleted',
    API_KEY: 'deleted',
    SENTRY_URL: 'deleted'
  },
  prod: {
    ENV: 'production',
    API_BASE: 'deleted',
    API_KEY: 'deleted',
    SENTRY_URL: 'deleted'
  }
}

// Expected hosts:
// local  :             local.deleted
// dev    :   <name>-dev-<random>.example.com
// staging: <name>-stage-<random>.example.com
// prod   :                       example.com

const localHostnames = ['local.deleted', 'localhost', '127.0.0.1']
const developmentHostnames = [
  'https://deleted_name-dev-uqnhoqd.deleted',
  'deleted_name-dev-uqnhoqd.deleted'
]
const stagingHostnames = [
  'https://deleted_name-staging-uqnhoqd.deleted',
  'deleted_name-staging-uqnhoqd.deleted'
]
const productionHostnames = [
  'https://deleted_name-prod-uqnhoqd.deleted',
  'deleted_name-prod-uqnhoqd.deleted'
]

const calculateConfig = () => {
  const { hostname } = window.location

  if (localHostnames.indexOf(hostname) > -1) {
    return envs['local']
  } else if (developmentHostnames.indexOf(hostname) > -1) {
    return envs['dev']
  } else if (stagingHostnames.indexOf(hostname) > -1) {
    return envs['staging']
  } else if (productionHostnames.indexOf(hostname) > -1) {
    return envs['prod']
  } else {
    console.log('No matching hostname found. Using local configuration for API Endpoints.')
    return envs['local']
  }
}

const config = calculateConfig()
if (config.SENTRY_URL) {
  Raven.config(config.SENTRY_URL).install()
  Raven.setTagsContext({ environment: config.ENV })
}
export default calculateConfig()
