import { injectGlobal } from 'styled-components'
import styledNormalize from 'styled-normalize'

injectGlobal`
  ${styledNormalize}

  body {
    font-family: "Helvetica Neue", "HelveticaNeue-Light", "Helvetica Neue Light", Helvetica, Arial, "Lucida Grande", sans-serif;
  }
`

export const theme = {
  brand: '#22bf9a',
  brandDarker: '#0eb48f',
  gray: '#898989',
  lightGray: '#bbbbbb',
  lighterGray: '#f4f6f9',
  darkBackground: '#3c414e',
  orderStatuses: {
    open: {
      text: '#AAB0C0',
      background: '#F3F6F6'
    },
    scheduled: {
      text: '#0BB58E',
      background: '#D1F9D8'
    },
    cancelled: {
      text: '#4A4A4A',
      background: '#AAB0C0'
    }
  },
  appointmentStatuses: {
    draft: {
      background: '#F3F6F6',
      text: '#AAB0C0',
      opacity: 1
    },
    scheduled: {
      background: '#D2F8D8',
      text: '#0EB48F',
      opacity: 1
    },
    checkedIn: {
      background: '#FEF4C1',
      text: '#FF8100',
      opacity: 1
    },
    loading: {
      background: '#FFA747',
      text: '#FF8100',
      opacity: 1
    },
    checkedOut: {
      background: '#AAB1C0',
      text: '#4A4A4A',
      opacity: 1
    },
    inProgress: {
      background: '#d2f8d8',
      text: '#0eb48f',
      opacity: 0.5
    },
    reschedule: {
      background: '#F3F6F6',
      text: '#AAB0C0',
      opacity: 1
    },
    canceled: {
      background: '#F3F6F6',
      text: '#AAB0C0',
      opacity: 1
    },
    carrierLate: {
      text: '#FE1F3A',
      background: '#FDEDEF'
    }
  },
  requestStatuses: {
    pending: {
      background: '#fff',
      text: '#AAB0C0'
    },
    reschedule: {
      background: '#FEEAD2',
      text: '#FF861A'
    },
    canceled: {
      text: '#FE1F3A',
      background: '#FDEDEF'
    }
  }
}
