import React, { Component } from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { Droppable } from 'react-beautiful-dnd'

import AppActions from '../modules/app/actions'
import DoorsActions from '../modules/doors/actions'
import OrdersActions from '../modules/orders/actions'
import BuildingsActions from '../modules/buildings/actions'
import AppointmentsActions from '../modules/appointments/actions'
import LocationsActions from '../modules/locations/actions'
import CustomersActions from '../modules/customers/actions'

import { getAppointments, getAllAppointmentStatuses } from '../modules/appointments/selectors'
import { getAllLocations } from '../modules/locations/selectors'
import { getAllCustomers } from '../modules/customers/selectors'
import { getAllDoors, getAllAreas, getAllBuildings } from '../modules/entities/selectors'
import { getWarehouse } from '../modules/app/selectors'

import AppointmentCard from '../components/AppointmentCard'
import DeleteAppointmentModal from '../components/DeleteAppointmentModal'
import { generateDropppableId } from '../utils/utils'
import { isRequestLate } from '../utils/time'
import Select from '../components/Select'
import DatePicker from '../components/DatePicker'
import searchIcon from '../assets/images/search-icon.svg'
import closeIcon from '../assets/images/close.svg'

import { requestStatuses } from '../containers/Requests'

export const appointmentStatuses = {
  draft: 'Draft',
  scheduled: 'Scheduled',
  checkedIn: 'Checked In',
  loading: 'Loading',
  checkedOut: 'Checket Out'
}

const DEFAULT_STATUS = appointmentStatuses.draft
const DEFAULT_REQUEST_STATUS = requestStatuses.pending
const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 240px;
  z-index: 2;
`

const SearchContainer = styled.div`
  width: 240px;
  padding-bottom: 12px;
  margin-bottom: 10px;
  background-color: #C4C6CE;
`

const AppointmentsContainer = styled.div`
  flex: 1;
  overflow-y: auto;
  padding: 10px;
`

const SearchContainerHeader = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 12px;
`

const ClosePanelButton = styled.div`
  font-size: 12px;
  font-weight: bold;
  color: #3c414e;
  cursor: pointer;
`

const SearchBox = styled.div`
  background-color: #ffffff;
  margin-left: 10px;
  margin-right: 12px;
  margin-bottom: 4px;
  width: 220px;
  font-size: 85%;
  height: 38px;
  display: flex;
`

const SearchInput = styled.input`
  flex: 1;
  color: ${props => props.theme.gray};
  width: 23px;
  height: 38px;
  padding: 0 0 0 28px;
  font-size: 12px;
  border-width: 1px;
  border-color: #aab0c0;
  border-style: solid;
  background: url(${searchIcon}) no-repeat 8px center;

  &:focus {
    outline: none;
  }

  &::-webkit-input-placeholder {
    color: ${props => props.theme.lightGray};
  }
`

const AttributesSelect = styled.div`
  margin-left: -1px;
  height: 38px;
  font-size: 85%;
  width: 80px;
  border-width: 1px;
  border-color: #aab0c0;
  border-style: solid;
`

const AppointmentsStatusSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 6px;
  height: 38px;
  font-size: 85%;
`

const CustomerSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
`

const ShippingDateSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
  > div {
    height: 38px;
  }
`

const DestinationSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
`

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

`

const EmptyHackButton = styled.div`
  width: 135px;
  height: 30px;
  margin-left: 10px;
  padding: 0;
`

const ClearAll = styled.div`
  height: 14px;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  color: #3c414e;
  cursor: pointer;
  margin-left: auto;
  margin-right: 11px;
  text-align: center;
  align-self: center;
`

const StyledAppointmentCard = styled(AppointmentCard)`
  box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12);
  border-radius: 4px;
  padding: 1px;
  margin-bottom: 10px;
`

const searchAttributesSelect = [{ label: 'SO', value: 'primaryRefValue' }, { label: 'PO', value: 'PO' }, { label: 'Appt Number', value: 'id' }]

class Appointments extends Component {
  constructor (props) {
    super(props)

    this.state = {
      searchText: '',
      attributesSelect: null,
      appointmentsStatusSelect: null,
      customerSelect: null,
      shippingDateSelect: null,
      destinationSelect: null,
      isDeleteAppointmentOpen: false,
      appointmentForDelete: null,
      warehouse: props.warehouse,
      startDate: props.startDate
    }
  }

  componentDidMount = () => {
    this.props.getAppointmentsForWarehouse()
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    const { warehouse, getAppointmentsForWarehouse, startDate } = nextProps
    if (prevState.warehouse !== warehouse || !prevState.startDate.isSame(nextProps.startDate, 'day')) {
      getAppointmentsForWarehouse()
      return { warehouse, startDate }
    }
  }

  onSearchChange = e => {
    const { attributesSelect } = this.state
    const searchText = e.target.value
    this.setState({ searchText: searchText }, () => {
      if ((searchText === '' && attributesSelect) || (searchText && attributesSelect)) {
        this.requestAppointments()
      }
    })
  }

  onAttributesSelectChange = attributesSelect => {
    const { searchText } = this.state
    this.setState({ attributesSelect: attributesSelect }, () => {
      if ((searchText === '' && attributesSelect) || (searchText && attributesSelect)) {
        this.requestAppointments()
      }
    })
  }

  onAppointmentsStatusSelectChange = appointmentsStatusSelect => {
    this.setState({ appointmentsStatusSelect }, () => this.requestAppointments())
  }

  onCustomerSelectChange = customerSelect => {
    this.setState({ customerSelect }, () => this.requestAppointments())
  }

  onShippingDateSelectChange = shippingDateSelect => {
    this.setState({ shippingDateSelect }, () => this.requestAppointments())
  }

  onDestinationSelectChange = destinationSelect => {
    this.setState({ destinationSelect }, () => this.requestAppointments())
  }

  requestAppointments = () => {
    const { getAppointments } = this.props
    const { searchText, attributesSelect, appointmentsStatusSelect, customerSelect, shippingDateSelect, destinationSelect } = this.state

    let dateFrom = null
    let dateTo = null

    if (shippingDateSelect) {
      dateFrom = shippingDateSelect.subtract(1, 'day').format('L')
      dateTo = shippingDateSelect.add(2, 'day').format('L')
      // change is done inplace so we need to normalize
      shippingDateSelect.subtract(1, 'day')
    }

    getAppointments({
      searchText,
      attributesSelect,
      appointmentStatusId: appointmentsStatusSelect,
      customerId: customerSelect,
      dateFrom,
      dateTo,
      destinationId: destinationSelect
    })
  }

  clearFilters = () => {
    this.setState({
      searchText: '',
      attributesSelect: '',
      appointmentsStatusSelect: '',
      customerSelect: '',
      shippingDateSelect: '',
      destinationSelect: ''
    })
    this.onAttributesSelectChange(null)
    this.onAppointmentsStatusSelectChange(null)
    this.onCustomerSelectChange(null)
    this.onShippingDateSelectChange(null)
    this.onDestinationSelectChange(null)
  }

  onAppointmentClick (appointment) {
    if (appointment.get('door', null)) {
      const {
        setStartDate,
        setFocusAppointment,
        setSite,
        setWarehouse,
        getAppointmentsForDoors,
        getAreasForBuilding,
        getDoorsForBuilding,
        onChangeDate
      } = this.props

      const date = moment(appointment.get('date'))

      const site = appointment.get('door').get('area').get('building').get('siteId')
      const warehouse = appointment.get('door').get('area').get('buildingId')
      const startDate = date.format('L')

      const timezone = (appointment.get('door').get('area').get('building').get('timezone')) || 'UTC'

      const formattedStartTime = date.startOf('day').format('HH:mm:ss')
      // const formattedEndTime = (selectedEndTime || endTime).format('HH:mm:ss')
      const formattedStartDate = date.format('YYYY-MM-DD')
      // const formattedEndDate = (selectedEndDate || endDate).format('YYYY-MM-DD')
      const tzStart = moment.tz(
        `${formattedStartDate} ${formattedStartTime}`,
        timezone
      ).utc()
      const tzEnd = tzStart.clone().add(2, 'day')

      onChangeDate(moment(startDate))

      setStartDate({ startDate: moment(startDate) })
      setFocusAppointment(appointment)
      setSite({ site, id: site })
      setWarehouse({ warehouse: warehouse })
      getAreasForBuilding({ id: warehouse })
      getDoorsForBuilding({ id: warehouse })

      getAppointmentsForDoors({
        buildingId: warehouse,
        startTime: tzStart.format('HH:mm:ss'),
        endTime: tzEnd.format('HH:mm:ss'),
        startDate: tzStart.format('L'),
        endDate: tzEnd.format('L')
      })
    }
  }

  async onAppointmentEdit (appointment) {
    await this.props.closeUpsertAppointment()
    await this.props.setSelectedOrders(appointment.get('orders'))
    await this.props.openEditAppointment(appointment)
  }

  onAppointmentDelete (appointment) {
    this.setState({
      isDeleteAppointmentOpen: true,
      appointmentForDelete: appointment
    })
  }

  onConfirmAppointmentDelete = () => {
    this.props.deleteAppointment(this.state.appointmentForDelete)
    this.onCloseDeleteAppointment()
  }

  onCloseDeleteAppointment = () => {
    this.setState({
      isDeleteAppointmentOpen: false,
      appointmentForDelete: null
    })
  }

  getRequestStatus = (carrierRequest, appointmentStatus) => {
    const carrierRequestStatus = carrierRequest ? carrierRequest.get('status') : null
    if (carrierRequestStatus === requestStatuses.canceled && appointmentStatus && (appointmentStatus.get('name') === appointmentStatuses.draft || appointmentStatus.get('name') === appointmentStatuses.scheduled)) {
      return requestStatuses.canceled
    } else if (carrierRequestStatus === requestStatuses.reschedule && appointmentStatus && (appointmentStatus.get('name') === appointmentStatuses.draft || appointmentStatus.get('name') === appointmentStatuses.scheduled)) {
      return requestStatuses.reschedule
    } else {
      return carrierRequestStatus || DEFAULT_REQUEST_STATUS
    }
  }

  render () {
    const {
      searchText,
      attributesSelect,
      appointmentsStatusSelect,
      customerSelect,
      shippingDateSelect,
      destinationSelect,
      isDeleteAppointmentOpen
    } = this.state

    const {
      appointments,
      locations = [],
      customers = [],
      appointmentStatuses = [],
      onClose,
      buildings,
      doors,
      areas,
      getAllLocations,
      getAllCustomers,
      warehouse
    } = this.props

    const building = buildings.find(b => b.get('id') === warehouse)

    const searchAttributesAppointmentStatus = appointmentStatuses ? appointmentStatuses.map(appointmentStatus => ({ label: appointmentStatus.get('name'), value: appointmentStatus.get('id') })) : []
    // let's not do that the array is HUGE
    const searchAttributesCustomer = customers ? customers.map(customer => ({ label: customer.get('name'), value: customer.get('id') })) : []
    // we probably need to do a "as you type search" here or something like that
    const searchAttributesDestination = locations
      ? locations.map(location => ({
        label: location.get('name'),
        value: location.get('id')
      })).toArray()
      : []

    return (
      <Droppable droppableId={generateDropppableId('appts')} isDropDisabled={true}>
        {(provided, snapshot) => (
          <Container
            innerRef={provided.innerRef}
            {...provided.droppableProps}>
            <DeleteAppointmentModal
              isOpen={isDeleteAppointmentOpen}
              onConfirmDelete={this.onConfirmAppointmentDelete}
              onClose={this.onCloseDeleteAppointment}
            />
            <SearchContainer>
              <SearchContainerHeader>
                <ClosePanelButton onClick={onClose}>
                  <img src={closeIcon} alt="close" />
                </ClosePanelButton>
              </SearchContainerHeader>
              <SearchBox>
                <SearchInput placeholder="Search" value={searchText} onChange={this.onSearchChange} />
                <AttributesSelect>
                  <Select
                    isSearchable={false}
                    options={searchAttributesSelect}
                    placeholder='Select'
                    value={attributesSelect}
                    onChange={value => this.onAttributesSelectChange(value)}
                  />
                </AttributesSelect>
              </SearchBox>
              <AppointmentsStatusSelect>
                <Select
                  isClearable
                  options={searchAttributesAppointmentStatus}
                  placeholder='Appointment Status'
                  value={appointmentsStatusSelect}
                  onChange={this.onAppointmentsStatusSelectChange}
                />
              </AppointmentsStatusSelect>
              <CustomerSelect>
                <Select
                  disableDropdownIndicator
                  disableEmptyOptions
                  isClearable
                  options={searchAttributesCustomer}
                  placeholder='Customer'
                  value={customerSelect || null}
                  onInputChange={payload => { payload && payload.length > 1 && getAllCustomers({ name: payload }) }}
                  onChange={this.onCustomerSelectChange}
                />
              </CustomerSelect>
              <ShippingDateSelect>
                <DatePicker
                  showIcon
                  SearchAppointmentDatePicker
                  dateFormat='LL'
                  placeholderText='Shipping Date'
                  selected={shippingDateSelect || null}
                  onChange={this.onShippingDateSelectChange}
                />
              </ShippingDateSelect>
              <DestinationSelect>
                <Select
                  disableDropdownIndicator
                  disableEmptyOptions
                  isClearable
                  options={searchAttributesDestination}
                  placeholder='Destination'
                  value={destinationSelect}
                  onInputChange={payload => { payload && payload.length > 1 && getAllLocations({ name: payload }) }}
                  onChange={this.onDestinationSelectChange}
                />
              </DestinationSelect>
              <ButtonsContainer>
                <EmptyHackButton />
                <ClearAll onClick={this.clearFilters}>
                  Clear all
                </ClearAll>
              </ButtonsContainer>
            </SearchContainer>
            <AppointmentsContainer>
              {appointments && appointments.map((appointment, index) => {
                const appointmentStatus = appointmentStatuses ? appointmentStatuses.find(as => as.get('id') === appointment.get('appointmentStatusId')) : null
                const carrierRequest = appointment.get('carrierRequests').valueSeq().first()

                // handle date timezone locally. We can optionally handle local timezone data retrieval in the backend
                if (shippingDateSelect && building &&
                  shippingDateSelect.format('L') === moment.tz(appointment.get('date'), building.get('timezone')).format('L')) {
                  return (
                    <StyledAppointmentCard
                      appointmentStatuses={appointmentStatuses}
                      buildings={buildings}
                      doors={doors}
                      areas={areas}
                      key={appointment.get('id')}
                      appointment={appointment}
                      onClick={() => this.onAppointmentClick(appointment)}
                      onEdit={() => this.onAppointmentEdit(appointment)}
                      onDelete={() => this.onAppointmentDelete(appointment)}
                      appointmentStatus={appointmentStatus ? appointmentStatus.get('name') : DEFAULT_STATUS}
                      isRequestLate={isRequestLate(carrierRequest, appointment, appointmentStatus)}
                      requestStatus={this.getRequestStatus(carrierRequest, appointmentStatus)}
                    />
                  )
                } else if (!shippingDateSelect) {
                  return (
                    <StyledAppointmentCard
                      appointmentStatuses={appointmentStatuses}
                      buildings={buildings}
                      doors={doors}
                      areas={areas}
                      key={appointment.get('id')}
                      appointment={appointment}
                      onClick={() => this.onAppointmentClick(appointment)}
                      onEdit={() => this.onAppointmentEdit(appointment)}
                      onDelete={() => this.onAppointmentDelete(appointment)}
                      appointmentStatus={appointmentStatus ? appointmentStatus.get('name') : DEFAULT_STATUS}
                      isRequestLate={isRequestLate(carrierRequest, appointment, appointmentStatus)}
                      requestStatus={this.getRequestStatus(carrierRequest, appointmentStatus)}
                    />
                  )
                }
              }
              )}
            </AppointmentsContainer>
          </Container>
        )}
      </Droppable>
    )
  }
}

Appointments.propTypes = {
  warehouse: PropTypes.any,
  onChangeDate: PropTypes.func,
  doors: PropTypes.object,
  areas: PropTypes.object,
  buildings: PropTypes.object,
  getAppointments: PropTypes.func,
  deleteAppointment: PropTypes.func,
  removeOrderFromAppointment: PropTypes.func,
  getAllAppointmentStatuses: PropTypes.func,
  setSelectedOrders: PropTypes.func,
  openEditAppointment: PropTypes.func,
  closeUpsertAppointment: PropTypes.func,

  appointments: PropTypes.object,
  customers: PropTypes.object,
  locations: PropTypes.object,
  appointmentStatuses: PropTypes.object
}

const mapStateToProps = state => ({
  warehouse: getWarehouse(state),
  customers: getAllCustomers(state),
  locations: getAllLocations(state),
  doors: getAllDoors(state),
  areas: getAllAreas(state),
  buildings: getAllBuildings(state),
  appointments: getAppointments(state),
  appointmentStatuses: getAllAppointmentStatuses(state)
})

const mapDispatchToProps = dispatch => ({
  getAppointments: (payload = {}) => dispatch(AppointmentsActions.getAppointments(payload)),
  setStartDate: payload => dispatch(AppActions.setStartDate(payload)),
  setFocusAppointment: payload => dispatch(AppActions.setFocusAppointment(payload)),
  setStartTime: payload => dispatch(AppActions.setStartTime(payload)),
  setEndDate: payload => dispatch(AppActions.setEndDate(payload)),
  setWarehouse: payload => dispatch(AppActions.setWarehouse(payload)),
  setSite: payload => dispatch(AppActions.setSite(payload)),
  getDoorsForBuilding: payload => dispatch(BuildingsActions.getDoorsForBuilding(payload)),
  getAreasForBuilding: payload => dispatch(BuildingsActions.getAreasForBuilding(payload)),
  getAppointmentsForDoors: payload => dispatch(DoorsActions.getAppointmentsForDoors(payload)),
  getAppointmentsForWarehouse: payload => dispatch(AppointmentsActions.getAppointmentsForWarehouse(payload)),
  deleteAppointment: appointment => dispatch(AppointmentsActions.deleteAppointment(appointment.get('id'))),
  removeOrderFromAppointment: (appointmentId, orderId) => dispatch(AppointmentsActions.removeOrderFromAppointment(appointmentId, orderId)),
  setSelectedOrders: orders => dispatch(OrdersActions.setSelectedOrders(orders)),
  openEditAppointment: appointment => dispatch(AppointmentsActions.openEditAppointment(appointment)),
  closeUpsertAppointment: appointment => dispatch(AppointmentsActions.closeUpsertAppointment(appointment)),
  getAllLocations: payload => dispatch(LocationsActions.getAllLocations(payload)),
  getAllCustomers: payload => dispatch(CustomersActions.getAllCustomers(payload))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Appointments)
