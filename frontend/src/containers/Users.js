import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import Table, { Column } from '../components/Table'
import Button from '../components/Button'
import Select from '../components/Select'
import AccountSelect from '../components/AccountSelect'
import Modal from '../components/Modal'
import UpsertUserForm from '../forms/UpsertUserForm'
import Tag from '../components/Tag'
import UsersActions from '../modules/users/actions'
import { getUsers, getRoles, getAccounts } from '../modules/users/selectors'
import userIcon from '../assets/images/user-icon.svg'
import searchIcon from '../assets/images/search-icon.svg'
import editIcon from '../assets/images/edit-icon.svg'
import deleteIcon from '../assets/images/delete-icon.svg'

import { getAllBuildings } from '../modules/entities/selectors'

const customModalStyles = {
  overlay: {
    backgroundColor: 'rgba(60, 65, 78, 0.6)',
    zIndex: 2000
  }
}

const Container = styled.div`
  flex: 1;
  padding: 20px;
  font-size: 12px;
  font-weight: 300;
  color: ${props => props.theme.gray};
`

const TopNavigation = styled.div`
  display: flex;
  border-bottom: 1px solid ${props => props.theme.lighterGray};
  padding-bottom: 10px;
  margin-bottom: 10px;
`

const SearchFilters = styled.div`
  flex: 1;
`

const SearchBox = styled.div`
  display: flex;
`

const SearchInput = styled.input`
  color: ${props => props.theme.gray};
  border: solid 1px #ebebeb;
  height: 36px;
  padding: 0px 12px 0px 38px;
  background: url(${searchIcon}) no-repeat 12px center;

  &:focus {
    outline: none;
  }

  &::-webkit-input-placeholder {
    color: ${props => props.theme.lightGray};
  }
`

const RoleSelect = styled.div`
  margin-left: -1px;
  width: 120px;
`

const RoleTags = styled.div`
  display: flex;
  align-items: center
  margin: 0px 5px;
`

const ClearAll = styled.div`
  cursor: pointer;
  font-weight: 700;
  margin: 0px 5px;
`

const UsersTable = styled(Table)`
  width: 100%;
  th {
    padding: 10px;
  }

  td {
    padding: 10px;
  }
`

const EditDeleteIcons = styled.div`
  display: flex;
  justify-content: center;
  img {
    cursor: pointer;
    margin: 0px 10px;
  }
`
const OnButton = styled.button`
  cursor: pointer;
  padding: 10px;
  border-radius: 3px 0 0 3px;
  color: ${props => props.theme.gray};
  &:disabled {
    background-color: ${props => props.theme.lightGray};
    color: white;
  }
`
const OffButton = styled.button`
  cursor: pointer;
  padding: 10px;
  border-radius: 0 3px 3px 0;
  margin-left: -1px;
  color: ${props => props.theme.gray};
  &:disabled {
    background-color: ${props => props.theme.lightGray};
    color: white;
  }
`

class Users extends Component {
  state = {
    searchText: '',
    selectedRoles: [],
    isOpen: false,
    isEditUser: false,
    initialValues: {},
    user: null
  }

  openModal = () => {
    this.setState({
      isOpen: true
    })
  }

  closeModal = () => {
    this.setState({
      isOpen: false
    })
  }

  createUser = () => {
    this.setState({ isEditUser: false })
    this.openModal()
  }

  editUser = user => {
    const userRole = user.get('roles').first()
    const userAccounts = user.get('accounts') && user.get('accounts').toJS()

    const initialValues = {
      id: user.get('id'),
      roles: userRole && userRole.get('id'),
      accounts: userAccounts,
      firstName: user.get('firstName'),
      lastName: user.get('lastName'),
      email: user.get('email')
    }

    this.setState({ isEditUser: true, user, initialValues })
    this.openModal()
  }

  onSubmit = data => {
    const { roles, accounts, firstName, lastName, email } = data
    const { createUser, updateUser } = this.props
    const { user } = this.state
    if (data.id) {
      const userJSON = user.toJSON()
      userJSON.roles = [{
        id: roles
      }]
      userJSON.accounts = accounts
      userJSON.firstName = firstName
      userJSON.lastName = lastName
      userJSON.email = email
      updateUser(userJSON)
    } else {
      createUser(data)
    }
    this.closeModal()
  }

  componentDidMount () {
    this.props.getUsers()
    this.props.getAccounts()
  }

  onSearchChange = e => {
    this.setState({ searchText: e.target.value })
  }

  onClickActivationButon = (id, active) => {
    const { changeUserActivation } = this.props
    changeUserActivation({ id, active })
  }

  deleteUser = id => {
    const { deleteUser } = this.props

    const r = window.confirm('Are you sure you want to delete?')

    if (r === true) {
      deleteUser({ id })
    }
  }

  onRoleSelect = role => {
    const { selectedRoles } = this.state
    if (!selectedRoles.includes(role)) {
      this.setState(state => ({
        ...state,
        selectedRoles: [...selectedRoles, role]
      }))
    }
  }

  onRemoveRole = role => {
    this.setState(state => ({
      ...state,
      selectedRoles: state.selectedRoles.filter(r => r !== role)
    }))
  }

  onClearRoles = () => {
    this.setState({ selectedRoles: [] })
  }

  updateDefaultBuilding (user, building) {
    const userJSON = user.toJSON()
    userJSON.defaultBuildingId = building
    this.props.updateUser(userJSON)
  }

  updateUserAccounts (user, accounts) {
    const userJSON = user.toJSON()
    userJSON.accounts = accounts.map(account => ({ id: account.value }))
    this.props.updateUser(userJSON)
  }

  render () {
    const { searchText, selectedRoles, isOpen, isEditUser, initialValues } = this.state
    const { users, roles, buildings, accounts } = this.props

    const roleOptions = roles
      ? roles.map(role => ({
        label: role.get('name'),
        value: role.get('id')
      }))
      : []

    const buildingOptions = buildings
      ? buildings.toArray().map(building => ({
        label: building.get('name'),
        value: building.get('id')
      }))
      : []

    const accountOptions = accounts
      ? accounts.toArray().map(account => ({
        label: account.get('name'),
        value: account.get('id')
      }))
      : []

    const filteredUsers =
      users &&
      users
        .filter(
          user =>
            !searchText ||
            user.get('firstName').indexOf(searchText) >= 0 ||
            user.get('lastName').indexOf(searchText) >= 0 ||
            user.get('email').indexOf(searchText) >= 0
        )
        .filter(user => {
          if (!selectedRoles.length) {
            return true
          } else {
            const roles = user.get('roles') || []
            return roles.some(role => selectedRoles.includes(role.get('id')))
          }
        })
    return (
      <Container>
        <Modal
          title={isEditUser ? 'Update User' : 'Create User'}
          isOpen={isOpen}
          onRequestClose={this.closeModal}
          styles={customModalStyles}
        >
          <UpsertUserForm
            initialValues={isEditUser ? initialValues : {}}
            onSubmit={this.onSubmit}
            subscription={{ submitting: true, pristine: true }}
            accountOptions={accountOptions}
            roleOptions={roleOptions}
            label={isEditUser ? 'Update User' : 'Create User'}
          />
        </Modal>
        <TopNavigation>
          <SearchFilters>
            <SearchBox>
              <SearchInput
                placeholder='Search'
                value={searchText}
                onChange={this.onSearchChange}
              />
              <RoleSelect>
                <Select
                  options={roleOptions}
                  placeholder='Role'
                  value={null}
                  onChange={this.onRoleSelect}
                />
              </RoleSelect>
              <RoleTags>
                {selectedRoles
                  .map(roleId => roles.find(r => r.get('id') === roleId))
                  .map(role => (
                    <Tag
                      key={role.get('id')}
                      label={role.get('name')}
                      onClose={() => this.onRemoveRole(role.get('id'))}
                    />
                  ))}
                {selectedRoles.length > 0 &&
                  <ClearAll onClick={() => this.onClearRoles()}>
                    Clear all
                  </ClearAll>}
              </RoleTags>
            </SearchBox>
          </SearchFilters>
          <div>
            <Button onClick={this.createUser}>Create User +</Button>
          </div>
        </TopNavigation>
        <UsersTable data={filteredUsers}>
          <Column
            title='User'
            content={user => <img src={userIcon} alt='User avatar' />}
          />
          <Column title='First Name' content={user => user.get('firstName')} />
          <Column title='Last Name' content={user => user.get('lastName')} />
          <Column
            title='Role(s) / Assignment(s)'
            content={user => {
              const roles = user.get('roles')
              if (roles) {
                return roles.map(role => role.get('name'))
              }
            }}
          />
          <Column
            content={user => (
              <AccountSelect
                options={accountOptions}
                value={user.get('accounts') && user.get('accounts').toJS()}
                onChange={accounts => this.updateUserAccounts(user, accounts)}
              />
            )}
          />
          <Column title='Email' content={user => user.get('email')} />
          <Column
            title='Default Building'
            content={user => (
              <Select
                options={buildingOptions}
                placeholder='Default building'
                value={user.get('defaultBuildingId')}
                onChange={building => this.updateDefaultBuilding(user, building)}
              />
            )}
          />
          <Column
            title='Activation'
            content={user => (
              <div>
                <OnButton
                  disabled={user.get('active')}
                  onClick={() =>
                    this.onClickActivationButon(user.get('id'), true)}
                >
                  On
                </OnButton>
                <OffButton
                  disabled={!user.get('active')}
                  onClick={() =>
                    this.onClickActivationButon(user.get('id'), false)}
                >
                  Off
                </OffButton>
              </div>
            )}
          />
          <Column
            title='Edit / Delete'
            content={user => (
              <EditDeleteIcons>
                <img
                  src={editIcon}
                  alt='Edit'
                  onClick={() => this.editUser(user)}
                />
                <img
                  src={deleteIcon}
                  alt='Delete'
                  onClick={() => this.deleteUser(user.get('id'))}
                />
              </EditDeleteIcons>
            )}
          />
        </UsersTable>
      </Container>
    )
  }
}

Users.propTypes = {
  getUsers: PropTypes.func,
  getRoles: PropTypes.func,
  getAccounts: PropTypes.func,
  createUser: PropTypes.func,
  changeUserActivation: PropTypes.func,
  deleteUser: PropTypes.func,
  users: PropTypes.object,
  roles: PropTypes.object,
  accounts: PropTypes.object,
  updateUser: PropTypes.func,
  buildings: PropTypes.object
}

const mapStateToProps = state => ({
  users: getUsers(state),
  roles: getRoles(state),
  accounts: getAccounts(state),
  buildings: getAllBuildings(state)
})

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(UsersActions.getUsers()),
  getAccounts: () => dispatch(UsersActions.getAccounts()),
  changeUserActivation: payload =>
    dispatch(UsersActions.changeUserActivation(payload)),
  deleteUser: payload => dispatch(UsersActions.deleteUser(payload)),
  createUser: payload => dispatch(UsersActions.createUser(payload)),
  updateUser: payload => dispatch(UsersActions.updateUser(payload))
})

export default connect(mapStateToProps, mapDispatchToProps)(Users)
