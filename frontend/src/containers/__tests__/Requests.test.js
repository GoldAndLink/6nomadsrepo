/* eslint-disable react/jsx-key */
import React from 'react'
import { Provider } from 'react-redux'
import renderer from 'react-test-renderer'

import Requests from '../Requests'

import createStore from '../../store'
const { store } = createStore()

describe('Request container', async () => {
  test('Component should render without error', () => {
    const component = renderer.create(
      <Provider store={store}>
        <Requests />
      </Provider>
    )

    expect(component.toJSON()).toMatchSnapshot()
  })
})
