import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { CSVLink } from 'react-csv'

import ReportsActions from '../modules/reports/actions'

import {
  getSummary,
  getSummaryYesterday,
  getTodayAppointmentsSummary,
  getTodayAppointments,
  getYesterdayAppointments,
  getLastWeekAppointments,
  getSummaryCustomerYesterday,
  getSummaryCustomerLastWeek
} from '../modules/reports/selectors'

const Container = styled.div`
  overflow-y: scroll;
  height: 100%;
  width: 100%;
`

const ReportContainer = styled.div`
  background-color: #ffffff;
  margin-bottom: 30px;
  padding-bottom: 10px;
`

const Header = styled.h4`
  margin-left: 33px;
  font-size: 18px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const ReportHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  margin: 10px 20px 10px 20px;
  padding-top: 10px;
  font-size: 16px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
`

const ColumnHeader = styled.th`
  text-align: left;
  padding: 8px;
  font-size: 12px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #61c9b5;
  background-color: #d2f8d8;
`

const ColumnRow = styled.td`
  text-align: left;
  padding: 8px;
`

const Row = styled.tr`
  font-size: 12px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const NoDataText = styled.div`
  font-size: 12px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
  margin: 10px 0 0 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
`

const CSVLinkStyled = styled(CSVLink)`
  font-size: 12px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const TodaySummary = ({ summary, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Summary - 6am to now <CSVLinkStyled filename={'report.csv'} data={generateCSV(summary)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>DATE</ColumnHeader>
          <ColumnHeader>TIME</ColumnHeader>
          <ColumnHeader>WAREHOUSE</ColumnHeader>
          <ColumnHeader>TRUCKS</ColumnHeader>
          <ColumnHeader>TOTAL CASES</ColumnHeader>
          <ColumnHeader>TOTAL ORDERS</ColumnHeader>
          <ColumnHeader>TOTAL SKUS</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {summary && summary.map((s, key) => (
          <Row key={s.get('warehouse')}>
            <ColumnRow>{key + 1}</ColumnRow>
            <ColumnRow>{s.get('date')}</ColumnRow>
            <ColumnRow>{s.get('time')}</ColumnRow>
            <ColumnRow>{s.get('warehouse')}</ColumnRow>
            <ColumnRow>{s.get('trucks')}</ColumnRow>
            <ColumnRow>{s.get('totalCases')}</ColumnRow>
            <ColumnRow>{s.get('totalOrders')}</ColumnRow>
            <ColumnRow>{s.get('totalSkus')}</ColumnRow>
          </Row>
        ))
        }
      </tbody>
    </Table>
    {(!summary || summary.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

TodaySummary.propTypes = {
  summary: PropTypes.object,
  generateCSV: PropTypes.func
}

const YesterdaySummary = ({ summaryYesterday, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Summary - previous day <CSVLinkStyled filename={'report.csv'} data={generateCSV(summaryYesterday)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>WAREHOUSE</ColumnHeader>
          <ColumnHeader>TRUCKS</ColumnHeader>
          <ColumnHeader>TOTAL CASES</ColumnHeader>
          <ColumnHeader>TOTAL ORDERS</ColumnHeader>
          <ColumnHeader>AVG LOAD TIME</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {summaryYesterday && summaryYesterday.map((s, key) => (
          <Row key={s.get('warehouse')}>
            <ColumnRow>{key + 1}</ColumnRow>
            <ColumnRow>{s.get('warehouse')}</ColumnRow>
            <ColumnRow>{s.get('trucks')}</ColumnRow>
            <ColumnRow>{s.get('totalCases')}</ColumnRow>
            <ColumnRow>{s.get('totalOrders')}</ColumnRow>
            <ColumnRow>{s.get('averageLoadTime')}</ColumnRow>
          </Row>
        ))
        }
      </tbody>
    </Table>
    {(!summaryYesterday || summaryYesterday.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

YesterdaySummary.propTypes = {
  summaryYesterday: PropTypes.object,
  generateCSV: PropTypes.func
}

const TodayAppointmentsSummary = ({ todayAppointmentsSummary, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Today&apos;s - Appointment Summary <CSVLinkStyled filename={'report.csv'} data={generateCSV(todayAppointmentsSummary)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>WAREHOUSE</ColumnHeader>
          <ColumnHeader>DATE</ColumnHeader>
          <ColumnHeader>TIME</ColumnHeader>
          <ColumnHeader>TRUCKS</ColumnHeader>
          <ColumnHeader>TOTAL ORDERS</ColumnHeader>
          <ColumnHeader>TOTAL SKUS</ColumnHeader>
          <ColumnHeader>TOTAL CASES</ColumnHeader>
          <ColumnHeader>TOTAL WEIGHT</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {todayAppointmentsSummary && todayAppointmentsSummary.map((s, key) => (
          <Row key={s.get('warehouse')}>
            <ColumnRow>{key + 1}</ColumnRow>
            <ColumnRow>{s.get('warehouse')}</ColumnRow>
            <ColumnRow>{s.get('date')}</ColumnRow>
            <ColumnRow>{s.get('time')}</ColumnRow>
            <ColumnRow>{s.get('trucks')}</ColumnRow>
            <ColumnRow>{s.get('totalOrders')}</ColumnRow>
            <ColumnRow>{s.get('totalSkus')}</ColumnRow>
            <ColumnRow>{s.get('totalCases')}</ColumnRow>
            <ColumnRow>{s.get('totalWeight')}</ColumnRow>
          </Row>
        ))
        }
      </tbody>
    </Table>
    {(!todayAppointmentsSummary || todayAppointmentsSummary.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

TodayAppointmentsSummary.propTypes = {
  todayAppointmentsSummary: PropTypes.object,
  generateCSV: PropTypes.func
}

const TodayAppointments = ({ todayAppointments, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Today&apos;s - Appointments <CSVLinkStyled filename={'report.csv'} data={generateCSV(todayAppointments)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>WAREHOUSE</ColumnHeader>
          <ColumnHeader>DATE</ColumnHeader>
          <ColumnHeader>TIME</ColumnHeader>
          <ColumnHeader>ORDERS</ColumnHeader>
          <ColumnHeader>CUSTOMERS</ColumnHeader>
          <ColumnHeader>DESTINATION</ColumnHeader>
          <ColumnHeader>CARRIER</ColumnHeader>
          <ColumnHeader>SKUS</ColumnHeader>
          <ColumnHeader>CASES</ColumnHeader>
          <ColumnHeader>WEIGHT</ColumnHeader>
          <ColumnHeader>CHECK IN TIME</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {todayAppointments && todayAppointments.map((a, key) => (
          <Row key={a.get('warehouse')}>
            <ColumnRow>{key + 1}</ColumnRow>
            <ColumnRow>{a.get('warehouse')}</ColumnRow>
            <ColumnRow>{a.get('date')}</ColumnRow>
            <ColumnRow>{a.get('time')}</ColumnRow>
            <ColumnRow>{a.get('orders')}</ColumnRow>
            <ColumnRow>{a.get('customers')}</ColumnRow>
            <ColumnRow>{a.get('destination')}</ColumnRow>
            <ColumnRow>{a.get('carrier')}</ColumnRow>
            <ColumnRow>{a.get('skus')}</ColumnRow>
            <ColumnRow>{a.get('cases')}</ColumnRow>
            <ColumnRow>{a.get('weight')}</ColumnRow>
            <ColumnRow>{a.get('checkInTime')}</ColumnRow>
          </Row>
        ))
        }
      </tbody>
    </Table>
    {(!todayAppointments || todayAppointments.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

TodayAppointments.propTypes = {
  todayAppointments: PropTypes.object,
  generateCSV: PropTypes.func
}

const LastWeekAppointments = ({ lastWeekAppointments, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Last Week&apos;s - Appointments <CSVLinkStyled filename={'report.csv'} data={generateCSV(lastWeekAppointments)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>WAREHOUSE</ColumnHeader>
          <ColumnHeader>DATE</ColumnHeader>
          <ColumnHeader>TIME</ColumnHeader>
          <ColumnHeader>ORDERS</ColumnHeader>
          <ColumnHeader>CUSTOMERS</ColumnHeader>
          <ColumnHeader>DESTINATION</ColumnHeader>
          <ColumnHeader>CARRIER</ColumnHeader>
          <ColumnHeader>SKUS</ColumnHeader>
          <ColumnHeader>CASES</ColumnHeader>
          <ColumnHeader>WEIGHT</ColumnHeader>
          <ColumnHeader>CHECK IN TIME</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {lastWeekAppointments && lastWeekAppointments.map((a, key) => (
          <Row key={a.get('warehouse')}>
            <ColumnRow>{key + 1}</ColumnRow>
            <ColumnRow>{a.get('warehouse')}</ColumnRow>
            <ColumnRow>{a.get('date')}</ColumnRow>
            <ColumnRow>{a.get('time')}</ColumnRow>
            <ColumnRow>{a.get('orders')}</ColumnRow>
            <ColumnRow>{a.get('customers')}</ColumnRow>
            <ColumnRow>{a.get('destination')}</ColumnRow>
            <ColumnRow>{a.get('carrier')}</ColumnRow>
            <ColumnRow>{a.get('skus')}</ColumnRow>
            <ColumnRow>{a.get('cases')}</ColumnRow>
            <ColumnRow>{a.get('weight')}</ColumnRow>
            <ColumnRow>{a.get('checkInTime')}</ColumnRow>
          </Row>
        ))
        }
      </tbody>
    </Table>
    {(!lastWeekAppointments || lastWeekAppointments.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

LastWeekAppointments.propTypes = {
  lastWeekAppointments: PropTypes.object,
  generateCSV: PropTypes.func
}

const YesterdayAppointments = ({ yesterdayAppointments, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Yesterday&apos;s - Appointments <CSVLinkStyled filename={'report.csv'} data={generateCSV(yesterdayAppointments)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>WAREHOUSE</ColumnHeader>
          <ColumnHeader>DATE</ColumnHeader>
          <ColumnHeader>TIME</ColumnHeader>
          <ColumnHeader>ORDERS</ColumnHeader>
          <ColumnHeader>CUSTOMERS</ColumnHeader>
          <ColumnHeader>DESTINATION</ColumnHeader>
          <ColumnHeader>CARRIER</ColumnHeader>
          <ColumnHeader>SKUS</ColumnHeader>
          <ColumnHeader>CASES</ColumnHeader>
          <ColumnHeader>WEIGHT</ColumnHeader>
          <ColumnHeader>CHECK IN TIME</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {yesterdayAppointments && yesterdayAppointments.map((a, key) => (
          <Row key={a.get('warehouse')}>
            <ColumnRow>{key + 1}</ColumnRow>
            <ColumnRow>{a.get('warehouse')}</ColumnRow>
            <ColumnRow>{a.get('date')}</ColumnRow>
            <ColumnRow>{a.get('time')}</ColumnRow>
            <ColumnRow>{a.get('orders')}</ColumnRow>
            <ColumnRow>{a.get('customers')}</ColumnRow>
            <ColumnRow>{a.get('destination')}</ColumnRow>
            <ColumnRow>{a.get('carrier')}</ColumnRow>
            <ColumnRow>{a.get('skus')}</ColumnRow>
            <ColumnRow>{a.get('cases')}</ColumnRow>
            <ColumnRow>{a.get('weight')}</ColumnRow>
            <ColumnRow>{a.get('checkInTime')}</ColumnRow>
          </Row>
        ))
        }
      </tbody>
    </Table>
    {(!yesterdayAppointments || yesterdayAppointments.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

YesterdayAppointments.propTypes = {
  yesterdayAppointments: PropTypes.object,
  generateCSV: PropTypes.func
}

const CustomerSummaryPreviousDay = ({ summaryCustomerYesterday, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Summary by Customer, Previous Day <CSVLinkStyled filename={'report.csv'} data={generateCSV(summaryCustomerYesterday)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>CUSTOMER</ColumnHeader>
          <ColumnHeader>TRUCKS</ColumnHeader>
          <ColumnHeader>CASES</ColumnHeader>
          <ColumnHeader>ORDERS</ColumnHeader>
          <ColumnHeader>AVG LOAD TIME</ColumnHeader>
          <ColumnHeader>NO SHOWS</ColumnHeader>
          <ColumnHeader>OVER 2 HOUR LOAD TIMES</ColumnHeader>
          <ColumnHeader>OVER 4 HOUR LOAD TIMES</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {
          summaryCustomerYesterday && summaryCustomerYesterday.map((s, key) => (
            <Row key={s.get('customer')}>
              <ColumnRow>{key + 1}</ColumnRow>
              <ColumnRow>{s.get('customer')}</ColumnRow>
              <ColumnRow>{s.get('trucks')}</ColumnRow>
              <ColumnRow>{s.get('totalCases')}</ColumnRow>
              <ColumnRow>{s.get('totalOrders')}</ColumnRow>
              <ColumnRow>{s.get('averageLoadTime')}</ColumnRow>
              <ColumnRow>{s.get('noShows')}</ColumnRow>
              <ColumnRow>{s.get('overTwoHoursLoad')}</ColumnRow>
              <ColumnRow>{s.get('overFourHoursLoad')}</ColumnRow>
            </Row>
          ))
        }
      </tbody>
    </Table>
    {(!summaryCustomerYesterday || summaryCustomerYesterday.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

CustomerSummaryPreviousDay.propTypes = {
  summaryCustomerYesterday: PropTypes.object,
  generateCSV: PropTypes.func
}

const CustomerSummaryPreviousWeek = ({ summaryCustomerLastWeek, generateCSV }) => (
  <ReportContainer>
    <ReportHeader> Summary by Customer, Previous Week <CSVLinkStyled filename={'report.csv'} data={generateCSV(summaryCustomerLastWeek)}>DOWNLOAD CSV</CSVLinkStyled> </ReportHeader>
    <Table>
      <thead>
        <Row>
          <ColumnHeader></ColumnHeader>
          <ColumnHeader>CUSTOMER</ColumnHeader>
          <ColumnHeader>TRUCKS</ColumnHeader>
          <ColumnHeader>CASES</ColumnHeader>
          <ColumnHeader>ORDERS</ColumnHeader>
          <ColumnHeader>AVG LOAD TIME</ColumnHeader>
          <ColumnHeader>NO SHOWS</ColumnHeader>
          <ColumnHeader>OVER 2 HOUR LOAD TIMES</ColumnHeader>
          <ColumnHeader>OVER 4 HOUR LOAD TIMES</ColumnHeader>
        </Row>
      </thead>
      <tbody>
        {
          summaryCustomerLastWeek && summaryCustomerLastWeek.map((s, key) => (
            <Row key={s.get('customer')}>
              <ColumnRow>{key + 1}</ColumnRow>
              <ColumnRow>{s.get('customer')}</ColumnRow>
              <ColumnRow>{s.get('trucks')}</ColumnRow>
              <ColumnRow>{s.get('totalCases')}</ColumnRow>
              <ColumnRow>{s.get('totalOrders')}</ColumnRow>
              <ColumnRow>{s.get('averageLoadTime')}</ColumnRow>
              <ColumnRow>{s.get('noShows')}</ColumnRow>
              <ColumnRow>{s.get('overTwoHoursLoad')}</ColumnRow>
              <ColumnRow>{s.get('overFourHoursLoad')}</ColumnRow>
            </Row>
          ))
        }
      </tbody>
    </Table>
    {(!summaryCustomerLastWeek || summaryCustomerLastWeek.isEmpty()) && <NoDataText> NO DATA FOUND </NoDataText>}
  </ReportContainer>
)

CustomerSummaryPreviousWeek.propTypes = {
  summaryCustomerLastWeek: PropTypes.object,
  generateCSV: PropTypes.func
}

class Reports extends Component {
  componentDidMount () {
    const {
      getSummary,
      getSummaryYesterday,
      getTodayAppointmentsSummary,
      getTodayAppointments,
      getYesterdayAppointments,
      getLastWeekAppointments,
      getSummaryCustomerYesterday,
      getSummaryCustomerLastWeek
    } = this.props

    getSummary()
    getSummaryYesterday()
    getTodayAppointmentsSummary()
    getTodayAppointments()
    getYesterdayAppointments()
    getLastWeekAppointments()
    getSummaryCustomerYesterday()
    getSummaryCustomerLastWeek()
  }

  generateCSV = data => {
    return data ? data.toJS() : []
  }

  render () {
    const {
      summary,
      summaryYesterday,
      todayAppointmentsSummary,
      todayAppointments,
      yesterdayAppointments,
      lastWeekAppointments,
      summaryCustomerYesterday,
      summaryCustomerLastWeek
    } = this.props

    return (
      <Container>
        <Header> REPORTS </Header>
        <TodaySummary generateCSV={this.generateCSV} summary={summary} />
        <YesterdaySummary generateCSV={this.generateCSV} summaryYesterday={summaryYesterday} />
        <TodayAppointmentsSummary generateCSV={this.generateCSV} todayAppointmentsSummary={todayAppointmentsSummary} />
        <TodayAppointments generateCSV={this.generateCSV} todayAppointments={todayAppointments} />
        <YesterdayAppointments generateCSV={this.generateCSV} yesterdayAppointments={yesterdayAppointments} />
        <LastWeekAppointments generateCSV={this.generateCSV} lastWeekAppointments={lastWeekAppointments} />
        <CustomerSummaryPreviousDay generateCSV={this.generateCSV} summaryCustomerYesterday={summaryCustomerYesterday}/>
        <CustomerSummaryPreviousWeek generateCSV={this.generateCSV} summaryCustomerLastWeek={summaryCustomerLastWeek} />
      </Container>
    )
  }
}

Reports.propTypes = {
  summary: PropTypes.object,
  summaryYesterday: PropTypes.object,
  todayAppointmentsSummary: PropTypes.object,
  todayAppointments: PropTypes.object,
  yesterdayAppointments: PropTypes.object,
  lastWeekAppointments: PropTypes.object,
  summaryCustomerYesterday: PropTypes.object,
  summaryCustomerLastWeek: PropTypes.object,
  getSummary: PropTypes.func,
  getSummaryYesterday: PropTypes.func,
  getTodayAppointmentsSummary: PropTypes.func,
  getTodayAppointments: PropTypes.func,
  getYesterdayAppointments: PropTypes.func,
  getLastWeekAppointments: PropTypes.func,
  getSummaryCustomerYesterday: PropTypes.func,
  getSummaryCustomerLastWeek: PropTypes.func
}

const mapStateToProps = state => ({
  summary: getSummary(state),
  summaryYesterday: getSummaryYesterday(state),
  todayAppointmentsSummary: getTodayAppointmentsSummary(state),
  todayAppointments: getTodayAppointments(state),
  yesterdayAppointments: getYesterdayAppointments(state),
  lastWeekAppointments: getLastWeekAppointments(state),
  summaryCustomerYesterday: getSummaryCustomerYesterday(state),
  summaryCustomerLastWeek: getSummaryCustomerLastWeek(state)
})

const mapDispatchToProps = dispatch => ({
  getSummary: () => dispatch(ReportsActions.getSummary()),
  getSummaryYesterday: () => dispatch(ReportsActions.getSummaryYesterday()),
  getTodayAppointmentsSummary: () => dispatch(ReportsActions.getTodayAppointmentsSummary()),
  getTodayAppointments: () => dispatch(ReportsActions.getTodayAppointments()),
  getYesterdayAppointments: () => dispatch(ReportsActions.getYesterdayAppointments()),
  getLastWeekAppointments: () => dispatch(ReportsActions.getLastWeekAppointments()),
  getSummaryCustomerYesterday: () => dispatch(ReportsActions.getSummaryCustomerYesterday()),
  getSummaryCustomerLastWeek: () => dispatch(ReportsActions.getSummaryCustomerLastWeek())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reports)
