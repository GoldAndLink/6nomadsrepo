/* eslint-disable react/jsx-key */
import React from 'react'
import PropTypes from 'prop-types'
import { mount } from 'enzyme'

// import { mount, shallow, render } from 'enzyme'
// import toJson from 'enzyme-to-json'

// Component to be tested
import Login from '../Login' // Smart components

// Actions to be tested
import UsersActions, { UsersTypes } from '../../../modules/users/actions'

// Reducer to be tested
import { reducer as usersReducer } from '../../../modules/users/reducers'

// InitialState
import { INITIAL_STATE } from '../../../modules/users/initialState'

import createStore from '../../../store'
const { store } = createStore()

describe('Login Component', async () => {
  test('Component should render without throwing an error', () => {
    const component = mount(<Login />, {
      context: {
        store
      },
      childContextTypes: {
        store: PropTypes.object
      }
    })
    expect(component).toMatchSnapshot()
  })

  test('Component should render deleted_name h1 tag', () => {
    const component = mount(<Login />, {
      context: {
        store
      },
      childContextTypes: {
        store: PropTypes.object
      }
    })
    expect(component.containsAnyMatchingElements([<h1>deleted_name</h1>])).toEqual(true)
  })

  test('UsersActions LOGIN should dispatch the correct action and payload to login', async () => {
    const expectedAction =
      {
        'payload': { email: 'admin@rewoodscs.com', password: 'admin' },
        'type': UsersTypes.LOGIN
      }
    const login = await store.dispatch(UsersActions.login({ email: 'admin@rewoodscs.com', password: 'admin' }))
    expect(login).toEqual(expectedAction)
  })

  test('UsersReducer LOGIN should return INITIAL_STATE', async () => {
    const action = { type: UsersTypes.LOGIN }
    const initialState = INITIAL_STATE
    const loginReducer = await usersReducer(undefined, action)
    expect(loginReducer).toEqual(initialState)
  })

  test('UsersReducer LOGIN_SUCCESS should not return INITIAL_STATE', async () => {
    const payload =
      {
        'token': 'token',
        'user': {},
        'type': UsersTypes.LOGIN_SUCCESS
      }
    const initialState = INITIAL_STATE
    const loginReducer = await usersReducer(undefined, payload)
    expect(loginReducer).not.toEqual(initialState)
  })
})
