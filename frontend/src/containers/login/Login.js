import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import styled from 'styled-components'

import UsersActions from '../../modules/users/actions'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import backgroundImage from '../../assets/images/background.png'
import logoImage from '../../assets/images/deleted@2x.png'

const Container = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
  background: url(${backgroundImage});
  background-size: cover;
`

const Panel = styled.div`
  width: 450px;
`

const Header = styled.div`
  margin: 0px;
  height: 120px;
  background-color: ${props => props.theme.brand};
  display: flex;
  justify-content: center;
  align-items: center;

  h1 {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    height: 80px;
    color: white;
    font-size: 23px;
    font-weight: 700;
    border-right: solid 1px white;
    padding-right: 30px;
    margin-right: 30px;
  }

  img {
    width: 80px;
    height: 80px;
  }
`

const Form = styled.form`
  background-color: white;
  padding: 40px 75px;
`

const ForgottenLink = styled.div`
  font-size: 12px;
  margin-top: 6px;
  margin-bottom: 30px;
  text-align: right;
  color: ${props => props.theme.brand};
`

const LoginButton = styled(Button)`
  width: 100%;
  height: 54px;
  font-size: 18px;

  ${({ error }) => error && `
    background-color: red;
    border-color: red;
  `}
`

class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      resetError: false
    }
  }

  onUsernameChange = e => {
    const { value: username } = e.target
    this.setState(state => ({
      ...state,
      username,
      resetError: true
    }))
  }

  onPasswordChange = e => {
    const { value: password } = e.target
    this.setState(state => ({
      ...state,
      password,
      resetError: true
    }))
  }

  onSubmit = e => {
    e.preventDefault()
    const { username = '', password } = this.state
    const { login } = this.props
    this.setState({ resetError: false })
    login({ username: username.toLowerCase(), password })
  }

  render () {
    const { username, password, resetError } = this.state
    const { isLoggedIn, loginErrorMessage } = this.props

    if (isLoggedIn) {
      return <Redirect to='/home' />
    }

    return (
      <Container>
        <Panel>
          <Header>
            <h1>deleted_name</h1>
            <img src={logoImage} alt='Deleted' />
          </Header>
          <Form onSubmit={this.onSubmit}>
            <TextInput
              fullwidth
              label='Username'
              type='text'
              placeholder='user@deleted.com'
              value={username}
              onChange={this.onUsernameChange}
            />
            <ForgottenLink>Forgot Username</ForgottenLink>
            <TextInput
              fullwidth
              label='Password'
              type='password'
              value={password}
              onChange={this.onPasswordChange}
            />
            <ForgottenLink>Forgot Password</ForgottenLink>
            {!!loginErrorMessage && !resetError
              ? <LoginButton type='submit' primary error>Invalid Credentials</LoginButton>
              : <LoginButton type='submit' primary>Login</LoginButton>
            }
          </Form>
        </Panel>
      </Container>
    )
  }
}

Login.propTypes = {
  isLoggedIn: PropTypes.bool,
  loginErrorMessage: PropTypes.string,
  login: PropTypes.func
}

const mapStateToProps = state => ({
  loginErrorMessage: state.users.get('loginErrorMessage'),
  isLoggedIn: !!state.users.get('token')
})

const mapDispatchToProps = dispatch => ({
  login: payload => dispatch(UsersActions.login(payload))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
