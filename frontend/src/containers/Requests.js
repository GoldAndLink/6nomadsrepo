import React, { Component } from 'react'
import PropTypes from 'prop-types'
import config from '../config'
import styled from 'styled-components'
import { connect } from 'react-redux'
import io from 'socket.io-client'
import { Droppable, Draggable } from 'react-beautiful-dnd'
import { generateDraggableId, generateDropppableId } from '../utils/utils'
import searchIcon from '../assets/images/search-icon.svg'
import radioOn from '../assets/images/radio-button-on.svg'
import radioOff from '../assets/images/radio-button-off.svg'
// import { appointmentStatuses as aptStatuses } from './Appointments'
import canceledIcon from '../assets/images/warning-icon-canceled.svg'
import rescheduleIcon from '../assets/images/warning-icon-reschedule.svg'
import closeIcon from '../assets/images/close.svg'
import { getDoorById } from '../modules/doors/selectors'

import Select from '../components/Select'

import OrdersActions from '../modules/orders/actions'
import AppointmentsActions from '../modules/appointments/actions'
import CarrierRequestsActions from '../modules/carrierRequests/actions'
import CarrierActions from '../modules/carriers/actions'
import DriverActions from '../modules/drivers/actions'
import { getOrders } from '../modules/orders/selectors'

import { getAllCarrierRequests, numberOfCarrierRequests } from '../modules/carrierRequests/selectors'
import { getAllCarriers } from '../modules/carriers/selectors'
import {
  getSite,
  getWarehouse
} from '../modules/app/selectors'

import { getBuildingsForSite } from '../modules/sites/selectors'
import { getAllSites } from '../modules/entities/selectors'

import RequestCard from '../components/RequestCard'
import { RadioGroup, Radio, FormControlLabel } from '@material-ui/core'

export const requestStatuses = {
  pending: 'pending',
  scheduled: 'scheduled',
  reschedule: 'reschedule',
  canceled: 'canceled',
  carrierLate: 'carrier Late'
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 240px;
  z-index: 2;
`

const RequestsContainer = styled.div`
  flex: 1;
  overflow-y: auto;
  padding: 10px;
`

const StyledRequestCard = styled(RequestCard)`
  box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12);
  border-radius: 4px;
  padding: 1px;
  margin-bottom: 10px;
`

const CollapseCommand = styled.div`
  height: 14px;
  font-size: 10px;
  font-weight: bold;
  font-style: normal;
  color: #aab0c0;
  cursor: pointer;
  margin: 0 5px;
`
const Separator = styled.span`
  height: 14px;
  font-size: 10px;
  color: #aab0c0;
`

const CollapseCommandContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin: 10px 5px;
`
const HorizontalLine = styled.div`
    border-bottom: 1px solid #aab0c0;
    margin: 0 10px;
`
const SearchContainer = styled.div`
  width: 240px;
  padding-bottom: 12px;
  background-color: #C4C6CE;
`

const SearchContainerHeader = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 12px;
`

const ClosePanelButton = styled.div`
  font-size: 12px;
  font-weight: bold;
  color: #3c414e;
  cursor: pointer;
`

const CancellationNotification = styled.div`
  font-size: 11px;
  color: #FE1F3A;
  vertical-align: middle;
  line-height: 18px;
  border-width: 1px;
  border-color: #FE1F3A;
  border-style: solid;
  background-color: #FDEDEF;
  padding: 5px 0;
  margin: 10px;
  display: flex;
  flex-direction: row;
`

const RescheduleNotification = styled.div`
  font-size: 11px;
  color: #FF861A;
  vertical-align: middle;
  line-height: 18px;
  border-width: 1px;
  border-color: #FF861A;
  border-style: solid;
  background-color: #FEF4C1;
  padding: 5px 0;
  margin: 10px;
  display: flex;
  flex-direction: row;
`

const WarningIcon = styled.img`
  width: 24px;
  height: 28px;
  margin: 5px 10px;
`

const SearchBox = styled.div`
  font-size: 12px;
  background-color: #ffffff;
  margin-left: 10px;
  margin-right: 12px;
  margin-bottom: 4px;
  width: 220px;
  font-size: 85%;
  height: 38px;
  display: flex;
`

const SearchInput = styled.input`
  flex: 1;
  color: ${props => props.theme.gray};
  width: 23px;
  height: 38px;
  padding: 0 0 0 28px;
  font-size: 12px;
  border-width: 1px;
  border-color: #aab0c0;
  border-style: solid;
  background: url(${searchIcon}) no-repeat 8px center;

  &:focus {
    outline: none;
  }

  &::-webkit-input-placeholder {
    color: ${props => props.theme.lightGray};
  }
`
const AttributesSelect = styled.div`
  margin-left: -1px;
  height: 38px;
  font-size: 85%;
  width: 80px;
  border-width: 1px;
  border-color: #aab0c0;
  border-style: solid;
`

const RequestStatusSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
`

const CarrierSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
`

const SortByDateRadioButtons = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 10px;
`
const RadioButton = styled(Radio)`
  && {
    margin-left: 5px;
    padding: 0;
    background-color: white;
    }
  }
`
const RadioButtonGroup = styled(RadioGroup)`
  && {
    margin: 10px;
    flex-direction:row;
  }
`
const RadioButtonsLabel = styled(FormControlLabel)`
  && {
    margin-right: 40px;
  }
`
const RadioButtonTitle = styled.p`
  color: #3c414e;
  font-size: 14px;
  margin: 0;
`
const RadioButtonLabelText = styled.span`
  color: #3c414e;
  margin-left: 5px;
`
const searchOptions = [{ label: 'SO', value: 'primaryRefValue' }, { label: 'PO', value: 'PO' }]

const statusOptions = [
  { label: 'Pending', value: 'pending' },
  { label: 'Cancel', value: 'canceled' },
  { label: 'Reschedule', value: 'reschedule' }
]
class Requests extends Component {
  state = {
    hide: false,
    searchText: '',
    attributesSelect: null,
    carrierRequestStatus: null,
    carrierSelect: null,
    sortByDateSelect: null,
    socket: io(`${config.API_BASE}/carrierRequests`)
  }

  componentDidMount () {
    const {
      getAllCarrierRequests,
      updateCarrierRequestsWithSocketCarrierRequest,
      updateCountCarrierRequestsWithSocketCountCarrierRequests,
      updateCarrierWithSocketCarrier,
      updateDriverWithSocketDriver,
      getNumberOfCarrierRequests
    } = this.props

    getAllCarrierRequests({ includes: ['carrier', 'appointment', 'carrierRequestOrder'] })
    getNumberOfCarrierRequests()
    // remove listeners from previous event
    this.state.socket.off('carrierRequest')
    // keep listening for changes
    this.state.socket.on('carrierRequest', socketCarrierRequest => {
      updateCarrierWithSocketCarrier({ carrier: socketCarrierRequest.carrier })
      updateDriverWithSocketDriver({ driver: socketCarrierRequest.driver })
      updateCountCarrierRequestsWithSocketCountCarrierRequests({ numberOfCarrierRequests: socketCarrierRequest.count })
      updateCarrierRequestsWithSocketCarrierRequest({ socketCarrierRequest })
    })
  }

  onSearchChange = e => {
    const { attributesSelect } = this.state
    const searchText = e.target.value
    this.setState({ searchText: searchText }, () => {
      if ((searchText === '' && attributesSelect) || (searchText && attributesSelect)) {
        this.requestCarrierRequest()
      }
    })
  }

  onAttributesSelectChange = attributesSelect => {
    const { searchText } = this.state
    this.setState({ attributesSelect: attributesSelect }, () => {
      if ((searchText === '' && attributesSelect) || (searchText && attributesSelect)) {
        this.requestCarrierRequest()
      }
    })
  }

  onRequestStatusSelectChange = carrierRequestStatus => {
    this.setState({ carrierRequestStatus }, () => this.requestCarrierRequest())
  }

  onCarrierSelectChange = carrierSelect => {
    this.setState({ carrierSelect }, () => this.requestCarrierRequest())
  }

  onCustomerSelectChange = customerSelect => {
    this.setState({ customerSelect }, () => this.requestCarrierRequest())
  }

  onSortByDateSelectChange = e => {
    this.setState({ sortByDateSelect: e.target.value }, () => this.requestCarrierRequest())
  }

  requestCarrierRequest = () => {
    const { getAllCarrierRequests } = this.props
    const { searchText, attributesSelect, carrierRequestStatus, carrierSelect, sortByDateSelect } = this.state
    getAllCarrierRequests({ searchText, attributesSelect, carrierRequestStatus, carrierSelect, sortByDate: sortByDateSelect, includes: ['carrier', 'carrierRequestOrder'] })
  }

  collapseAll = () => this.setState(state => ({
    hide: true
  }))

  expandAll = () => this.setState(state => ({
    hide: false
  }))

  onCarrierRequestClick (request) {
    const { site, warehouse, sites, siteBuildings } = this.props

    const selectedSite = sites.find(s => s.get('id') === site)
    const selectedBuilding = siteBuildings.find(sb => sb.get('id') === warehouse)

    if (request.get('appointment')) {
      let appt = request.get('appointment').set(
        'carrierRequests',
        request.getIn(['appointment', 'carrierRequests'], []).map(cr =>
          cr.get('id') === request.get('id') ? request : cr
        )
      )

      this.props.setSelectedOrders(appt.get('orders'))
      appt = appt.set('recalculateDuration', true)

      this.props.openEditAppointment(appt)
    } else {
      request = request.set('site', selectedSite)
      request = request.set('building', selectedBuilding)
      request = request.set('recalculateDuration', true)
      this.props.createFromRequest(request)
    }
  }

  showRequestCard (carrierRequest) {
    // Carrier request should be shown only if they don’t have an appointment or in case they have an appointment
    // request should be either in cancel status or reschedule status.
    // if it has one, appointment status should be either draft or scheduled
    // const { appointmentStatuses } = this.props

    if (carrierRequest.get('deletedAt')) return false

    // const appointment = carrierRequest.get('appointment')
    // const appointmentStatus = appointmentStatuses && appointment ? appointmentStatuses.find(as => as.get('id') === appointment.get('appointmentStatusId')) : null

    return carrierRequest.get('status') !== requestStatuses.scheduled
  }

  renderRequestCard (carrierRequest, index) {
    const { hide } = this.state
    const { getDoorById } = this.props

    const door = carrierRequest && carrierRequest.get('appointment') ? getDoorById(carrierRequest.get('appointment').get('doorId')) : null
    const building = door ? door.getIn(['area', 'building']) : null
    const timezone = (door && door.get('area').get('building').get('timezone')) || 'UTC'
    return (
      <Draggable
        key={carrierRequest.get('id')}
        draggableId={generateDraggableId('request', carrierRequest.get('id'))}
        index={index}
      >
        {(provided, snapshot) => (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <StyledRequestCard
              onClick={() => this.onCarrierRequestClick(carrierRequest)}
              hide={hide}
              timezone={timezone}
              carrierRequest={carrierRequest}
              building={building}
              onCreate={() => this.onCarrierRequestClick(carrierRequest)}
            />
          </div>
        )}
      </Draggable>
    )
  }

  render () {
    const { carrierRequests, onClose, carriers, numberOfRequests } = this.props
    const { searchText, attributesSelect, carrierRequestStatus, sortByDateSelect, carrierSelect } = this.state
    const carrierOptions = carriers ? carriers.map(carrier => ({ label: carrier.get('name'), value: carrier.get('id') })) : []
    return (
      <Container>
        <SearchContainer>
          <SearchContainerHeader>
            <ClosePanelButton onClick={onClose}>
              <img src={closeIcon} alt="close" />
            </ClosePanelButton>
          </SearchContainerHeader>
          {numberOfRequests && numberOfRequests.get('numberOfCanceledRequests') > 0 && <CancellationNotification><WarningIcon src={canceledIcon} alt='canceledIcon' />({numberOfRequests && numberOfRequests.get('numberOfCanceledRequests')}) Scheduled Appointment Cancellation Request(s)</CancellationNotification>}
          {numberOfRequests && numberOfRequests.get('numberOfRescheduledRequests') > 0 && <RescheduleNotification><WarningIcon src={rescheduleIcon} alt='rescheduleIcon' />({numberOfRequests && numberOfRequests.get('numberOfRescheduledRequests')}) Scheduled Appointment Reschedule Request(s)</RescheduleNotification>}
          <SortByDateRadioButtons>
            <RadioButtonTitle>Sort requests by</RadioButtonTitle>
            <RadioButtonGroup onChange={this.onSortByDateSelectChange}>
              <RadioButtonsLabel value="desc" control={<RadioButton icon={<img alt='' src={radioOff} />} checkedIcon={<img alt='' src={radioOn} />} checked={sortByDateSelect === 'desc'} />} label={<RadioButtonLabelText>Newest</RadioButtonLabelText>} />
              <RadioButtonsLabel value="asc" control={<RadioButton icon={<img alt='' src={radioOff} />} checkedIcon={<img alt='' src={radioOn} />} checked={sortByDateSelect === 'asc'} />} label={<RadioButtonLabelText>Oldest</RadioButtonLabelText>} />
            </RadioButtonGroup>
          </SortByDateRadioButtons>
          <SearchBox>
            <SearchInput placeholder='Search' value={searchText} onChange={this.onSearchChange} />
            <AttributesSelect>
              <Select
                isSearchable={false}
                options={searchOptions}
                placeholder='Select'
                value={attributesSelect}
                onChange={this.onAttributesSelectChange}
              />
            </AttributesSelect>
          </SearchBox>
          <RequestStatusSelect>
            <Select
              isClearable
              options={statusOptions}
              placeholder='Request type'
              value={carrierRequestStatus}
              onChange={this.onRequestStatusSelectChange}
            />
          </RequestStatusSelect>
          <CarrierSelect>
            <Select
              isClearable
              options={carrierOptions}
              placeholder='Carrier'
              value={carrierSelect}
              onChange={this.onCarrierSelectChange}
            />
          </CarrierSelect>
        </SearchContainer>
        <CollapseCommandContainer>
          <CollapseCommand onClick={this.expandAll}>
            Expand all
          </CollapseCommand>
          <Separator>|</Separator>
          <CollapseCommand onClick={this.collapseAll}>
            Collapse all
          </CollapseCommand>
        </CollapseCommandContainer>
        <HorizontalLine />
        <Droppable
          droppableId={generateDropppableId('requests')}
          isDropDisabled={true}
        >
          {(provided, snapshot) => (
            <RequestsContainer
              innerRef={provided.innerRef}
              {...provided.droppableProps}
            >
              {carrierRequests && carrierRequests.map((carrierRequest, index) =>
                this.showRequestCard(carrierRequest) ? this.renderRequestCard(carrierRequest, index) : null
              )}
            </RequestsContainer>
          )}
        </Droppable>
      </Container>
    )
  }
}

Requests.propTypes = {
  site: PropTypes.object,
  warehouse: PropTypes.object,
  getAllCarrierRequests: PropTypes.func,
  getAllCarrierRequestStatuses: PropTypes.func,
  setSelectedOrders: PropTypes.func,
  openEditAppointment: PropTypes.func,
  createFromRequest: PropTypes.func,
  carrierRequests: PropTypes.object,
  carrierRequestStatuses: PropTypes.object,
  appointmentStatuses: PropTypes.any,
  onClose: PropTypes.func,
  updateCarrierRequestsWithSocketCarrierRequest: PropTypes.func,
  updateCountCarrierRequestsWithSocketCountCarrierRequests: PropTypes.func,
  updateCarrierWithSocketCarrier: PropTypes.func,
  getNumberOfCarrierRequests: PropTypes.func,
  numberOfRequests: PropTypes.object,
  carriers: PropTypes.object,
  orders: PropTypes.object
}

const mapStateToProps = state => ({
  site: getSite(state),
  warehouse: getWarehouse(state),
  sites: getAllSites(state),
  siteBuildings: getBuildingsForSite(state),
  carrierRequests: getAllCarrierRequests(state),
  carriers: getAllCarriers(state),
  numberOfRequests: numberOfCarrierRequests(state),
  getDoorById: id => getDoorById(state, id),
  orders: getOrders(state)
})

const mapDispatchToProps = dispatch => ({
  getAllCarrierRequests: payload => dispatch(CarrierRequestsActions.getAllCarrierRequests(payload)),
  openEditAppointment: appointment => dispatch(AppointmentsActions.openEditAppointment(appointment)),
  createFromRequest: request => dispatch(AppointmentsActions.createFromRequest(request)),
  setSelectedOrders: orders => dispatch(OrdersActions.setSelectedOrders(orders)),
  getNumberOfCarrierRequests: () => dispatch(CarrierRequestsActions.getNumberOfCarrierRequests()),
  updateCarrierRequestsWithSocketCarrierRequest: payload => dispatch(CarrierRequestsActions.updateCarrierRequestsWithSocketCarrierRequest(payload)),
  updateCarrierWithSocketCarrier: payload => dispatch(CarrierActions.updateCarrierWithSocketCarrier(payload)),
  updateCountCarrierRequestsWithSocketCountCarrierRequests: payload => dispatch(CarrierRequestsActions.updateCountCarrierRequestsWithSocketCountCarrierRequests(payload)),
  updateDriverWithSocketDriver: payload => dispatch(DriverActions.updateDriverWithSocketDriver(payload))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Requests)
