import React, { Component } from 'react'
import config from '../config'
import io from 'socket.io-client'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import styled, { css } from 'styled-components'
import moment from 'moment'
import { DragDropContext } from 'react-beautiful-dnd'
import { Map } from 'immutable'

import Select from '../components/Select'
import DayTabs from '../components/DayTabs'
import {
  Tab,
  Tabs as UnstyledTabs,
  TabList,
  TabPanel
} from '../components/Tabs'
import TimeTable from '../components/TimeTable'
import DatePicker from '../components/DatePicker'
import UserDropdown from '../components/UserDropdown'
import SystemSettingsModal from '../components/SystemSettingsModal'
import ProgressLoader from '../components/ProgressLoader'
import Orders from './Orders'
import Reports from './Reports'
import Appointments from './Appointments'
import Requests from './Requests'
import UpsertAppointmentModal from '../components/UpsertAppointmentModal'
import OrderDetailsModal from '../components/OrderDetailsModal'
import calendarRightArrowIcon from '../assets/images/right-date-arrow-icon.svg'
import Modal from '../components/Modal'
import ZoomSelector from '../components/ZoomSelector'
import HourFormatSwitch from '../components/HourFormatSwitch'
import { parseDroppableType, parseDoorId, parseDraggableId, parseDraggableType, parseDroppableId } from '../utils/utils'

import AppActions from '../modules/app/actions'
import AppointmentsActions from '../modules/appointments/actions'
import SitesActions from '../modules/sites/actions'
import BuildingsActions from '../modules/buildings/actions'
import OrdersActions from '../modules/orders/actions'
import CarrierRequestsActions from '../modules/carrierRequests/actions'
import FeedsActions from '../modules/feeds/actions'
import UsersActions from '../modules/users/actions'

import {
  getSite,
  getWarehouse,
  getStartDate,
  getEndDate,
  getStartTime,
  getEndTime,
  getFocusAppointment
} from '../modules/app/selectors'
import { getAllSites, getAllBuildings, getAppointments, getAllOrders } from '../modules/entities/selectors'
import { getBuildingsForSite } from '../modules/sites/selectors'
import { isLoggedIn, getDefaultBuilding } from '../modules/users/selectors'
import { getAppointmentCounts, getAllAppointmentStatuses, getIsUpsertAppointmentVisible, getAppointmentsIsLoading } from '../modules/appointments/selectors'
import { getAppointmentsForDoors, getAppointmentsForDoorsIsLoading } from '../modules/doors/selectors'
import { getOrdersForAppointment, getOrders, getCheckedOrders, getOrdersIsLoading, getSelectedOrders } from '../modules/orders/selectors'
import { getAllDrivers } from '../modules/drivers/selectors'
import { getDoorsForBuilding, getAreasForBuilding } from '../modules/buildings/selectors'
import { getAllCarriers } from '../modules/carriers/selectors'
import { getAreas } from '../modules/areas/selectors'
import { numberOfCarrierRequests, getAllCarrierRequests, getAllCarrierRequestsIsLoading } from '../modules/carrierRequests/selectors'
import { getScheduleForDoors } from '../modules/schedules/selectors'
import { getBootstrapDataIsLoading } from '../modules/feeds/selectors'

import DoorsActions from '../modules/doors/actions'

import ordersIcon from '../assets/images/orders-icon.svg'
import appointmentsIcon from '../assets/images/appointments-icon.svg'
import requestsIcon from '../assets/images/requests-icon.svg'
import reportsIcon from '../assets/images/reportsIcon.svg'

const OrdersIndex = 0
const AppointmentsIndex = 1
const RequestsIndex = 2
const ReportsIndex = 3

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
`

const Header = styled.div`
  color: #898989;
  display: flex;
  align-items: center;
  height: 50px;
  padding: 0px 41px;
  z-index: 4;
  background-color: #ffffff;
`

const HeaderSelects = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
`

const HeaderTitle = styled.div`
    display: flex;
    width: 331px;
    flex-direction: row;
    align-self: center;
    h1 {
      align-self: center;
      font-size: 16px;
      font-weight: normal;
      font-style: normal;
      color: #898989;
    }
    h2 {
      align-self: center;
      padding-left: 6px;
      font-size: 16px;
      font-weight: 500;
      font-style: normal;
      color: #898989;
    }
`

const SelectContainer = styled.div`
    display: flex;
    flex-direction: row;
    align-self: center;
    margin-left: 30px;
    margin-right: 33px;
`

const SelectLabel = styled.h2`
    font-size: 12px;
    align-self: center;
    font-weight: 300;
    font-style: normal;
    color: #898989;
    margin-right: 5px;
`

const HeaderFilterSelect = styled.div`
    width: 175px;
    height: 38px;
    font-weight: lighter;
    align-self: center;
    font-size: 12px;
`

const Content = styled.div`
  display: flex;
  flex: 1;
  background-color: #f4f6f9;
  overflow: hidden;
`

const Tabs = styled(UnstyledTabs)`
  z-index: 3;
  display: flex;
  flex-direction: row;
  width: 110px;
  height: 100%;

  ${props => props.selectedIndex >= 0 && props.selectedIndex !== 3 && css`
    width: 350px;
  `}
`

const RightPanel = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 10px;
  overflow: hidden;
`

const GridHeader = styled.div`
  display: flex;
  justify-content: flex-start;
  background-color: #ffffff;
  border-radius: 4px;
  border: solid 1px #ebebeb;
  flex-direction: row;
  padding-right: 40px;

  div {
    align-self: center;
  }
  h4 {
    align-self: center;
    margin-left: 12px;
    font-weight: normal;
  }
`

const GridTimeSelection = styled.div`
  display: flex;
  flex: 1;
  justify-content: space-between;
`

/* const NumberOfRequests = styled.span`
  margin-left: 5px;
  padding: 1px 3px;
  color: #3c414e;
  background-color: #ffffff;
  border: 1px solid #ffffff;
  border-radius: 2px;
` */

const TabIconContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  white-space: nowrap;
  align-items: center;
  p {
    display: block

    span.counter {
      color: red;
    }
  }
`

class Home extends Component {
  constructor (props) {
    super(props)

    this.state = {
      selectedTab: 0,
      selectedSite: null,
      selectedWarehouse: null,
      droppedOrder: null,
      selectedHeaderGridStartDate: moment(),
      selectedHeaderGridEndDate: moment().add(1, 'day'),
      selectedHeaderGridStartTime: moment(),
      selectedHeaderGridEndTime: moment(),
      isCreateAppointmentOpen: false,
      createAppointmentOrders: [],
      initialValues: {},
      zoomLevel: 100,
      socket: io(`${config.API_BASE}/appointments`),
      is24Format: true,
      days: [
        { id: 0, status: 'past', date: moment().subtract(1, 'days'), countKey: 'appointmentYesterdayCount' },
        { id: 1, status: 'selected', date: moment(), countKey: 'appointmentTodayCount' },
        { id: 2, status: 'active', date: moment().add(1, 'days'), countKey: 'appointmentNext1Count' },
        { id: 3, status: 'active', date: moment().add(2, 'days'), countKey: 'appointmentNext2Count' },
        { id: 4, status: 'active', date: moment().add(3, 'days'), countKey: 'appointmentNext3Count' }
      ]
    }
  }

  componentDidMount = () => {
    const {
      getBootstrapData,
      getUsers,
      warehouse,
      // getAppointments,
      getNumberOfCarrierRequests
    } = this.props
    /* SYSTEM SETTINGS */
    getBootstrapData({})
    getUsers()

    if (warehouse) {
      this.requestGetAppointmentsForDoors({ selectedWarehouse: warehouse })
    }

    /* APPOINTMENTS CONTAINER */
    // We should not request ALL appointments
    // Just appts for that site/building
    // getAppointments({})

    getNumberOfCarrierRequests()
  }

  componentDidUpdate = (prevProps, prevState) => {
    // const { appointments: prevAppointments, isUpsertAppointmentVisible: prevIsUpsertAppointmentVisible } = prevProps
    const {
    // appointments,
    // isUpsertAppointmentVisible,
      site,
      warehouse,
      sites,
      siteBuildings,
      buildings,
      defaultBuildingId
    } = this.props

    /*
    // if (!!prevAppointments && !!appointments && prevAppointments.count() !== appointments.count()) {
    //   // this.requestGetAppointmentsForDoors({})
    // }
    // refresh once modal closes
    // if (isUpsertAppointmentVisible !== prevIsUpsertAppointmentVisible && prevIsUpsertAppointmentVisible) {
    //   // this.requestGetAppointmentsForDoors({})
    // }
    */

    let defaultBuilding
    if (defaultBuildingId && buildings) {
      defaultBuilding = buildings.get(defaultBuildingId + '')
    }
    if (defaultBuilding && !warehouse) {
      this.onSiteSelectChange(defaultBuilding.get('siteId'))
      this.onWarehouseSelectChange(defaultBuilding.get('id'))
    } else {
      if (sites && sites.size && !site) {
        this.onSiteSelectChange(sites.first().get('id'))
      }
      if (siteBuildings && siteBuildings.size && !warehouse) {
        this.onWarehouseSelectChange(siteBuildings.first().get('id'))
      }
    }
  }

  updateDayTabs = id => {
    const { days } = this.state
    this.setState({
      days: days.map(day => {
        if (day.id !== id) {
          if (day.status === 'selected') {
            return {
              ...day,
              status: day.id === 0 ? 'past' : 'active'
            }
          } else {
            return day
          }
        }

        return {
          ...day,
          status: 'selected'
        }
      })
    })
  }

  handleClickDayTabs = id => {
    const { days } = this.state
    this.onHeaderGridStartDateChange(days[id].date)
  }

  onSiteSelectChange = siteId => {
    const { setSite } = this.props
    setSite({ site: siteId, id: siteId })
  }

  onWarehouseSelectChange = selectedWarehouse => {
    const { warehouse, setWarehouse, getAreasForBuilding, getDoorsForBuilding, updateAppointmentWithSocketAppointment } = this.props
    setWarehouse({ warehouse: selectedWarehouse })
    getAreasForBuilding({ id: selectedWarehouse })
    getDoorsForBuilding({ id: selectedWarehouse })
    this.requestGetAppointmentsForDoors({ selectedWarehouse })
    // remove listeners from previous event
    this.state.socket.off(`building:${warehouse}`)
    // keep listening for changes
    this.state.socket.on(`building:${selectedWarehouse}`, data => {
      updateAppointmentWithSocketAppointment(data)
    })
  }

  onHeaderGridStartTimeChange = time => {
    const { setStartTime } = this.props
    setStartTime({ startTime: time })
    this.requestGetAppointmentsForDoors({ selectedStartTime: time })
  }

  onHeaderGridEndTimeChange = time => {
    const { setEndTime } = this.props
    setEndTime({ endTime: time })
    this.requestGetAppointmentsForDoors({ selectedEndTime: time })
  }

  onHeaderGridStartDateChange = date => {
    const { setStartDate } = this.props
    const { days } = this.state

    setStartDate({ startDate: date })
    const selectedDay = days.find(d => d.date.format('L') === date.format('L'))
    this.updateDayTabs(selectedDay && selectedDay.id)
    this.requestGetAppointmentsForDoors({ selectedStartDate: date })
  }

  onDateChange = date => {
    const { days } = this.state
    const selectedDay = days.find(d => d.date.format('L') === date.format('L'))
    this.updateDayTabs(selectedDay && selectedDay.id)
    this.requestGetAppointmentsForDoors({ selectedStartDate: date })
  }

  onHeaderGridEndDateChange = date => {
    const { setEndDate } = this.props
    setEndDate({ endDate: date })
    this.requestGetAppointmentsForDoors({ selectedEndDate: date })
  }

  requestGetAppointmentsForDoors = ({
    selectedWarehouse,
    selectedStartTime,
    selectedEndTime,
    selectedStartDate,
    selectedEndDate
  }) => {
    const { getAppointmentsForDoors, warehouse, startTime, /* endTime,  */startDate, /* endDate,  */ buildings } = this.props

    const selectedBuilding = buildings.find(b => b.get('id') === (selectedWarehouse || warehouse))
    const timezone = (selectedBuilding && selectedBuilding.get('timezone')) || 'UTC'

    const formattedStartTime = (selectedStartTime || startTime).format('HH:mm:ss')
    // const formattedEndTime = (selectedEndTime || endTime).format('HH:mm:ss')
    const formattedStartDate = (selectedStartDate || startDate).format('YYYY-MM-DD')
    // const formattedEndDate = (selectedEndDate || endDate).format('YYYY-MM-DD')
    const tzStart = moment.tz(
      `${formattedStartDate} ${formattedStartTime}`,
      timezone
    ).utc()
    const tzEnd = tzStart.clone().add(2, 'day')

    getAppointmentsForDoors({
      buildingId: selectedWarehouse || warehouse,
      startTime: tzStart.format('HH:mm:ss'),
      endTime: tzEnd.format('HH:mm:ss'),
      startDate: tzStart.format('L'),
      endDate: tzEnd.format('L')
    })
  }

  onClickLeftArrowIcon = () => {
    const { setStartDate, setEndDate, startDate, endDate } = this.props
    setStartDate({ startDate: startDate.subtract(1, 'day') })
    setEndDate({ endDate: endDate.subtract(1, 'day') })
  }

  onClickRightArrowIcon = () => {
    const { setStartDate, setEndDate, startDate, endDate } = this.props
    setStartDate({ startDate: startDate.add(1, 'day') })
    setEndDate({ endDate: endDate.add(1, 'day') })
  }

  getDroppableDate (droppableTime) {
    const { siteBuildings, warehouse } = this.props
    const selectedBuilding = siteBuildings.find(b => b.get('id') === warehouse)
    const timezone = selectedBuilding.get('timezone') || 'UTC'
    return moment.tz(
      moment.unix(droppableTime / 1000).format('YYYY-MM-DD HH:mm'),
      timezone
    )
  }

  onDragEnd = result => {
    const { destination, draggableId } = result
    const {
      ordersForAppointment,
      areas,
      orders,
      appointments,
      buildingDoors,
      siteBuildings,
      setSelectedOrders,
      openEditAppointment,
      createFromRequest,
      warehouse,
      createAppointment,
      moveAppointment,
      carrierRequests,
      checkedOrders,
      selectedOrders,
      allOrders,
      setRecalculateDurationFlag
    } = this.props

    if (destination) {
      const droppableType = parseDroppableType(destination)
      const draggableType = parseDraggableType(draggableId)

      if (droppableType === 'event' && draggableType === 'order') {
        const eventId = parseDroppableId(destination)
        const orderId = parseDraggableId(draggableId)
        const orders = ordersForAppointment(parseInt(eventId))
        const appointment = appointments.find(appt => appt.get('id') === eventId).toJS()

        const orderIds = orders.toJS().map(order => order.id)

        if (!orderIds.includes(orderId)) {
          orderIds.push(orderId)
        }

        // updateAppointment({ id: parseInt(eventId), orderIds: orderIds, ...appointment })
        setSelectedOrders(orderIds.map(oid => allOrders.get(oid.toString()) || allOrders.get(oid)))
        openEditAppointment({ ...appointment, recalculateDuration: true })
      } else if (droppableType === 'appointment' && draggableType === 'order') {
        const orderIds = selectedOrders.map(order => order.get('id'))
        const droppedOrder = parseDraggableId(draggableId)
        const newSelectedOrders = [...orderIds, droppedOrder]
        setSelectedOrders(newSelectedOrders.map(oid => allOrders.get(oid.toString())))
        setRecalculateDurationFlag(true)
      } else if (droppableType === 'timeline' && draggableType === 'order') {
        const droppableTime = parseDroppableId(destination)
        const droppableDoor = parseDoorId(destination)
        const droppedOrder = parseDraggableId(draggableId)

        const foundOrder = checkedOrders.filter(o => o.get('id') === droppedOrder)
        const appointmentOrders = (checkedOrders && checkedOrders.size > 0 && foundOrder.size === 1) ? checkedOrders : [orders.find(o => o.get('id') === droppedOrder)]

        let newAppointment = new Map()
        const selectedDoor = buildingDoors.find(door => door.get('id') === droppableDoor)
        const selectedArea = areas.find(a => a.get('id') === selectedDoor.get('areaId'))
        const selectedBuilding = siteBuildings.find(b => b.get('id') === warehouse)

        newAppointment = newAppointment.set('door',
          selectedDoor.set('area',
            selectedArea.set('building', selectedBuilding)))

        newAppointment = newAppointment.set('doorId', droppableDoor)
        newAppointment = newAppointment.set('date', this.getDroppableDate(droppableTime))
        newAppointment = newAppointment.set('inProgress', true)
        newAppointment = newAppointment.set('duration', 60)

        setSelectedOrders(appointmentOrders)
        openEditAppointment({ ...newAppointment.toJS(), recalculateDuration: true })
        createAppointment(newAppointment)
      } else if (droppableType === 'timeline' && draggableType === 'event') {
        const droppableTime = parseDroppableId(destination)
        const droppableDoor = parseDoorId(destination)
        const droppedAppointment = parseDraggableId(draggableId)

        let editingAppointment = appointments.find(appt => appt.get('id') === droppedAppointment)
        editingAppointment = editingAppointment.set('doorId', droppableDoor)
        // const selectedBuilding = siteBuildings.find(b => b.get('id') === warehouse)
        editingAppointment = editingAppointment.set('date', this.getDroppableDate(droppableTime))
        moveAppointment(editingAppointment.toJS())
      } else if (droppableType === 'timeline' && draggableType === 'request') {
        const droppableTime = parseDroppableId(destination)
        const droppableDoor = parseDoorId(destination)
        const droppedRequest = parseDraggableId(draggableId)
        const carrierRequest = carrierRequests.find(r => r.get('id') === droppedRequest)

        createFromRequest(carrierRequest, {
          doorId: droppableDoor,
          date: this.getDroppableDate(droppableTime)
        })
      }
    }
  }

  onCloseCreateAppointment = () => {
    this.setState({
      isCreateAppointmentOpen: false
    })
  }

  onEditAppointment = appointment => {
    this.props.setSelectedOrders(appointment.get('orders'))
    this.props.openEditAppointment(appointment)
  }

  onAppointmentMessage = appointment => {
    this.props.openEditAppointment(appointment)
    if (this.appointmentModal) {
      this.appointmentModal.switchToSummonSMSTab()
    }
  }

  onAppointmentDurationChange = (appointment, duration) => {
    const {
      updateAppointment,
      getAppointmentsForDoors,
      warehouse,
      startTime,
      endTime,
      startDate,
      endDate
    } = this.props

    const newApppointment = {
      ...appointment.toObject(),
      duration
    }

    updateAppointment(newApppointment)
    getAppointmentsForDoors({ buildingId: warehouse, startTime: startTime.format('HH:mm:ss'), endTime: endTime.format('HH:mm:ss'), startDate: startDate.format('L'), endDate: endDate.format('L') })
  }

  render () {
    const {
      isLoggedIn,
      sites,
      siteBuildings,
      appointmentsForDoors,
      site,
      warehouse,
      startDate,
      startTime,
      endDate,
      endTime,
      focusAppointment,
      isUpsertAppointmentVisible,
      appointmentStatuses,
      deleteAppointment,
      buildingDoors,
      doorsSchedule,
      setSelectedOrders,
      numberOfRequests,
      closeUpsertAppointment,
      appointmentCounts,
      appointmentsForDoorsAreLoading,
      getBootstrapDataIsLoading
    } = this.props

    const {
      droppedOrder,
      isCreateAppointmentOpen,
      zoomLevel,
      selectedTab,
      is24Format,
      days
    } = this.state

    const siteOptions = sites
      ? sites.valueSeq().map(site => ({
        label: site.get('name'), value: site.get('id')
      })).toArray()
      : []

    const buildingOptions = siteBuildings ? siteBuildings.map(building => ({ label: building.get('name'), value: building.get('id') })) : []

    const selectedBuilding = siteBuildings ? siteBuildings.find(b => b.get('id') === warehouse) : ''
    if (!isLoggedIn) {
      return <Redirect to='/' />
    }

    const buildingStartDate = moment.tz(startDate.format('YYYY-MM-DD'), selectedBuilding && selectedBuilding.get('timezone'))
    const requestCount = numberOfRequests && numberOfRequests.get && (numberOfRequests.get('allCarrierRequests') || 0)

    return (
      <DragDropContext
        onDragStart={this.onDragStart}
        onDragEnd={this.onDragEnd}>
        <ProgressLoader isOpen={appointmentsForDoorsAreLoading || getBootstrapDataIsLoading}/>
        <Modal
          title='Create Appointment'
          isOpen={isCreateAppointmentOpen}
          onRequestClose={this.onCloseCreateAppointment}
        >
        </Modal>
        <Container>
          <Header>
            <HeaderTitle>
              <h1>deleted_name |</h1>
              <h2>Deleted</h2>
            </HeaderTitle>
            <HeaderSelects>
              <SelectContainer>
                <SelectLabel>Sites</SelectLabel>
                <HeaderFilterSelect>
                  <Select
                    options={siteOptions}
                    placeholder='Select Site'
                    value={site}
                    onChange={this.onSiteSelectChange}
                  />
                </HeaderFilterSelect>
              </SelectContainer>
              <SelectContainer>
                <SelectLabel>Warehouse</SelectLabel>
                <HeaderFilterSelect>
                  <Select
                    options={buildingOptions}
                    placeholder='Select Warehouse'
                    value={warehouse}
                    onChange={this.onWarehouseSelectChange}
                  />
                </HeaderFilterSelect>
              </SelectContainer>
            </HeaderSelects>
            <UserDropdown />
          </Header>
          <Content>
            <Tabs
              selectedIndex={selectedTab}
              onSelect={selectedTab => {
                if (selectedTab > 1) closeUpsertAppointment()
                this.setState({ selectedTab })
              }}
            >
              <TabList>
                <Tab><TabIconContainer visible={selectedTab === OrdersIndex}><img src={ordersIcon} alt="orders" /><p>Orders</p></TabIconContainer></Tab>
                <Tab><TabIconContainer visible={selectedTab === AppointmentsIndex}><img src={appointmentsIcon} alt="appointments" /><p>Appointments</p></TabIconContainer></Tab>
                <Tab>
                  <TabIconContainer visible={selectedTab === RequestsIndex}><img src={requestsIcon} alt="requests" /><p>Requests <span className={requestCount ? 'counter' : ''}>({requestCount})</span></p></TabIconContainer>
                </Tab>
                <Tab>
                  <TabIconContainer visible={selectedTab === ReportsIndex}><img src={reportsIcon} alt="reports" /><p>Reports</p></TabIconContainer>
                </Tab>
              </TabList>
              <TabPanel>
                <Orders
                  droppedOrder={droppedOrder}
                  onClose={() => this.setState({ selectedTab: -1 })}
                />
              </TabPanel>
              <TabPanel>
                <Appointments
                  onChangeDate={date => { this.onDateChange(date) }}
                  warehouse={warehouse}
                  startDate={buildingStartDate}
                  onClose={() => this.setState({ selectedTab: -1 })}
                />
              </TabPanel>
              <TabPanel>
                <Requests
                  appointmentStatuses={appointmentStatuses}
                  onClose={() => this.setState({ selectedTab: -1 })}
                />
              </TabPanel>
              <TabPanel>
              </TabPanel>
            </Tabs>
            {
              selectedTab === 3
                ? <Reports/>
                : (
                  <RightPanel>
                    <GridHeader>
                      <GridTimeSelection>
                        <DatePicker
                          HeaderGridDatePicker
                          dateFormat='dddd, MMMM Do'
                          onChangeStartDate={this.onHeaderGridStartDateChange}
                          onChangeEndDate={this.onHeaderGridEndDateChange}
                          startDate={startDate ? moment(startDate) : null}
                          endDate={endDate ? moment(endDate) : null}
                          onChangeStartTime={this.onHeaderGridStartTimeChange}
                          onChangeEndTime={this.onHeaderGridEndTimeChange}
                          startTime={moment(startTime)}
                          endTime={moment(endTime)}
                          rightArrowIcon={calendarRightArrowIcon}
                        />
                        <DayTabs
                          days={days}
                          handleClick={this.handleClickDayTabs}
                          appointmentCounts={appointmentCounts}
                        />
                      </GridTimeSelection>
                      <HourFormatSwitch
                        is24Format={is24Format}
                        onChange={is24Format => this.setState({ is24Format })}
                      />
                      <ZoomSelector
                        level={zoomLevel}
                        minLevel={25}
                        maxLevel={100}
                        step={25}
                        onChange={zoomLevel => this.setState({ zoomLevel })}
                      />
                    </GridHeader>
                    <TimeTable
                      doors={buildingDoors}
                      is24Format={is24Format}
                      doorsSchedule={doorsSchedule}
                      timezone={selectedBuilding && selectedBuilding.get('timezone')}
                      zoomLevel={zoomLevel}
                      appointments={appointmentsForDoors}
                      isUpsertAppointmentVisible={isUpsertAppointmentVisible}
                      appointmentStatuses={appointmentStatuses}
                      deleteAppointment={deleteAppointment}
                      startDate={moment(startDate)}
                      focusAppointment={focusAppointment}
                      setSelectedOrders={setSelectedOrders}
                      onEditAppointment={appt => this.onEditAppointment(appt)}
                      onAppointmentMessage={this.onAppointmentMessage}
                      onAppointmentDurationChange={this.onAppointmentDurationChange}
                    />
                  </RightPanel>
                )
            }
          </Content>
        </Container>
        <UpsertAppointmentModal
          ref={modal => { this.appointmentModal = modal ? modal.getWrappedInstance() : null }}
        />
        <OrderDetailsModal />
        <SystemSettingsModal />
      </DragDropContext>

    )
  }
}

Home.propTypes = {
  getBootstrapDataIsLoading: PropTypes.bool,
  allOrders: PropTypes.any,
  selectedOrders: PropTypes.any,
  appointmentCounts: PropTypes.any,
  closeUpsertAppointment: PropTypes.any,
  isLoggedIn: PropTypes.bool,
  sites: PropTypes.object,
  siteBuildings: PropTypes.object,
  appointments: PropTypes.object,
  getAppointments: PropTypes.func,
  getAllSites: PropTypes.func,
  getBuildingsForSite: PropTypes.func,
  getAppointmentsForDoors: PropTypes.func,
  appointmentsForDoors: PropTypes.any,
  updateAppointment: PropTypes.func,
  ordersForAppointment: PropTypes.any,
  getAllCarriers: PropTypes.any,
  getAllDrivers: PropTypes.any,
  getAllAppointmentStatuses: PropTypes.any,
  getDoorsForBuilding: PropTypes.any,
  getAreasForBuilding: PropTypes.any,
  buildings: PropTypes.object,
  areas: PropTypes.object,
  orders: PropTypes.object,
  carriers: PropTypes.array,
  drivers: PropTypes.array,
  appointmentStatuses: PropTypes.object,
  buildingDoors: PropTypes.object,
  doorsSchedule: PropTypes.object,
  buildingAreas: PropTypes.object,
  setSelectedOrders: PropTypes.any,
  openEditAppointment: PropTypes.any,
  createFromRequest: PropTypes.func,
  warehouse: PropTypes.number,
  setSite: PropTypes.func,
  setWarehouse: PropTypes.func,
  setStartTime: PropTypes.func,
  setEndTime: PropTypes.func,
  setStartDate: PropTypes.func,
  setEndDate: PropTypes.func,
  startTime: PropTypes.object,
  endTime: PropTypes.object,
  startDate: PropTypes.object,
  endDate: PropTypes.object,
  focusAppointment: PropTypes.object,
  site: PropTypes.number,
  isUpsertAppointmentVisible: PropTypes.bool,
  deleteAppointment: PropTypes.func,
  createAppointment: PropTypes.func,
  getDoorDuration: PropTypes.func,
  moveAppointment: PropTypes.func,
  getNumberOfCarrierRequests: PropTypes.func,
  numberOfRequests: PropTypes.object,
  carrierRequests: PropTypes.object,
  checkedOrders: PropTypes.Any,
  location: PropTypes.object,
  appointmentsAreLoading: PropTypes.bool,
  updateAppointmentWithSocketAppointment: PropTypes.func,
  updateAppointmentForDoorWithSocketAppointment: PropTypes.func,
  getBootstrapData: PropTypes.func,
  defaultBuildingId: PropTypes.string,
  appointmentsForDoorsAreLoading: PropTypes.bool,
  ordersAreLoading: PropTypes.bool,
  carrierRequestsAreLoading: PropTypes.bool,
  getUsers: PropTypes.func,
  setRecalculateDurationFlag: PropTypes.func
}

const mapStateToProps = state => ({
  getBootstrapDataIsLoading: getBootstrapDataIsLoading(state),
  allOrders: getAllOrders(state) || [],
  selectedOrders: getSelectedOrders(state) || [],
  site: getSite(state),
  warehouse: getWarehouse(state),
  startDate: getStartDate(state),
  endDate: getEndDate(state),
  focusAppointment: getFocusAppointment(state),
  startTime: getStartTime(state),
  endTime: getEndTime(state),
  isLoggedIn: isLoggedIn(state),
  appointments: getAppointments(state),
  appointmentCounts: getAppointmentCounts(state),
  sites: getAllSites(state),
  siteBuildings: getBuildingsForSite(state),
  appointmentsForDoors: getAppointmentsForDoors(state),
  ordersForAppointment: appointmentId => getOrdersForAppointment(state, appointmentId),
  carriers: getAllCarriers(state),
  drivers: getAllDrivers(state),
  appointmentStatuses: getAllAppointmentStatuses(state),
  buildingDoors: getDoorsForBuilding(state),
  doorsSchedule: getScheduleForDoors(state),
  buildingAreas: getAreasForBuilding(state),
  buildings: getAllBuildings(state),
  areas: getAreas(state),
  orders: getOrders(state),
  isUpsertAppointmentVisible: getIsUpsertAppointmentVisible(state),
  numberOfRequests: numberOfCarrierRequests(state),
  checkedOrders: getCheckedOrders(state) || [],
  carrierRequests: getAllCarrierRequests(state),
  defaultBuildingId: getDefaultBuilding(state),
  appointmentsForDoorsAreLoading: getAppointmentsForDoorsIsLoading(state),
  ordersAreLoading: getOrdersIsLoading(state),
  appointmentsAreLoading: getAppointmentsIsLoading(state),
  carrierRequestsAreLoading: getAllCarrierRequestsIsLoading(state)
})

const mapDispatchToProps = dispatch => ({
  setRecalculateDurationFlag: flag => dispatch(AppointmentsActions.setRecalculateDurationFlag(flag)),
  closeUpsertAppointment: () => dispatch(AppointmentsActions.closeUpsertAppointment()),
  setSite: payload => dispatch(AppActions.setSite(payload)),
  setWarehouse: payload => dispatch(AppActions.setWarehouse(payload)),
  setStartDate: payload => dispatch(AppActions.setStartDate(payload)),
  setEndDate: payload => dispatch(AppActions.setEndDate(payload)),
  setStartTime: payload => dispatch(AppActions.setStartTime(payload)),
  setEndTime: payload => dispatch(AppActions.setEndTime(payload)),
  getAppointments: (payload = {}) => dispatch(AppointmentsActions.getAppointments(payload)),
  getBuildingsForSite: payload => dispatch(SitesActions.getBuildingsForSite(payload)),
  getAppointmentsForDoors: payload => dispatch(DoorsActions.getAppointmentsForDoors(payload)),
  updateAppointment: payload => dispatch(AppointmentsActions.updateAppointment(payload)),
  getDoorsForBuilding: payload => dispatch(BuildingsActions.getDoorsForBuilding(payload)),
  getAreasForBuilding: payload => dispatch(BuildingsActions.getAreasForBuilding(payload)),
  openEditAppointment: appointment => dispatch(AppointmentsActions.openEditAppointment(appointment)),
  createFromRequest: (request, apptData) => dispatch(AppointmentsActions.createFromRequest(request, apptData)),
  setSelectedOrders: orders => dispatch(OrdersActions.setSelectedOrders(orders)),
  deleteAppointment: appointment => dispatch(AppointmentsActions.deleteAppointment(appointment.get('id'))),
  createAppointment: payload => dispatch(AppointmentsActions.createAppointment(payload)),
  getDoorDuration: payload => dispatch(DoorsActions.getDoorDuration(payload)),
  moveAppointment: payload => dispatch(AppointmentsActions.moveAppointment(payload)),
  getNumberOfCarrierRequests: () => dispatch(CarrierRequestsActions.getNumberOfCarrierRequests()),
  updateAppointmentWithSocketAppointment: payload => dispatch(AppointmentsActions.updateAppointmentWithSocketAppointment(payload)),
  updateAppointmentForDoorWithSocketAppointment: payload => dispatch(DoorsActions.updateAppointmentForDoorWithSocketAppointment(payload)),
  getBootstrapData: payload => dispatch(FeedsActions.getBootstrapData(payload)),
  getUsers: () => dispatch(UsersActions.getUsers())
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
