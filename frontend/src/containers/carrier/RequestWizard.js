import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import range from 'lodash.range'
import { withRouter } from 'react-router-dom'
import CarrierRequestsActions from '../../modules/carrierRequests/actions'
import EmailsActions from '../../modules/emails/actions'

import ConfirmOrder from './ConfirmOrder'
import RequestTimeRange from './RequestTimeRange'
import ContactInfo from './ContactInfo'
import ConfirmCarrierRequest from './ConfirmCarrierRequest'
import Success from './Success'

import ConfirmCarrierRequestEmailTemplate from './emailTemplates/appointmentConfirmations'

const Container = styled.div`
  margin: 100px auto;
`

const Steps = styled.div`
  display: flex;
  position: relative;
  margin: 0 40px;
`

const StepContainer = styled.div`
  display: flex;
`

const Triangle = styled.div`
  margin-top: 18px;
  width: 0;
  height: 0;
  border-top: 6px solid transparent;
  border-bottom: 6px solid transparent;
  border-left: 6px solid ${props => props.theme.gray};
`

const Step = styled.div`
margin: 10px 50px;
width: 30px;
height: 30px;
border-radius: 50%;
text-align: center;
line-height: 30px;
  border: 1px solid ${props => props.active || props.finished ? props.theme.brandDarker : props.theme.lightGray};
  background-color: ${props => props.finished ? props.theme.brandDarker : '#fff'};
  color: ${props => props.finished ? '#fff' : props.active ? props.theme.brandDarker : props.theme.lightGray};
`

const Popup = styled.div`
  position: relative;
  left: 73px;
  top: -70px;
`

const PopupText = styled.div`
  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.07);
  font-size: 14px;
  width: 110px;
  background-color: #fff;
  color: ${props => props.theme.brandDarker};
  text-align: center;
  border-radius: 2px;
  padding: 15px;
  position: absolute;
  z-index: 1;
  left: ${props => props.left}px;
  margin-left: -80px;
  &:after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #fff transparent transparent transparent;
}
`

const NUMBER_OF_STEPS = 4
const CONFIRM_ORDER_STEP = 1
const REQUEST_TIME_RANGE_STEP = 2
const CONTACT_INFO_STEP = 3
const CONFIRM_CARRIER_REQUEST_STEP = 4
const SUCCESS_STEP = 5
const POPUP_TEXTS = [
  'Get appointment details',
  'Request a time range',
  'Enter contact information',
  'Confirm Request'
]

class RequestWizard extends Component {
  state = {
    activeScreen: CONFIRM_ORDER_STEP // Set to SUCCESS_STEP to display Success Information view
  }

  componentDidUpdate (prevProps) {
    // force component re-render in case we click the same link again
    if (prevProps.location.key !== this.props.location.key) {
      this.setState({
        activeScreen: CONFIRM_ORDER_STEP
      })

      this.props.resetCreatingCarrierRequest()
    }
  }

  componentDidMount () {
    this.props.resetCreatingCarrierRequest()
  }

  stepBack = () => {
    this.setState(state => ({
      activeScreen: state.activeScreen - 1
    })
    )
  }

  stepForward = () => {
    this.setState(state => ({
      activeScreen: state.activeScreen + 1
    })
    )
  }

  onRestartSteps = () => {
    this.setState({
      activeScreen: CONFIRM_ORDER_STEP
    })
  }

  onSendEmail = values => {
    const { sendEmail } = this.props
    const { from, subject, body } = ConfirmCarrierRequestEmailTemplate
    let content = body
    content = content.replace('<PICKUP_DATE>', values.pickupDate)
    content = content.replace('<PICKUP_TIME>', values.pickupTime)
    content = content.replace('<SITE>', values.site)
    content = content.replace('<BUILDING_NAME>', values.buildingName)
    content = content.replace('<ADDRESS>', values.address)
    content = content.replace('<APPOINTMENT_ID>', values.appointmentId)
    content = content.replace('<SOs>', values.SOs)
    content = content.replace('<POs>', values.POs)

    sendEmail({
      template: 'basic',
      subject: subject,
      from: from,
      to: values.to,
      content
    })
  }

  onCreateAccount = () => {
    this.props.history.push('/carrier/account/create')
  }

  render () {
    const { activeScreen } = this.state

    return (
      <Container>
        {activeScreen !== SUCCESS_STEP && <Steps>
          {
            range(1, NUMBER_OF_STEPS + 1).map((step, index) =>
              <StepContainer key={index}>
                <Popup>
                  {activeScreen === step && <PopupText left={step}> {POPUP_TEXTS[index]} </PopupText>}
                </Popup>
                <Step finished={activeScreen > step} active={activeScreen === step}> {step} </Step>
                {step !== 4 && <Triangle />}
              </StepContainer>
            )
          }
        </Steps>
        }
        {activeScreen === CONFIRM_ORDER_STEP && <ConfirmOrder stepForward={this.stepForward} onCreateAccount={this.onCreateAccount}/>}
        {activeScreen === REQUEST_TIME_RANGE_STEP && <RequestTimeRange stepBack={this.stepBack} stepForward={this.stepForward} />}
        {activeScreen === CONTACT_INFO_STEP && <ContactInfo stepBack={this.stepBack} stepForward={this.stepForward} />}
        {activeScreen === CONFIRM_CARRIER_REQUEST_STEP && <ConfirmCarrierRequest stepBack={this.stepBack} stepForward={this.stepForward} onSendEmail={this.onSendEmail} />}
        {activeScreen === SUCCESS_STEP && <Success onRestartSteps={this.onRestartSteps} onCreateAccount={this.onCreateAccount} />}
      </Container>
    )
  }
}

RequestWizard.propTypes = {
  location: PropTypes.object,
  resetCreatingCarrierRequest: PropTypes.func,
  sendEmail: PropTypes.func,
  history: PropTypes.object
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  resetCreatingCarrierRequest: () => dispatch(CarrierRequestsActions.resetCreatingCarrierRequest()),
  sendEmail: payload => dispatch(EmailsActions.sendEmail(payload))
})

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestWizard))
