import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Form, Field } from 'react-final-form'
import styled from 'styled-components'
import UnstyledTextInput from '../../components/TextInput'
import Button from '../../components/Button'
import PropTypes from 'prop-types'

import { getAllCarriers, getCarrierUser } from '../../modules/carriers/selectors'
import { getAllDrivers } from '../../modules/drivers/selectors'
import CarriersActions from '../../modules/carriers/actions'
import DriversActions from '../../modules/drivers/actions'
import UsersActions from '../../modules/users/actions'

const Container = styled.div`
  margin: 100px auto;
  margin-top: 50px;
`

const Row = styled.div`
  display: flex;
  margin: 0px -10px 10px 0px;
`

const Column = styled.div`
  width: 50%;
  padding: 0px 10px;
`

const ContactMethod = styled.div`
  margin-top: 12px;
  color: #898989;
  font-weight: bold;
  font-size: 12px;
`

const TextInput = styled(UnstyledTextInput)`
  background-color: ${props => props.theme.lighterGray};
  color: hsl(0,0%,20%);
`

const RadioOptions = styled.div`
  display: flex;
  padding: 10px 0px;
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #898989;
`

const Radio = styled(Field)`
  margin-left: 10px;
  margin-right: 5px;
`

const ConfirmButton = styled(Button)`
  height: 36px;
  font-size: 12px;
  width: 100%;
  color: ${props => props.disabled ? props.theme.brandDarker : '#fff'};
  background-color: ${props => props.disabled ? '#fff' : props.theme.brandDarker};
`

const Header = styled.h4`
  font-size: 16px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #0eb48f;
`

const StyledForm = styled.form`
 width: 600px;
`

class UpsertUser extends Component {
  state = {
    carrierId: null,
    carrierName: null,
    driverId: null,
    driverName: null,
    email: null,
    phone: null,
    password: null,
    password2: null,
    prefContactMethod: 'email'
  }

  componentDidMount () {
    this.props.getAllCarriers()
    this.props.getAllDrivers()
  }

  onPhoneChange = e => {
    const { value } = e.target
    const { prefContactMethod } = this.state

    this.setState({
      phone: value,
      prefContactMethod: value ? prefContactMethod : 'email'
    })
  }

  onBack = () => {
    const { updateCreatingCarrierRequest, stepBack } = this.props
    updateCreatingCarrierRequest({ ...this.state })
    stepBack()
  }

  onSubmit = data => {
    const { user, createCarrierUser, updateMe, history } = this.props
    const payload = {
      email: data.email,
      prefContactMethod: data.prefContactMethod,
      phone: data.phone,
      password: data.password,
      roles: [3]
    }

    if (data.carrier) {
      payload.carrierName = data.carrier
    }

    if (data.driver) {
      payload.driverName = data.driver
    }
    if (user) {
      updateMe({
        id: user.get('id'),
        ...payload
      })
      history.push('/carrier')
    } else {
      createCarrierUser(payload)
      history.push('/carrier')
    }
  }

  validate = values => {
    const { user } = this.props
    const errors = {}

    const reNumber = new RegExp('[0-9]')
    const reLowerCase = new RegExp('[a-z]')
    const reUpperCase = new RegExp('[A-Z]')
    const reSpecialChar = new RegExp('[^A-Za-z0-9]')

    if (!values.carrier) {
      errors.carrier = 'Required'
    }

    if (!values.email) {
      errors.email = 'Required'
    }

    if (!values.prefContactMethod) {
      errors.prefContactMethod = 'Required'
    }

    if (!user && !values.password) {
      errors.password = 'Required'
    }

    if (values.password) {
      if (values.password.length < 6) {
        if (!errors.password) errors.password = ''
        errors.password += 'Must be at least 6 characters long. '
      } if (!reNumber.test(values.password)) {
        if (!errors.password) errors.password = ''
        errors.password += 'Must contain a number. '
      } if (!reLowerCase.test(values.password)) {
        if (!errors.password) errors.password = ''
        errors.password += 'Must contain a lower case. '
      } if (!reUpperCase.test(values.password)) {
        if (!errors.password) errors.password = ''
        errors.password += 'Must contain a upper case. '
      } if (!reSpecialChar.test(values.password)) {
        if (!errors.password) errors.password = ''
        errors.password += 'Must contain a special character. '
      }
    }

    if (!user && !values.password2) {
      errors.password2 = 'Required'
    }

    if (values.password && values.password2 && (values.password !== values.password2)) {
      errors.password = 'Passwords needs to match'
      errors.password2 = 'Passwords needs to match'
    }

    return errors
  }

  render () {
    const { user } = this.props
    const carriers = this.props.carriers || []
    const drivers = this.props.drivers || []

    const userDriver = user && user.get('driverId') && drivers.find(d => d.get('id') === user.get('driverId'))
    const userCarrier = user && user.get('carrierId') && carriers.find(c => c.get('id') === user.get('carrierId'))
    let accountTitle = 'Create an Account'
    let passwordTitle = 'Password'
    let buttonText = 'Create Account'
    const initialValues = {
      id: user && user.get('id'),
      email: user && user.get('email'),
      phone: user && user.get('phone'),
      driver: userDriver && `${userDriver.get('firstName') || ''} ${userDriver.get('lastName') || ''}`,
      carrier: userCarrier && userCarrier.get('name'),
      prefContactMethod: user && (user.get('prefContactMethod') || 'email')
    }

    if (initialValues && initialValues.id) {
      accountTitle = 'Update Account'
      passwordTitle = 'Change Password'
      buttonText = 'Update Changes'
    }

    return (
      <Container>
        <Form
          initialValues={initialValues || { prefContactMethod: 'email' }}
          onSubmit={this.onSubmit}
          validate={this.validate}
          subscription={{ submitting: true, pristine: true }}
          render={({ handleSubmit, pristine, submitting, invalid, form }) => (
            <StyledForm onSubmit={handleSubmit}>
              <Header>{accountTitle}</Header>
              <Row>
                <Column>
                  <Field
                    name="carrier"
                    render={({ input, meta }) => (
                      <TextInput
                        {...input}
                        error={meta.touched && meta.error}
                        label="Carrier Name*"
                      />
                    )}
                  />
                </Column>
                <Column>
                  <Field
                    name="email"
                    render={({ input, meta }) => (
                      <TextInput
                        {...input}
                        error={meta.touched && meta.error}
                        label="Email*"
                      />
                    )}
                  />
                </Column>
              </Row>
              <Row>
                <Column>
                  <Field
                    name="driver"
                    render={({ input, meta }) => (
                      <TextInput
                        {...input}
                        error={meta.touched && meta.error}
                        label="Driver"
                      />
                    )}
                  />
                </Column>
                <Column>
                  <Field
                    name="phone"
                    render={({ input, meta }) => (
                      <TextInput
                        {...input}
                        error={meta.touched && meta.error}
                        fixedLabel={true}
                        label="Mobile Number"
                      />
                    )}
                  />
                </Column>
              </Row>
              <Row>
                <Column> </Column>
                <Column>
                  <ContactMethod>Preferred method of contact</ContactMethod>
                  <RadioOptions>
                    <Radio
                      type="radio"
                      name="prefContactMethod"
                      value="email"
                      component="input"
                    /> {' '} Email
                    <Radio
                      type="radio"
                      name="prefContactMethod"
                      value="sms"
                      component="input"
                    /> {' '} Text
                  </RadioOptions>
                </Column>
              </Row>
              <Header>{passwordTitle}</Header>
              <Row>
                <Column>
                  <Field
                    name="password"
                    render={({ input, meta }) => (
                      <TextInput
                        {...input}
                        error={meta.touched && meta.error}
                        type={'password'}
                        fixedLabel={true}
                        label="New password*"
                      />
                    )}
                  />
                </Column>
                <Column>
                  <Field
                    name="password2"
                    render={({ input, meta }) => (
                      <TextInput
                        {...input}
                        error={meta.touched && meta.error}
                        type={'password'}
                        fixedLabel={true}
                        label="Re-Enter New Password*"
                      />
                    )}
                  />
                </Column>
              </Row>
              <Row/>
              <Row/>
              <Row>
                <Column>
                  <ConfirmButton
                    type="submit"
                    disabled={invalid}
                  >
                    {buttonText}
                  </ConfirmButton>
                </Column>
              </Row>
            </StyledForm>
          )}
        />
      </Container>
    )
  }
}

UpsertUser.propTypes = {
  createCarrierUser: PropTypes.func,
  updateMe: PropTypes.func,
  stepBack: PropTypes.func,
  stepForward: PropTypes.func,
  createCarrierRequest: PropTypes.func,
  getAllCarriers: PropTypes.func,
  getAllDrivers: PropTypes.func,
  carriers: PropTypes.object,
  drivers: PropTypes.object
}

const mapStateToProps = state => ({
  carriers: getAllCarriers(state),
  drivers: getAllDrivers(state),
  user: getCarrierUser(state)
})

const mapDispatchToProps = dispatch => ({
  createCarrierUser: payload => dispatch(UsersActions.createCarrierUser(payload)),
  updateMe: payload => dispatch(UsersActions.updateMe(payload)),
  getAllCarriers: () => dispatch(CarriersActions.getAllCarriers()),
  getAllDrivers: () => dispatch(DriversActions.getAllDrivers())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpsertUser)
