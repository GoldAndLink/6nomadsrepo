import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import moment from 'moment'
import Button from '../../components/Button'
import FormDatePicker from '../../components/FormDatePicker'
import { connect } from 'react-redux'
import CarrierRequestsActions from '../../modules/carrierRequests/actions'
import { getCarrierRequestPickUpTimes, getCreatingCarrierRequest } from '../../modules/carrierRequests/selectors'
import Select from '../../components/Select'

const MAX_COLUMN = 12
const FULL_WIDTH_PERCENTAGE = 100

const selectStyles = error => ({
  container: base => ({ ...base, border: 'none', boxShadow: 'none', outline: 'none', fontSize: '18px' }),
  control: base => ({
    ...base,
    borderRadius: 2,
    borderWidth: '0px 0px 1px 0px',
    borderColor: error ? '#d9534f' : '#0eb48f',
    boxShadow: 'none',
    outline: 'none',
    backgroundColor: '#f4f6f9',
    color: '#bbbbbb'
  }),
  valueContainer: base => ({
    ...base,
    padding: '2px 0px',
    color: '#bbbbbb'
  }),
  menuList: base => ({
    ...base,
    maxHeight: '140px',
    overflow: 'auto'
  }),
  menu: base => ({
    ...base,
    marginTop: '2px',
    color: '#bbbbbb'
  }),
  singleValue: base => ({
    color: '#bbbbbb'
  }),
  placeholder: base => ({
    color: '#bbbbbb'
  }),
  option: (base, { isSelected, isFocused }) => ({
    ...base,
    backgroundColor: isSelected || isFocused ? '#d2f8d8' : null,
    color: isSelected || isFocused ? '#61c9b5' : 'bbbbbb'
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: base => ({
    ...base,
    transform: 'scale(0.6)',
    padding: '8px 0px'
  })
})

const FormContent = styled.form`
  display: flex;
  flex-direction: column;
  padding: 18px 41px 0 41px;
`
const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const Col = styled.div`
  width: ${props => FULL_WIDTH_PERCENTAGE / (MAX_COLUMN / props.md)}%;
  padding: 0px 10px;
`

const DatePickerContainter = styled.div`
  margin-bottom: 20px;
  & .react-datepicker-wrapper {
    display: block;
  }
  & .react-datepicker__input-container {
    display: block;
  }
`
const StyledFormDatePicker = styled(FormDatePicker)`
  background-color: ${props => props.theme.lighterGray};
  color: ${props => props.theme.lightGray};
  border-color: ${props => props.theme.brandDarker} !important;
  font-size: 18px;
`

const BackContainer = styled.div`
  display: flex;
  align-items: center;
  margin-top: 100px;
`
const Back = styled.div`
  width: 25px;
  height: 25px;
  border-radius: 50%;
  border: 1px solid ${props => props.theme.brandDarker};
  background-color: #fff;
  margin-right: 10px;
  font-size: 18px;
  line-height: 25px;
  text-align: center;
  color: ${props => props.theme.brandDarker}
  cursor: pointer;
`

const BackText = styled.div`
  text-transform: uppercase;
  color: ${props => props.theme.brandDarker}
  font-size: 12px;
`
const ContactInfoButton = styled(Button)`
  height: 36px;
  margin-top: 18px;
  font-size: 12px;
  width: 100%;
  color: ${props => props.disabled ? props.theme.brandDarker : '#fff'};
  background-color: ${props => props.disabled ? '#fff' : props.theme.brandDarker};
`
const SelectLabel = styled.div`
  color: ${props => props.theme.gray};
  pointer-events: none;
  font-weight: 600;
  font-size: 9px;
`
const SelectContainer = styled.div`
  margin-left: 5px;
`
class RequestTimeRange extends Component {
  state = {
    selectedTime: null,
    selectedDate: null
  }
  componentDidMount = () => {
    const { creatingCarrierRequest } = this.props
    let date = null
    this.props.getCarrierRequestPickUpTimes()
    if (creatingCarrierRequest.get('date')) {
      date = moment(creatingCarrierRequest.get('date'))
    } else {
      const dates = creatingCarrierRequest.get('orders').toJS().map(order => moment(order.requiredShipDate))
      date = moment.min(dates)
    }

    this.setState({
      selectedDate: date,
      selectedTime: creatingCarrierRequest.get('pickupTime')
    })
  }
  onChangeDate = date => {
    this.setState({
      selectedDate: date
    })
  }

  onChangeTime = time => {
    this.setState({
      selectedTime: time
    })
  }

  onClickContactInfo = () => {
    const { updateCreatingCarrierRequest, stepForward } = this.props
    const { selectedTime, selectedDate } = this.state

    updateCreatingCarrierRequest({
      pickupTime: selectedTime,
      date: selectedDate
    })
    stepForward()
  }

  onClickBack = () => {
    const { updateCreatingCarrierRequest, stepBack } = this.props
    const { selectedTime, selectedDate } = this.state

    updateCreatingCarrierRequest({
      pickupTime: selectedTime,
      date: selectedDate
    })
    stepBack()
  }

  render () {
    const { carrierRequestsPickUpTimes } = this.props
    const { selectedTime, selectedDate } = this.state
    const timeOptions = carrierRequestsPickUpTimes ? carrierRequestsPickUpTimes.map(time => ({ value: time.get('id'), label: moment.utc(time.get('startTime')).format('HH:mm').toString() + ' - ' + moment.utc(time.get('endTime')).format('HH:mm') })) : []

    return (
      <FormContent>
        <Row>
          <Col md={2} />
          <Col md={8}>
            <DatePickerContainter>
              <StyledFormDatePicker
                value={selectedDate}
                label="Pick Up Date"
                dateFormat='ddd, MMMM D, YYYY'
                selected={selectedDate}
                placeholderText=''
                autoComplete="off"
                onChange={this.onChangeDate}
              />
            </DatePickerContainter>
          </Col>
          <Col md={2} />
        </Row>
        <Row>
          <Col md={2} />
          <Col md={4}>
            <SelectContainer>
              <SelectLabel>Appointment Request Start Time</SelectLabel>
              <Select
                styles={selectStyles}
                value={selectedTime}
                options={timeOptions}
                placeholder={''}
                onChange={this.onChangeTime}
              />
            </SelectContainer>
          </Col>
          <Col md={4}>
            <ContactInfoButton disabled={!(selectedTime && selectedDate)} type="button" onClick={() => this.onClickContactInfo()}>Enter Contact Info</ContactInfoButton>
          </Col>
          <Col md={2} />
        </Row>
        <BackContainer>
          <Back onClick={() => this.onClickBack()}>&#8810;</Back>
          <BackText>Back</BackText>
        </BackContainer>
      </FormContent>
    )
  }
}
RequestTimeRange.propTypes = {
  stepBack: PropTypes.func,
  stepForward: PropTypes.func,
  updateCreatingCarrierRequest: PropTypes.func,
  carrierRequestsPickUpTimes: PropTypes.object,
  getCarrierRequestPickUpTimes: PropTypes.func,
  creatingCarrierRequest: PropTypes.object
}

const mapStateToProps = state => ({
  carrierRequestsPickUpTimes: getCarrierRequestPickUpTimes(state),
  creatingCarrierRequest: getCreatingCarrierRequest(state)
})

const mapDispatchToProps = dispatch => ({
  updateCreatingCarrierRequest: payload => dispatch(CarrierRequestsActions.updateCreatingCarrierRequest(payload)),
  getCarrierRequestPickUpTimes: () => dispatch(CarrierRequestsActions.getCarrierRequestPickUpTimes())
})

export default connect(mapStateToProps, mapDispatchToProps)(RequestTimeRange)
