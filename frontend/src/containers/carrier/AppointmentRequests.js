/* eslint-disable no-useless-constructor */
import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import moment from 'moment'
import config from '../../config'
import io from 'socket.io-client'

import Button from '../../components/Button'
import RescheduleRequestModal from '../../components/RescheduleRequestModal'

import { getAllCarrierRequests } from '../../modules/carrierRequests/selectors'
import { getAppointments } from '../../modules/entities/selectors'
import { getAllCarriers } from '../../modules/carriers/selectors'
import { getAllAppointmentStatuses } from '../../modules/appointments/selectors'
import { getDoorById } from '../../modules/doors/selectors'

import CarrierRequestsActions from '../../modules/carrierRequests/actions'
import AppointmentsActions from '../../modules/appointments/actions'
import LocationsActions from '../../modules/locations/actions'
import CarriersActions from '../../modules/carriers/actions'
import FeedsActions from '../../modules/feeds/actions'

const MAX_COLUMN = 12
const FULL_WIDTH_PERCENTAGE = 100

const PENDING_STATUS = 'pending'
const SCHEDULED_STATUS = 'scheduled'
const DRAFT_STATUS = 'draft'
const CANCELED_STATUS = 'canceled'
const RESCHEDULED_STATUS = 'reschedule'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 18px;
  margin-bottom: 100px;
  flex: 1;
  padding-left: 20%;
  padding-right: 20%;
`

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const Col = styled.div`
  width: ${props => FULL_WIDTH_PERCENTAGE / (MAX_COLUMN / props.md)}%;
  ${props => props.capitalize && css`
    text-transform: capitalize;
  `}
`

const SectionTitle = styled.div`
  color: ${props => props.theme.brandDarker}
  font-size: 16px;
  margin-top: 10px;
  margin-bottom: 25px;
  font-weight: bold;
`

const Section = styled.div`
`

const HorizontalLine = styled.div`
  border-bottom: 1px solid ${props => props.theme.lightGray}
  margin-bottom: 15px;
`

const Label = styled.div`
  color: ${props => props.theme.gray}
  margin: 6px 0;
  font-size: 12px;
`

const Value = styled.div`
  color: ${props => props.theme.lightGray}
  font-size: 16px;
  font-weight: 300;
`

const ButtonsContainer = styled.div`
  display: flex;
  width: 40%;
  justify-content: space-between;
  font-size: 12px;
  margin-bottom: 20px;
  margin-top: 20px;
`

const StyledGreenButton = styled(Button)`
  height: 36px;
  width: 155px;
  color: ${props => props.disabled ? props.theme.lightGray : '#fff'};
  border-color: ${props => props.disabled ? props.theme.lightGray : props.theme.brandDarker};
  background-color: ${props => props.disabled ? '#fff' : props.theme.brandDarker};
`

const StyledWhiteButton = styled(Button)`
  height: 36px;
  width: 155px;
  color: ${props => props.disabled ? props.theme.lightGray : props.theme.brandDarker};
  border-color: ${props => props.disabled ? props.theme.lightGray : props.theme.brandDarker};
`

class AppointmentRequests extends Component {
  state = {
    isRescheduleRequestModalOpen: false,
    socket: io(`${config.API_BASE}/carrierRequests`)
  }

  componentDidMount () {
    this.props.getAllCarrierRequests({ carrierPortal: true, sortByDate: 'desc', guid: this.props.match.params.guid })
    this.props.getAllAppointmentStatuses()
    // array too big
    // this.props.getAllLocations()
    this.props.getBootstrapData({ carrierPortal: true })
    this.props.getAllCarriers()

    this.state.socket.off('carrierRequest')
    // keep listening for changes
    this.state.socket.on('carrierRequest', socketCarrierRequest => {
      this.props.updateCarrierRequestsWithSocketCarrierRequestOnCarrierSide({ socketCarrierRequest })
    })
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.match.params && this.props.match.params.guid && this.props.match.params.guid !== prevProps.match.params.guid) {
      this.props.getAllCarrierRequests({ carrierPortal: true, sortByDate: 'desc', guid: this.props.match.params.guid })
    }
  }

  onCloseRescheduleRequestModal = () => {
    this.setState({
      isRescheduleRequestModalOpen: false
    })
  }

  onRequestModifications = carrierRequestId => {
    this.setState({
      isRescheduleRequestModalOpen: carrierRequestId
    })
  }

  onSelectRescheduleDate = (carrierRequestId, selectedTime) => {
    const { carrierRequests } = this.props
    const carrierRequest = carrierRequests &&
      carrierRequests.find(cr => cr.get('id') === carrierRequestId)

    if (carrierRequest && !carrierRequest.get('appointmentId')) {
      this.props.updateCarrierRequest({
        id: carrierRequestId,
        status: PENDING_STATUS,
        timeStart: moment(selectedTime),
        timeEnd: moment(selectedTime).add(1, 'hour')
      })
    } else {
      this.props.updateCarrierRequest({
        id: carrierRequestId,
        status: RESCHEDULED_STATUS,
        rescheduleTimeSuggestion: selectedTime
      })
    }
    this.onCloseRescheduleRequestModal()
  }

  onRequestCancellation = carrierRequestId => this.props.updateCarrierRequest({ id: carrierRequestId, status: CANCELED_STATUS })
  onCancelRequest = carrierRequestId => this.props.updateCarrierRequest({ id: carrierRequestId, status: CANCELED_STATUS })

  _renderRow = carrierRequest => {
    const { carriers, getDoorById } = this.props

    const door = carrierRequest && carrierRequest.get('appointment') && getDoorById(carrierRequest.get('appointment').get('doorId'))
    const timezone = (door && door.get('area').get('building').get('timezone')) || 'UTC'
    const carrier = carriers && carriers.find(carrier => carrier.get('id') === parseInt(carrierRequest.get('carrierId')))

    let timeStart = 'N/A'
    let date = 'N/A'
    if (carrierRequest.get('status') === SCHEDULED_STATUS && carrierRequest.get('appointment')) {
      timeStart = moment.tz(carrierRequest.get('appointment').get('date'), timezone).format('HH:mm')
      date = moment.tz(carrierRequest.get('appointment').get('date'), timezone).format('ddd, MM/DD/YYYY')
    } else if (carrierRequest.get('status') === PENDING_STATUS && !carrierRequest.get('appointmentId')) {
      timeStart = moment.utc(carrierRequest.get('timeStart')).format('HH:mm')
      date = moment.utc(carrierRequest.get('date')).format('ddd, MM/DD/YYYY')
    } else if (carrierRequest.get('status') === RESCHEDULED_STATUS && carrierRequest.get('rescheduleTimeSuggestion')) {
      timeStart = moment.tz(carrierRequest.get('rescheduleTimeSuggestion'), timezone).format('HH:mm')
      date = moment.tz(carrierRequest.get('rescheduleTimeSuggestion'), timezone).format('ddd, MM/DD/YYYY')
    }

    return (
      <Row>
        <Col md={4}><Value>{carrier ? carrier.get('name') : 'N/A'}</Value></Col>
        <Col md={4}><Value>{date}</Value></Col>
        <Col md={4}><Value>{timeStart}</Value></Col>
        <Col md={4} capitalize><Value>{carrierRequest ? carrierRequest.get('status') : 'N/A'}</Value></Col>
      </Row>
    )
  }

  _renderRescheduleModal = () => {
    const { isRescheduleRequestModalOpen } = this.state
    const { carrierRequests, getDoorById } = this.props
    const selectedRequest = carrierRequests &&
      carrierRequests.find(cr => cr.get('id') === isRescheduleRequestModalOpen)
    const door = selectedRequest && selectedRequest.get('appointment') &&
      getDoorById(selectedRequest.get('appointment').get('doorId'))
    const timezone = (door && door.get('area').get('building').get('timezone')) || 'UTC'
    const selectedRequestsDate = selectedRequest && (selectedRequest.get('appointment') ? moment.tz(selectedRequest.get('appointment').get('date'), timezone) : moment.utc(selectedRequest.get('date')))

    return (
      selectedRequestsDate && <RescheduleRequestModal
        id={isRescheduleRequestModalOpen}
        isOpen={!!isRescheduleRequestModalOpen}
        onClose={this.onCloseRescheduleRequestModal}
        onSelectDate={this.onSelectRescheduleDate}
        date={selectedRequestsDate}
        timezone={timezone}/>
    )
  }

  /* A carrier can only request cancel/change if there is:
  (1) no appt associated with the CR,
  (2) the appt is in draft or scheduled status. */
  _isRescheduleDisabled = (carrierRequest, appointment, appointmentStatus) => {
    // invalid state
    if (carrierRequest.get('status') === SCHEDULED_STATUS && !carrierRequest.get('appointment')) return true

    const disabled = !!carrierRequest && carrierRequest.get('status') ? carrierRequest.get('status').toLowerCase() === RESCHEDULED_STATUS : false
    const apptStatus = appointmentStatus ? appointmentStatus.get('name').toLowerCase() : null
    return disabled && (!appointment || (appointment && (apptStatus === SCHEDULED_STATUS || apptStatus === DRAFT_STATUS)))
  }

  _isCancelDisabled = (carrierRequest, appointment, appointmentStatus) => {
    // invalid state
    if (carrierRequest.get('status') === SCHEDULED_STATUS && !carrierRequest.get('appointment')) return true

    const disabled = !!carrierRequest && carrierRequest.get('status') ? carrierRequest.get('status').toLowerCase() === CANCELED_STATUS : false
    const apptStatus = appointmentStatus ? appointmentStatus.get('name').toLowerCase() : null
    return disabled && (!appointment || (appointment && (apptStatus === SCHEDULED_STATUS || apptStatus === DRAFT_STATUS)))
  }

  _renderButtons = carrierRequest => {
    const { appointments, appointmentStatuses } = this.props
    const appointment = appointments && appointments.find(appointment => appointment.get('id') === parseInt(carrierRequest.get('appointmentId')))
    let requestedStatus = 'N/A'
    let selectedAppointmentStatus
    if (appointment) {
      selectedAppointmentStatus = appointmentStatuses ? appointmentStatuses.find(appointmentStatus => appointmentStatus.get('id') === parseInt(appointment.get('appointmentStatusId'))) : null
      requestedStatus = selectedAppointmentStatus ? selectedAppointmentStatus.get('name') : 'N/A'
    } else {
      requestedStatus = carrierRequest ? carrierRequest.get('status') : null
    }
    const rescheduleDisabled = this._isRescheduleDisabled(carrierRequest, appointment, selectedAppointmentStatus)
    const cancelDisabled = this._isCancelDisabled(carrierRequest, appointment, selectedAppointmentStatus)

    if (appointment && (requestedStatus.toLowerCase() === SCHEDULED_STATUS || requestedStatus.toLowerCase() === DRAFT_STATUS)) {
      return (
        <ButtonsContainer>
          <StyledGreenButton disabled={rescheduleDisabled} onClick={() => this.onRequestModifications(carrierRequest.get('id'))}>Request Modifications</StyledGreenButton>
          <StyledWhiteButton disabled={cancelDisabled} onClick={() => this.onRequestCancellation(carrierRequest.get('id'))} >Request Cancellation</StyledWhiteButton>
        </ButtonsContainer>
      )
    }
    if (!appointment) {
      return (
        <ButtonsContainer>
          <StyledGreenButton disabled={rescheduleDisabled} onClick={() => this.onRequestModifications(carrierRequest.get('id'))}>Request Modifications</StyledGreenButton>
          <StyledWhiteButton disabled={cancelDisabled} onClick={() => this.onCancelRequest(carrierRequest.get('id'))}>Cancel Request</StyledWhiteButton>
        </ButtonsContainer>
      )
    }
  }

  render () {
    const { carrierRequests } = this.props
    return (
      <Container>
        <SectionTitle>Appointment Requests</SectionTitle>
        {this._renderRescheduleModal()}
        {carrierRequests && carrierRequests.map((carrierRequest, index) => <Section key={index}>
          <Row>
            <Col md={4}><Label>Carrier Name</Label></Col>
            <Col md={4}><Label>Pick Up Date</Label></Col>
            <Col md={4}><Label>Requested Time</Label></Col>
            <Col md={4}><Label>Requested Status</Label></Col>
          </Row>
          {this._renderRow(carrierRequest)}
          {this._renderButtons(carrierRequest)}
          <HorizontalLine />
        </Section>)}
      </Container>
    )
  }
}

AppointmentRequests.propTypes = {
  carrierRequests: PropTypes.object,
  appointments: PropTypes.object,
  carriers: PropTypes.array,
  appointmentStatuses: PropTypes.object,
  match: PropTypes.object,

  getAllAppointmentStatuses: PropTypes.any,
  getAllCarrierRequests: PropTypes.func,
  getAllCarriers: PropTypes.func,
  updateCarrierRequest: PropTypes.func,
  updateCarrierRequestsWithSocketCarrierRequestOnCarrierSide: PropTypes.func,
  getAllLocations: PropTypes.func,
  getBootstrapData: PropTypes.func,
  getDoorById: PropTypes.func
}

const mapStateToProps = state => ({
  carrierRequests: getAllCarrierRequests(state),
  appointments: getAppointments(state),
  carriers: getAllCarriers(state),
  appointmentStatuses: getAllAppointmentStatuses(state),
  getDoorById: id => getDoorById(state, id)
})

const mapDispatchToProps = dispatch => ({
  getAllCarrierRequests: payload => dispatch(CarrierRequestsActions.getAllCarrierRequests(payload)),
  updateCarrierRequest: payload => dispatch(CarrierRequestsActions.updateCarrierRequest(payload)),
  updateCarrierRequestsWithSocketCarrierRequestOnCarrierSide: payload => dispatch(CarrierRequestsActions.updateCarrierRequestsWithSocketCarrierRequestOnCarrierSide(payload)),
  getAllAppointmentStatuses: () => dispatch(AppointmentsActions.getAllAppointmentStatuses()),
  getAllLocations: () => dispatch(LocationsActions.getAllLocations()),
  getAllCarriers: () => dispatch(CarriersActions.getAllCarriers()),
  getBootstrapData: payload => dispatch(FeedsActions.getBootstrapData(payload))
})

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentRequests)
