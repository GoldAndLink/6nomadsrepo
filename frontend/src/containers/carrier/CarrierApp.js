import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Route, NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

import CarrierLogin from './CarrierLogin'
import RequestWizard from './RequestWizard'
import UpsertUser from './UpsertUser'
import ContactUs from './ContactUs'
import AppointmentRequests from './AppointmentRequests'
import brandImage from '../../assets/images/deleted.png'
import headphoneIcon from '../../assets/images/headphone-icon.png'
import { isLoggedIn } from '../../modules/carriers/selectors'
import CarriersActions from '../../modules/carriers/actions'

import { NotificationContainer } from 'react-notifications'
import 'react-notifications/lib/notifications.css'

const Container = styled.div`
  display: flex;
  min-height: 100%;
  flex-direction: column;
`
const Header = styled.div`
  border-top: 5px solid #0eb48f;
  background-color: white;
  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.07);
  height: 80px;
`

const Navbar = styled.div`
  display: flex;
  justify-content: space-between;
`

const NavBrandImage = styled.img`
  height: 80px;
  margin-left: 40px;
`

const NavbarNav = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

const NavLinks = styled(NavLink)`
  margin: 0 40px;
  color: ${props => props.theme.gray};
  text-decoration: none;
  font-size: 12px;
  &.active {
    color: ${props => props.theme.brandDarker};
  }
`

const NavLinkLogin = styled.a`
  cursor: pointer;
  margin: 0 40px;
  color: #fff;
  background-color: ${props => props.theme.brand}
  padding: 10px 30px;
  border-radius: 2px;
  text-decoration: none;
  font-size: 12px;
`

const Content = styled.div`
  flex: 1;
  display: flex;
  background-color: #f4f6f9;
`

const Footer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #fff;
  height: 240px;
`

const Copyright = styled.div`
  font-size: 12px;
  text-align: center;
  color: ${props => props.theme.lightGray};
  margin-top: 40px;
`

const TermsAndConditions = styled.div`
  font-size: 12px;
  text-align: center;
  color: ${props => props.theme.lightGray};
`

const HelpContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const HelpIcon = styled.img`
  width: 30px;
  margin: 20px 0;
`

const HelpText = styled.div`
  font-size: 14px;
  color: ${props => props.theme.lightGray};
  text-transform: uppercase;
`

const CarrierApp = ({ logout, showCarrierLogin, loggedIn }) => (
  <Container>
    <Header>
      <Navbar>
        <NavBrandImage src={brandImage} />
        <NavbarNav>
          <NavLinks to={'/carrier/contactUs'} activeClassName='active' >Contact Us</NavLinks>
          <NavLinks to={'/carrier'} exact={true} activeClassName='active' >Request Appointment</NavLinks>
          {loggedIn && <NavLinks to={'/carrier/appointmentRequests'} activeClassName='active' >Appointment Request</NavLinks>}
          <NavLinks to={loggedIn ? '/carrier/account' : '/carrier/account/create'} activeClassName='active' >{loggedIn ? 'Update Account' : 'Create Account'}</NavLinks>
          <NavLinkLogin onClick={loggedIn ? logout : showCarrierLogin}>{loggedIn ? 'Logout' : 'Login'}</NavLinkLogin>
        </NavbarNav>
      </Navbar>
    </Header>
    <Content>
      <Route exact path='/carrier' component={RequestWizard} />
      <Route exact path='/carrier/account/create' component={UpsertUser} />
      <Route exact path='/carrier/account' component={UpsertUser} />
      <Route exact path='/carrier/contactUs' component={ContactUs} />
      <Route exact path='/carrier/appointmentRequests' component={AppointmentRequests} />
      <Route exact path='/carrier/appointmentRequests/:guid' component={AppointmentRequests} />
    </Content>
    <Footer>
      <HelpContainer>
        <HelpIcon src={headphoneIcon}></HelpIcon>
        <HelpText>Need help?</HelpText>
        <HelpText>Call us for assistance</HelpText>
      </HelpContainer>
      <Copyright>&copy; Copyright Information, 2018</Copyright>
      <TermsAndConditions>Terms and Conditions</TermsAndConditions>
    </Footer>
    <CarrierLogin />
    <NotificationContainer />
  </Container>
)
CarrierApp.propTypes = {
  logout: PropTypes.func,
  showCarrierLogin: PropTypes.func,
  loggedIn: PropTypes.bool
}
const mapStateToProps = state => ({
  loggedIn: isLoggedIn(state)
})

const mapDispatchToProps = dispatch => ({
  showCarrierLogin: () => dispatch(CarriersActions.showCarrierLogin()),
  logout: () => dispatch(CarriersActions.logout())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CarrierApp)
