import React, { Component } from 'react'
import styled from 'styled-components'
import debounce from 'lodash.debounce'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import AppointmentOrdersActions from '../../modules/appointmentOrders/actions'

import OrdersActions from '../../modules/orders/actions'
import { getOrders } from '../../modules/orders/selectors'
import { getAppointmentOrders } from '../../modules/appointmentOrders/selectors'
import { isLoggedIn } from '../../modules/carriers/selectors'

import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import AppointmentExistModal from '../../components/AppointmentExistModal'
import DeletePOConfirmationModal from '../../components/DeletePOConfirmationModal'
import deleteIcon from '../../assets/images/delete-icon.svg'
import CarrierRequestsActions from '../../modules/carrierRequests/actions'
import { getCreatingCarrierRequest } from '../../modules/carrierRequests/selectors'

const MAX_COLUMN = 12
const FULL_WIDTH_PERCENTAGE = 100

const FormContent = styled.form`
  display: flex;
  flex-direction: column;
  padding: 18px 41px 0 41px;
`
const Buttons = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 12px;
`
const StyledTextInput = styled(TextInput)`
  background-color: ${props => props.theme.lighterGray};
  color: ${props => props.theme.lightGray};
`

const StyledButton = styled(Button)`
  height: 36px;
  width: 48%;
  margin-top: 10px;
  color: ${props => props.disabled ? props.theme.lightGray : '#fff'};
  border-color: ${props => props.disabled ? props.theme.lightGray : props.theme.brandDarker};
  background-color: ${props => props.disabled ? '#fff' : props.theme.brandDarker};
`

const CreateAccountButton = styled(Button)`
  height: 36px;
  width: 48%;
  margin-top: 10px;
`
const AddPOButton = styled(Button)`
  height: 36px;
  margin-top: 10px;
  width: 100%;
  font-size: 12px;
`

const ErrorMessage = styled.div`
  color: red;
  font-size: 12px;
  margin-left: 20px;
`
const FormRow = styled.div`
  display: flex;
  margin: 0px -18px 10px -18px;
  box-sizing: border-box;
`

const Col = styled.div`
  width: ${props => FULL_WIDTH_PERCENTAGE / (MAX_COLUMN / props.md)}%;
  padding: 0px 18px;
`
const Label = styled.div`
  color: ${props => props.theme.brandDarker}
  font-size: 12px;
  margin-top: 5px;
`
const HorizontalLine = styled.div`
  border-bottom: 1px solid ${props => props.theme.lightGray}
  margin: 20px 0;
`

const ListElement = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid ${props => props.theme.lightGray};
  padding-top: 15px;
  padding-bottom: 5px;
`

const PONumber = styled.div`
  font-size: 18px;
  color: ${props => props.theme.brandDarker};
`

const DeletePO = styled.div`
  margin-right: 10px;
`
const DeleteOrdersButton = styled(Button)`
  height: 36px;
  margin-top: 10px;
  width: 100%;
  background-color: #FDEDEF;
  color: red;
  border-color: red;
  font-size: 12px;
  `

class ConfirmOrder extends Component {
  state = {
    PO: '',
    errorMessage: '',
    order: null,
    isModalOpen: false,
    isDeletePOConfirmationModalOpen: false,
    addedOrders: []
  }

  componentDidMount = () => {
    const { creatingCarrierRequest } = this.props
    this.props.getAppointmentOrders()
    if (creatingCarrierRequest && creatingCarrierRequest.get('orders')) {
      this.setState({
        addedOrders: creatingCarrierRequest.get('orders').toJS()
      })
    }
  }

  componentDidUpdate = prevProps => {
    const { creatingCarrierRequest } = this.props
    if (creatingCarrierRequest !== prevProps.creatingCarrierRequest) {
      if (creatingCarrierRequest) {
        this.setState({
          addedOrders: creatingCarrierRequest.get('orders').toJS()
        })
      } else {
        this.setState({ addedOrders: [] })
      }
    }
  }

  addPO = () => {
    const { appointmentOrders, orders } = this.props
    const { PO, addedOrders } = this.state
    const order = (PO && orders) ? orders.find(order => (
      order.getIn(['data', 'otherRefs'], []).find(otherRef => (
        otherRef.get('val').indexOf(PO) !== -1
      ))
    )) : null
    const appointmentOrder = (appointmentOrders && order) ? appointmentOrders.find(ao => ao.get('orderId') === order.get('id')) : null
    if (order) {
      if (appointmentOrder) {
        this.setState(state => ({
          isModalOpen: true,
          order
        }))
      } else {
        const isAddedPO = (addedOrders.length > 0) ? addedOrders.find(order => order.PO === PO) : false
        if (!isAddedPO) {
          this.setState(state => ({
            PO: '',
            errorMessage: '',
            addedOrders: [...state.addedOrders, order.toJS()]
          }))
        } else {
          this.setState({
            errorMessage: 'PO is already added'
          })
        }
      }
    } else {
      this.setState({
        errorMessage: 'No results found. Try Again'
      })
    }
  }

  onDeletePO = PO => {
    this.setState(state => ({
      addedOrders: state.addedOrders.filter(order => order.PO !== PO),
      PO: ''
    }))
  }

  onChangePO = e => {
    this.setState({
      PO: e.target.value
    })
  }

  handleEnterKey = e => {
    e.preventDefault()
    this.addPO()
  }

  checkOrders = debounce(() => {
    this.props.getOrders({ searchText: this.state.PO, attributesSelect: 'PO', isOpen: true })
  }, 300)

  onCloseModal = () => {
    this.setState({
      isModalOpen: false
    })
  }

  onCloseConfirmDeletePOsModal = () => {
    this.setState({
      isDeletePOConfirmationModalOpen: false
    })
  }

  onDeleteOrders = () => {
    this.setState({
      isDeletePOConfirmationModalOpen: true
    })
  }

  onConfirmDeleteOrders = () => {
    this.setState({
      addedOrders: []
    })
    this.onCloseConfirmDeletePOsModal()
  }

  confirmOrder = () => {
    const { updateCreatingCarrierRequest, stepForward } = this.props
    const { addedOrders } = this.state

    updateCreatingCarrierRequest({
      orders: addedOrders
    })
    stepForward()
  }

  render () {
    const { PO, errorMessage, order, isModalOpen, addedOrders, isDeletePOConfirmationModalOpen } = this.state

    return (
      <FormContent onSubmit={e => this.handleEnterKey(e)}>
        <AppointmentExistModal isOpen={isModalOpen} onClose={this.onCloseModal} order={order} />
        <DeletePOConfirmationModal isOpen={isDeletePOConfirmationModalOpen} onClose={this.onCloseConfirmDeletePOsModal} onConfirmDelete={this.onConfirmDeleteOrders}/>
        <Label>Search / Add PO Number(s)</Label>
        {
          addedOrders.map((order, index) =>
            <ListElement key={index}>
              <PONumber>{order.PO}</PONumber>
              <DeletePO onClick={() => this.onDeletePO(order.PO)}><img src={deleteIcon} /></DeletePO>
            </ListElement>
          )
        }
        <FormRow>
          <Col md={7}>
            <StyledTextInput
              value={PO}
              placeholder='Enter PO Number'
              type='text'
              onChange={e => {
                this.onChangePO(e)
                this.checkOrders(e)
              }}
            />
          </Col>
          <Col md={4}>
            <AddPOButton primary type="button" onClick={this.addPO}>{addedOrders.length > 0 ? 'Add another PO' : 'Add PO'}</AddPOButton>
          </Col>
        </FormRow>
        <FormRow>
          {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
        </FormRow>
        {(addedOrders.length > 0) &&
        <FormRow>
          <Col md={4}>
          </Col>
          <Col md={4}>
            <DeleteOrdersButton type="button" onClick={() => this.onDeleteOrders()}>Remove All Orders</DeleteOrdersButton>
          </Col>
          <Col md={4}>
          </Col>
        </FormRow>
        }
        <HorizontalLine />
        <Label>Confirm this order and proceed</Label>
        <Buttons>
          <CreateAccountButton type="button" onClick={this.props.onCreateAccount}>{this.props.loggedIn ? 'Update Account' : 'Create Account'}</CreateAccountButton>
          <StyledButton type="button" onClick={() => this.confirmOrder()} disabled={!(addedOrders.length > 0)}>Confirm and Pick a Time Range</StyledButton>
        </Buttons>
      </FormContent>
    )
  }
}

ConfirmOrder.propTypes = {
  orders: PropTypes.object,
  appointmentOrders: PropTypes.object,
  getOrders: PropTypes.func,
  stepForward: PropTypes.func,
  onCreateAccount: PropTypes.func,
  getAppointmentOrders: PropTypes.func,
  updateCreatingCarrierRequest: PropTypes.func,
  creatingCarrierRequest: PropTypes.object,
  loggedIn: PropTypes.bool
}

const mapStateToProps = state => ({
  orders: getOrders(state),
  appointmentOrders: getAppointmentOrders(state),
  creatingCarrierRequest: getCreatingCarrierRequest(state),
  loggedIn: isLoggedIn(state)
})

const mapDispatchToProps = dispatch => ({
  getOrders: (payload = {}) => dispatch(OrdersActions.getOrders(payload)),
  getAppointmentOrders: () => dispatch(AppointmentOrdersActions.getAppointmentOrders()),
  updateCreatingCarrierRequest: payload => dispatch(CarrierRequestsActions.updateCreatingCarrierRequest(payload))
})

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmOrder)
