import React, { Component } from 'react'
import Modal from '../../components/Modal'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import { connect } from 'react-redux'
import styled from 'styled-components'
import RightArrow from '../../assets/images/right-white-arrow.svg'
import CarriersActions from '../../modules/carriers/actions'
import { getCarrierLoginOpen, isLoggedIn } from '../../modules/carriers/selectors'
import PropTypes from 'prop-types'

const customModalStyles = {
  overlay: {
    backgroundColor: 'rgba(60, 65, 78, 0.6)',
    zIndex: 1000
  }
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 400px;
`

const Header = styled.div`
  height: 60px;
  background-color: #22bf9a;
  color: white;
  display: flex;
  align-items: center;
  padding: 0px 50px;

  h1 {
    flex: 1;
    font-size: 20px;
  }
`

const GuestLoginLink = styled.div`
  font-size: 12px;
  cursor: pointer;
  img {
    margin-left: 6px
  }
`

const Content = styled.div`
  padding: 30px 50px;
`

const StyledTextInput = styled(TextInput)`
  margin-bottom: 30px;
`

const LoginButton = styled(Button)`
  width: 100%;
  height: 30px;
  font-size: 14px;

  ${({ error }) => error && `
    background-color: red;
    border-color: red;
  `}
`

class CarrierLogin extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      resetError: false
    }
  }

  componentDidMount () {
    this.props.hideCarrierLogin()
  }

  componentDidUpdate (prevProps) {
    const { isLoggedIn, hideCarrierLogin } = this.props
    if (isLoggedIn && !prevProps.isLoggedIn) {
      hideCarrierLogin()
    }
  }

  onUsernameChange = e => {
    const { value: username } = e.target
    this.setState(state => ({
      ...state,
      username,
      resetError: true
    }))
  }

  onPasswordChange = e => {
    const { value: password } = e.target
    this.setState(state => ({
      ...state,
      password,
      resetError: true
    }))
  }

  onSubmit = e => {
    e.preventDefault()
    const { username = '', password } = this.state
    const { login } = this.props
    this.setState({ resetError: false })
    login({ username: username.toLowerCase(), password })
  }

  render () {
    const { username, password, resetError } = this.state
    const {
      isOpen,
      hideCarrierLogin,
      loginErrorMessage
    } = this.props

    return (
      <Modal
        hideHeader={true}
        isOpen={isOpen}
        contentLabel="Carrier Login"
        onRequestClose={hideCarrierLogin}
        styles={customModalStyles}
      >
        <Container>
          <Header>
            <h1>Login</h1>
            <GuestLoginLink onClick={hideCarrierLogin}>
              Continue as guest
              <img src={RightArrow} alt="arrow" />
            </GuestLoginLink>
          </Header>
          <Content>
            <form onSubmit={this.onSubmit}>
              <StyledTextInput
                fullwidth
                label='Username'
                type='text'
                placeholder='user@deleted.com'
                value={username}
                onChange={this.onUsernameChange}
              />
              <StyledTextInput
                fullwidth
                label='Password'
                type='password'
                value={password}
                onChange={this.onPasswordChange}
              />
              {!!loginErrorMessage && !resetError
                ? <LoginButton type='submit' primary error>Invalid Credentials</LoginButton>
                : <LoginButton type='submit' primary>Login</LoginButton>
              }
            </form>
          </Content>
        </Container>
      </Modal>
    )
  }
}
CarrierLogin.propTypes = {
  isLoggedIn: PropTypes.bool,
  hideCarrierLogin: PropTypes.func,
  login: PropTypes.func,
  isOpen: PropTypes.bool,
  loginErrorMessage: PropTypes.string
}

const mapStateToProps = state => ({
  isOpen: getCarrierLoginOpen(state),
  loginErrorMessage: state.carriers.get('loginErrorMessage'),
  isLoggedIn: isLoggedIn(state)
})

const mapDispatchToProps = dispatch => ({
  login: payload => dispatch(CarriersActions.login(payload)),
  hideCarrierLogin: () => dispatch(CarriersActions.hideCarrierLogin())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CarrierLogin)
