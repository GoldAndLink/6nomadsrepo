import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import moment from 'moment'

import Button from '../../components/Button'
import CarrierRequestsActions from '../../modules/carrierRequests/actions'
import { getCreatingCarrierRequest, getCarrierRequestPickUpTimes } from '../../modules/carrierRequests/selectors'
import { getAllCarriers } from '../../modules/carriers/selectors'
import { getAllDrivers } from '../../modules/drivers/selectors'

const MAX_COLUMN = 12
const FULL_WIDTH_PERCENTAGE = 100

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 18px;
  margin-bottom: 100px;
`

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const TableRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`

const Col = styled.div`
  width: ${props => FULL_WIDTH_PERCENTAGE / (MAX_COLUMN / props.md)}%;
`

const SectionTitle = styled.div`
  color: ${props => props.theme.brandDarker}
  font-size: 16px;
  margin-top: 10px;
  margin-bottom: 25px;
  font-weight: bold;
`

const Section = styled.div`
  margin-bottom: 30px;
`

const HorizontalLine = styled.div`
  border-bottom: 1px solid ${props => props.theme.lightGray}
  margin-bottom: 15px;
`

const Label = styled.div`
  color: ${props => props.theme.gray}
  margin: 6px 0;
  font-size: 12px;
`

const Value = styled.div`
  color: ${props => props.theme.lightGray}
  font-size: 16px;
  font-weight: 300;
  text-overflow: ellipsis;
  overflow: hidden;
  padding-right: 16px;
`

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 60px;
`

const BackContainer = styled.div`
  display: flex;
  align-items: center;
`

const Back = styled.div`
  width: 25px;
  height: 25px;
  border-radius: 50%;
  border: 1px solid ${props => props.theme.brandDarker};
  background-color: #fff;
  margin-right: 10px;
  font-size: 18px;
  line-height: 25px;
  text-align: center;
  color: ${props => props.theme.brandDarker}
  cursor: pointer;
`

const BackText = styled.div`
  text-transform: uppercase;
  color: ${props => props.theme.brandDarker}
  font-size: 12px;
`

const SubmitRequestButton = styled(Button)`
  font-size: 12px;
  background-color: ${props => props.theme.brandDarker};
  color: #fff;
  width: 34%;
`

const SuccessMessage = styled.div`
    color: ${props => props.theme.lightGray};
    font-size: 12px;
    margin-left: 160px;
`

const ErrorMessage = styled.div`
    color: ${props => props.theme.lightGray};
    font-size: 12px;
    margin-left: 160px;
`

class ConfirmCarrierRequest extends Component {
  onSubmitRequest = () => {
    const { stepForward, createCarrierRequest, creatingCarrierRequest, carrierRequestsPickUpTimes } = this.props
    const POs = creatingCarrierRequest.get('orders').toJS().map(order => order.PO)
    const date = creatingCarrierRequest.get('date')
    const pickupTime = carrierRequestsPickUpTimes.find(time => time.get('id') === creatingCarrierRequest.get('pickupTime'))
    const timeStart = pickupTime.get('startTime')
    const timeEnd = pickupTime.get('endTime')
    const email = creatingCarrierRequest.get('email')
    const driverName = creatingCarrierRequest.get('driverName') || ''
    const driverId = creatingCarrierRequest.get('driverId') || undefined
    const carrierName = creatingCarrierRequest.get('carrierName') || ''
    const carrierId = creatingCarrierRequest.get('carrierId') || undefined
    const phone = creatingCarrierRequest.get('phone')
    const tractorNumber = creatingCarrierRequest.get('tractorNumber')
    const trailerLicense = creatingCarrierRequest.get('trailerLicense')
    const prefContactMethod = creatingCarrierRequest.get('prefContactMethod')
    const appointmentId = creatingCarrierRequest.get('appointmentId')

    createCarrierRequest({ date, timeStart, timeEnd, POs, email, phone, driverName, driverId, carrierName, carrierId, tractorNumber, trailerLicense, prefContactMethod, appointmentId })

    stepForward()
  }

  getMessage = () => {
    const { creatingCarrierRequest } = this.props
    if ((creatingCarrierRequest.get('carrierId') || creatingCarrierRequest.get('carrierName')) &&
    creatingCarrierRequest.get('email') &&
    creatingCarrierRequest.get('phone') &&
    creatingCarrierRequest.get('tractorNumber') &&
    creatingCarrierRequest.get('trailerLicense') &&
    (creatingCarrierRequest.get('driverId') || creatingCarrierRequest.get('driverName')) &&
    creatingCarrierRequest.get('date') &&
    creatingCarrierRequest.get('pickupTime') &&
    creatingCarrierRequest.get('orders')) {
      return (<SuccessMessage>Everything looks great!</SuccessMessage>)
    } else {
      return (<ErrorMessage>Not all fields are filled in</ErrorMessage>)
    }
  }
  render () {
    const { creatingCarrierRequest, stepBack, carrierRequestsPickUpTimes, drivers, carriers } = this.props
    const pickupTime = carrierRequestsPickUpTimes.find(time => time.get('id') === creatingCarrierRequest.get('pickupTime'))
    const driver = drivers.find(driver => driver.get('id') === parseInt(creatingCarrierRequest.get('driverId')))
    const carrier = carriers.find(carrier => carrier.get('id') === parseInt(creatingCarrierRequest.get('carrierId')))

    return (
      <Container>
        <SectionTitle>Carrier information</SectionTitle>
        <Section>
          <Row>
            <Col md={4}><Label>Carrier Name</Label></Col>
            <Col md={4}><Label>Email</Label></Col>
            <Col md={4}><Label>Phone Number</Label></Col>
          </Row>
          <Row>
            <Col md={4}><Value>{creatingCarrierRequest.get('carrierName') || carrier.get('name')}</Value></Col>
            <Col md={4}><Value>{creatingCarrierRequest.get('email')}</Value></Col>
            <Col md={4}><Value>{creatingCarrierRequest.get('phone')}</Value></Col>
          </Row>
        </Section>
        <Section>
          <Row>
            <Col md={4}><Label>Tractor Number</Label></Col>
            <Col md={4}><Label>Trailer License</Label></Col>
            <Col md={4}><Label>Driver Name</Label></Col>
          </Row>
          <Row>
            <Col md={4}><Value>{creatingCarrierRequest.get('tractorNumber')}</Value></Col>
            <Col md={4}><Value>{creatingCarrierRequest.get('trailerLicense')}</Value></Col>
            <Col md={4}><Value>{creatingCarrierRequest.get('driverName') || (driver ? driver.get('firstName') + ' ' + driver.get('lastName') : 'N/A') }</Value></Col>
          </Row>
        </Section>
        <HorizontalLine />
        <SectionTitle>Request(s)</SectionTitle>
        <Section>
          <Row>
            <Col md={4}><Label>Pick up date</Label></Col>
            <Col md={4}><Label>Requested pick up time</Label></Col>
            <Col md={4}></Col>
          </Row>
          <Row>
            <Col md={4}><Value>{moment(creatingCarrierRequest.get('date')).format('ddd, MMMM D, YYYY')}</Value></Col>
            <Col md={4}><Value>{pickupTime.get('name')}</Value></Col>
            <Col md={4}></Col>
          </Row>
        </Section>
        <Row>
          <Col md={4}><Label>PO Number</Label></Col>
          <Col md={4}><Label>Customer Name</Label></Col>
          <Col md={4}><Label>Destination</Label></Col>
        </Row>
        <HorizontalLine />
        { creatingCarrierRequest.toJS().orders.map((order, index) =>
          <TableRow key={index}>
            <Col md={4}><Value>{order.PO}</Value></Col>
            <Col md={4}><Value>{order.customer ? order.customer.name : 'N/A'}</Value></Col>
            <Col md={4}><Value>{order.destination ? order.destination.name : 'N/A'}</Value></Col>
          </TableRow>
        )}
        <ButtonsContainer>
          <BackContainer>
            <Back onClick={stepBack}>&#8810;</Back>
            <BackText>BACK</BackText>
          </BackContainer>
          {this.getMessage()}
          <SubmitRequestButton type="button" onClick={() => this.onSubmitRequest()}>Submit request</SubmitRequestButton>
        </ButtonsContainer>
      </Container>
    )
  }
}

ConfirmCarrierRequest.propTypes = {
  updateCreatingCarrierRequest: PropTypes.func,
  creatingCarrierRequest: PropTypes.object,
  stepBack: PropTypes.func,
  stepForward: PropTypes.func,
  createCarrierRequest: PropTypes.func,
  carrierRequestsPickUpTimes: PropTypes.object,
  drivers: PropTypes.object,
  carriers: PropTypes.object
}

const mapStateToProps = state => ({
  creatingCarrierRequest: getCreatingCarrierRequest(state),
  carrierRequestsPickUpTimes: getCarrierRequestPickUpTimes(state),
  carriers: getAllCarriers(state),
  drivers: getAllDrivers(state)
})

const mapDispatchToProps = dispatch => ({
  updateCreatingCarrierRequest: payload => dispatch(CarrierRequestsActions.updateCreatingCarrierRequest(payload)),
  createCarrierRequest: payload => dispatch(CarrierRequestsActions.createCarrierRequest(payload))
})

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmCarrierRequest)
