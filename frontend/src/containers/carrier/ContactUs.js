import React, { Component } from 'react'
import styled from 'styled-components'

import headphoneIcon from '../../assets/images/headphone-green-icon.svg'
import mailIcon from '../../assets/images/mail-icon.svg'

const Container = styled.div`
width: 100%;
margin-top: 60px;
div {
  justify-content: center;
}`
const Title = styled.div`
  display: flex;
  color: ${props => props.theme.brandDarker}
  font-size: 16px;
  font-weight: bold;
  margin-top: 5px;
  margin-bottom: 20px;
`
const Icon = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  color: #898989;
  margin-bottom: 20px;
`
const HelpIcon = styled.img`
  width: 30px;
  margin: 10px 0;
`
class ContactUs extends Component {
  render () {
    return (
      <Container>
        <Title>CONTACT</Title>
        <Icon><HelpIcon src={headphoneIcon}></HelpIcon>1-800-DELETED</Icon>
        <Icon><HelpIcon src={mailIcon}></HelpIcon>support@deleted.com</Icon>
      </Container>
    )
  }
}

export default ContactUs
