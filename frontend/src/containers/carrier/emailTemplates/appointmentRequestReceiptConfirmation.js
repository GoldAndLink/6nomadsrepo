const from = 'lorem@ipsum.com'

const subject = 'DO NOT REPLY: (accountID) Appointment Request Received'

const body =
`
We've received your appointment request.

Pickup date: Mon Nov 26 2018 (shipDateRequested from PO tied to SOs)
(requested appointment time) time requested

Deliver to: PRO ACT / WATERTOWN- NY (RENZI) (appointment/order customer name)
Destination: Watertown, NY (appointment/order destination city and state)

PO Numbers(s):
    (List all POs associated with appointment)
    TY3402
    010351114113
SO Number(s):
    (List all SOs associated with appointment)
    SO-1206457
    SO-1207553

Tractor #: (appointment Tractor #)
Trailer License Plate: (appointment Trailer license plate #)

If you need to cancel or reschedule this appointment, click here (link to carrier portal options to request reschedule or cancellation) or log into the carrier portal (link to carrier portal login screen).

For other changes or more assistance, contact support at (800) 444-5555 (insert phone number for origin shipping facility - TBD)
`

export default { subject, body, from }
