/* eslint-disable react/jsx-key */
import React from 'react'
import * as TestRenderer from 'react-test-renderer'

import Success from '../Success'

let renderer: TestRenderer.ReactTestRenderer
let instance: TestRenderer.ReactTestInstance

beforeAll(() => {
  renderer = TestRenderer.create(
    <Success
      onRestartSteps={() => {}}
    />
  )
  instance = renderer.root
})

describe('Success component', async () => {
  test('Component should render without error', () => {
    const component = TestRenderer.create(
      <Success />
    )

    expect(component.toJSON()).toMatchSnapshot()
  })
  test('Component should render 2 button', () => {
    const buttons = instance.findAll(
      el => el.type === 'button'
    )

    expect(buttons).toHaveLength(2)
  })
})
