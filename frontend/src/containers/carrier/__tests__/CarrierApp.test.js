/* eslint-disable react/jsx-key */
import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import renderer from 'react-test-renderer'

import CarrierApp from '../CarrierApp'
import RequestWizard from '../RequestWizard'

describe('CarrierApp container', async () => {
  test('Component should render without error', () => {
    const component = renderer.create(
      <MemoryRouter>
        <CarrierApp />
      </MemoryRouter>
    )

    expect(component.toJSON()).toMatchSnapshot()
  })

  test('Should show RequestWizard when you visit /carrier', () => {
    const component = renderer.create(
      <MemoryRouter initialEntries={[ '/carrier' ]}>
        <CarrierApp />
      </MemoryRouter>
    )

    expect(component.root.findAllByType(RequestWizard)).toHaveLength(1)
  })
})
