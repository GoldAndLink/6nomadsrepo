/* eslint-disable react/jsx-key */
import React from 'react'
import renderer from 'react-test-renderer'

import RequestWizard from '../RequestWizard'

describe('RequestWizard container', async () => {
  test('Component should render without error', () => {
    const component = renderer.create(
      <RequestWizard />
    )

    expect(component.toJSON()).toMatchSnapshot()
  })
})
