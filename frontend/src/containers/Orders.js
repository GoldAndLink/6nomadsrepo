import React, { PureComponent } from 'react'
import { List } from 'immutable'
import PropTypes from 'prop-types'
import config from '../config'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { Droppable, Draggable } from 'react-beautiful-dnd'
import io from 'socket.io-client'
import { Checkbox } from '@material-ui/core'

import Select from '../components/Select'
import Button from '../components/Button'
import OrderCard from '../components/OrderCard'
import DatePicker from '../components/DatePicker'
import searchIcon from '../assets/images/search-icon.svg'
import closeIcon from '../assets/images/close.svg'
import { generateDraggableId, generateDropppableId } from '../utils/utils'

import OrdersActions from '../modules/orders/actions'
import {
  getOrders,
  getAllOrderStatuses,
  getSelectedOrders,
  getCheckedOrders,
  getPages,
  getOrdersIsLoading,
  getSearchAttributes
} from '../modules/orders/selectors'
import ItemsActions from '../modules/items/actions'
import OrderItemsActions from '../modules/orderItems/actions'
import LocationsActions from '../modules/locations/actions'
import CustomersActions from '../modules/customers/actions'
import AppointmentsActions from '../modules/appointments/actions'
import { getAppointments, getIsUpsertAppointmentVisible } from '../modules/appointments/selectors'
import AppointmentOrdersActions from '../modules/appointmentOrders/actions'
import { getAppointmentOrders } from '../modules/appointmentOrders/selectors'
import { getAllLocations } from '../modules/locations/selectors'
import { getAllCustomers } from '../modules/customers/selectors'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 240px;
  z-index: 2;
`

const SearchContainer = styled.div`
  width: 240px;
  padding-bottom: 12px;
  background-color: #C4C6CE;
`

const SearchContainerHeader = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 12px;
`

const ClosePanelButton = styled.div`
  font-size: 12px;
  font-weight: bold;
  color: #3c414e;
  cursor: pointer;
`

const SearchBox = styled.div`
  background-color: #ffffff;
  margin-left: 10px;
  margin-right: 12px;
  margin-bottom: 4px;
  width: 220px;
  font-size: 85%;
  height: 38px;
  display: flex;
`

const SearchInput = styled.input`
  flex: 1;
  color: ${props => props.theme.gray};
  width: 23px;
  height: 38px;
  padding: 0 0 0 28px;
  font-size: 12px;
  border-width: 1px;
  border-color: #aab0c0;
  border-style: solid;
  background: url(${searchIcon}) no-repeat 8px center;
  max-width: 109px;

  &:focus {
    outline: none;
  }

  &::-webkit-input-placeholder {
    color: ${props => props.theme.lightGray};
  }
`

const AttributesSelect = styled.div`
  margin-left: -1px;
  height: 38px;
  font-size: 85%;
  min-width: 80px;
  border-width: 1px;
  border-color: #aab0c0;
  border-style: solid;
`

/* const AppointmentsSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
` */

const OrdersStatusSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 6px;
  height: 38px;
  font-size: 85%;
`

const CustomerSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
`

const DeliveryDateSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
  > div {
    height: 38px;
  }
`

const DestinationSelect = styled.div`
  border: solid 1px #aab0c0;
  margin-left: 10px;
  margin-right: 10px;
  margin-bottom: 4px;
  margin-top: 4px;
  height: 38px;
  font-size: 85%;
`

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

`

const CreateAppointmentButton = styled(Button)`
  width: 135px;
  height: 30px;
  border-radius: 2px;
  border: solid 1px #0eb48f;
  background-color: #61c9b5;
  margin-left: 10px;
  justify-content: center;
  padding: 0;
`

const CreateAppointmentLabel = styled.h2`
  width: 115px;
  height: 14px;
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  color: #f3f6f6;
  margin: auto;
`

const ClearAll = styled.div`
  height: 14px;
  font-size: 12px;
  font-weight: bold;
  font-style: normal;
  color: #3c414e;
  cursor: pointer;
  margin-left: auto;
  margin-right: 11px;
  text-align: center;
  align-self: center;
`

const OrdersList = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  overflow-y: auto;
`

const CollapseOrders = styled.div`
  height: 14px;
  font-size: 10px;
  font-style: normal;
  color: #aab0c0;
  cursor: pointer;
  margin: 0 5px;
`
const Separator = styled.span`
  height: 14px;
  font-size: 10px;
  color: #aab0c0;
`

const CollapseOrdersContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin: 10px 5px;
`
const HorizontalLine = styled.div`
    border-bottom: 1px solid #aab0c0;
    margin: 0 10px;
`
const ActionsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-self: center;
  width: 100%;
`
const CheckboxContainer = styled.div`
  display: flex;
  justify-content: center;
  align-self: center;
  height: 36px;
  span {
    height: 14px;
    font-size: 10px;
    color: #aab0c0;
    padding-left: 4px;
    padding-right: 0;
    align-self: center;
  }
`
const CustomCheckbox = styled(Checkbox)`
`

const NoResultContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 10px 0 0 0;
`

const searchAttributesSelect = [{ label: 'SO', value: 'primaryRefValue' }, { label: 'PO', value: 'PO' }]
// const searchAttributesAppointments = [{ label: 'Scheduled and Unscheduled', value: null }, { label: 'Scheduled only', value: true }, { label: 'Unscheduled only', value: false }]

class Orders extends PureComponent {
  state = {
    activeDroppedOrder: this.props.droppedOrder,
    socket: io(`${config.API_BASE}/orders`),
    collapseAll: false,
    expandAll: false,
    isSelectAllOrders: false
  }

  componentDidMount () {
    const { getAppointmentOrders, searchOrders } = this.props

    searchOrders({ currentPage: 0 })
    getAppointmentOrders()
    // remove listeners from previous event
    this.state.socket.off('order')
    // keep listening for changes
    this.state.socket.on('order', socketOrder => {
      this.props.updateOrdersWithSocketOrder({ socketOrder })
    })
  }

  onSelectOrder = order => selected => {
    const orderId = order.get('id')
    const { checkedOrders, setCheckedOrders } = this.props

    if (selected) {
      setCheckedOrders(checkedOrders.push(order))
    } else {
      setCheckedOrders(checkedOrders.filter(o => o.get('id') !== orderId))
    }
  }

  getCases = order => {
    const items = order.get('items', List([]))

    let orderItems = List([])
    items.map(si => {
      orderItems = orderItems.concat(List([si.get('orderItem')]))
    })

    const cases = orderItems.reduce((sumQuantity, orderOrderItem) => sumQuantity + orderOrderItem.get('quantity') || 0, 0)
    const UOM = orderItems && !!orderItems.size ? orderItems.first().get('quantityUOM') : ''
    return { value: cases, UOM }
  }

  getWeight = order => {
    const items = order.get('items', List([]))

    let orderItems = List([])
    items.map(si => {
      orderItems = orderItems.concat(List([si.get('orderItem')]))
    })

    const weight = orderItems.reduce((sumWeight, orderOrderItem) => sumWeight + orderOrderItem.get('weight') || 0, 0)
    const UOM = orderItems && !!orderItems.size ? orderItems.first().get('weightUOM') : ''
    return { value: weight, UOM }
  }

  getSKUs = order => {
    const items = order.get('items', List([]))
    const uniqSKUs = Array.from(new Set(items.map(s => s.get('sku'))))
    return { value: uniqSKUs }
  }

  getPallets = order => order.get('pallets') || null

  orderCardRenderer = index => {
    const { collapseAll, expandAll, isSelectAllOrders } = this.state
    const { orders, selectedOrders, checkedOrders, orderStatuses, isUpsertAppointmentVisible } = this.props
    const order = orders.get(index)
    const orderCases = this.getCases(order)
    const orderWeight = this.getWeight(order)
    const orderSKUs = this.getSKUs(order)
    const orderPallets = this.getPallets(order)

    const orderStatus = orderStatuses && orderStatuses.find(s => s.get('id') === order.get('orderStatusId'))
    const isScheduled = orderStatus && orderStatus.get('name', 'draft').toLowerCase() === 'scheduled'

    const isDisabled = isUpsertAppointmentVisible && selectedOrders.some(o => o.get('id') === order.get('id'))
    return (
      <Draggable key={index} draggableId={generateDraggableId('order', order.get('id'))} isDragDisabled={isScheduled || isDisabled} index={index}>
        {(provided, snapshot) => {
          return (
            <div
              key={index}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              {order &&
                <OrderCard
                  order={order}
                  collapseAll={collapseAll}
                  expandAll={expandAll}
                  selected={order && checkedOrders.some(o => o.get('id') === order.get('id'))}
                  onSelectedChange={this.onSelectOrder(order)}
                  onCreateAppointment={this.onCreateAppointmentForOrder(order)}
                  onEditAppointment={this.onEditAppointmentForOrder(order)}
                  onOpenOrderDetailsModal={this.onOpenOrderDetailsModal(order)}
                  checkedOrders={checkedOrders}
                  isDragging={snapshot.isDragging}
                  isDisabled={isDisabled}
                  orderCases={orderCases}
                  orderWeight={orderWeight}
                  orderSKUs={orderSKUs}
                  orderPallets={orderPallets}
                  isCreateAppointment={false}
                  isSelectAllOrders={isSelectAllOrders}
                />
              }
            </div>
          )
        }}
      </Draggable>
    )
  }

  onCreateAppointment = () => {
    const { checkedOrders, setSelectedOrders, openCreateAppointment } = this.props

    setSelectedOrders(checkedOrders)
    openCreateAppointment({ recalculateDuration: true })
  }

  clearFilters = () => {
    this.props.searchOrders({
      searchText: '',
      attributesSelect: 'primaryRefValue',
      isScheduledSelect: null,
      ordersStatusSelect: null,
      customerSelect: null,
      deliveryDateSelect: null,
      destinationSelect: null
    })
  }

  onCreateAppointmentForOrder = order => () => {
    const { setSelectedOrders, openCreateAppointment } = this.props

    setSelectedOrders([order])
    openCreateAppointment()
  }

  onEditAppointmentForOrder = order => () => {
    const { setSelectedOrders, openEditAppointment, appointmentOrders, appointments } = this.props

    const appointmentOrder = appointmentOrders ? appointmentOrders.find(ao => ao.get('orderId') === order.get('id')) : null
    const appointment = appointments && appointmentOrder ? appointments.find(appt => appt.get('id') === appointmentOrder.get('appointmentId')) : null

    if (appointment) {
      setSelectedOrders(appointment.get('orders'))
      openEditAppointment(appointment)
    }
  }

  onOpenOrderDetailsModal = order => () => {
    const { openOrderDetailsModal, setOrderDetails } = this.props
    const selectedItems = order.get('items', List([]))

    let selectedOrderItems = List([])
    selectedItems.map(si => {
      selectedOrderItems = selectedOrderItems.concat(List([si.get('orderItem')]))
    })

    const orderDetails = {
      orderItems: selectedOrderItems,
      items: selectedItems,
      order
    }
    setOrderDetails(orderDetails)
    openOrderDetailsModal()
  }

  onScrollOrderList = () => {
    const { pages, loading, searchOrders, searchAttributes } = this.props
    const { orderList } = this
    if (searchAttributes.currentPage < pages && !loading) {
      const scrollPos = orderList.scrollTop + orderList.clientHeight
      if (scrollPos + 50 > orderList.scrollHeight) {
        searchOrders({ currentPage: searchAttributes.currentPage + 1 })
      }
    }
  }

  collapseAll = () => this.setState(state => ({
    collapseAll: true,
    expandAll: false
  }))

  expandAll = () => this.setState(state => ({
    expandAll: true,
    collapseAll: false
  }))

  handleSelectAllOrdersChange = event => {
    const { setCheckedOrders, orders = [] } = this.props
    if (event.target.checked) {
      setCheckedOrders(orders)
      this.setState({
        isSelectAllOrders: true
      })
    } else {
      setCheckedOrders([])
      this.setState({
        isSelectAllOrders: false
      })
    }
  }

  render () {
    const {
      orders = new List([]),
      locations = new List([]),
      orderStatuses = new List([]),
      customers = new List([]),
      onClose,
      searchAttributes,
      searchOrders,
      getAllLocations,
      getAllCustomers
    } = this.props

    const {
      searchText,
      attributesSelect,
      /* isScheduledSelect, */
      ordersStatusSelect,
      deliveryDateSelect,
      destinationSelect,
      customerSelect
    } = searchAttributes
    const searchAttributesOrderStatus = orderStatuses ? orderStatuses.map(orderStatus => ({ label: orderStatus.get('name'), value: orderStatus.get('id') })) : []
    // lets not do that the array is HUGE
    const searchAttributesCustomer = customers ? customers.map(customer => ({ label: customer.get('name'), value: customer.get('id') })) : []
    // we probably need to do some kind of as you type search here. Not download a huge list and to filter locally
    const searchAttributesDestination = locations
      ? locations.map(location => ({
        label: location.get('name'),
        value: location.get('id')
      })).toArray() || []
      : []

    return (
      <Container>
        <SearchContainer>
          <SearchContainerHeader>
            <ClosePanelButton onClick={onClose}>
              <img src={closeIcon} alt="close" />
            </ClosePanelButton>
          </SearchContainerHeader>
          <SearchBox>
            <SearchInput placeholder='Search' value={searchText} onChange={payload => searchOrders({ searchText: payload.target.value, currentPage: 1 })} />
            <AttributesSelect>
              <Select
                isSearchable={false}
                options={searchAttributesSelect}
                placeholder='Select'
                value={attributesSelect}
                onChange={payload => searchOrders({ attributesSelect: payload, currentPage: 1 })}
              />
            </AttributesSelect>
          </SearchBox>
          {/* <AppointmentsSelect>
            <Select
              // options={searchAttributesAppointments}
              placeholder='Appointments'
              value={isScheduledSelect}
              onChange={this.onIsScheduledSelectChange}
            />
          </AppointmentsSelect> */}
          <OrdersStatusSelect>
            <Select
              isClearable
              options={searchAttributesOrderStatus}
              placeholder='Order Status'
              value={ordersStatusSelect}
              onChange={payload => searchOrders({ ordersStatusSelect: payload, currentPage: 1 })}
            />
          </OrdersStatusSelect>
          <CustomerSelect>
            <Select
              disableDropdownIndicator
              disableEmptyOptions
              isClearable
              options={searchAttributesCustomer}
              placeholder='Customer'
              value={customerSelect}
              onInputChange={payload => { payload && payload.length > 1 && getAllCustomers({ name: payload }) }}
              onChange={payload => searchOrders({ customerSelect: payload, currentPage: 1 })}
            />
          </CustomerSelect>
          <DeliveryDateSelect>
            <DatePicker
              showIcon
              SearchOrderDatePicker
              dateFormat='LL'
              placeholderText='Delivery Date'
              selected={deliveryDateSelect || null}
              onChange={payload => searchOrders({ deliveryDateSelect: payload, currentPage: 1 })}
            />
          </DeliveryDateSelect>
          <DestinationSelect>
            <Select
              disableDropdownIndicator
              disableEmptyOptions
              isClearable
              options={searchAttributesDestination}
              placeholder='Destination'
              value={destinationSelect}
              onInputChange={payload => { payload && payload.length > 1 && getAllLocations({ name: payload }) }}
              onChange={payload => searchOrders({ destinationSelect: payload, currentPage: 1 })}
            />
          </DestinationSelect>
          <ButtonsContainer>
            <CreateAppointmentButton onClick={this.onCreateAppointment}>
              <CreateAppointmentLabel>Create Appointment</CreateAppointmentLabel>
            </CreateAppointmentButton>
            <ClearAll onClick={this.clearFilters}>
              Clear all
            </ClearAll>
          </ButtonsContainer>
        </SearchContainer>
        <ActionsContainer>
          <CheckboxContainer>
            <CustomCheckbox
              checked={this.state.isSelectAllOrders}
              onChange={this.handleSelectAllOrdersChange}
              value='selectAllOrdersCheckbox'
              color='default'
            />
            <span>Select all</span>
          </CheckboxContainer>
          <CollapseOrdersContainer>
            <CollapseOrders onClick={this.expandAll}>
              Expand all
            </CollapseOrders>
            <Separator>|</Separator>
            <CollapseOrders onClick={this.collapseAll}>
              Collapse all
            </CollapseOrders>
          </CollapseOrdersContainer>
        </ActionsContainer>
        <HorizontalLine />
        <Droppable droppableId={generateDropppableId('orders')} isDropDisabled={true}>
          {(provided, snapshot) => (
            <OrdersList
              innerRef={ref => {
                this.orderList = ref
                provided.innerRef(ref)
              }}
              onScroll={this.onScrollOrderList}
              {...provided.droppableProps}>

              {
                (orders && orders.size > 0)
                  ? orders.map((order, index) => {
                    return this.orderCardRenderer(index)
                  })
                  : (<NoResultContainer>{'No Results'}</NoResultContainer>)
              }
            </OrdersList>
          )}
        </Droppable>
      </Container>
    )
  }
}

Orders.propTypes = {
  getOrders: PropTypes.func,
  getItems: PropTypes.func,
  getOrderItems: PropTypes.func,
  getAllLocations: PropTypes.func,
  getAppointments: PropTypes.func,
  getAllCustomers: PropTypes.func,
  getAppointmentOrders: PropTypes.func,
  createAppointment: PropTypes.func,
  getAllOrderStatuses: PropTypes.func,
  updateOrdersWithSocketOrder: PropTypes.func,
  searchOrders: PropTypes.func,

  orders: PropTypes.object,
  searchAttributes: PropTypes.object,
  appointmentOrders: PropTypes.any,
  orderStatuses: PropTypes.object,
  pages: PropTypes.number,
  loading: PropTypes.bool
}

const mapStateToProps = state => ({
  customers: getAllCustomers(state),
  locations: getAllLocations(state),
  orders: getOrders(state),
  appointmentOrders: getAppointmentOrders(state) || [],
  orderStatuses: getAllOrderStatuses(state),
  selectedOrders: getSelectedOrders(state) || [],
  checkedOrders: getCheckedOrders(state) || List(),
  appointments: getAppointments(state),
  isUpsertAppointmentVisible: getIsUpsertAppointmentVisible(state),
  pages: getPages(state),
  loading: getOrdersIsLoading(state),
  searchAttributes: getSearchAttributes(state)
})

const mapDispatchToProps = dispatch => ({
  getOrders: (payload = {}) => dispatch(OrdersActions.getOrders(payload)),
  getItems: (payload = {}) => dispatch(ItemsActions.getItems(payload)),
  getOrderItems: (payload = {}) => dispatch(OrderItemsActions.getOrderItems(payload)),
  setSelectedOrders: orders => dispatch(OrdersActions.setSelectedOrders(orders)),
  setCheckedOrders: orders => dispatch(OrdersActions.setCheckedOrders(orders)),
  getAppointmentOrders: () => dispatch(AppointmentOrdersActions.getAppointmentOrders()),
  openCreateAppointment: () => dispatch(AppointmentsActions.openCreateAppointment()),
  openEditAppointment: appointment => dispatch(AppointmentsActions.openEditAppointment(appointment)),
  openOrderDetailsModal: () => dispatch(OrdersActions.openOrderDetailsModal()),
  setOrderDetails: orderDetails => dispatch(OrdersActions.setOrderDetails(orderDetails)),
  updateOrdersWithSocketOrder: payload => dispatch(OrdersActions.updateOrdersWithSocketOrder(payload)),
  searchOrders: payload => dispatch(OrdersActions.searchOrders(payload)),
  getAllLocations: payload => dispatch(LocationsActions.getAllLocations(payload)),
  getAllCustomers: payload => dispatch(CustomersActions.getAllCustomers(payload))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orders)
