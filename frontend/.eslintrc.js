module.exports = {
  parser: 'babel-eslint',
  extends: ['standard', 'plugin:react/recommended', 'mocha'],
  rules: {
    'no-var': 'error',
    'semi': [2, 'never'],
    'prefer-const': 'error'
  },
  env: {
    "jest": true
  }
}
