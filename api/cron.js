const moment = require('moment')
const cron = require('node-cron')
const op = require('sequelize').Op
const socket = require('./src/modules/socket')
const Appointment = require('./src/models').appointment

const prefix = 'CRON-JOB -'

cron.schedule('* * * * *', async () => {
  console.log(`${prefix} NOW: ${moment()}`)
  const now = moment().subtract(30, 'minutes')
  console.log(`${prefix} Removing appointments older than: ${now}`)
  const appts = await Appointment.findAll({ where: {
    [op.and]: [
      { inProgress: true },
      { createdAt: { [op.lte]: now } }
    ]
  } })

  for (const appt of appts) {
    console.log(`${prefix} removing temp appointment: ${appt.id}`)
    await appt.destroy()
    socket.appointments.broadcastOne(appt)
  }
})
