const socketio = require('socket.io')
const socketHandler = require('./src/modules/socket/index.js')
const io = []

module.exports = {
  io: () => {
    return io
  },
  init: server => {
    const socketioserver = socketio(server)

    for (const namespace in socketHandler) {
      const namespaceio = socketioserver.of(`/${namespace}`)
      io[namespace] = namespaceio
      namespaceio.on('connection', function (socket) {
        // a new client connected to the namespace
        socketHandler[namespace].connection(namespaceio, socket)
        for (const event in socketHandler[namespace]) {
          socket.on(event,
            payload => socketHandler[namespace][event](namespaceio, socket, payload)
          )
        }
      })
    }
  }
}
