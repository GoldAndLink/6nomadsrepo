const Sequelize = require('sequelize')
const path = require('path')
const env = process.env.NODE_ENV || 'development'
const config = require(path.join(__dirname, './config/sequelize.js'))[env]

module.exports = async jest => {
  const sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  )
  await sequelize.close()
}
