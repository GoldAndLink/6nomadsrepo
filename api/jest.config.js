// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  'globalSetup': '<rootDir>/jest.setup.js',
  'globalTeardown': '<rootDir>/jest.teardown.js',
  'coverageDirectory': './coverage/',
  'collectCoverage': true,
  'testEnvironment': 'node',
  'bail': true,
  'verbose': true,
  'globals': {
    NODE_ENV: 'test',
    JWT: 'test'
  },
  'moduleFileExtensions': [
    'js'
  ],
  'transformIgnorePatterns': [
    'node_modules/(?!(express-validator)/)'
  ],
  'testMatch': [
    '<rootDir>/src/**/*.test.(js)'
  ]
}
