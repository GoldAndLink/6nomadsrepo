const moment = require('moment')
const {
  DRAFT,
  SCHEDULED,
  CHECKED_IN,
  LOADING,
  CHECKED_OUT
} = require('../modules/appointments/appointments.constants').STATUS

const Appointment = require('../models').appointment
const Order = require('../models').order
const Door = require('../models').door
const Area = require('../models').area
const Building = require('../models').building
const Site = require('../models').site
const Carrier = require('../models').carrier
const Location = require('../models').location
const Driver = require('../models').driver
const Customer = require('../models').customer
const Account = require('../models').account

module.exports = {
  mapAppointmentToRC: async ({ appointmentId, userId, previousData, event }) => {
    const appointment = await Appointment.findOne({
      where: { id: +appointmentId },
      order: [['date', 'ASC']],
      include: [
        {
          model: Order,
          required: false,
          include: [
            {
              model: Customer
            },
            {
              model: Account
            }
          ]
        },
        {
          model: Carrier
        },
        {
          model: Driver
        },
        {
          model: Door,
          include: [
            {
              model: Area,
              include: [
                {
                  model: Building,
                  include: [
                    {
                      model: Site
                    },
                    {
                      model: Location
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })

    if (!appointment) throw new Error(`Appointment not found on RC mapping: ${appointmentId} ${userId}`)

    const data = {
      UserID: null,
      Application: '',
      original: null,
      updated: {
        // appointments.guid - needs to be created in the appointments table
        appointmentGUID: appointment.guid,
        // users.id
        createdbyUserID: userId,
        // sites.id
        siteID: appointment.door.area.building.site.id,
        // areas.id
        areaID: appointment.door.area.id,
        // doors.id
        doorID: appointment.doorId,
        // appointments.duration
        durationMin: appointment.duration,
        // appointments.date
        startUTC: appointment.date,
        // appointments.appointmentStatusId
        status: appointment.appointmentStatusId,
        // appointments.carrierId
        carrierID: appointment.carrierId,
        // appointments.contactPhone
        contactPhone: appointment.contactPhone && parseInt(appointment.contactPhone.replace(/\D/g, '')),
        // appointments.notes
        notes: appointment.notes,
        // appointments.trailer
        trailerNumber: appointment.trailer,
        // appointments.tractor
        licensePlate: appointment.tractor,
        // appointments.driverId
        driverID: appointment.driverId,
        // yyyy-mm-dd format of the appointment.date
        grid_id: moment(appointment.date).format('YYYY-MM-DD'),
        // buildings.name
        buildingName: appointment.door.area.building.name,
        // buildings.name ???
        buildingDescription: appointment.door.area.building.name,
        // appointments.deletedAt != null
        deleted: appointment.deletedAt != null,
        // areas.name
        areaDescription: appointment.door.area.name,
        // carriers.name
        carrierName: appointment.carrier && appointment.carrier.name,
        // appointments.appointmentStatusId
        appointmentStatus: appointment.appointmentStatusId,
        // Find timezone offset for the building this appointment is in
        tzOffset: appointment.door.area.building.location.timezoneName,
        // appointments.primaryRef
        appointmentAlias: appointment.primaryRef,
        // buildings.locations.timeZone
        locationTimezone: appointment.door.area.building.location.timezoneName,
        // sites.name
        siteName: appointment.door.area.building.site.name,
        // appointments.id
        appointmentID: appointment.id,
        // areas.name
        areaName: appointment.door.area.name,
        // appointments.buildingId
        buildingID: appointment.door.area.building.id,
        // drivers.firstName + drivers.lastName
        driverName: appointment.driver && `${appointment.driver.firstName || ''} ${appointment.driver.lastName || ''}`,
        // determine if it DST
        locationDST: true,
        // sites.name
        siteDescription: appointment.door.area.building.site.name,
        // generate a message guid here
        _id: appointment.guid,
        // orders
        orders: appointment.orders
      },
      type: 'appointment',
      event: 'create'
    }

    if (data.updated.status === DRAFT) {
      data.updated.status = 'draft'
      data.updated.appointmentStatus = 'draft'
    } else if (data.updated.status === SCHEDULED) {
      data.updated.status = 'scheduled'
      data.updated.appointmentStatus = 'scheduled'
    } else if (data.updated.status === CHECKED_IN) {
      data.updated.status = 'checked_in'
      data.updated.appointmentStatus = 'checked_in'
    } else if (data.updated.status === LOADING) {
      data.updated.status = 'loading'
      data.updated.appointmentStatus = 'loading'
    } else if (data.updated.status === CHECKED_OUT) {
      data.updated.status = 'checked_out'
      data.updated.appointmentStatus = 'checked_out'
    }

    // subAccountId
    if (appointment.orders && appointment.orders.length > 0 && appointment.orders[0].account) {
      data.subAccountId = appointment.orders[0].account.name
    }

    // orders
    // order with appointments.id = orders.appointmentId
    data.updated.orders = data.updated.orders.map(o => {
      return {
        // orders.guid
        orderGUID: o.orderGuid,
        // orders.primaryRef
        orderValue: o.primaryRefValue
      }
    })

    if (event === 'create') {
      data.original = data.updated
    } else if (event === 'edit') {
      data.original = previousData
    }

    return data
  }
}
