const moment = require('moment')
const Customer = require('../models').customer
const Item = require('../models').item
const Order = require('../models').order
const Appointment = require('../models').appointment
const Door = require('../models').door
const Area = require('../models').area
const Building = require('../models').building
const Location = require('../models').location
const InventoryItem = require('../models').inventoryItem
const op = require('sequelize').Op
const socket = require('../modules/socket')

const self = module.exports = {
  validateDownstreamAppointments: async (appointmentObject) => {
    try {
      const threats = {}

      // 1. get appt location code
      const locationData = await Location.findOne(
        {
          attributes: ['id', 'code'],
          include: [{
            model: Building,
            required: true,
            attributes: ['id'],
            include: [{
              model: Area,
              required: true,
              attributes: ['id'],
              include: [{
                model: Door,
                required: true,
                attributes: ['id'],
                where: {
                  id: appointmentObject.doorId
                }
              }]
            }]
          }]
        })

      // 2. get all door IDs for that location code
      const doorData = await Door.findAll({
        include: [{
          model: Area,
          required: true,
          attributes: ['id'],
          include: [{
            model: Building,
            required: true,
            attributes: ['id'],
            include: [{
              model: Location,
              required: true,
              attributes: ['id'],
              where: {
                code: locationData.code
              }
            }]
          }]
        }]
      })

      // 3. get all downstream appointments
      const appointmentData = await Appointment.findAll({
        // attributes: ['id'], // uncommented to support socketio
        where: {
          date: {
            [op.gte]: moment().toDate()
          },
          doorId: doorData.map(door => door.id),
          appointmentStatusId: { [op.notIn]: [1, 5] } // not draft + not checked out
        },
        include: [{
          model: Order,
          include: [
            {
              model: Customer
            },
            {
              model: Item
            },
            {
              model: Location,
              as: 'destination'
            },
            {
              model: Location,
              as: 'origin'
            }
          ]
        }]
      })

      // 4. get all skus we need to look up
      const appointmentItems = await Item.findAll({
        attributes: ['id', 'sku'],
        include: [{
          model: Order,
          required: true,
          attributes: ['id'],
          include: {
            model: Appointment,
            required: true,
            where: {
              id: appointmentData.map(appointment => appointment.id)
            }
          }
        }]
      })

      // 5. finally get the inventory projection
      const projection = await self.getProjection(appointmentItems.map(item => item.sku), locationData.code)

      // 6. walk through SKUs and detect threats
      for (const sku in projection) {
        const p = projection[sku]
        let stock = 0

        for (let j = 0, jcap = p.deltas.length; j < jcap; j++) {
          const delta = p.deltas[j]
          stock += delta.quantity
          // 6a if we're on an appointment, save threats
          if (delta.appointmentId && (stock < 0)) {
            threats[delta.appointmentId] ? threats[delta.appointmentId].push(sku) : threats[delta.appointmentId] = [sku]
          }
        }
      }

      // 6.5. broadcast everything to everyone
      socket.appointments.broadcastMany(appointmentData.map(appt => ({
        ...appt.toJSON(),
        inventoryChecked: new Date(),
        inventoryIssues: threats[appt.id] || null
      })))

      // 7. clear issues on appointments we looked at
      await Appointment.update({
        inventoryChecked: new Date(),
        inventoryIssues: null
      }, {
        where: {
          id: appointmentData.map(appointment => appointment.id)
        }
      })

      // 8. save issues to appointments with threats
      for (const appointmentId in threats) {
        await Appointment.update({
          inventoryChecked: new Date(),
          inventoryIssues: threats[appointmentId]
        }, {
          where: {
            id: appointmentId
          }
        })
      }

      return threats[appointmentObject.id] || null
    } catch (e) {
      console.error('[utils/projector/validateDownstreamAppointments] ERROR:', e)
      return false
    }
  },
  getProjection: async (skus, locationCode) => {
    const projection = {}

    // Loop over skus and prepare the projection object
    for (let i = 0, icap = skus.length; i < icap; i++) {
      projection[skus[i]] = {
        onHandTime: null,
        deltas: [],
        appointments: [],
        production: []
      }
    }

    // Get doors for the location code
    const doorData = await Door.findAll({
      include: [{
        model: Area,
        required: true,
        attributes: ['id'],
        include: [{
          model: Building,
          required: true,
          attributes: ['id'],
          include: [{
            model: Location,
            required: true,
            attributes: ['id'],
            where: {
              code: locationCode
            }
          }]
        }]
      }]
    })

    // Get inventory data
    const inventoryData = await InventoryItem.findAll({
      where: {
        sku: skus,
        shippingLocationCode: locationCode
      }
    })

    // Extract onHandTimes
    inventoryData.filter(row => row.isOnHand).map(inventory => {
      projection[inventory.sku].onHandTime = inventory.availabilityReadyTime
    })

    // Get item and appointment data
    // TODO: only appointments not checked out before onHand AND status other than draft
    const itemData = await Item.findAll({
      where: {
        sku: skus
      },
      attributes: ['sku'],
      order: [
        [Order, Appointment, 'date', 'asc'],
        [Order, Appointment, 'id', 'asc']
      ],
      include: {
        model: Order,
        required: true,
        attributes: ['id'],
        include: {
          model: Appointment,
          required: true,
          where: {
            doorId: doorData.map(door => door.id),
            appointmentStatusId: { [op.ne]: 1 } // not draft
          },
          attributes: ['id', 'date', 'appointmentStatusId', 'checkoutTime']
        }
      }
    })

    // load appointments
    for (let i = 0, icap = itemData.length; i < icap; i++) {
      const sku = itemData[i] // loop over skus
      for (let j = 0, jcap = sku.orders.length; j < jcap; j++) {
        const order = sku.orders[j] // loop over orders
        const appointment = order.appointments[0]

        // skip condition based on appointment
        // scheduled BEFORE onHand AND has checked out
        if (projection[sku.sku] && moment(projection[sku.sku].onHandTime).diff(moment(appointment.date)) > 0 && appointment.appointmentStatusId === 5) {
          continue
        }

        projection[sku.sku].appointments.push({
          time: appointment.date,
          appointmentId: appointment.id,
          quantity: order.orderItem.quantity
        })
      }
    }

    // save onhand to production
    // TODO: move baseline to delta
    const baselineData = inventoryData.filter(inventory => inventory.isOnHand)
    for (let i = 0, icap = skus.length; i < icap; i++) {
      const baselineRow = baselineData.find(inventory => inventory.sku === skus[i])
      if (!baselineRow) continue
      projection[baselineRow.sku].production.push({
        id: baselineRow.id,
        quantity: baselineRow.quantity,
        quantityUOM: baselineRow.quantityUOM,
        time: baselineRow.availabilityReadyTime,
        onHand: true
      })
    }

    // save production
    const productionData = inventoryData.filter(inventory => !inventory.isOnHand)
    for (let i = 0, icap = productionData.length; i < icap; i++) {
      const productionRow = productionData[i]
      // ignore production order than onHand
      if (moment(projection[productionRow.sku].onHandTime).diff(moment(productionRow.availabilityReadyTime)) > 0) {
        continue
      }
      projection[productionRow.sku].production.push({
        id: productionRow.id,
        quantity: productionRow.quantity,
        quantityUOM: productionRow.quantityUOM,
        time: productionRow.availabilityReadyTime,
        onHand: false
      })
    }

    // build deltas
    for (let i = 0, icap = skus.length; i < icap; i++) {
      const p = projection[skus[i]]
      p.deltas = p.production.concat(p.appointments.map(appt => ({ ...appt, quantity: -appt.quantity })))
      p.deltas.sort((a, b) => (moment(a.time).diff(moment(b.time))))
    }
    return projection
  }
}
