const models = require('../models')

module.exports = {
  qualifier: (req, allowedParams = ['id']) => {
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    return qualifier
  },
  sorter: (req, allowedSorters = ['id']) => {
    const sorter = []
    if (req.query.sort) {
      const sort = req.query.sort.split(':')
      if (allowedSorters.includes(sort[0]) && ['asc', 'desc'].includes(sort[1])) {
        sorter.push(sort)
      }
    }
    return sorter
  },
  includer: (req, allowedModels = [{ modelName: ['field'] }]) => {
    const includer = []
    if (!req.query._include) {
      return includer
    }
    if (typeof req.query._include === 'string') {
      req.query._include = [req.query._include]
    }

    for (const model of req.query._include) {
      if (models[model]) {
        const allowedModel = allowedModels.find(amodel => amodel.modelName === model)
        if (allowedModel) {
          const qualifier = {}
          if (allowedModel.allowedParams) {
            for (const param of allowedModel.allowedParams) {
              if (req.query[`${model}_${param}`]) {
                qualifier[param] = req.query[`${model}_${param}`]
              }
            }
          }

          includer.push({
            model: models[model],
            where: qualifier,
            attributes: allowedModel.fields || undefined,
            required: allowedModel.required || false
          })
        }
      }
    }

    return includer
  }
}
