const mapper = require('./mapper')
const request = require('request-promise')

module.exports = {
  createAppointment: async ({ appointmentId, userId }) => {
    if (!process.env.REDWOOD_CONNECT_URI) return
    try {
      const data = await mapper.mapAppointmentToRC({ appointmentId, userId, event: 'create' })
      data.event = 'create'
      const options = {
        method: 'POST',
        headers: {
          Authorization: process.env.REDWOOD_CONNECT_TOKEN
        },
        uri: process.env.REDWOOD_CONNECT_URI,
        body: data,
        json: true
      }
      const res = await request(options)
      console.log('RC data: ', data)
      console.log('POST /redwoodConnect/appointment: ', res)
    } catch (e) {
      console.log('redwoodConnect ERROR:', e)
    }
  },
  updateAppointment: async ({ appointmentId, userId, previousData }) => {
    if (!process.env.REDWOOD_CONNECT_URI) return
    try {
      const data = await mapper.mapAppointmentToRC({ appointmentId, userId, previousData, event: 'edit' })
      data.event = 'edit'
      const options = {
        method: 'POST',
        headers: {
          Authorization: process.env.REDWOOD_CONNECT_TOKEN
        },
        uri: process.env.REDWOOD_CONNECT_URI,
        body: data,
        json: true
      }
      const res = await request(options)
      console.log('RC data: ', data)
      console.log('PUT /redwoodConnect/appointment: ', res)
    } catch (e) {
      console.log('redwoodConnect ERROR:', e)
    }
  },
  deleteAppointment: async ({ appointmentId, userId }) => {
    if (!process.env.REDWOOD_CONNECT_URI) return
    try {
      const data = await mapper.mapAppointmentToRC({ appointmentId, userId, event: 'delete' })
      data.event = 'delete'
      const options = {
        method: 'POST',
        headers: {
          Authorization: process.env.REDWOOD_CONNECT_TOKEN
        },
        uri: process.env.REDWOOD_CONNECT_URI,
        body: data,
        json: true
      }
      const res = await request(options)
      console.log('RC data: ', data)
      console.log('DELETE /redwoodConnect/appointment: ', res)
    } catch (e) {
      console.log('redwoodConnect ERROR:', e)
    }
  }
}
