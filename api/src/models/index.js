const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const _ = require('lodash')
const env = process.env.NODE_ENV || 'development'
const config = require(path.join(__dirname, '../../config/sequelize.js'))[env]
const db = {}

let sequelize

if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config)
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  )
}

const recursive = (modules, filePath) => {
  const file = path.resolve(modules, filePath)
  const isFile = fs.statSync(file).isFile()
  if (!isFile) {
    return fs.readdirSync(file).map(f => {
      return recursive(file, f)
    })
  }

  return file
}

const modules = path.resolve(__dirname, '../modules/')
const files = recursive(modules, '')

_.flattenDeep(files).forEach(file => {
  if (file.slice(-9) === '.model.js') {
    const model = sequelize['import'](path.join(file))
    db[model.name] = model
  }
})

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

sequelize
  .authenticate()
  .then(() => {
    process.env.NODE_ENV !== 'test' && console.log('Database connection established')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
