module.exports = (sequelize, DataTypes) => {
  const OrderStatus = sequelize.define(
    'orderStatus',
    {
      name: DataTypes.STRING
    },
    {}
  )
  OrderStatus.associate = function (models) {
    OrderStatus.hasMany(models.order)
  }
  return OrderStatus
}
