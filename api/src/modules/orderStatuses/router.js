const controller = require('./orderStatuses.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/order_statuses'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAllOrderStatuses })
)

module.exports = {
  router,
  baseUrl
}
