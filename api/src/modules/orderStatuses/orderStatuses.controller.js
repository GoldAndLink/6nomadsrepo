const OrderStatus = require('../../models').orderStatus

module.exports = {
  getAllOrderStatuses: async (req, res, next) => {
    try {
      const orderStatuses = await OrderStatus.findAll()

      res.send(orderStatuses)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
