module.exports = (sequelize, DataTypes) => {
  const Area = sequelize.define(
    'area',
    {
      name: DataTypes.STRING,
      buildingId: DataTypes.INTEGER
    },
    {}
  )
  Area.associate = function (models) {
    Area.hasMany(models.door)
    Area.belongsTo(models.building)
  }
  return Area
}
