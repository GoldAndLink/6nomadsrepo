const Area = require('../../models').area

module.exports = {
  getAreas: async (req, res, next) => {
    const allowedParams = ['id', 'name', 'buildingId']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    try {
      const entities = await Area.findAll({ where: qualifier })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
