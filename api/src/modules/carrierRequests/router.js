const controller = require('./carrierRequests.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/carrierRequests'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getCarrierRequests })
)

router.get(
  '/count',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.countCarrierRequestsInStatusPending })
)

module.exports = {
  router,
  baseUrl
}
