module.exports = (sequelize, DataTypes) => {
  const CarrierRequest = sequelize.define(
    'carrierRequest',
    {
      guid: DataTypes.UUID,
      carrierId: DataTypes.INTEGER,
      driverId: DataTypes.INTEGER,
      appointmentId: DataTypes.INTEGER,
      date: DataTypes.DATE,
      timeStart: DataTypes.DATE,
      timeEnd: DataTypes.DATE,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      trailerLicense: DataTypes.STRING,
      tractorNumber: DataTypes.STRING,
      prefContactMethod: DataTypes.STRING,
      status: DataTypes.ENUM('pending', 'canceled', 'reschedule', 'scheduled'),
      rescheduleTimeSuggestion: DataTypes.DATE,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER
    },
    {}
  )
  CarrierRequest.associate = function (models) {
    CarrierRequest.belongsTo(models.carrier)
    CarrierRequest.belongsTo(models.driver)
    CarrierRequest.belongsTo(models.appointment)
    CarrierRequest.belongsTo(models.user, { foreignKey: 'createdBy' })
    CarrierRequest.belongsTo(models.user, { foreignKey: 'updatedBy' })
    CarrierRequest.hasMany(models.carrierRequestOrder)
  }
  return CarrierRequest
}
