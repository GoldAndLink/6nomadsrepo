// findsecrets-ignore-file
const uuidv4 = require('uuid/v4')
const _ = require('lodash')
const CarrierRequest = require('../../models').carrierRequest
const CarrierRequestOrder = require('../../models').carrierRequestOrder
const Carrier = require('../../models').carrier
const Driver = require('../../models').driver
const Order = require('../../models').order
const Location = require('../../models').location
const Customer = require('../../models').customer
const Item = require('../../models').item
const Appointment = require('../../models').appointment
const AppointmentStatus = require('../../models').appointmentStatus
const AppointmentOrder = require('../../models').appointmentOrder
const {
  check,
  validationResult
} = require('express-validator/check')
const builder = require('../../utils/builder')
const sms = require('../sms/utils')
const email = require('../email/utils')
const moment = require('moment')
const socket = require('../socket')
const Op = require('sequelize').Op

const countCarrierRequestsInStatusPending = async (req, res, next) => {
  try {
    const withoutAppointmentQuery = {
      where: {
        appointmentId: null
      }
    }
    const rescheduledWithoutAppointmentQuery = {
      where: {
        [Op.and]: {
          appointmentId: null,
          status: 'reschedule'
        }
      }
    }
    const canceledWithoutAppointmentQuery = {
      where: {
        [Op.and]: {
          appointmentId: null,
          status: 'canceled'
        }
      }
    }
    const withAppointmentQueryReschedule = {
      where: { status: 'reschedule' },
      include: [
        {
          model: Appointment,
          required: true,
          include: [
            {
              model: AppointmentStatus,
              required: true,
              where: {
                [Op.or]: [
                  { name: 'Draft' },
                  { name: 'Scheduled' }
                ]
              }
            }
          ]
        }
      ]
    }
    const withAppointmentQueryCanceled = {
      where: { status: 'canceled' },
      include: [
        {
          model: Appointment,
          required: true,
          include: [
            {
              model: AppointmentStatus,
              required: true,
              where: {
                [Op.or]: [
                  { name: 'Draft' },
                  { name: 'Scheduled' }
                ]
              }
            }
          ]
        }
      ]
    }

    // only see your own requests
    if (!req.user.roles.some(role => ['admin', 'scheduler'].includes(role.name))) {
      withoutAppointmentQuery.where.createdBy = req.user.id
      withAppointmentQueryReschedule.where.createdBy = req.user.id
      withAppointmentQueryCanceled.where.createdBy = req.user.id
      rescheduledWithoutAppointmentQuery.where.createdBy = req.user.id
      canceledWithoutAppointmentQuery.where.createdBy = req.user.id
    }

    const requestsWithoutAppointment = await CarrierRequest.count(withoutAppointmentQuery)
    const requestsRescheduledWithAppointment = await CarrierRequest.count(withAppointmentQueryReschedule)
    const requestsCanceledWithAppointment = await CarrierRequest.count(withAppointmentQueryCanceled)
    const requestsRescheduledWithoutAppointment = await CarrierRequest.count(rescheduledWithoutAppointmentQuery)
    const requestsCanceledWithoutAppointment = await CarrierRequest.count(canceledWithoutAppointmentQuery)

    const numberOfCarrierRequests = {
      allCarrierRequests: requestsWithoutAppointment + requestsRescheduledWithAppointment + requestsCanceledWithAppointment,
      numberOfRescheduledRequests: requestsRescheduledWithAppointment + requestsRescheduledWithoutAppointment,
      numberOfCanceledRequests: requestsCanceledWithAppointment + requestsCanceledWithoutAppointment
    }

    if (res) {
      return res.send({
        numberOfCarrierRequests
      })
    } else {
      return numberOfCarrierRequests
    }
  } catch (err) {
    if (res) {
      return res.status(500).send(err)
    } else {
      throw err
    }
  }
}

module.exports = {
  getCarrierRequests: async (req, res, next) => {
    const allowedParams = ['guid', 'id', 'carrierId', 'date', 'email', 'phone', 'firstName', 'lastName', 'rescheduleTimeSuggestion', 'prefContactMethod', 'status', 'appointmentId', 'createdBy']
    const allowedSorters = ['date', 'updatedAt']
    const allowedInclusions = [{
      modelName: 'appointment',
      required: false,
      allowedParams: ['appointmentStatusId'],
      fields: ['id']
    },
    {
      modelName: 'carrier'
    }
    ]

    let includeSet = builder.includer(req, allowedInclusions)
    if (req.query.carrierPortal) {
      includeSet = [{
        model: Appointment,
        attributes: { exclude: ['createdAt', 'updatedAt'] },
        include: [
          {
            model: Order,
            attributes: { exclude: ['createdAt', 'updatedAt'] },
            include: [
              {
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: Customer
              },
              {
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: Item
              },
              {
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: Location,
                as: 'destination'
              },
              {
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: Location,
                as: 'origin'
              }
            ]
          },
          {
            attributes: { exclude: ['createdAt', 'updatedAt'] },
            model: CarrierRequest
          }
        ]
      }]
    }
    try {
    // ugly af, getting orders first
      if (req.query.PO || req.query.primaryRefValue) {
        const orderWhere = {}

        if (req.query.primaryRefValue) {
          if (typeof req.query.primaryRefValue === 'string') {
            req.query.primaryRefValue = [req.query.primaryRefValue]
          }
          orderWhere.primaryRefValue = {
            [Op.or]: req.query.primaryRefValue.map(value => ({
              [Op.iLike]: `%${value.replace(/(_|%|\\)/g, '\\$1')}%`
            }))
          }
        }

        if (req.query.PO) {
          if (typeof req.query.PO === 'string') {
            req.query.PO = [req.query.PO]
          }
          orderWhere.data = {
            [Op.or]: req.query.PO.map(_po => ({ [Op.contains]: { otherRefs: [{ val: _po }] } }))
          }
        }

        const orders = await Order.findAll({
          where: orderWhere,
          attributes: ['id', 'data']
        })

        const orderRefs = _.flatten(orders
          .map(o => o.data.otherRefs
            .filter(ref => ref.type === 'PO')
            .map(ref => ref.val || undefined)
          ))

        includeSet.push({
          model: CarrierRequestOrder,
          required: true,
          where: {
            poNumber: orderRefs
          }
        })
      } else {
        includeSet.push({
          model: CarrierRequestOrder,
          required: false
        })
      }
    } catch (err) {
      console.warn(err)
      return res.status(500).send(err.message)
    }

    const qualifier = builder.qualifier(req, allowedParams)
    if (req.query.carrierPortal) {
      // only see your own requests - match id OR email
      qualifier[Op.or] = [
        { createdBy: req.user.id },
        { email: req.user.email }
      ]
    }

    try {
      const entities = await CarrierRequest.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
        where: qualifier,
        order: builder.sorter(req, allowedSorters),
        include: includeSet
      })
      return res.send(entities)
    } catch (err) {
      console.warn(err)
      return res.status(500).send(err)
    }
  },
  createCarrierRequest: async (req, res, next) => {
    const carrierRequestEmailTemplate = {
      APPOINTMENT_REQUEST_RECEIPT_CONFIRMATION: 2
    }
    const carrierRequestSMSTemplate = {
      APPOINTMENT_REQUEST_RECEIPT_CONFIRMATION: 2
    }
    const allowedFields = ['driverId', 'driverName', 'carrierId', 'carrierName', 'POs', 'date', 'timeStart', 'timeEnd', 'email', 'phone', 'firstName', 'lastName', 'prefContactMethod', 'status', 'appointmentId', 'trailerLicense', 'tractorNumber', 'createdBy']
    const entity = {}

    for (const field of allowedFields) {
      entity[field] = req.body[field]
    }

    let carrier
    if (entity.carrierName) {
      carrier = await Carrier.findOne({
        where: {
          name: { [Op.iLike]: '%' + entity.carrierName + '%' }
        }
      })
      if (carrier) {
        entity.carrierId = carrier.id
      } else {
        carrier = new Carrier({
          name: entity.carrierName
        })
        const c = await carrier.save()
        entity.carrierId = c.id
      }
    } else if (entity.carrierId) {
      carrier = await Carrier.findOne({
        where: {
          id: entity.carrierId
        }
      })
      entity.carrierName = carrier.name
    }

    let driver
    if (entity.driverName) {
      const name = entity.driverName.split(' ')
      driver = await Driver.findOne({
        where: {
          [Op.or]: [
            { [Op.and]: {
              firstName: { [Op.iLike]: '%' + name[0] + '%' },
              lastName: { [Op.iLike]: '%' + name[1] + '%' }
            } },
            { [Op.and]: {
              firstName: { [Op.iLike]: '%' + name[1] + '%' },
              lastName: { [Op.iLike]: '%' + name[0] + '%' }
            } }
          ]
        }
      })
      if (driver) {
        entity.driverId = driver.id
      } else {
        driver = new Driver({
          firstName: name[0],
          lastName: name[1],
          carrierId: entity.carrierId,
          email: entity.email,
          phone: entity.phone,
          contactMethod: entity.prefContactMethod
        })
        const d = await driver.save()
        entity.driverId = d.id
      }
    } else if (entity.driverId) {
      const d = await Driver.findOne({
        where: {
          id: entity.driverId
        }
      })
      entity.driverId = d.id
    }

    try {
      entity.guid = uuidv4()
      entity.createdBy = req.user.id
      entity.status = 'pending'
      const cq = new CarrierRequest(entity)
      const result = await cq.save()
      const orders = await Order.findAll({
        where: {
          data: {
            [Op.contains]: {
              otherRefs: entity.POs.map(po => {
                return {
                  val: po
                }
              })
            }
          }
        },
        include: [{
          model: Location,
          as: 'destination'
        }]
      })

      let destination = ''
      if (orders.length > 0) destination = orders[0].destination ? orders[0].destination.name : null

      const primaryRefValues = orders.map(o => o.primaryRefValue)

      for (const PO of entity.POs) {
        const CRO = new CarrierRequestOrder({
          carrierRequestId: result.id,
          poNumber: PO
        })
        await CRO.save()
        result.carrierRequestOrders = await result.getCarrierRequestOrders()
        result.carrier = await result.getCarrier()
        result.driver = await result.getDriver()
      }

      // send sms
      if (entity.prefContactMethod === 'sms') {
        await sms.send(entity.phone, carrierRequestSMSTemplate.APPOINTMENT_REQUEST_RECEIPT_CONFIRMATION, {
          date: moment(entity.date).format('LL'),
          timeStart: moment(entity.timeStart).format('HH:mm'),
          customer: entity.carrierName,
          // TODO we need to include city / state in the destination table
          destination: destination,
          poList: entity.POs,
          soList: [...new Set(primaryRefValues)],
          tractor: entity.tractorNumber,
          trailer: entity.trailerLicense,
          linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${result.guid || ''}`,
          linkLogin: ''
        })
      }

      // send email
      if (entity.prefContactMethod === 'email') {
        await email.send({
          htmlTemplate: 'basic',
          emailTemplateId: carrierRequestEmailTemplate.APPOINTMENT_REQUEST_RECEIPT_CONFIRMATION,
          to: entity.email,
          subject: 'DO NOT REPLY: Appointment Request Received',
          data: {
            date: moment(entity.date).format('ddd, MMMM D, YYYY'),
            timeStart: `${moment(entity.timeStart).format('HH:mm')} - ${moment(entity.timeEnd).format('HH:mm')}`,
            customer: entity.carrierName,
            destination: destination,
            poList: entity.POs,
            soList: [...new Set(primaryRefValues)],
            tractor: entity.tractorNumber,
            trailer: entity.trailerLicense,
            linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${result.guid || ''}`,
            linkLogin: ''
          }
        })
      }

      const rawResult = result.toJSON()
      rawResult.carrierRequestOrders = result.carrierRequestOrders
      rawResult.carrier = result.carrier
      rawResult.driver = result.driver
      rawResult.count = await countCarrierRequestsInStatusPending(req)
      socket.carrierRequests.broadcastOne(rawResult)

      res.status(201).send(result)
    } catch (err) {
      console.error(err)
      res.status(500).send({
        message: err
      })
    }
  },
  countCarrierRequestsInStatusPending: async (req, res, next) => { return countCarrierRequestsInStatusPending(req, res, next) },
  updateCarrierRequest: async (req, res, next) => {
    const carrierRequestStatus = {
      SCHEDULED_STATUS: 'scheduled',
      RESCHEDULED_STATUS: 'reschedule',
      CANCELED_STATUS: 'canceled'
    }
    const carrierRequestSMSTemplate = {
      APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION: 4,
      APPOINTMENT_RESCHEDULE_REQUEST: 5,
      APPOINTMENT_CANCELLATION_REQUEST: 6,
      APPOINTMENT_CANCELLATION: 7
    }
    const carrierRequestEmailTemplate = {
      APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION: 3,
      APPOINTMENT_RESCHEDULE_REQUEST: 4,
      APPOINTMENT_CANCELLATION_REQUEST: 5,
      APPOINTMENT_CANCELLATION: 6
    }

    const editables = ['carrierId', 'PO', 'date', 'timeStart', 'timeEnd', 'email', 'phone', 'firstName', 'lastName', 'prefContactMethod', 'status', 'appointmentId', 'trailerLicense', 'tractorNumber', 'rescheduleTimeSuggestion']

    if (!req.params.id) return res.sendStatus(404)

    const carrierRequest = await CarrierRequest.findOne({
      where: {
        id: req.params.id
      }
    })
    if (!carrierRequest) return res.sendStatus(404)

    const delta = {}

    for (const field of editables) {
      delta[field] = req.body[field]
    }

    try {
      const entity = await CarrierRequest.update(delta, {
        where: {
          id: req.params.id
        },
        returning: true,
        plain: true
      })

      if (!entity) return res.sendStatus(404)

      const { dataValues } = entity[1]
      let orders = []
      let ordersPoNumbers = []
      let carrierName = null
      let tractorNumber = dataValues.tractorNumber
      let trailerLicense = dataValues.trailerLicense

      let carrierRequestOrders = []
      carrierRequestOrders = await CarrierRequestOrder.findAll({
        where: {
          carrierRequestId: dataValues.id
        }
      })
      ordersPoNumbers = carrierRequestOrders.map(carrierRequestOrder => carrierRequestOrder.poNumber)

      if (dataValues && dataValues.appointmentId) {
        const appointment = await Appointment.findOne({
          where: {
            id: dataValues.appointmentId
          },
          include: [
            {
              model: Order,
              include: [
                {
                  model: Customer
                },
                {
                  model: Item
                },
                {
                  model: Location,
                  as: 'destination'
                },
                {
                  model: Location,
                  as: 'origin'
                }
              ]
            },
            {
              model: CarrierRequest
            }
          ]
        })
        if (appointment) {
          socket.appointments.broadcastOne(appointment)

          const appointmentOrdersIds = await AppointmentOrder.findAll({
            where: {
              appointmentId: dataValues.appointmentId
            }
          }).map(appointmentOrder => appointmentOrder.orderId)

          tractorNumber = appointment.tractor
          trailerLicense = appointment.trailer

          orders = appointmentOrdersIds ? await Order.findAll({
            where: {
              id: appointmentOrdersIds
            },
            include: [{
              model: Location,
              as: 'destination'
            }]
          }) : []
        }
      } else {
        const allOrders = ordersPoNumbers ? await Order.findAll() : []
        orders = allOrders.filter(order => (
          order.data.otherRefs.find(otherRef => (
            otherRef.val.indexOf(ordersPoNumbers) !== -1
          ))
        ))
      }

      if (dataValues.carrierId) {
        const carrier = await Carrier.findOne({
          where: {
            id: dataValues.carrierId
          }
        })
        carrierName = carrier.name
      }

      dataValues.count = await countCarrierRequestsInStatusPending(req)
      socket.carrierRequests.broadcastOne(dataValues)

      const destinationId = orders && !!orders.length ? orders[0].destinationId : null
      let destination = ''
      if (destinationId) destination = await Location.findOne({ where: { id: destinationId } })

      const primaryRefValues = orders.map(o => o.primaryRefValue)

      // Email and SMS sending depending on the CR status
      if (dataValues.status === carrierRequestStatus.SCHEDULED_STATUS) {
        // send sms
        if (dataValues.prefContactMethod === 'sms') {
          await sms.send(dataValues.phone, carrierRequestSMSTemplate.APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION, {
            date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
            timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
            customer: carrierName,
            destination: destination.name,
            poList: _.flatten(ordersPoNumbers),
            soList: [...new Set(primaryRefValues)],
            tractor: tractorNumber,
            trailer: trailerLicense,
            linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
            linkLogin: ''
          })
        }

        // send email
        if (dataValues.prefContactMethod === 'email') {
          await email.send({
            htmlTemplate: 'basic',
            emailTemplateId: carrierRequestEmailTemplate.APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION,
            to: dataValues.email,
            subject: 'DO NOT REPLY: Appointment Request Scheduled',
            data: {
              date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
              timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
              customer: carrierName,
              destination: destination.name,
              poList: _.flatten(ordersPoNumbers),
              soList: [...new Set(primaryRefValues)],
              tractor: tractorNumber,
              trailer: trailerLicense,
              linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
              linkLogin: ''
            }
          })
        }
      }
      if (dataValues.status === carrierRequestStatus.RESCHEDULED_STATUS) {
        // send sms
        if (dataValues.prefContactMethod === 'sms') {
          await sms.send(dataValues.phone, carrierRequestSMSTemplate.APPOINTMENT_RESCHEDULE_REQUEST, {
            date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
            timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
            customer: carrierName,
            destination: destination.name,
            poList: _.flatten(ordersPoNumbers),
            soList: [...new Set(primaryRefValues)],
            tractor: tractorNumber,
            trailer: trailerLicense,
            linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
            linkLogin: ''
          })
        }

        // send email
        if (dataValues.prefContactMethod === 'email') {
          await email.send({
            htmlTemplate: 'basic',
            emailTemplateId: carrierRequestEmailTemplate.APPOINTMENT_RESCHEDULE_REQUEST,
            to: dataValues.email,
            subject: 'DO NOT REPLY: Request to Reschedule Appointment',
            data: {
              date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
              timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
              customer: carrierName,
              destination: destination.name,
              poList: _.flatten(ordersPoNumbers),
              soList: [...new Set(primaryRefValues)],
              tractor: tractorNumber,
              trailer: trailerLicense,
              linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
              linkLogin: ''
            }
          })
        }
      }
      if (dataValues.status === carrierRequestStatus.CANCELED_STATUS && dataValues.appointmentId) {
        // send sms
        if (dataValues.prefContactMethod === 'sms') {
          await sms.send(dataValues.phone, carrierRequestSMSTemplate.APPOINTMENT_CANCELLATION_REQUEST, {
            date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
            timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
            customer: carrierName,
            destination: destination.name,
            poList: _.flatten(ordersPoNumbers),
            soList: [...new Set(primaryRefValues)],
            tractor: tractorNumber,
            trailer: trailerLicense,
            linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
            linkLogin: ''
          })
        }

        // send email
        if (dataValues.prefContactMethod === 'email') {
          await email.send({
            htmlTemplate: 'basic',
            emailTemplateId: carrierRequestEmailTemplate.APPOINTMENT_CANCELLATION_REQUEST,
            to: dataValues.email,
            subject: 'DO NOT REPLY: Request to Cancel Appointment',
            data: {
              date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
              timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
              customer: carrierName,
              destination: destination.name,
              poList: _.flatten(ordersPoNumbers),
              soList: [...new Set(primaryRefValues)],
              tractor: tractorNumber,
              trailer: trailerLicense,
              linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
              linkLogin: ''
            }
          })
        }
      }
      if (dataValues.status === carrierRequestStatus.CANCELED_STATUS && !dataValues.appointmentId) {
        await CarrierRequestOrder.destroy({
          where: {
            carrierRequestId: dataValues.id
          }
        })
        await CarrierRequest.destroy({
          where: {
            id: dataValues.id
          }
        })
        dataValues.count = await countCarrierRequestsInStatusPending(req)
        socket.carrierRequests.broadcastOne(Object.assign(
          {},
          dataValues,
          { deletedAt: moment().toISOString() }
        ))

        // send sms
        if (dataValues.prefContactMethod === 'sms') {
          await sms.send(dataValues.phone, carrierRequestSMSTemplate.APPOINTMENT_CANCELLATION, {
            date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
            timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
            customer: carrierName,
            destination: destination.name,
            poList: _.flatten(ordersPoNumbers),
            soList: [...new Set(primaryRefValues)],
            tractor: tractorNumber,
            trailer: trailerLicense,
            linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
            linkLogin: ''
          })
        }

        // send email
        if (dataValues.prefContactMethod === 'email') {
          await email.send({
            htmlTemplate: 'basic',
            emailTemplateId: carrierRequestEmailTemplate.APPOINTMENT_CANCELLATION,
            to: dataValues.email,
            subject: 'DO NOT REPLY: Appointment Cancellation',
            data: {
              date: moment(dataValues.date).format('ddd, MMMM D, YYYY'),
              timeStart: `${moment(dataValues.timeStart).format('HH:mm')} - ${moment(dataValues.timeEnd).format('HH:mm')}`,
              customer: carrierName,
              destination: destination.name,
              poList: _.flatten(ordersPoNumbers),
              soList: [...new Set(primaryRefValues)],
              tractor: tractorNumber,
              trailer: trailerLicense,
              linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
              linkLogin: ''
            }
          })
        }
      }

      return res.send(dataValues)
    } catch (err) {
      console.error(err)
      return res.sendStatus(500)
    }
  },
  validateCreateCarrierRequest: [
    check('POs').not().isEmpty(),
    check('date').not().isEmpty(),
    check('timeStart').not().isEmpty(),
    check('timeEnd').not().isEmpty(),
    check('carrierId').optional(),
    check('carrierName').optional().isString(),
    check('driverId').optional(),
    check('driverName').optional().isString(),
    check('email').not().isEmpty(),
    check('email').isEmail(),
    check('prefContactMethod').not().isEmpty(),
    check('prefContactMethod').isIn(['email', 'sms']),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.array()
        })
      }

      if (!req.body.carrierId && !req.body.carrierName) {
        return res.status(422).json({
          errors: ['carrierId or carrierName must be provided']
        })
      }

      next()
    }
  ]
}
