const Site = require('../../models').site
const Building = require('../../models').building
const Area = require('../../models').area
const Door = require('../../models').door

module.exports = {
  getAllSites: async (req, res, next) => {
    const allowedParams = ['id', 'name']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }

    try {
      const options = {
        where: qualifier,
        include: [
          {
            model: Building,
            include: [
              {
                model: Area,
                include: [
                  {
                    model: Door }
                ]
              }
            ]
          }
        ]
      }

      const sites = await Site.findAll(options)

      res.send(sites)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  getBuildingsForSite: async (req, res, next) => {
    try {
      const buildings = await Building.findAll({
        where: { siteId: req.params.id }
      })

      res.send(buildings)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
