module.exports = (sequelize, DataTypes) => {
  const Site = sequelize.define(
    'site',
    {
      name: DataTypes.STRING
    },
    {}
  )
  Site.associate = function (models) {
    Site.hasMany(models.building)
  }
  return Site
}
