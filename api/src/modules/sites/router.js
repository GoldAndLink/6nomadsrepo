const controller = require('./sites.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/sites'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAllSites })
)
router.get(
  '/:id/buildings',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getBuildingsForSite })
)
module.exports = {
  router,
  baseUrl
}
