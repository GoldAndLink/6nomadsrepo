const DoorSchedule = require('../../models').doorSchedule
const builder = require('../../utils/builder')

module.exports = {
  getDoorSchedules: async (req, res, next) => {
    const allowedParams = ['id', 'scheduleId', 'doorId', 'holiday', 'dayOfWeek', 'open', 'close', 'name', 'active']
    const allowedSorters = ['startDate', 'endDate', 'open', 'close', 'dayOfWeek']

    try {
      const entities = await DoorSchedule.findAll({
        where: builder.qualifier(req, allowedParams),
        order: builder.sorter(req, allowedSorters),
        include: []
      })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
