module.exports = (sequelize, DataTypes) => {
  const DoorSchedule = sequelize.define(
    'doorSchedule',
    {
      scheduleId: DataTypes.INTEGER,
      doorId: DataTypes.INTEGER,
      holiday: DataTypes.BOOLEAN,
      dayOfWeek: DataTypes.INTEGER,
      open: DataTypes.TIME,
      close: DataTypes.TIME,
      active: DataTypes.BOOLEAN,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER
    },
    {
      paranoid: true
    }
  )
  DoorSchedule.associate = function (models) {
    DoorSchedule.belongsTo(models.user, { foreignKey: 'createdBy' })
    DoorSchedule.belongsTo(models.user, { foreignKey: 'updatedBy' })
    DoorSchedule.belongsTo(models.schedule)
    DoorSchedule.belongsTo(models.door)
  }
  return DoorSchedule
}
