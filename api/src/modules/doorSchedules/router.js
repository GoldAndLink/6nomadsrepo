const controller = require('./doorSchedules.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/doorSchedules'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getDoorSchedules })
)

module.exports = {
  router,
  baseUrl
}
