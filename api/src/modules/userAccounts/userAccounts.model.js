module.exports = (sequelize, DataTypes) => {
  const userAccount = sequelize.define(
    'userAccount',
    {},
    {}
  )

  userAccount.associate = function (models) {
    userAccount.belongsTo(models.user)
    userAccount.belongsTo(models.account)
  }

  return userAccount
}
