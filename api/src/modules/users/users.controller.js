const jwt = require('jsonwebtoken')
const { validationResult } = require('express-validator/check')
const { check } = require('express-validator/check')
const url = require('url')
const passport = require('../../../config/passport/index')
const User = require('../../models').user
const Role = require('../../models').role
const UserRole = require('../../models').userRole
const UserAccount = require('../../models').userAccount
const Account = require('../../models').account
const Carrier = require('../../models').carrier
const Driver = require('../../models').driver
const Op = require('sequelize').Op

module.exports = {
  getUsers: async (req, res, next) => {
    const urlParts = url.parse(req.url, true)
    const query = urlParts.query
    let users = []
    if (query.roles) {
      const rolesIds = query.roles.split(',')
      users = await User.findAll({
        where: {
          '$Roles.id$': rolesIds
        }
      })
    } else {
      users = await User.findAll()
    }

    res.json({ users })
  },
  getUser: async (req, res, next) => {
    const user = await User.findOne({
      where: { id: req.params.id }
    })
    res.json({ user })
  },
  createUser: async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const user = await User.build({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password
      })
        .generateHashPassword()
        .save()

      if (req.body.roles) {
        const roles = await Role.findAll({ where: { id: req.body.roles } })
        user.setRoles(roles)
      }

      if (req.body.accounts) {
        const accounts = await Account.findAll({
          where: { id: { [Op.in]: (req.body.accounts || []).map(r => r.id) } }
        })
        user.setAccounts(accounts)
      }

      res.json({ message: 'User created' })
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  createCarrierUser: async (req, res, next) => {
    try {
      const allowedFields = ['driverId', 'driverName', 'carrierId', 'carrierName', 'email', 'phone', 'firstName', 'lastName', 'prefContactMethod', 'password']
      const entity = {}

      for (const field of allowedFields) {
        entity[field] = req.body[field]
      }

      let carrier
      if (!entity.carrierId && entity.carrierName) {
        carrier = await Carrier.findOne({
          where: {
            name: { [Op.iLike]: '%' + entity.carrierName + '%' }
          }
        })
        if (carrier) {
          entity.carrierId = carrier.id
        } else {
          carrier = new Carrier({
            name: entity.carrierName
          })
          const c = await carrier.save()
          entity.carrierId = c.id
        }
      } else {
        carrier = await Carrier.findOne({
          where: {
            id: entity.carrierId
          }
        })
        entity.carrierName = carrier.name
      }

      let driver
      if (!entity.driverId && entity.driverName) {
        const name = entity.driverName.split(' ')
        driver = await Driver.findOne({
          where: {
            [Op.or]: [
              { [Op.and]: {
                firstName: { [Op.iLike]: '%' + name[0] + '%' },
                lastName: { [Op.iLike]: '%' + name[1] + '%' }
              } },
              { [Op.and]: {
                firstName: { [Op.iLike]: '%' + name[1] + '%' },
                lastName: { [Op.iLike]: '%' + name[0] + '%' }
              } }
            ]
          }
        })
        if (driver) {
          entity.driverId = driver.id
        } else {
          driver = new Driver({
            firstName: name[0],
            lastName: name[1],
            carrierId: entity.carrierId,
            email: entity.email,
            phone: entity.phone,
            contactMethod: entity.prefContactMethod
          })
          const d = await driver.save()
          entity.driverId = d.id
        }
      }

      const user = await User.build(entity)
        .generateHashPassword()
        .save()

      if (req.body.roles) {
        const roles = await Role.findAll({ where: { id: req.body.roles } })
        user.setRoles(roles)
      }

      res.status(201).json(user)
    } catch (err) {
      console.log(err)
      res.status(500).send({ message: err })
    }
  },
  updateMe: async (req, res, next) => {
    try {
      const user = await User.findOne({ where: { id: req.user.id } })
      const roles = await Role.findAll({ where: { id: req.body.roles } })

      user.firstName = req.body.firstName
      user.lastName = req.body.lastName
      user.email = req.body.email
      user.phone = req.body.phone
      user.prefContactMethod = req.body.prefContactMethod

      let carrier
      if (!req.body.carrierId && req.body.carrierName) {
        carrier = await Carrier.findOne({
          where: {
            name: { [Op.iLike]: req.body.carrierName.trim() + '%' }
          }
        })
        if (carrier) {
          user.carrierId = carrier.id
        } else {
          carrier = new Carrier({
            name: req.body.carrierName
          })
          const c = await carrier.save()
          user.carrierId = c.id
        }
      } else {
        user.carrierId = req.body.carrierId
      }

      let driver
      if (!req.body.driverId && req.body.driverName) {
        const name = req.body.driverName.trim().split(' ')
        const firstName = name[0] && name[0].trim()
        const lastName = name[1] && name[1].trim()
        let where = {}

        if (firstName && lastName) {
          where = {
            [Op.and]: [
              { firstName: { [Op.iLike]: firstName + '%' } },
              { lastName: { [Op.iLike]: lastName + '%' } }
            ]
          }
        } else if (firstName) {
          where = { firstName: { [Op.iLike]: firstName + '%' } }
        } else if (lastName) {
          where = { lastName: { [Op.iLike]: lastName + '%' } }
        }

        driver = await Driver.findOne({ where })

        if (driver) {
          user.driverId = driver.id
        } else {
          driver = new Driver({
            firstName: firstName,
            lastName: lastName,
            carrierId: req.body.carrierId,
            email: req.body.email,
            phone: req.body.phone,
            contactMethod: req.body.prefContactMethod
          })
          const d = await driver.save()
          user.driverId = d.id
        }
      } else {
        user.driverId = req.body.driverId
      }

      if (req.body.password) {
        user.password = req.body.password
        await user.generateHashPassword().save()
      } else {
        await user.save()
      }

      user.setRoles(roles)

      // load dependencies
      await user.reload({ include: [{ model: Driver }, { model: Carrier }, { model: Role }] })

      res.status(201).json(user)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  updateUser: async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const user = await User.findOne({ where: { id: req.params.id } })

      const roles = await Role.findAll({
        where: { id: { [Op.in]: (req.body.roles || []).map(r => r.id) } }
      })

      const accounts = await Account.findAll({
        where: { id: { [Op.in]: (req.body.accounts || []).map(r => r.id) } }
      })

      user.firstName = req.body.firstName
      user.lastName = req.body.lastName
      user.email = req.body.email
      user.defaultBuildingId = req.body.defaultBuildingId

      await user.setRoles(roles)
      await user.setAccounts(accounts)

      if (req.body.password) {
        user.password = req.body.password
        user.generateHashPassword()
      }

      await user.save()
      const updatedUser = await User.findOne({ where: { id: req.params.id } })

      res.status(201).send(updatedUser)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  changeUserActivation: async (req, res, next) => {
    try {
      const user = await User.update(
        {
          active: req.body.active
        },
        {
          where: {
            id: req.params.id
          },
          returning: true
        }
      )

      res.json({ user: user[1] })
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  deleteUser: async (req, res, next) => {
    try {
      // delete roles
      await UserRole.destroy({
        where: {
          userId: req.params.id
        }
      })
      // delete accounts
      await UserAccount.destroy({
        where: {
          userId: req.params.id
        }
      })
      // delete user
      await User.destroy({
        where: {
          id: req.params.id
        }
      })

      res.json({ userId: Number(req.params.id) })
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  login: async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const user = await User.scope('withPassword').findOne({ where: { email: req.body.email } })
      if (!user) {
        return res.status(401).json({ message: 'No such user found' })
      }

      if (User.isValidPassword(req.body.password, user.password)) {
        const payload = { id: user.id }
        const token = jwt.sign(payload, passport.jwtOptions.secretOrKey, {
          expiresIn: '30d'
        })

        // load dependencies
        await user.reload({ include: [{ model: Driver }, { model: Carrier }, { model: Role }] })

        res.json({ token, user })
      } else {
        res.status(401).json({ message: 'Password did not match' })
      }
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  resetPassword: async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const user = await User.findOne({ where: { email: req.body.email } })
      if (!user) {
        res.status(401).json({ message: 'user with that email does not exist' })
      } else {
        const token = await User.generateResetPasswordToken()
        await User.update(
          {
            resetPasswordToken: token,
            resetPasswordSentAt: new Date()
          },
          {
            where: {
              email: req.body.email
            }
          }
        )
        res.json({ resetPasswordToken: token })
      }
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  changePassword: async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const user = await User.findOne({ where: { email: req.body.email } })
      const date = new Date()

      if (!user) {
        res.status(401).json({ message: 'user with that email does not exist' })
      } else {
        if (
          user.resetPasswordToken === req.params.token &&
          user.resetPasswordSentAt > date.setDate(date.getDate() - 10)
        ) {
          user.password = req.body.password
          await user.generateHashPassword().save()
          res.json({ message: 'password changed' })
        } else {
          res.json({ message: 'password was not changed' })
        }
      }
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  validateCreateUser: [
    check('firstName').not().isEmpty(),
    check('lastName').not().isEmpty(),
    check('email').isEmail(),
    check('password')
      .isLength({ min: 6 })
      .withMessage('must be at least 6 chars long')
      .matches('[0-9]')
      .withMessage('must contain a number')
      .matches('[a-z]')
      .withMessage('must contain a lower case')
      .matches('[A-Z]')
      .withMessage('must contain a upper case')
      .matches('[^A-Za-z0-9]')
      .withMessage('must contain a special character'),
    (req, res, next) => {
      next()
    }
  ],
  validateCreateCarrierUser: [
    check('carrierId').optional(),
    check('carrierName').optional().isString(),
    check('driverId').optional(),
    check('driverName').optional().isString(),
    check('roles').not().isEmpty(),
    check('email').not().isEmpty(),
    check('email').isEmail(),
    check('prefContactMethod').not().isEmpty(),
    check('prefContactMethod').isIn(['email', 'sms']),
    check('password')
      .isLength({ min: 6 })
      .withMessage('must be at least 6 chars long')
      .matches('[0-9]')
      .withMessage('must contain a number')
      .matches('[a-z]')
      .withMessage('must contain a lower case')
      .matches('[A-Z]')
      .withMessage('must contain a upper case')
      .matches('[^A-Za-z0-9]')
      .withMessage('must contain a special character'),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }

      if (!req.body.carrierId && !req.body.carrierName) {
        return res.status(422).json({ errors: ['carrierId or carrierName must be provided'] })
      }

      next()
    }
  ],
  validateUpdateUser: [
    check('firstName').not().isEmpty(),
    check('lastName').not().isEmpty(),
    check('email').isEmail(),
    check('password')
      .optional()
      .isLength({ min: 6 })
      .withMessage('must be at least 6 chars long')
      .matches('[0-9]')
      .withMessage('must contain a number')
      .matches('[a-z]')
      .withMessage('must contain a lower case')
      .matches('[A-Z]')
      .withMessage('must contain a upper case')
      .matches('[^A-Za-z0-9]')
      .withMessage('must contain a special character'),
    check('roles').isArray().withMessage('must be a array'),
    (req, res, next) => {
      next()
    }
  ],
  validateLoginUser: [
    check('email').not().isEmpty(),
    check('email').isEmail(),
    check('password').not().isEmpty(),
    (req, res, next) => {
      next()
    }
  ],
  validateResetPassword: [
    check('email').not().isEmpty(),
    check('email').isEmail(),
    (req, res, next) => {
      next()
    }
  ],
  validateChangePassword: [
    check('email').not().isEmpty(),
    check('email').isEmail(),
    check('password').not().isEmpty(),
    check('passwordConfirmation').custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('Password confirmation does not match password')
      } else {
        return true
      }
    }),
    (req, res, next) => {
      next()
    }
  ]
}
