const bcrypt = require('bcryptjs')
const crypto = require('crypto')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'user',
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      resetPasswordToken: DataTypes.STRING,
      resetPasswordSentAt: DataTypes.DATE,
      active: DataTypes.BOOLEAN,
      carrierId: DataTypes.INTEGER,
      driverId: DataTypes.INTEGER,
      defaultBuildingId: DataTypes.INTEGER,
      phone: DataTypes.STRING,
      prefContactMethod: DataTypes.ENUM('email', 'sms')
    },
    {
      defaultScope: {
        include: [
          { model: sequelize.models.role },
          {
            model: sequelize.models.account,
            through: sequelize.models.userAccount
          }
        ],
        attributes: { exclude: ['password'] }
      },
      scopes: {
        withPassword: {
          include: [
            { model: sequelize.models.role },
            {
              model: sequelize.models.account,
              through: sequelize.models.userAccount
            }
          ],
          attributes: { }
        }
      }
    }
  )

  User.associate = function (models) {
    User.belongsToMany(models.role, {
      through: { model: models.userRole }
    })
    User.belongsToMany(models.account, {
      through: { model: models.userAccount }
    })
    User.belongsTo(models.carrier)
    User.belongsTo(models.driver)
    User.belongsTo(models.building, { as: 'defaultBuilding' })
  }

  User.prototype.generateHashPassword = function () {
    const user = this
    user.password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(), null)
    return user
  }

  User.isValidPassword = function (password, userPassword) {
    return bcrypt.compareSync(password, userPassword)
  }

  User.generateResetPasswordToken = function () {
    return crypto.randomBytes(20).toString('hex')
  }

  return User
}
