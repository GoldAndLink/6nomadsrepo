const controller = require('./users.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const authorize = require('../../../lib/authorize').authorize
const baseUrl = '/users'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  authorize(['admin']),
  version({ '1.0.0': controller.getUsers })
)
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  authorize(['admin']),
  version({ '1.0.0': controller.getUser })
)
router.post(
  '/create',
  passport.authenticate('jwt', { session: false }),
  authorize(['admin']),
  controller.validateCreateUser,
  version({ '1.0.0': controller.createUser })
)
router.post(
  '/carrier/create',
  passport.authenticate('headerapikey', { session: false }),
  controller.validateCreateCarrierUser,
  version({ '1.0.0': controller.createCarrierUser })
)
router.put(
  '/me',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.updateMe })
)
router.put(
  '/:id/update',
  passport.authenticate('jwt', { session: false }),
  authorize(['admin']),
  controller.validateUpdateUser,
  version({ '1.0.0': controller.updateUser })
)
router.put(
  '/:id/activation',
  passport.authenticate('jwt', { session: false }),
  authorize(['admin']),
  version({ '1.0.0': controller.changeUserActivation })
)
router.post(
  '/login',
  controller.validateLoginUser,
  version({ '1.0.0': controller.login })
)
router.post(
  '/reset-password',
  controller.validateResetPassword,
  version({ '1.0.0': controller.resetPassword })
)
router.post(
  '/change-password/:token?',
  version({ '1.0.0': controller.changePassword })
)
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  authorize(['admin']),
  version({ '1.0.0': controller.deleteUser })
)

module.exports = {
  router,
  baseUrl
}
