const mustache = require('mustache')
const rp = require('request-promise')
const SmsTemplate = require('../../models').smsTemplate

module.exports = {
  send: async (to, templateId, data) => {
    const tp = await SmsTemplate.findOne({ where: { id: templateId } })

    if (!tp) {
      throw new Error('Missing SMS template')
    }

    const text = mustache.render(tp.template, data)

    const options = {
      method: 'POST',
      uri: process.env.TWILIO_SMS_URI,
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + Buffer.from(process.env.TWILIO_USER + ':' + process.env.TWILIO_PASSWORD).toString('base64')
      },
      form: {
        To: to,
        From: process.env.TWILIO_NUMBER_FROM,
        Body: text
      },
      json: true
    }
    // redundant use of await on return value
    // return await rp(options)
    return rp(options)
  }
}
