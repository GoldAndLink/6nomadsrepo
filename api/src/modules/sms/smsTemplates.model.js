module.exports = (sequelize, DataTypes) => {
  const SmsTemplate = sequelize.define(
    'smsTemplate',
    {
      template: DataTypes.TEXT
    },
    {}
  )
  return SmsTemplate
}
