const ctrl = require('./sms.controller')
const mockRes = require('jest-mock-express').response
const sandbox = require('sinon').createSandbox()
const rp = require('request-promise')

describe('sms', () => {
  it('responds to POST', async done => {
    const res = mockRes()
    sandbox.stub(rp).resolves()

    await ctrl.smsSend(
      {
        body: {
          template: 1,
          to: '+5592981233668',
          content: {
            customer: 'Awesome Customer',
            door: 1,
            address: 'cool street, awesome number',
            poList: ['ABC123', 'ABC231', 'ABC321'],
            phoneNumber: '123456'
          }
        }
      },
      res
    )

    expect(res.send).toHaveReturned()
    expect(res.send).toHaveBeenCalled()
    expect(res.send).toHaveBeenCalledWith(expect.any(Array))
    done()
  })
})
