const { check, validationResult } = require('express-validator/check')
const sms = require('./utils')

module.exports = {
  smsSend: async (req, res, next) => {
    const { template, content, to } = req.body

    try {
      await sms.send(to, template, content)
      return res.send()
    } catch (e) {
      console.log('error: ', e)
      return res.status(500).send({ message: e.message || e })
    }
  },
  validateSmsSend: [
    check('template')
      .not()
      .isEmpty(),
    check('to')
      .not()
      .isEmpty(),
    check('content')
      .not()
      .isEmpty(),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      next()
    }
  ]
}
