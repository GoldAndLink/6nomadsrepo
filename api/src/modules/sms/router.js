const controller = require('./sms.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/sms'

router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  controller.validateSmsSend,
  version({ '1.0.0': controller.smsSend })
)

module.exports = {
  router,
  baseUrl
}
