module.exports = (sequelize, DataTypes) => {
  const AppointmentStatus = sequelize.define(
    'appointmentStatus',
    {
      name: DataTypes.STRING
    },
    {}
  )
  AppointmentStatus.associate = function (models) {
    AppointmentStatus.hasMany(models.appointment)
  }
  return AppointmentStatus
}
