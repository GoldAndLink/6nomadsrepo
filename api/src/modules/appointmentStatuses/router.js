const controller = require('./appointmentStatuses.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/appointment_statuses'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAllAppointmentStatuses })
)

module.exports = {
  router,
  baseUrl
}
