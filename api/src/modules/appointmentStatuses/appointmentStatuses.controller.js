const AppointmentStatus = require('../../models').appointmentStatus

module.exports = {
  getAllAppointmentStatuses: async (req, res, next) => {
    try {
      const appointmentStatuses = await AppointmentStatus.findAll({})

      res.send(appointmentStatuses)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
