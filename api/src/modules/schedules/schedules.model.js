module.exports = (sequelize, DataTypes) => {
  const Schedule = sequelize.define(
    'schedule',
    {
      name: DataTypes.STRING,
      active: DataTypes.BOOLEAN,
      startDate: DataTypes.DATE,
      endDate: DataTypes.DATE,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER
    },
    {
      paranoid: true
    }
  )
  Schedule.associate = function (models) {
    Schedule.belongsTo(models.user, { foreignKey: 'createdBy' })
    Schedule.belongsTo(models.user, { foreignKey: 'updatedBy' })
  }
  return Schedule
}
