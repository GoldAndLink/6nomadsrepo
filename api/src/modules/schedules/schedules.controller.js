const Schedule = require('../../models').schedule
const builder = require('../../utils/builder')

module.exports = {
  getSchedules: async (req, res, next) => {
    const allowedParams = ['id', 'name', 'active']
    const allowedSorters = ['startDate', 'endDate']

    try {
      const entities = await Schedule.findAll({
        where: builder.qualifier(req, allowedParams),
        order: builder.sorter(req, allowedSorters),
        include: []
      })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
