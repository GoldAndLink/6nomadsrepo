const controller = require('./schedules.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/schedules'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getSchedules })
)

module.exports = {
  router,
  baseUrl
}
