const sequelize = require('../../models').sequelize

module.exports = {
  summary: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT current_date AS "date",
           to_char(current_timestamp, 'HH:MI') AS "time",
           bu.name as Warehouse,
           count(distinct ao."appointmentId") as "trucks",
           SUM(oi.quantity) AS "totalCases",
           COUNT(DISTINCT ao."orderId") as "totalOrders",
           COUNT(DISTINCT i."sku") AS "totalSkus"
        FROM orders o
        INNER JOIN "orderItems" oi ON oi."orderId" = o.id
        INNER JOIN "items" i ON i.id = oi."itemId"
        INNER JOIN "appointmentOrders" ao ON ao."orderId" = o.id
        INNER JOIN appointments a ON a.id = ao."appointmentId"
        INNER JOIN doors d ON d.id = a."doorId"
        INNER JOIN areas area ON area.id = d."areaId"
        INNER JOIN buildings bu ON bu.id = area."buildingId"
        WHERE o."deliveryDate" between current_date and current_date + interval '23 hours'
        GROUP BY bu.name
        `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  summaryYesterday: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT
           bu.name as Warehouse,
           count(distinct ao."appointmentId") as "trucks",
           SUM(oi.quantity) AS "totalCases",
           COUNT(DISTINCT ao."orderId") as "totalOrders",
           0 AS "averageLoadTime"
        FROM orders o
        INNER JOIN "orderItems" oi ON oi."orderId" = o.id
        INNER JOIN "items" i ON i.id = oi."itemId"
        INNER JOIN "appointmentOrders" ao ON ao."orderId" = o.id
        INNER JOIN appointments a ON a.id = ao."appointmentId"
        INNER JOIN doors d ON d.id = a."doorId"
        INNER JOIN areas area ON area.id = d."areaId"
        INNER JOIN buildings bu ON bu.id = area."buildingId"
        WHERE o."deliveryDate" between current_date - interval '1 day' and current_date - interval '1 second'
        GROUP BY bu.name
        `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  todayAppointmentsSummary: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT
          bu.name as warehouse,
          to_char(a.date, 'YYYY-MM-DD') AS "date",
          to_char(a.date, 'HH24:MI') AS "time",
          count(distinct ao."appointmentId") as "trucks",
          COUNT(DISTINCT ao."orderId") as "totalOrders",
          COUNT(DISTINCT i."sku") AS "totalSkus",
          SUM(oi.quantity) AS "totalCases",
          SUM(oi.weight) AS "totalWeight"
        FROM orders o
          INNER JOIN "orderItems" oi ON oi."orderId" = o.id
          INNER JOIN "items" i ON i.id = oi."itemId"
          INNER JOIN "appointmentOrders" ao ON ao."orderId" = o.id
          INNER JOIN appointments a ON a.id = ao."appointmentId"
          INNER JOIN doors d ON d.id = a."doorId"
          INNER JOIN areas area ON area.id = d."areaId"
          INNER JOIN buildings bu ON bu.id = area."buildingId"
        WHERE a."date" between current_date and current_date + interval '23 hours' and a."deletedAt" IS NULL and ao."deletedAt" IS NULL
        GROUP BY bu.name, a.date
        `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  todayAppointments: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT
          bu.name as warehouse,
          to_char(a.date , 'YYYY-MM-DD') AS "date",
          to_char(a.date, 'HH24:MI') AS "time",
          COUNT(DISTINCT i."sku") AS "skus",
          COALESCE(SUM(oi.quantity),0) AS "cases",
          COALESCE(SUM(oi.weight),0) AS "weight",
          to_char(a."checkInTime", 'YYYY-MM-DD HH24:MI') AS "checkInTime",
          a.destination AS "destination",
          c.name as carrier,
          string_agg(DISTINCT o."primaryRefValue",', ') as "orders",
          string_agg(DISTINCT cu."name",', ') as "customers"
        FROM orders o
        LEFT JOIN "customers" cu ON cu.id = o."customerId"
        LEFT JOIN "orderItems" oi ON oi."orderId" = o.id
        LEFT JOIN "items" i ON i.id = oi."itemId"
        LEFT JOIN "appointmentOrders" ao ON ao."orderId" = o.id
        LEFT JOIN appointments a ON a.id = ao."appointmentId"
        LEFT JOIN carriers c ON c.id = a."carrierId"
        LEFT JOIN doors d ON d.id = a."doorId"
        LEFT JOIN areas area ON area.id = d."areaId"
        LEFT JOIN buildings bu ON bu.id = area."buildingId"
        WHERE a."date" between current_date and current_date + interval '23 hours' and a."deletedAt" IS NULL and ao."deletedAt" IS NULL
        GROUP BY bu.name, a.date, a."checkInTime", a.destination, c.name, bu.timezone
        `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  yesterdayAppointments: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT
          bu.name as warehouse,
          to_char(a.date, 'YYYY-MM-DD') AS "date",
          to_char(a.date, 'HH24:MI') AS "time",
          COUNT(DISTINCT i."sku") AS "skus",
          COALESCE(SUM(oi.quantity),0) AS "cases",
          COALESCE(SUM(oi.weight),0) AS "weight",
          to_char(a."checkInTime", 'YYYY-MM-DD HH24:MI') AS "checkInTime",
          a.destination AS "destination",
          c.name as carrier,
          string_agg(DISTINCT o."primaryRefValue",', ') as "orders",
          string_agg(DISTINCT cu."name",', ') as "customers"
        FROM orders o
        INNER JOIN "customers" cu ON cu.id = o."customerId"
        LEFT JOIN "orderItems" oi ON oi."orderId" = o.id
        LEFT JOIN "items" i ON i.id = oi."itemId"
        INNER JOIN "appointmentOrders" ao ON ao."orderId" = o.id
        INNER JOIN appointments a ON a.id = ao."appointmentId"
        LEFT JOIN carriers c ON c.id = a."carrierId"
        INNER JOIN doors d ON d.id = a."doorId"
        INNER JOIN areas area ON area.id = d."areaId"
        INNER JOIN buildings bu ON bu.id = area."buildingId"
        WHERE a."date" between current_date - interval '1 day' and current_date - interval '1 second' and a."deletedAt" IS NULL and ao."deletedAt" IS NULL
        GROUP BY bu.name, a.date, a."checkInTime", a.destination, c.name, bu.timezone
        `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  lastWeekAppointments: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT
          bu.name as warehouse,
          to_char(a.date, 'YYYY-MM-DD') AS "date",
          to_char(a.date, 'HH24:MI') AS "time",
          COUNT(DISTINCT i."sku") AS "skus",
          COALESCE(SUM(oi.quantity),0) AS "cases",
          COALESCE(SUM(oi.weight),0) AS "weight",
          to_char(a."checkInTime", 'YYYY-MM-DD HH24:MI') AS "checkInTime",
          a.destination AS "destination",
          c.name as carrier,
          string_agg(DISTINCT o."primaryRefValue",', ') as "orders",
          string_agg(DISTINCT cu."name",', ') as "customers"
        FROM orders o
        INNER JOIN "customers" cu ON cu.id = o."customerId"
        LEFT JOIN "orderItems" oi ON oi."orderId" = o.id
        LEFT JOIN "items" i ON i.id = oi."itemId"
        INNER JOIN "appointmentOrders" ao ON ao."orderId" = o.id
        INNER JOIN appointments a ON a.id = ao."appointmentId"
        LEFT JOIN carriers c ON c.id = a."carrierId"
        INNER JOIN doors d ON d.id = a."doorId"
        INNER JOIN areas area ON area.id = d."areaId"
        INNER JOIN buildings bu ON bu.id = area."buildingId"
        WHERE a."date" between date_trunc('week', current_date) - interval '1 week' and date_trunc('week', current_date) and a."deletedAt" IS NULL and ao."deletedAt" IS NULL
        GROUP BY bu.name, a.date, a."checkInTime", a.destination, c.name, bu.timezone
       `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  summaryCustomerYesterday: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT
           string_agg(DISTINCT cu."name",', ') AS "customer",
           count(distinct ao."appointmentId") AS "trucks",
           SUM(oi.quantity) AS "totalCases",
           COUNT(DISTINCT ao."orderId") AS "totalOrders",
           0 AS "averageLoadTime",
           0 AS "noShows",
           0 AS "overTwoHoursLoad",
           0 AS "overFourHoursLoad"
        FROM orders o
        INNER JOIN "customers" cu ON cu.id = o."customerId"
        INNER JOIN "orderItems" oi ON oi."orderId" = o.id
        INNER JOIN "items" i ON i.id = oi."itemId"
        INNER JOIN "appointmentOrders" ao ON ao."orderId" = o.id
        INNER JOIN appointments a ON a.id = ao."appointmentId"
        INNER JOIN doors d ON d.id = a."doorId"
        INNER JOIN areas area ON area.id = d."areaId"
        INNER JOIN buildings bu ON bu.id = area."buildingId"
        WHERE o."deliveryDate" between current_date - interval '1 day' and current_date - interval '1 second'
        GROUP BY cu.id
        `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  summaryCustomerLastWeek: async (req, res, next) => {
    try {
      const entities = await sequelize.query(`
        SELECT
           string_agg(DISTINCT cu."name",', ') AS "customer",
           count(distinct ao."appointmentId") AS "trucks",
           SUM(oi.quantity) AS "totalCases",
           COUNT(DISTINCT ao."orderId") AS "totalOrders",
           0 AS "averageLoadTime",
           0 AS "noShows",
           0 AS "overTwoHoursLoad",
           0 AS "overFourHoursLoad"
        FROM orders o
        INNER JOIN "customers" cu ON cu.id = o."customerId"
        INNER JOIN "orderItems" oi ON oi."orderId" = o.id
        INNER JOIN "items" i ON i.id = oi."itemId"
        INNER JOIN "appointmentOrders" ao ON ao."orderId" = o.id
        INNER JOIN appointments a ON a.id = ao."appointmentId"
        INNER JOIN doors d ON d.id = a."doorId"
        INNER JOIN areas area ON area.id = d."areaId"
        INNER JOIN buildings bu ON bu.id = area."buildingId"
        WHERE o."deliveryDate" between current_date - interval '1 week' and current_date - interval '1 second'
        GROUP BY cu.id
        `, { type: sequelize.QueryTypes.SELECT })
      res.send(entities)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  getWarnings: async (req, res, next) => {
    try {
      let { timezone } = req.query
      const { doors } = req.query
      if (!timezone) timezone = 'UTC'

      const [
        appointmentYesterdayCount,
        appointmentTodayCount,
        appointmentNext1Count,
        appointmentNext2Count,
        appointmentNext3Count
      ] = await Promise.all([
        sequelize.query(`
          SELECT
          (SELECT COUNT(id) FROM "carrierRequests" cr
            WHERE status = 'pending' AND cr.date::date = (current_date - interval '1 day')) AS pendingCarrierRequestCount,
          (SELECT COUNT(id) FROM appointments a
            WHERE ((a.date) AT TIME ZONE '${timezone}')::date = (current_date - interval '1 day') AND a."deletedAt" IS NULL AND a."appointmentStatusId" = 2 AND a."doorId" IN (${doors})) AS lateApptCount,
          (SELECT COUNT(id) FROM appointments a
            WHERE ((a.date) AT TIME ZONE '${timezone}')::date = (current_date - interval '1 day') AND a."deletedAt" IS NULL AND a."inventoryReviewUserId" IS NULL AND a."inventoryIssues" IS NOT NULL AND a."doorId" IN (${doors})) AS allowIssuesCount`,
        { type: sequelize.QueryTypes.SELECT }),
        sequelize.query(`
          SELECT
          (SELECT COUNT(id) FROM "carrierRequests" cr
            WHERE status = 'pending' AND cr.date::date = current_date) AS pendingCarrierRequestCount,
          (SELECT COUNT(id) FROM appointments a
            WHERE ((a.date) AT TIME ZONE '${timezone}')::date = current_date AND current_time > "time"(a."date") AT TIME ZONE '${timezone}' AND a."deletedAt" IS NULL AND a."appointmentStatusId" = 2 AND a."doorId" IN (${doors})) AS lateApptCount,
          (SELECT COUNT(id) FROM appointments a
            WHERE ((a.date) AT TIME ZONE '${timezone}')::date = current_date AND a."deletedAt" IS NULL AND a."inventoryReviewUserId" IS NULL AND a."inventoryIssues" IS NOT NULL AND a."doorId" IN (${doors})) AS allowIssuesCount`,
        { type: sequelize.QueryTypes.SELECT }),
        sequelize.query(`
          SELECT
          (SELECT COUNT(id) FROM "carrierRequests" cr
            WHERE status = 'pending' AND cr.date::date = (current_date + interval '1 day')) AS pendingCarrierRequestCount,
          (SELECT 0) AS lateApptCount,
          (SELECT COUNT(id) FROM appointments a
            WHERE ((a.date) AT TIME ZONE '${timezone}')::date = (current_date + interval '1 day') AND a."deletedAt" IS NULL AND a."inventoryReviewUserId" IS NULL AND a."inventoryIssues" IS NOT NULL AND a."doorId" IN (${doors})) AS allowIssuesCount`,
        { type: sequelize.QueryTypes.SELECT }),
        sequelize.query(`
          SELECT
          (SELECT COUNT(id) FROM "carrierRequests" cr
            WHERE status = 'pending' AND cr.date::date = (current_date + interval '2 day')) AS pendingCarrierRequestCount,
          (SELECT 0) AS lateApptCount,
          (SELECT COUNT(id) FROM appointments a
            WHERE ((a.date) AT TIME ZONE '${timezone}')::date = (current_date + interval '2 day') AND a."deletedAt" IS NULL AND a."inventoryReviewUserId" IS NULL AND a."inventoryIssues" IS NOT NULL AND a."doorId" IN (${doors})) AS allowIssuesCount`,
        { type: sequelize.QueryTypes.SELECT }),
        sequelize.query(`
          SELECT
          (SELECT COUNT(id) FROM "carrierRequests" cr
            WHERE status = 'pending' AND cr.date::date = (current_date + interval '3 day')) AS pendingCarrierRequestCount,
          (SELECT 0) AS lateApptCount,
          (SELECT COUNT(id) FROM appointments a
            WHERE ((a.date) AT TIME ZONE '${timezone}')::date = (current_date + interval '3 day') AND a."deletedAt" IS NULL AND a."inventoryReviewUserId" IS NULL AND a."inventoryIssues" IS NOT NULL AND a."doorId" IN (${doors})) AS allowIssuesCount`,
        { type: sequelize.QueryTypes.SELECT })
      ])

      res.send({
        appointmentYesterdayCount: parseInt((appointmentYesterdayCount && appointmentYesterdayCount.length) && Object.values(appointmentYesterdayCount[0]).reduce((a, b) => parseInt(a) + parseInt(b), 0)),
        appointmentTodayCount: parseInt((appointmentTodayCount && appointmentTodayCount.length) && Object.values(appointmentTodayCount[0]).reduce((a, b) => parseInt(a) + parseInt(b), 0)),
        appointmentNext1Count: parseInt((appointmentNext1Count && appointmentNext1Count.length) && Object.values(appointmentNext1Count[0]).reduce((a, b) => parseInt(a) + parseInt(b), 0)),
        appointmentNext2Count: parseInt((appointmentNext2Count && appointmentNext2Count.length) && Object.values(appointmentNext2Count[0]).reduce((a, b) => parseInt(a) + parseInt(b), 0)),
        appointmentNext3Count: parseInt((appointmentNext3Count && appointmentNext3Count.length) && Object.values(appointmentNext3Count[0]).reduce((a, b) => parseInt(a) + parseInt(b), 0))
      })
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  }
}
