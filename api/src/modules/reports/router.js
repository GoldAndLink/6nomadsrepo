const controller = require('./reports.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/reports'

router.get(
  '/summary',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.summary })
)

router.get(
  '/summaryYesterday',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.summaryYesterday })
)

router.get(
  '/todayAppointmentsSummary',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.todayAppointmentsSummary })
)

router.get(
  '/todayAppointments',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.todayAppointments })
)

router.get(
  '/yesterdayAppointments',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.yesterdayAppointments })
)

router.get(
  '/lastWeekAppointments',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.lastWeekAppointments })
)

router.get(
  '/summaryCustomerYesterday',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.summaryCustomerYesterday })
)

router.get(
  '/summaryCustomerLastWeek',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.summaryCustomerLastWeek })
)

router.get(
  '/warnings',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getWarnings })
)

module.exports = {
  router,
  baseUrl
}
