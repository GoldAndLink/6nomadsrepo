const controller = require('./doorDurations.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/doorDurations'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getDoors })
)

module.exports = {
  router,
  baseUrl
}
