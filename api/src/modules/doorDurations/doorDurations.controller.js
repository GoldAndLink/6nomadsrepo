const DoorDuration = require('../../models').doorDuration

module.exports = {
  getDoors: async (req, res, next) => {
    const allowedParams = ['doorId']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    try {
      let entities = await DoorDuration.findAll({
        where: qualifier,
        order: [
          ['quantityFrom', 'desc']
        ]
      })
      if (!entities || !entities.length) {
        entities = await DoorDuration.findAll({
          where: { doorId: null },
          order: [
            ['quantityFrom', 'desc']
          ]
        })
      }
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
