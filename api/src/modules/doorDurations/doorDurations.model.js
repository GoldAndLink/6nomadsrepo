module.exports = (sequelize, DataTypes) => {
  const DoorDuration = sequelize.define(
    'doorDuration',
    {
      doorId: DataTypes.INTEGER,
      quantityFrom: DataTypes.INTEGER,
      duration: DataTypes.INTEGER
    },
    {}
  )
  DoorDuration.associate = function (models) {
    DoorDuration.belongsTo(models.door)
  }
  return DoorDuration
}
