module.exports = function initModules (app) {
  const API_MODE = process.env.API_MODE || 'default'
  const modules = {
    'default': [
      'sms',
      'users',
      'orders',
      'appointments',
      'appointmentOrders',
      'roles',
      'accounts',
      'sites',
      'locations',
      'appointmentStatuses',
      'carriers',
      'buildings',
      'drivers',
      'customers',
      'orderStatuses',
      'areas',
      'doors',
      'items',
      'orderItems',
      'inventoryItems',
      'config',
      'doorDurations',
      'feeds',
      'carrierRequests',
      'carrierRequestOrders',
      'carrierRequestTimeRanges',
      'email',
      'open',
      'reports',
      'schedules',
      'doorSchedules'
    ],
    ingest: [
      'orders',
      'inventoryItems',
      'open'
    ]
  }

  console.info(`API running in [${API_MODE}] mode`)

  for (const moduleName of modules[API_MODE]) {
    process.env.NODE_ENV !== 'test' && console.info(`loading [${moduleName}] module`)
    const module = require(`${__dirname}/${moduleName}/router`)
    app.use(module.baseUrl, module.router)
  }
}
