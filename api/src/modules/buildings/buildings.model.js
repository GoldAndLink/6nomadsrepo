module.exports = (sequelize, DataTypes) => {
  const Building = sequelize.define(
    'building',
    {
      name: DataTypes.STRING,
      locationId: DataTypes.INTEGER,
      siteId: DataTypes.INTEGER,
      timezone: DataTypes.STRING
    },
    {}
  )
  Building.associate = function (models) {
    Building.belongsTo(models.location)
    Building.belongsTo(models.site)
    Building.hasMany(models.area)
  }
  return Building
}
