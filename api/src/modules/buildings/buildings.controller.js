const Door = require('../../models').door
const Area = require('../../models').area
const Building = require('../../models').building

module.exports = {
  getBuildings: async (req, res, next) => {
    const allowedParams = ['id', 'name', 'locationId', 'siteId']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    try {
      const entities = await Building.findAll({ where: qualifier })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  },

  getDoorsForBuilding: async (req, res, next) => {
    try {
      let doors = []
      const areas = await Area.findAll({
        where: { buildingId: req.params.id },
        include: [{ model: Door }]
      })

      areas.forEach(area => {
        doors = doors.concat(area.doors)
      })

      res.send(doors)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },

  getAreasForBuilding: async (req, res, next) => {
    try {
      const areas = await Area.findAll({
        where: { buildingId: req.params.id }
      })

      res.send(areas)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
