const controller = require('./buildings.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/buildings'

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getBuildings })
)

router.get(
  '/:id/doors',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getDoorsForBuilding })
)

router.get(
  '/:id/areas',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAreasForBuilding })
)

module.exports = {
  router,
  baseUrl
}
