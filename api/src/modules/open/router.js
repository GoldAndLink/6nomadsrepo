const controller = require('./open.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/open'

const controllers = {
  orders: require('../orders/orders.controller'),
  carrierRequestTimeRanges: require('../carrierRequestTimeRanges/carrierRequestTimeRanges.controller'),
  inventoryItems: require('../inventoryItems/inventoryItems.controller'),
  drivers: require('../drivers/drivers.controller'),
  carriers: require('../carriers/carriers.controller'),
  carrierRequests: require('../carrierRequests/carrierRequests.controller'),
  users: require('../users/users.controller'),
  doors: require('../doors/doors.controller'),
  feeds: require('../feeds/feeds.controller')
}

router.get(
  '/check',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controller.checkKey })
)

router.get(
  '/orders',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.orders.getOrders })
)

router.get(
  '/orders/:id',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.orders.getOrder })
)

router.post(
  '/orders/',
  passport.authenticate('headerapikey', { session: false }),
  controllers.orders.validateCreateOrder,
  version({ '1.0.0': controllers.orders.createOrder })
)

router.put(
  '/orders/',
  passport.authenticate('headerapikey', { session: false }),
  controllers.orders.validateUpdateOrder,
  version({ '1.0.0': controllers.orders.updateOrder })
)

router.delete(
  '/orders/',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.orders.deleteOrder })
)

router.get(
  '/carrierRequestTimeRanges',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.carrierRequestTimeRanges.getCarrierRequestTimeRanges })
)

router.put(
  '/inventoryItems/',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.inventoryItems.putItems })
)

router.get(
  '/carriers',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.carriers.getAllCarriers })
)

router.get(
  '/drivers',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.drivers.getAllDrivers })
)

router.put(
  '/carrierRequests/:id',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.carrierRequests.updateCarrierRequest })
)

router.put(
  '/carrierRequests',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.carrierRequests.updateCarrierRequest })
)

router.get(
  '/carrierRequests',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.carrierRequests.getCarrierRequests })
)

router.post(
  '/carrierRequests',
  passport.authenticate('headerapikey', { session: false }),
  controllers.carrierRequests.validateCreateCarrierRequest,
  version({ '1.0.0': controllers.carrierRequests.createCarrierRequest })
)

router.get(
  '/users/:id',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.users.getUser })
)

router.get(
  '/doors',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.doors.getDoors })
)

router.get(
  '/bootstrap',
  passport.authenticate('headerapikey', { session: false }),
  version({ '1.0.0': controllers.feeds.bootstrap })
)

module.exports = {
  router,
  baseUrl
}
