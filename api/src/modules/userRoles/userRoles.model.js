module.exports = (sequelize, DataTypes) => {
  const UserRole = sequelize.define(
    'userRole',
    {
      userId: DataTypes.INTEGER,
      roleId: DataTypes.INTEGER
    },
    {}
  )
  UserRole.associate = function (models) {}
  return UserRole
}
