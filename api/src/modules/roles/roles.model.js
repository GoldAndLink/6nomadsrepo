module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define(
    'role',
    {
      name: DataTypes.STRING
    },
    {}
  )
  Role.associate = function (models) {
    Role.belongsToMany(models.user, {
      through: { model: models.userRole }
    })
  }
  return Role
}
