const controller = require('./roles.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const authorize = require('../../../lib/authorize').authorize
const baseUrl = '/roles'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  authorize(['admin']),
  version({ '1.0.0': controller.getAllRoles })
)

module.exports = {
  router,
  baseUrl
}
