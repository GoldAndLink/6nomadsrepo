const Role = require('../../models').role

module.exports = {
  getAllRoles: async (req, res, next) => {
    try {
      const roles = await Role.findAll()

      res.send(roles)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
