const { check, validationResult } = require('express-validator/check')
const InventoryItem = require('../../models').inventoryItem
const Location = require('../../models').location
const sequelize = require('../../models').sequelize
const op = require('sequelize').Op
const moment = require('moment')
const builder = require('../../utils/builder')
const projector = require('../../utils/projector')
const NS_PER_SEC = 1e9
const steps = {
  SANITY: true,
  VALIDATION: false
}

module.exports = {
  getItems: async (req, res, next) => {
    const allowedParams = ['id', 'source', 'isOnHand', 'sku', 'reportTime', 'batchDate']
    const allowedSorters = ['id', 'isOnHand', 'reportTime', 'batchDate']

    try {
      const entities = await InventoryItem.findAll({
        where: builder.qualifier(req, allowedParams),
        order: builder.sorter(req, allowedSorters)
      })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  putItems: async (req, res, next) => {
    const time = process.hrtime()
    const log = []
    let diff = process.hrtime(time)
    log.push(`init @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)
    const allowedFields = ['reportTime', 'accountId', 'shippingLocationCode', 'isOnHand', 'availabilityReadyTime', 'sku', 'name', 'quantity', 'quantityUOM', 'batchDate']
    const requiredFields = ['reportTime', 'accountId', 'shippingLocationCode', 'isOnHand', 'availabilityReadyTime', 'sku', 'name', 'quantity', 'quantityUOM', 'batchDate'].sort()

    if (steps.SANITY) {
      // bounce aggressively
      if (!req.body || !Array.isArray(req.body) || !req.body.length) {
        return res.status(422).send({ status: 'ERROR: Incorrect input.' })
      }

      diff = process.hrtime(time)
      log.push(`sanity @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)
    }

    if (steps.VALIDATION) {
      // functional validation by parameters step 1 - enforce type and required params
      let incorrectEntry = req.body.find(entry => {
        return !(typeof entry === 'object' && requiredFields.every(el => entry[el] !== undefined))
      })

      if (incorrectEntry) {
        return res.status(422).send({ status: 'ERROR: Unexpected entry. Required fields not present. Refer to the documentation. No data has been modified.', incorrectEntry })
      }

      // functional validation by parameters step 2 - enforce allowed params
      incorrectEntry = req.body.find(entry => {
        return !(typeof entry === 'object' && Object.keys(entry).every(el => allowedFields.includes(el)))
      })
      if (incorrectEntry) {
        return res.status(422).send({ status: 'ERROR: Unexpected entry. Disallowed fields present. Refer to the documentation. No data has been modified.', incorrectEntry })
      }

      diff = process.hrtime(time)
      log.push(`validation @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)
    }

    // pull and determine the incoming timezone
    const location = await Location.findOne({
      where: {
        code: req.body[0].shippingLocationCode
      }
    })

    if (!location) {
      return res.status(409).send('location missing for first data row')
    }

    // structure deemed safe-ish to proceed
    // transform
    req.body = req.body.map(entry => ({
      ...entry,
      reportTime: moment.tz(entry.reportTime, location.timezoneName).utc().toDate(),
      availabilityReadyTime: moment.tz(entry.availabilityReadyTime, location.timezoneName).utc().toDate(),
      batchDate: moment.tz(entry.batchDate, location.timezoneName).utc().toDate()
    }))

    const dataset = []

    for (let i = 0, cap = req.body.length; i < cap; i++) {
      const entry = req.body[i]
      let datarow = dataset.find(row => row.isOnHand === entry.isOnHand && row.shippingLocationCode === entry.shippingLocationCode)
      if (!datarow) {
        datarow = {
          isOnHand: entry.isOnHand,
          shippingLocationCode: entry.shippingLocationCode,
          data: []
        }
        dataset.push(datarow)
      }
      datarow.data.push(entry)
    }

    diff = process.hrtime(time)
    log.push(`transformation @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)

    // delete MATCHING onHand values (remove old records to overwrite)
    const destroyer1 = dataset.map(datarow => ({
      isOnHand: datarow.isOnHand,
      shippingLocationCode: datarow.shippingLocationCode
    }))

    // delete SCHEDULES older than received ONHAND
    const destroyer2 = dataset.filter(datarow => datarow.isOnHand).map(datarow => ({
      isOnHand: false,
      shippingLocationCode: datarow.shippingLocationCode,
      availabilityReadyTime: { [op.lt]: moment(datarow.availabilityReadyTime) }
    }))

    diff = process.hrtime(time)
    log.push(`transaction prep @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)

    const transaction = await sequelize.transaction()

    try {
      diff = process.hrtime(time)
      log.push(`transaction open @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)

      await InventoryItem.destroy({
        where: {
          [op.or]: destroyer1
        }
      }, { transaction })

      diff = process.hrtime(time)
      log.push(`transaction step 1 @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)

      await InventoryItem.destroy({
        where: {
          [op.or]: destroyer2
        }
      }, { transaction })

      diff = process.hrtime(time)
      log.push(`transaction step 2 @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)

      await InventoryItem.bulkCreate(req.body, { transaction })

      diff = process.hrtime(time)
      log.push(`transaction step 3 @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)

      await transaction.commit()

      diff = process.hrtime(time)
      log.push(`transaction committed @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)
    } catch (e) {
      await transaction.rollback()
      console.error(e)
      return res.status(500).send({ status: 'ERROR: An error occured while loading data.', e })
    }

    diff = process.hrtime(time)
    log.push(`transaction cleared @ ${(diff[0] * NS_PER_SEC + diff[1]) / 1000000}ms`)
    return res.send(log)
  },
  getProjection: async (req, res, next) => {
    if (typeof req.query.sku === 'string') {
      req.query.sku = [req.query.sku]
    }

    const projection = await projector.getProjection(req.query.sku, req.query.locationCode)
    res.send(projection)
  },
  validateGetProjection: [
    check('locationCode').not().isEmpty(),
    check('sku').not().isEmpty(),
    (req, res, next) => {
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      next()
    }
  ]
}
