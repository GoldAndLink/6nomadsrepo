const controller = require('./inventoryItems.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/inventoryItems'

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getItems })
)

router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.putItems })
)

router.get(
  '/projection',
  passport.authenticate('jwt', { session: false }),
  controller.validateGetProjection,
  version({ '1.0.0': controller.getProjection })
)

module.exports = {
  router,
  baseUrl
}
