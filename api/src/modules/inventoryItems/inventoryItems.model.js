'use strict'
module.exports = (sequelize, DataTypes) => {
  const InventoryItem = sequelize.define('inventoryItem', {
    source: DataTypes.STRING,
    reportTime: DataTypes.DATE,
    accountId: DataTypes.STRING,
    shippingLocationCode: DataTypes.STRING,
    isOnHand: DataTypes.BOOLEAN,
    availabilityReadyTime: DataTypes.DATE,
    sku: DataTypes.STRING,
    name: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    quantityUOM: DataTypes.STRING,
    batchDate: DataTypes.DATE
  }, {})
  InventoryItem.associate = function (models) {
  }
  return InventoryItem
}
