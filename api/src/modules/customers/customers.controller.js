const Customer = require('../../models').customer
const op = require('sequelize').Op

module.exports = {
  getCustomers: async (req, res, next) => {
    try {
      const { name } = req.query

      const where = {}
      if (name) {
        where.name = {
          [op.iLike]: `%${name}%`
        }
      }

      const customers = await Customer.findAll({ where })

      res.send(customers)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
