module.exports = (sequelize, DataTypes) => {
  const Customer = sequelize.define(
    'customer',
    {
      name: DataTypes.STRING
    },
    {}
  )
  Customer.associate = function (models) {
    Customer.hasMany(models.order)
  }
  return Customer
}
