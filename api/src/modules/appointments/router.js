const controller = require('./appointments.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')

const baseUrl = '/appointments'

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  controller.sanitizeFilterAppointmentParams,
  version({ '1.0.0': controller.getAppointments })
)

router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAppointment })
)

router.get(
  '/:id/orders',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAppointmentOrders })
)

router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  controller.validateCreateAppointment,
  version({ '1.0.0': controller.createAppointment })
)

router.delete(
  '/:id/orders/:orderId',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.removeOrderFromAppointment })
)

router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.deleteAppointment })
)

router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  controller.validateUpdateAppointment,
  version({ '1.0.0': controller.updateAppointment })
)

router.post(
  '/:id/clear-request',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.clearRequest })
)

module.exports = {
  router,
  baseUrl
}
