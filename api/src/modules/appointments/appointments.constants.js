module.exports = {
  STATUS: {
    DRAFT: 1,
    SCHEDULED: 2,
    CHECKED_IN: 3,
    LOADING: 4,
    CHECKED_OUT: 5
  }
}
