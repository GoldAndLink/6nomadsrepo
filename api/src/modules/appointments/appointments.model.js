const moment = require('moment')
const op = require('sequelize').Op

module.exports = (sequelize, DataTypes) => {
  const Appointment = sequelize.define(
    'appointment',
    {
      guid: DataTypes.UUID,
      carrierId: DataTypes.INTEGER,
      appointmentStatusId: DataTypes.INTEGER,
      doorId: DataTypes.INTEGER,
      driverId: DataTypes.INTEGER,
      date: DataTypes.DATE,
      duration: {
        type: DataTypes.INTEGER,
        defaultValue: 60
      },
      trailer: DataTypes.STRING,
      notes: DataTypes.STRING,
      contactPhone: DataTypes.STRING,
      deletedAt: DataTypes.DATE,
      destination: DataTypes.STRING,
      tractor: DataTypes.STRING,
      inProgress: DataTypes.BOOLEAN,
      checkInTime: DataTypes.DATE,
      startLoadingTime: DataTypes.DATE,
      inventoryChecked: DataTypes.DATE,
      inventoryIssues: DataTypes.JSONB,
      inventoryReviewUserId: DataTypes.INTEGER,
      checkoutTime: DataTypes.DATE
    },
    {
      paranoid: true
    }
  )

  Appointment.associate = function (models) {
    Appointment.belongsTo(models.carrier)
    Appointment.belongsTo(models.appointmentStatus)
    Appointment.belongsTo(models.door)
    Appointment.belongsTo(models.driver)
    Appointment.hasMany(models.carrierRequest)
    Appointment.belongsToMany(models.order, {
      through: { model: models.appointmentOrder }
    })
  }

  Appointment.hasOverlappingAppointment = async (date, duration, doorId, id) => {
    if (!date) return false
    const initDateTime = moment(date)
    const endDateTime = moment(moment(date).add(duration, 'minutes'))

    const appts = await Appointment.findAll({
      where: {
        [op.and]: [
          { doorId: doorId },
          { date: { [op.gte]: initDateTime } },
          { date: { [op.lt]: endDateTime } }
        ]
      }
    })

    if (appts && appts.length > 0) {
      for (const appt of appts) {
        if (parseInt(appt.id) !== parseInt(id)) return appt.id
      }
    }
    return false
  }

  return Appointment
}
