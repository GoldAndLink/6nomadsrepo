const { sanitizeQuery } = require('express-validator/filter')
const { check, validationResult } = require('express-validator/check')
const _ = require('lodash')
const Appointment = require('../../models').appointment
const Door = require('../../models').door
const Area = require('../../models').area
const Building = require('../../models').building
const CarrierRequest = require('../../models').carrierRequest
const CarrierRequestOrder = require('../../models').carrierRequestOrder
const Order = require('../../models').order
const Customer = require('../../models').customer
const Driver = require('../../models').driver
const Carrier = require('../../models').carrier
const AppointmentOrder = require('../../models').appointmentOrder
const Location = require('../../models').location
const Item = require('../../models').item
const uuidv4 = require('uuid/v4')
const op = require('sequelize').Op
const moment = require('moment')
const socket = require('../socket')
const constants = require('./appointments.constants')
const mapper = require('../../utils/mapper')
const redwoodConnect = require('../../utils/redwoodConnect')
const projector = require('../../utils/projector')
const sms = require('../sms/utils')
const email = require('../email/utils')
const carrierRequestController = require('../carrierRequests/carrierRequests.controller')

module.exports = {
  getAppointments: async (req, res, next) => {
    const allowedParams = [
      'createdAt',
      'id',
      'date',
      'carrierId',
      'driverId',
      'doorId',
      'appointmentStatusId',
      'inProgress'
    ]

    const qualifier = {}

    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }

    if (req.query.dateFrom && req.query.dateTo) {
      qualifier.date = {
        [op.gte]: moment.utc(`${req.query.dateFrom} ${req.query.timeFrom || '00:00:00'}`).format(),
        [op.lte]: moment.utc(`${req.query.dateTo} ${req.query.timeTo || '23:59:59'}`).format()
      }
    }

    const orderWhere = {}
    if (req.query.customerId || req.query.PO || req.query.primaryRefValue || req.query.destinationId) {
      const { customerId, PO, primaryRefValue, destinationId } = req.query
      if (customerId) orderWhere.customerId = customerId
      if (destinationId) orderWhere.destinationId = destinationId
      if (PO) orderWhere.data = { [op.contains]: { otherRefs: [{ val: PO }] } }
      if (primaryRefValue) orderWhere.primaryRefValue = { [op.iLike]: `%${primaryRefValue.replace(/(_|%|\\)/g, '\\$1')}%` }
    }

    try {
      // TODO remove include once UpsertAppointmentForm.js is normalized
      const entities = await Appointment.findAll({
        order: [['date', 'ASC']],
        where: qualifier,
        include: [
          {
            model: Order,
            required: orderWhere !== {},
            include: [
              {
                model: Customer
              },
              {
                model: Item
              },
              {
                model: Location,
                as: 'destination'
              },
              {
                model: Location,
                as: 'origin'
              }
            ],
            where: orderWhere
          },
          {
            model: CarrierRequest
          }
        ]
      })

      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  getAppointment: async (req, res, next) => {
    const options = {
      include: [
        { model: Driver },
        { model: Order, include: [{ model: Item }, { model: Customer }, { model: Location, as: 'destination' }, { model: Location, as: 'origin' }] },
        { model: CarrierRequest }
      ],
      order: [['createdAt', 'DESC']],
      where: { id: req.params.id }
    }
    const appointment = await Appointment.findOne(options)

    if (!appointment) return res.status(404).json({ error: 'No appointment found!' })
    res.json({ appointment })
  },
  removeOrderFromAppointment: async (req, res, next) => {
    const { id, orderId } = req.params

    const order = await Order.findOne({ where: { id: orderId } })
    if (!order) return res.sendStatus(404)
    order.orderStatusId = 1
    socket.orders.broadcastOne(order)
    await order.save()

    await AppointmentOrder.destroy({
      where: {
        appointmentId: id,
        orderId: orderId
      }
    })
    res.send({ message: 'Order is deleted from appointment' })
  },
  createAppointment: async (req, res, next) => {
    const errors = validationResult(req)
    let carrier = null
    let c
    let d

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const overlapping = await Appointment.hasOverlappingAppointment(req.body.date, req.body.duration, req.body.doorId)
      if (overlapping) {
        return res.status(409).send({
          error: `Another appointment (#${overlapping}) is already set on this time frame.`,
          debug: {
            date: req.body.date,
            duration: req.body.duration,
            doorId: req.body.doorId
          }
        })
      }

      // check if all orders have a Open status
      let orders = await Order.findAll({
        where: {
          id: req.body.orderIds,
          orderStatusId: { [op.ne]: 2 }
        },
        include: [
          { model: Item },
          { model: Customer },
          { model: Location, as: 'destination' },
          { model: Location, as: 'origin' }
        ]
      })

      for (const order of orders) {
        if (order.orderStatusId && order.orderStatusId !== 1) {
          return res.status(412).send({
            error: `The order with PO: ${order.primaryRefValue} is not open`
          })
        }
      }

      if (!req.body.carrierId && req.body.carrierName) {
        const newCarrier = new Carrier({
          name: req.body.carrierName
        })
        c = await newCarrier.save()
        carrier = newCarrier
        req.body.carrierId = c.id
      } else {
        c = await Carrier.findOne({
          where: {
            id: req.body.carrierId
          }
        })
      }

      if (!req.body.driverId && req.body.driverName) {
        const name = req.body.driverName.split(' ')
        const driver = new Driver({
          firstName: name[0],
          lastName: name[1],
          carrierId: req.body.carrierId,
          email: req.body.email,
          phone: req.body.phone
        })
        d = await driver.save()
        req.body.driverId = d.id
      } else {
        d = await Driver.findOne({
          where: {
            id: req.body.driverId
          }
        })
      }

      const appointment = await Appointment.create({
        guid: uuidv4(),
        carrierId: req.body.carrierId,
        appointmentStatusId: req.body.appointmentStatusId,
        doorId: req.body.doorId,
        driverId: req.body.driverId,
        date: req.body.date,
        duration: req.body.duration,
        trailer: req.body.trailer,
        tractor: req.body.tractor,
        notes: req.body.notes,
        contactPhone: req.body.contactPhone,
        destination: req.body.destination,
        inProgress: !!req.body.inProgress
      })

      for (const order of orders) {
        order.orderStatusId = 2
        socket.orders.broadcastOne(order)
        await order.save()
      }

      let crequests = null

      if (req.body.carrierRequestId) {
        let destination = ''
        let ordersPoNumbers = []
        let carrierRequestOrders = []

        crequests = await CarrierRequest.update(
          { appointmentId: appointment.id,
            status: 'scheduled'
          },
          {
            where: { id: req.body.carrierRequestId },
            returning: true
          }
        )

        if (!carrier) {
          carrier = await Carrier.findOne({
            where: {
              id: req.body.carrierId
            }
          })
        }
        const { dataValues = {} } = crequests[1][0]

        // send email/sms
        const carrierRequestSMSTemplate = {
          APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION: 4
        }
        const carrierRequestEmailTemplate = {
          APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION: 3
        }

        carrierRequestOrders = await CarrierRequestOrder.findAll({
          where: {
            carrierRequestId: req.body.carrierRequestId
          }
        })
        const destinationId = orders && !!orders.length ? orders[0].destinationId : null
        if (destinationId) destination = await Location.findOne({ where: { id: destinationId } })
        ordersPoNumbers = carrierRequestOrders.map(carrierRequestOrder => carrierRequestOrder.poNumber)
        const primaryRefValues = orders.map(o => o.primaryRefValue)

        /* convert date/time to the local time */
        const door = await Door.findOne({
          where: { id: req.body.doorId },
          include: [
            {
              model: Area,
              required: true,
              include: [
                {
                  model: Building
                }
              ]
            }
          ]
        })

        const localDateTime = moment(req.body.date).tz(door.area.building.timezone)

        if (dataValues.prefContactMethod === 'sms') {
          await sms.send(dataValues.phone, carrierRequestSMSTemplate.APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION, {
            date: localDateTime.format('ddd, MMMM D, YYYY'),
            timeStart: `${localDateTime.format('HH:mm')} - ${localDateTime.add(req.body.duration, 'minutes').format('HH:mm')}`,
            customer: carrier.name,
            destination: destination.name,
            poList: _.flatten(ordersPoNumbers),
            soList: [...new Set(primaryRefValues)],
            tractor: req.body.tractor || dataValues.tractorNumber,
            trailer: req.body.trailer || dataValues.trailerLicense,
            linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
            linkLogin: ''
          })
        }

        // send email
        if (dataValues.prefContactMethod === 'email') {
          await email.send({
            htmlTemplate: 'basic',
            emailTemplateId: carrierRequestEmailTemplate.APPOINTMENT_REQUEST_SCHEDULED_CONFIRMATION,
            to: dataValues.email,
            subject: 'DO NOT REPLY: Appointment Request Scheduled',
            data: {
              date: localDateTime.format('ddd, MMMM D, YYYY'),
              timeStart: `${localDateTime.format('HH:mm')} - ${localDateTime.add(req.body.duration, 'minutes').format('HH:mm')}`,
              customer: carrier.name,
              destination: destination.name,
              poList: _.flatten(ordersPoNumbers),
              soList: [...new Set(primaryRefValues)],
              tractor: req.body.tractor || dataValues.tractorNumber,
              trailer: req.body.trailer || dataValues.trailerLicense,
              linkReschedule: `${process.env.BASE_URL}/carrier/appointmentRequests/${dataValues.guid || ''}`,
              linkLogin: ''
            }
          })
        }

        // socket
        const count = await carrierRequestController.countCarrierRequestsInStatusPending(req)
        crequests[1].forEach(crequests => {
          const rawResult = crequests.toJSON()
          rawResult.count = count
          rawResult.appointment = appointment
          socket.carrierRequests.broadcastOne(rawResult)
        })
      }

      await appointment.setOrders(orders)

      orders = orders || []
      if (!(orders instanceof Array)) orders = orders.toJSON()

      crequests = crequests || []
      if (!(crequests instanceof Array)) crequests = crequests.toJSON()

      const data = {
        ...appointment.toJSON(),
        orders: orders,
        carrierRequests: crequests.length > 1 ? crequests[1] : crequests
      }

      socket.appointments.broadcastOne(data)
      appointment.inventoryIssues = await projector.validateDownstreamAppointments(appointment)
      res.status(201).send({
        ...appointment.toJSON(),
        driver: d,
        carrier: c
      })

      // sync with redwood connect
      if (appointment.appointmentStatusId) {
        await redwoodConnect.createAppointment({ appointmentId: appointment.id, userId: req.user.id })
      }
    } catch (err) {
      console.log('ERROR:', err)
      res.status(500).send({ message: err })
    }
  },
  updateAppointment: async (req, res, next) => {
    const errors = validationResult(req)
    let c
    let d

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    if (!req.body.carrierId && req.body.carrierName) {
      const carrier = new Carrier({
        name: req.body.carrierName
      })
      c = await carrier.save()
      req.body.carrierId = c.id
    } else {
      c = await Carrier.findOne({
        where: {
          id: req.body.carrierId
        }
      })
    }

    if (!req.body.driverId && req.body.driverName) {
      const name = req.body.driverName.split(' ')
      const driver = new Driver({
        firstName: name[0],
        lastName: name[1],
        carrierId: req.body.carrierId,
        email: req.body.email,
        phone: req.body.phone
      })
      d = await driver.save()
      req.body.driverId = d.id
    } else {
      d = await Driver.findOne({
        where: {
          id: req.body.driverId
        }
      })
    }

    const appt = await Appointment.findOne({
      where: { id: req.params.id },
      include: [{ model: CarrierRequest }]
    })
    if (!appt) return res.sendStatus(404)

    const editables = ['carrierId', 'appointmentStatusId', 'doorId', 'driverId', 'date', 'duration', 'trailer',
      'tractor', 'notes', 'contactPhone', 'destination', 'inProgress', 'inventoryReviewUserId']

    try {
      const overlapping = await Appointment.hasOverlappingAppointment(req.body.date, req.body.duration, req.body.doorId, req.params.id)
      if (overlapping) return res.status(409).send({ error: 'Another appointment is already set on this time frame.' })

      const delta = {}
      for (const field of editables) {
        if (req.body[field]) {
          delta[field] = req.body[field]
        }
      }

      // force inProgress to be false when updating
      delta.inProgress = false

      if (req.body.inventoryReviewUserId === null) delta.inventoryReviewUserId = null

      if (req.body.appointmentStatusId && req.body.appointmentStatusId && +req.body.appointmentStatusId === constants.STATUS.CHECKED_IN) {
        delta.checkInTime = new Date()
      }
      if (req.body.appointmentStatusId && req.body.appointmentStatusId && +req.body.appointmentStatusId === constants.STATUS.LOADING) {
        delta.startLoadingTime = new Date()
      }
      if (req.body.appointmentStatusId && req.body.appointmentStatusId && +req.body.appointmentStatusId === constants.STATUS.CHECKED_OUT) {
        delta.checkoutTime = new Date()
      }

      if (!appt.guid) {
        delta.guid = uuidv4()
      }

      const previousData = await mapper.mapAppointmentToRC({ appointmentId: req.params.id, userId: req.user.id })
      const appointment = await Appointment.update(delta, {
        where: {
          id: req.params.id
        },
        returning: true,
        plain: true
      })

      const updatedAppointment = appointment[1]

      // return old orders if none is updated
      let orders = await updatedAppointment.getOrders({ include: [ { model: Item }, { model: Customer }, { model: Location, as: 'destination' }, { model: Location, as: 'origin' } ] })

      if (req.body.orderIds) {
        // get current orders we have for that appointment
        const currentOrders = orders

        // get the difference so we can change the status from the orders that are free now
        const diffOrders = currentOrders.filter(e => { return req.body.orderIds.indexOf(e) < 0 })

        // change the status for orders that are no longer in the appointment
        if (diffOrders) {
          for (const order of diffOrders) {
            order.orderStatusId = 1
            socket.orders.broadcastOne(order)
            await order.save()
          }
        }

        // set new orders to appointment
        orders = await Order.findAll({
          where: {
            id: req.body.orderIds,
            orderStatusId: { [op.ne]: 2 }
          },
          include: [
            { model: Item, required: false },
            { model: Customer },
            { model: Location, as: 'destination' },
            { model: Location, as: 'origin' }
          ]
        })

        await updatedAppointment.setOrders(orders.map(o => o.id))

        for (const order of orders) {
          order.orderStatusId = 2
          socket.orders.broadcastOne(order)
          await order.save()
        }
      }

      const carrierRequests = appt.carrierRequests
      let cr
      if (carrierRequests && carrierRequests.length) {
        cr = await CarrierRequest.update(
          { status: 'scheduled' },
          {
            where: { id: { [op.in]: carrierRequests.map(r => r.id) } },
            returning: true
          }
        )
        const count = await carrierRequestController.countCarrierRequestsInStatusPending(req)
        cr[1].forEach(cr => {
          const rawResult = cr.toJSON()
          rawResult.count = count
          rawResult.appointment = updatedAppointment
          socket.carrierRequests.broadcastOne(rawResult)
        })
        updatedAppointment.setCarrierRequests(cr[1])
      }

      orders = orders || []
      if (!(orders instanceof Array)) orders = orders.toJSON()

      cr = cr || []
      if (!(cr instanceof Array)) cr = cr.toJSON()

      updatedAppointment.inventoryIssues = await projector.validateDownstreamAppointments(updatedAppointment)

      const data = {
        ...updatedAppointment.toJSON(),
        orders: orders,
        carrierRequests: cr.length > 1 ? cr[1] : cr,
        driver: d,
        carrier: c
      }

      socket.appointments.broadcastOne(data)
      res.send({
        ...updatedAppointment.toJSON(),
        driver: d,
        carrier: c
      })

      // sync with redwood connect
      if (updatedAppointment.appointmentStatusId) {
        await redwoodConnect.updateAppointment({ appointmentId: updatedAppointment.id, userId: req.user.id, previousData: previousData.updated })
      }
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  deleteAppointment: async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const appointment = await Appointment.findOne({
        where: { id: +req.params.id },
        include: [{ model: CarrierRequest }]
      })
      if (!appointment) {
        return res.sendStatus(404)
      }

      const currentOrders = await appointment.getOrders()

      // delete relation in appointmentOrders
      for (const order of currentOrders) {
        order.orderStatusId = 1
        await order.save()
        await AppointmentOrder.destroy({
          where: {
            appointmentId: appointment.id,
            orderId: order.id
          }
        })

        socket.orders.broadcastOne(order)
      }

      if (appointment.carrierRequests && appointment.carrierRequests.length) {
        await CarrierRequestOrder.destroy({
          where: {
            carrierRequestId: { [op.in]: appointment.carrierRequests.map(r => r.id) }
          }
        })
        await CarrierRequest.destroy(
          {
            where: {
              id: { [op.in]: appointment.carrierRequests.map(r => r.id) }
            }
          }
        )

        const count = await carrierRequestController.countCarrierRequestsInStatusPending(req)
        appointment.carrierRequests.map(r => {
          socket.carrierRequests.broadcastOne(Object.assign(
            {},
            r.toJSON(),
            { deletedAt: moment().toISOString(), count }
          ))
        })
      }

      // sync with redwood connect
      if (appointment.appointmentStatusId) {
        await redwoodConnect.deleteAppointment({ appointmentId: +req.params.id, userId: req.user.id })
      }

      await projector.validateDownstreamAppointments(appointment)
      await Appointment.destroy({
        where: {
          id: parseInt(req.params.id)
        }
      })

      socket.appointments.broadcastOne(Object.assign(
        {},
        appointment.toJSON(),
        { deletedAt: moment().toISOString() }
      ))

      return res.send()
    } catch (err) {
      console.log('error:', err)
      res.status(500).send({ message: err })
    }
  },
  clearRequest: async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const appointment = await Appointment.findOne({
        where: { id: +req.params.id },
        include: [{ model: CarrierRequest }]
      })

      if (appointment.carrierRequests && appointment.carrierRequests.length) {
        const result = await CarrierRequest.update(
          { status: 'scheduled' },
          {
            where: {
              id: { [op.in]: appointment.carrierRequests.map(r => r.id) }
            },
            returning: true
          }
        )

        const updatedAppointment = await Appointment.findOne({
          where: { id: +req.params.id },
          include: [{ model: CarrierRequest }]
        })

        result[1].map(cr => ({ ...cr, appointment: updatedAppointment })).forEach(socket.carrierRequests.broadcastOne)
        socket.appointments.broadcastOne(updatedAppointment)
        return res.send(updatedAppointment)
      } else {
        return res.send(appointment)
      }
    } catch (err) {
      console.log('error:', err)
      res.status(500).send({ message: err })
    }
  },
  getAppointmentOrders: async (req, res, next) => {
    const result = await Order.findAll({
      include: [{
        model: Appointment,
        where: { id: req.params.id },
        include: [{ model: Item }, { model: Customer }, { model: Location, as: 'destination' }, { model: Location, as: 'origin' }]
      }]
    })
    res.json({ result })
  },
  sanitizeFilterAppointmentParams: [
    sanitizeQuery('createdAt').toDate(),
    (req, res, next) => {
      next()
    }
  ],
  validateCreateAppointment: [
    check('doorId').not().isEmpty(),
    check('date').not().isEmpty(),
    (req, res, next) => {
      next()
    }
  ],
  validateUpdateAppointment: [
    check('orderIds').optional().isArray().withMessage('must be an array'),
    (req, res, next) => {
      next()
    }
  ]
}
