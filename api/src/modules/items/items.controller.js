const Item = require('../../models').item

module.exports = {
  getItems: async (req, res, next) => {
    const allowedParams = ['id', 'guid', 'name', 'sku']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    try {
      const entities = await Item.findAll({ where: qualifier })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
