'use strict'
module.exports = (sequelize, DataTypes) => {
  const Item = sequelize.define('item', {
    guid: DataTypes.UUID,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    sku: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {})
  Item.associate = function (models) {
    Item.belongsTo(models.user, { foreignKey: 'createdBy' })
    Item.belongsTo(models.user, { foreignKey: 'updatedBy' })
    Item.belongsToMany(models.order, {
      through: { model: models.orderItem }
    })
  }
  return Item
}
