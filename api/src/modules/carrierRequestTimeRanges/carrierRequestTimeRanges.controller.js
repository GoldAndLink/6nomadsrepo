const CarrierRequestTimeRange = require('../../models').carrierRequestTimeRange
const builder = require('../../utils/builder')

module.exports = {
  getCarrierRequestTimeRanges: async (req, res, next) => {
    const allowedParams = ['name', 'startTime', 'endTime', 'active']
    const allowedSorters = ['startTime', 'endTime']

    try {
      const entities = await CarrierRequestTimeRange.findAll({
        where: builder.qualifier(req, allowedParams),
        order: builder.sorter(req, allowedSorters),
        include: []
      })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
