const controller = require('./carrierRequestTimeRanges.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/carrierRequestTimeRanges'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getCarrierRequestTimeRanges })
)

module.exports = {
  router,
  baseUrl
}
