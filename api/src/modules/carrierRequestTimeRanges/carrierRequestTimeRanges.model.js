module.exports = (sequelize, DataTypes) => {
  const CarrierRequestTimeRange = sequelize.define(
    'carrierRequestTimeRange',
    {
      name: DataTypes.STRING,
      startTime: DataTypes.DATE,
      endTime: DataTypes.DATE,
      active: DataTypes.BOOLEAN
    },
    {}
  )
  CarrierRequestTimeRange.associate = function (models) {
  }
  return CarrierRequestTimeRange
}
