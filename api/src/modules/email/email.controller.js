const email = require('./utils')
const { check, validationResult } = require('express-validator/check')

module.exports = {
  emailSend: async (req, res, next) => {
    const { htmlTemplate, emailTemplateId, data, to, subject } = req.body

    try {
      await email.send({ htmlTemplate, emailTemplateId, to, subject, data })
      return res.send()
    } catch (e) {
      console.log('error: ', e)
      return res.sendStatus(500)
    }
  },
  validateEmailSend: [
    check('htmlTemplate').isIn(['basic']),
    check('to')
      .not()
      .isEmpty(),
    check('subject')
      .not()
      .isEmpty(),
    check('data')
      .not()
      .isEmpty(),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      next()
    }
  ]
}
