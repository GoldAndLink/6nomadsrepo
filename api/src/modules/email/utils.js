const mustache = require('mustache')
const fs = require('fs')
const path = require('path')
const nodemailer = require('nodemailer')
const smtpTransport = require('nodemailer-smtp-transport')

const EmailTemplate = require('../../models').emailTemplate

const mailTransport = nodemailer.createTransport(smtpTransport({
  host: process.env.AWS_SMTP_HOST,
  port: process.env.AWS_SMTP_PORT,
  secure: true,
  auth: {
    user: process.env.AWS_SMTP_USERNAME,
    pass: process.env.AWS_SMTP_PASSWORD
  }
}))

module.exports = {
  send: async ({ htmlTemplate, emailTemplateId, to, subject, data }) => {
    let content = null
    if (emailTemplateId) {
      const emailTemplate = await EmailTemplate.findOne({ where: { id: emailTemplateId } })

      if (!emailTemplate) {
        throw new Error('Missing Email template')
      }

      content = mustache.render(emailTemplate.template, data)
    } else {
      content = data
    }

    const template = fs.readFileSync(
      path.join(__dirname, './templates/', htmlTemplate + '.html')
    )

    const html = template.toString().replace('{{content}}', content)

    mailTransport.sendMail({
      from: `deleted_name <${process.env.AWS_SMTP_FROM}>`,
      to,
      subject,
      html
    })
  }
}
