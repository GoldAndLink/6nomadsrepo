const controller = require('./email.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/email'

router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  controller.validateEmailSend,
  version({ '1.0.0': controller.emailSend })
)

module.exports = {
  router,
  baseUrl
}
