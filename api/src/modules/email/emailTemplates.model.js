module.exports = (sequelize, DataTypes) => {
  const EmailTemplate = sequelize.define(
    'emailTemplate',
    {
      template: DataTypes.TEXT
    },
    {}
  )
  return EmailTemplate
}
