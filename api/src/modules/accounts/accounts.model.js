module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define(
    'account',
    {
      name: DataTypes.STRING
    },
    {}
  )

  Account.associate = function (models) {
    Account.hasMany(models.userAccount)
    Account.belongsToMany(models.user, {
      through: { model: models.userAccount }
    })
  }

  return Account
}
