const Account = require('../../models').account

module.exports = {
  getAllAccounts: async (req, res, next) => {
    try {
      const accounts = await Account.findAll()

      res.send(accounts)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
