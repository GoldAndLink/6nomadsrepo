const controller = require('./accounts.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/accounts'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAllAccounts })
)

module.exports = {
  router,
  baseUrl
}
