const ctrl = require('./appointmentOrders.controller')
const mockRes = require('jest-mock-express').response

describe('appointmentOrders', () => {
  it('responds to GET', async done => {
    const res = mockRes()

    await ctrl.getAppointmentOrders(
      {
        query: {}
      },
      res
    )

    expect(res.send).toHaveReturned()
    expect(res.send).toHaveBeenCalled()
    expect(res.send).toHaveBeenCalledWith(expect.any(Array))
    done()
  })
})
