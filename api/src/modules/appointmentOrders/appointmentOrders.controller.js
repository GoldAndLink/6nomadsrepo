const AppointmentOrder = require('../../models').appointmentOrder

module.exports = {
  getAppointmentOrders: async (req, res, next) => {
    const allowedParams = ['id', 'appointmentId', 'orderId']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    try {
      const entities = await AppointmentOrder.findAll({ where: qualifier })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
