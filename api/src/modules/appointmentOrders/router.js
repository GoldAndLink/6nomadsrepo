const controller = require('./appointmentOrders.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/appointment_orders'

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAppointmentOrders })
)

module.exports = {
  router,
  baseUrl
}
