module.exports = (sequelize, DataTypes) => {
  const appointmentOrder = sequelize.define(
    'appointmentOrder',
    {
      appointmentId: DataTypes.INTEGER,
      orderId: DataTypes.INTEGER
    },
    {
      paranoid: true
    }
  )
  appointmentOrder.associate = function (models) {
    // associations can be defined here
  }
  return appointmentOrder
}
