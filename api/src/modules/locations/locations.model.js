module.exports = (sequelize, DataTypes) => {
  const Location = sequelize.define('location', {
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    address1: DataTypes.STRING,
    address2: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    postalCode: DataTypes.STRING,
    country: DataTypes.STRING,
    timezoneName: DataTypes.STRING,
    phone: DataTypes.STRING
  }, {})
  Location.associate = function (models) {
    Location.hasMany(models.building)
  }
  return Location
}
