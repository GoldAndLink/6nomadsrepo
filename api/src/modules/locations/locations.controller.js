const Location = require('../../models').location
const op = require('sequelize').Op

module.exports = {
  getLocations: async (req, res, next) => {
    try {
      const { name } = req.query

      const where = {}
      if (name) {
        where.name = {
          [op.iLike]: `%${name}%`
        }
      }

      const locations = await Location.findAll({ where })

      res.send(locations)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
