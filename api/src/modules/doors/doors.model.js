module.exports = (sequelize, DataTypes) => {
  const Door = sequelize.define(
    'door',
    {
      name: DataTypes.STRING,
      areaId: DataTypes.INTEGER
    },
    {}
  )
  Door.associate = function (models) {
    Door.hasMany(models.appointment)
    Door.hasMany(models.doorDuration)
    Door.belongsTo(models.area)
  }
  return Door
}
