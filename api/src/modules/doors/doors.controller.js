const Door = require('../../models').door

module.exports = {
  getDoors: async (req, res, next) => {
    const allowedParams = ['id', 'name', 'areaId']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    try {
      const entities = await Door.findAll({ where: qualifier })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
