const Config = require('../../models').config
const User = require('../../models').user
const sequelize = require('../../models').sequelize
const fs = require('fs')
const moment = require('moment')

module.exports = {
  userSeed: async (req, res, next) => {
    const users = await User.findAll()
    if (users.length) return res.sendStatus(404)

    await User.build({
      firstName: 'Seed',
      lastName: 'User',
      email: 'admin@redwoodscs.com',
      password: 'admin'
    }).generateHashPassword().save()
    return res.send('admin@redwoodscs.com|admin')
  },
  dbSeed: async (req, res, next) => {
    const seedLock = await Config.findOne({ where: { name: 'auto_seedLock' } })
    if (seedLock) return res && res.status(409).send('seedLock present')
    // Lock not present

    if (req.query.NODE_ENV) {
      process.env.NODE_ENV = req.query.NODE_ENV
    }

    const path = `./db/seed_${process.env.NODE_ENV}.sql`
    if (!fs.existsSync(path)) {
      console.error(`./db/seed_${process.env.NODE_ENV}.sql does not exist`)
      return res && res.status(500).send(`./db/seed_${process.env.NODE_ENV}.sql does not exist`)
    }

    // 1. place seedLock
    await Config.create({ name: 'auto_seedLock', value: moment.utc().format() })

    // 2. truncate all tables
    await sequelize.query(`
DO
$func$
BEGIN
  EXECUTE  -- dangerous, test before you execute!
  (SELECT 'TRUNCATE TABLE '
       || string_agg(format('%I.%I', schemaname, tablename), ', ')
       || ' RESTART IDENTITY CASCADE'
   FROM   pg_tables
   WHERE  tableowner = 'postgres'
   AND    schemaname = 'public'
   AND    tablename != 'config'
   AND    tablename != 'SequelizeMeta'
   );
END
$func$ LANGUAGE plpgsql;
`)

    // 3. run seed sql
    const sql = fs.readFileSync(path, 'utf8')
    try {
      sequelize.query(sql)
      return res && res.send(`[${process.env.NODE_ENV}] seeding complete`)
    } catch (e) {
      console.warn(e)
      return res && res.status(500).send(e)
    }
  }
}
