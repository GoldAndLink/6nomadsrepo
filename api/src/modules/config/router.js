const controller = require('./config.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/config'

router.post(
  '/user_seed',
  version({ '1.0.0': controller.userSeed })
)

router.post(
  '/db_seed',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.dbSeed })
)

module.exports = {
  router,
  baseUrl
}
