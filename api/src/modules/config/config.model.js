module.exports = (sequelize, DataTypes) => {
  const Config = sequelize.define(
    'config',
    {
      name: DataTypes.STRING,
      value: DataTypes.STRING
    },
    {
      freezeTableName: true
    }
  )
  return Config
}
