const ctrl = require('./orders.controller')
const sequelize = require('sequelize')
const mockRes = require('jest-mock-express').response
const Order = require('../../models').order

const testOrder = `
  {
    "orderGuid": "b3d8c7a22ce5430c83a1a2b9dc79c8f4",
    "jobGuid": "166b8f9228aa11e986670a7d69af53ac",
    "accountId": "Deleted",
    "primaryRef": {
        "type": "Sales Order",
        "val": "order2"
    },
    "otherRefs": [
        {
            "type": "PO",
            "val": "order2"
        }
    ],
    "attributes": [
        {
            "type": "AXCompanyLvl",
            "val": "tfv"
        },
        {
            "type": "Requester",
            "val": "DMSWorkFlow@deleted.com"
        },
        {
            "type": "CPorDelv",
            "val": "CP"
        },
        {
            "type": "action",
            "val": "ADD"
        },
        {
            "type": "SubAccountID",
            "val": "2"
        },
        {
            "type": "EstPallets",
            "val": "0.56"
        }
    ],
    "customerName": "TARGET - CEDAR FALLS 3895",
    "origin": {
        "name": "Deleted Retail Inc.",
        "address1": "Deleted",
        "city": "Deleted",
        "state": "CA",
        "zipcode": "93901",
        "country": "USA"
    },
    "destination": {
        "name": "TARGET - CEDAR FALLS 3895",
        "address1": "6601 HUDSON RD",
        "city": "Cedar Falls",
        "state": "IA",
        "zipcode": "50613",
        "country": "USA"
    },
    "requiredShipDate": "2019-02-02T00:00:00",
    "requestedReceiptDate": "2019-02-05T00:00:00",
    "items": [
        {
            "sku": "3570013",
            "name": "Deleted ",
            "weight": "112.800000",
            "weightUOM": "lb",
            "quantity": "400.000000000000",
            "quantityUOM": "CASE"
        }
    ]
  }
`

describe('orders', () => {
  describe('update orders', () => {
    beforeAll(() =>
      Order.destroy({
        where: {
          primaryRefValue: 'order2'
        }
      })
    )

    it('updateOrders insert order if doesnt exist', async done => {
      const res = mockRes()

      await ctrl.updateOrder(
        {
          body: JSON.parse(testOrder)
        },
        res
      )

      const order = await Order.findOne({
        where: {
          primaryRefValue: 'order2',
          data: {
            [sequelize.Op.contains]: {
              attributes: [
                { type: 'SubAccountID', val: '2' }
              ]
            }
          }
        }
      })

      expect(order).toBeTruthy()

      done()
    })
  })
})
