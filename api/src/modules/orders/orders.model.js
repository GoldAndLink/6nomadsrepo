const sequelizePaginate = require('sequelize-paginate')

module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define(
    'order',
    {
      userId: DataTypes.INTEGER,
      primaryRefName: DataTypes.STRING,
      primaryRefValue: DataTypes.STRING,
      deliveryDate: DataTypes.DATE,
      orderStatusId: DataTypes.INTEGER,
      customerId: DataTypes.INTEGER,
      destinationId: DataTypes.INTEGER,
      data: DataTypes.JSONB,
      accountId: DataTypes.INTEGER,
      orderGuid: DataTypes.UUID,
      jobGuid: DataTypes.UUID,
      originId: DataTypes.INTEGER,
      requiredShipDate: DataTypes.DATE,
      pallets: DataTypes.DOUBLE
    },
    {
      indexes: [
        {
          unique: false,
          fields: ['primaryRefValue']
        }
      ]
    }
  )

  Order.associate = function (models) {
    Order.belongsTo(models.user)
    Order.belongsToMany(models.appointment, {
      through: { model: models.appointmentOrder }
    })
    Order.belongsTo(models.orderStatus)
    Order.belongsTo(models.customer)
    Order.belongsTo(models.account)
    Order.belongsTo(models.location, { as: 'destination' })
    Order.belongsTo(models.location, { as: 'origin' })
    Order.belongsToMany(models.item, {
      through: { model: models.orderItem }
    })
  }

  sequelizePaginate.paginate(Order)

  return Order
}
