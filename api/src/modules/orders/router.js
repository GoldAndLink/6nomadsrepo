const controller = require('./orders.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')

const baseUrl = '/orders'

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getOrders })
)

router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getOrder })
)

router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.updateOrder })
)

module.exports = {
  router,
  baseUrl
}
