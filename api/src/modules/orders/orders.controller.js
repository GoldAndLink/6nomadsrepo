const sequelize = require('sequelize')
const uuidv4 = require('uuid/v4')
const moment = require('moment')
const Account = require('../../models').account
const Location = require('../../models').location
const Order = require('../../models').order
const OrderStatus = require('../../models').orderStatus
const Customer = require('../../models').customer
const Item = require('../../models').item
const Appointment = require('../../models').appointment
const AppointmentOrder = require('../../models').appointmentOrder
const UserAccount = require('../../models').userAccount
const CarrierRequest = require('../../models').carrierRequest
const CarrierRequestOrder = require('../../models').carrierRequestOrder
const sequelizeInstance = require('../../models').sequelize
const { check, validationResult } = require('express-validator/check')
const socket = require('../socket')
const op = require('sequelize').Op

module.exports = {
  getOrders: async (req, res, next) => {
    try {
      const Op = sequelize.Op

      const allowedParams = ['id', 'PO', 'destinationId', 'orderStatusId', 'customerId', 'accountId']
      const qualifier = {}
      const include = [
        {
          model: Customer,
          required: false,
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          where: req.query.customerName ? {
            name: { [Op.iLike]: `%${req.query.customerName}%` }
          } : {}
        },
        {
          model: Location,
          as: 'destination',
          required: false,
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          where: req.query.destination ? {
            name: { [Op.iLike]: `%${req.query.destination}%` }
          } : {}
        },
        {
          model: Location,
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          as: 'origin'
        },
        {
          model: OrderStatus,
          required: false,
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          where: req.query.orderStatusName ? {
            name: { [Op.iLike]: `%${req.query.orderStatusName}%` }
          } : {}
        },
        {
          model: Account,
          required: false,
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          include: {
            model: UserAccount,
            required: false,
            attributes: { exclude: ['createdAt', 'updatedAt'] },
            where: {
              userId: req.user.id
            }
          }
        },
        {
          model: Item,
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }
      ]

      for (const param of allowedParams) {
        if (req.query[param] && param !== 'PO') {
          qualifier[param] = req.query[param]
        }
      }
      if (req.query.deliveryDate) {
        qualifier.deliveryDate = {
          [op.gte]: moment.utc(`${req.query.deliveryDate} ${'00:00:00'}`).format(),
          [op.lte]: moment.utc(`${req.query.deliveryDate} ${'23:59:59'}`).format()
        }
      }

      if (req.query.primaryRefValue) {
        if (typeof req.query.primaryRefValue === 'string') {
          req.query.primaryRefValue = [req.query.primaryRefValue]
        }
        qualifier.primaryRefValue = {
          [Op.or]: req.query.primaryRefValue.map(value => ({
            [Op.iLike]: `%${value.replace(/(_|%|\\)/g, '\\$1')}%`
          }))
        }
      }

      if (req.query.PO) {
        if (typeof req.query.PO === 'string') {
          req.query.PO = [req.query.PO]
        }
        qualifier.data = {
          [Op.or]: req.query.PO.map(op => ({ [Op.contains]: { otherRefs: [{ val: op }] } }))
        }
      }

      if (req.query._include && req.query._include === 'appointments') {
        include.push(
          {
            model: Appointment,
            attributes: { exclude: ['createdAt', 'updatedAt'] },
            include: [
              {
                model: Order,
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                include: [
                  {
                    model: Customer,
                    attributes: { exclude: ['createdAt', 'updatedAt'] }
                  },
                  {
                    model: Item,
                    attributes: { exclude: ['createdAt', 'updatedAt'] }
                  },
                  {
                    model: Location,
                    attributes: { exclude: ['createdAt', 'updatedAt'] },
                    as: 'destination'
                  }, {
                    model: Location,
                    attributes: { exclude: ['createdAt', 'updatedAt'] },
                    as: 'origin'
                  }
                ]
              },
              {
                model: CarrierRequest,
                attributes: { exclude: ['createdAt', 'updatedAt'] }
              }
            ]
          }
        )
      }

      const { docs: orders, pages, total } = await Order.paginate({
        include,
        where: qualifier,
        sort: ['updatedAt', 'desc'],
        attributes: { exclude: ['createdAt', 'updatedAt'] },
        page: req.query.page || 1,
        paginate: 10
      })

      return res.json({ orders, pages, total })
    } catch (e) {
      console.error(e)
      return res.status(500).json({ message: 'Unexpected error' })
    }
  },
  getOrder: async (req, res, next) => {
    const order = await Order.findOne({
      where: { id: req.params.id },
      include: [
        {
          model: Customer,
          required: false
        },
        {
          model: Location,
          as: 'destination',
          required: false
        },
        {
          model: Location,
          as: 'origin',
          required: false
        },
        {
          model: OrderStatus,
          required: false
        },
        {
          model: Item,
          required: false
        }
      ]
    })

    if (!order) return res.sendStatus(404)
    res.json(order)
  },
  createOrder: async (req, res, next) => {
    try {
      const { body } = req
      const primaryRefValue = body.primaryRef && body.primaryRef.val
      let subAccountId
      if (body.attributes && body.attributes.length) {
        const subAccountIdAtt = body.attributes.find(a => a.type === 'SubAccountID')
        subAccountId = subAccountIdAtt ? subAccountIdAtt.val : ''
      }

      if (!subAccountId) {
        return res.status(400).send({ message: 'Missing SubAccountID attribute' })
      }

      if (!primaryRefValue) {
        return res.status(400).send({ message: 'Missing primaryRef.val in request payload' })
      }

      let order = await Order.findOne({
        where: {
          primaryRefValue,
          data: {
            [sequelize.Op.contains]: {
              attributes: [
                { type: 'SubAccountID', val: subAccountId }
              ]
            }
          }
        }
      })

      if (order) {
        return res.status(400).send({ message: 'Order already exists' })
      }

      order = await sequelizeInstance.transaction(async transaction => {
        return upsertOrderTransaction(transaction, body)
      })

      // we need the complete object to avoid big array on FE
      order = await Order.findOne({
        where: {
          id: order.id
        },
        include: [
          {
            model: Customer,
            required: false
          },
          {
            model: Location,
            as: 'destination',
            required: false
          },
          {
            model: Location,
            as: 'origin',
            required: false
          },
          {
            model: Item
          }
        ]
      })

      socket.orders.broadcastOne(order)
      res.status(201).send(order)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  updateOrder: async (req, res, next) => {
    try {
      const { body } = req
      const primaryRefValue = body.primaryRef && body.primaryRef.val
      let subAccountId
      if (body.attributes && body.attributes.length) {
        const subAccountIdAtt = body.attributes.find(a => a.type === 'SubAccountID')
        subAccountId = subAccountIdAtt ? subAccountIdAtt.val : ''
      }

      if (!subAccountId) {
        return res.status(400).send({ message: 'Missing SubAccountID attribute' })
      }

      if (!primaryRefValue) {
        return res.status(400).send({ message: 'Missing primaryRef.val in request payload' })
      }

      let order = await Order.findOne({
        where: {
          primaryRefValue,
          data: {
            [sequelize.Op.contains]: {
              attributes: [
                { type: 'SubAccountID', val: subAccountId }
              ]
            }
          }
        }
      })

      if (!order) {
        order = await sequelizeInstance.transaction(async transaction => {
          return upsertOrderTransaction(transaction, body)
        })
      } else {
        order = await sequelizeInstance.transaction(async transaction => {
          return upsertOrderTransaction(transaction, body, order)
        })
      }

      const appointmentOrder = await AppointmentOrder.findOne({
        where: { orderId: order.id }
      })

      if (appointmentOrder) {
        const { appointmentId } = appointmentOrder
        const appointment = await Appointment.findOne({
          where: { id: appointmentId }
        })

        if (appointment) {
          const appointmentData = {}
          if (!appointment.checkInTime && body.checkInTime) {
            appointmentData.checkInTime = body.checkInTime
            appointmentData.appointmentStatusId = 3
          }
          if (!appointment.startLoadingTime && body.loadTime) {
            appointmentData.startLoadingTime = body.loadTime
            appointmentData.appointmentStatusId = 4
          }

          if (!appointment.checkoutTime && body.checkOutTime) {
            appointmentData.checkoutTime = body.checkOutTime
            appointmentData.appointmentStatusId = 5
          }
          appointment.update(appointmentData)
          socket.appointments.broadcastOne(appointment)
        }
      }

      // we need the complete object to avoid big array on FE
      order = await Order.findOne({
        where: {
          id: order.id
        },
        include: [
          {
            model: Customer,
            required: false
          },
          {
            model: Location,
            as: 'destination',
            required: false
          },
          {
            model: Location,
            as: 'origin',
            required: false
          },
          {
            model: Item
          }
        ]
      })

      socket.orders.broadcastOne(order)
      res.send(order)
    } catch (err) {
      console.log(err)
      res.status(500).send({ message: err })
    }
  },
  deleteOrder: async (req, res, next) => {
    try {
      const { body } = req
      const primaryRefValue = body.primaryRef && body.primaryRef.val
      let subAccountId
      if (body.attributes && body.attributes.length) {
        const subAccountIdAtt = body.attributes.find(a => a.type === 'SubAccountID')
        subAccountId = subAccountIdAtt ? subAccountIdAtt.val : ''
      }

      if (!subAccountId) {
        return res.status(400).send({ message: 'Missing SubAccountID attribute' })
      }

      if (!primaryRefValue) {
        return res.status(400).send({ message: 'Missing primaryRef.val in request payload' })
      }

      const order = await Order.findOne({
        where: {
          primaryRefValue,
          data: {
            [sequelize.Op.contains]: {
              attributes: [
                { type: 'SubAccountID', val: subAccountId }
              ]
            }
          }
        }
      })

      if (!order) {
        return res.status(404).send({ message: 'Order not found' })
      }

      // 1. delete CRs related to the order
      let POs = []
      let CROs = []
      if (order.data.otherRefs) {
        POs = order.data.otherRefs.filter(ref => ref.type === 'PO').map(ref => ref.val)
      }
      if (POs.length) {
        CROs = await CarrierRequestOrder.findAll({
          where: {
            poNumber: POs
          }
        })
      }
      if (CROs.length) {
        await CarrierRequestOrder.destroy({
          where: {
            id: CROs.map(cro => cro.id)
          }
        })
        await CarrierRequest.destroy({
          where: {
            id: CROs.map(cro => cro.carrierRequestId)
          }
        })
      }

      // 2. check if there are appointments related to the order
      const appt = await Appointment.findOne({
        include: [{
          model: Order,
          required: true,
          where: {
            id: order.id
          }
        }]
      })

      if (appt) {
        // 3a. check if the appt has other orders on it
        const apptorders = await Appointment.findOne({
          where: {
            id: appt.id
          },
          include: [{
            model: Order,
            required: true,
            where: {
              id: {
                [op.ne]: order.id
              }
            }
          }]
        })

        // 3b. delete and broadcast deletion of the appt if there are no other orders on it
        if (!apptorders) {
          await Appointment.destroy({
            where: {
              id: appt.id
            }
          })

          socket.appointments.broadcastOne(Object.assign(
            {},
            appt.toJSON(),
            { deletedAt: moment().toISOString() }
          ))
        }
      }

      await Order.destroy({
        where: {
          primaryRefValue,
          data: {
            [sequelize.Op.contains]: {
              attributes: [
                { type: 'SubAccountID', val: subAccountId }
              ]
            }
          }
        }
      })

      socket.orders.broadcastOne(Object.assign(
        {},
        order.toJSON(),
        { deletedAt: moment().toISOString() }
      ))

      return res.send()
    } catch (err) {
      console.error(err)
      return res.status(500).send({ message: err })
    }
  },
  validateCreateOrder: [
    check('accountId').not().isEmpty(),
    check('primaryRef').not().isEmpty(),
    check('customerName').not().isEmpty(),
    check('origin').not().isEmpty(),
    check('destination').not().isEmpty(),
    check('requiredShipDate').not().isEmpty(),
    check('requestedReceiptDate').not().isEmpty(),
    check('items').not().isEmpty(),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      next()
    }
  ],
  validateUpdateOrder: [
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      next()
    }
  ]
}

const upsertOrderTransaction = async (transaction, body, currentOrder) => {
  // Get or create account
  let account = await Account.findOne({
    where: { name: body.accountId }
  })
  if (!account) {
    const newAccount = new Account({ name: body.accountId })
    account = await newAccount.save({ transaction })
  }

  // Get or create customer
  let customerRow = await Customer.findOne({
    where: { name: body.customerName }
  })
  if (!customerRow) {
    const newCustomer = new Customer({ name: body.customerName })
    customerRow = await newCustomer.save({ transaction })
  }

  const locationFields = [
    'name',
    'address1',
    'address2',
    'city',
    'state',
    'postalCode',
    'country',
    'timezoneName',
    'phone'
  ]

  // Get or create origin
  if (!body.origin && !body.origin.name) {
    throw new Error('Missing origin.name')
  }
  let origin = await Location.findOne({
    where: { name: body.origin.name }
  })
  if (!origin) {
    const newOrigin = new Location(
      locationFields.reduce((obj, field) => (
        { ...obj, [field]: body.origin[field] }
      ), {})
    )
    origin = await newOrigin.save({ transaction })
  }

  // Get or create destination
  if (!body.destination && !body.destination.name) {
    throw new Error('Missing destination.name')
  }
  let destination = await Location.findOne({
    where: { name: body.destination.name }
  })
  if (!destination) {
    const newDestination = new Location(
      locationFields.reduce((obj, field) => (
        { ...obj, [field]: body.destination[field] }
      ), {})
    )
    destination = await newDestination.save({ transaction })
  }

  const orderData = {
    accountId: account.id,
    customerId: customerRow.id,
    orderGuid: body.orderGuid || uuidv4(),
    jobGuid: body.jobGuid,
    originId: origin.id,
    destinationId: destination.id,
    requiredShipDate: moment(body.requiredShipDate, 'YYYY-MM-DD').toISOString(),
    deliveryDate: moment(body.requestedReceiptDate, 'YYYY-MM-DD').toISOString(),
    primaryRefName: body.primaryRef.type,
    primaryRefValue: body.primaryRef.val,
    data: {
      primaryRef: body.primaryRef,
      otherRefs: body.otherRefs || [],
      attributes: body.attributes
    }
  }

  if (!currentOrder) {
    orderData.orderStatusId = 1
  }

  if (body.orderStatusId) {
    orderData.orderStatusId = body.orderStatusId
  }

  if (body.attributes && body.attributes.length) {
    const palletAttr = body.attributes.find(row => row.type === 'EstPallets')
    if (palletAttr && palletAttr.val) {
      orderData.pallets = palletAttr.val
    }
  }

  let items = []

  if (body.items) {
    items = await Promise.all(body.items.map(async bodyItem => {
      let item = await Item.findOne({
        where: { sku: bodyItem.sku }
      })
      if (!item) {
        const newItem = new Item({
          name: bodyItem.name,
          sku: bodyItem.sku
        })
        item = await newItem.save({ transaction })
      }
      return item
    }))
  }

  let order
  if (!currentOrder) {
    order = await new Order(orderData).save({ transaction })
  } else {
    order = currentOrder
    await order.update(orderData, { transaction })
    await order.setItems([])
  }

  await Promise.all(items.map(async (item, i) => {
    const bodyItem = body.items[i]
    await order.addItem(item, {
      through: {
        weight: bodyItem.weight,
        weightUOM: bodyItem.weightUOM,
        quantity: bodyItem.quantity,
        quantityUOM: bodyItem.quantityUOM,
        freshnessThreshold: bodyItem.freshnessThreshold
      },
      transaction
    })
  }))

  return order
}
