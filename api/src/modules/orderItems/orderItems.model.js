'use strict'
module.exports = (sequelize, DataTypes) => {
  const OrderItem = sequelize.define('orderItem', {
    orderId: DataTypes.INTEGER,
    itemId: DataTypes.INTEGER,
    seqNum: DataTypes.INTEGER,
    weight: DataTypes.DOUBLE,
    weightUOM: DataTypes.STRING,
    quantity: DataTypes.DOUBLE,
    quantityUOM: DataTypes.STRING,
    freshnessThreshold: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {})
  OrderItem.associate = function (models) {
    OrderItem.belongsTo(models.item)
    OrderItem.belongsTo(models.order)
    OrderItem.belongsTo(models.user, { foreignKey: 'createdBy' })
    OrderItem.belongsTo(models.user, { foreignKey: 'updatedBy' })
  }
  return OrderItem
}
