const OrderItems = require('../../models').orderItem

module.exports = {
  getOrderItems: async (req, res, next) => {
    const allowedParams = ['id', 'orderId', 'itemId', 'jobGuid']
    const qualifier = {}
    for (const param of allowedParams) {
      if (req.query[param]) {
        qualifier[param] = req.query[param]
      }
    }
    try {
      const entities = await OrderItems.findAll({ where: qualifier })
      res.send(entities)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
