const controller = require('./orderItems.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/orderItems'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getOrderItems })
)

module.exports = {
  router,
  baseUrl
}
