module.exports = (sequelize, DataTypes) => {
  const Driver = sequelize.define(
    'driver',
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      active: DataTypes.BOOLEAN,
      carrierId: DataTypes.INTEGER,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      prefContactMethod: DataTypes.ENUM('email', 'sms')
    },
    {}
  )
  Driver.associate = function (models) {
    Driver.belongsTo(models.carrier)
    Driver.hasMany(models.appointment)
  }
  return Driver
}
