const Driver = require('../../models').driver

const { check, validationResult } = require('express-validator/check')

module.exports = {
  getAllDrivers: async (req, res, next) => {
    try {
      const drivers = await Driver.findAll()

      res.send(drivers)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  createDriver: async (req, res, next) => {
    const allowedFields = ['firstName', 'lastName', 'active', 'carrierId', 'email', 'phone', 'prefContactMethod']

    const entity = {}
    for (const field of allowedFields) {
      entity[field] = req.body[field]
    }

    try {
      const driver = new Driver(entity)
      const result = await driver.save()

      res.status(201).send(result)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  validateCreateDriver: [
    check('firstName').not().isEmpty(),
    check('carrierId').not().isEmpty(),
    check('email').not().isEmpty(),
    check('email').isEmail(),
    check('prefContactMethod').not().isEmpty(),
    check('prefContactMethod').isIn(['email', 'sms']),
    (req, res, next) => {
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      next()
    }
  ]
}
