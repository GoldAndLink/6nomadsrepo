const controller = require('./drivers.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/drivers'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getAllDrivers })
)

router.post(
  '/',
  passport.authenticate('headerapikey', { session: false }),
  controller.validateCreateDriver,
  version({ '1.0.0': controller.createDriver })
)

module.exports = {
  router,
  baseUrl
}
