
module.exports = {
  connection: async (namespaceio, socket) => {
  },
  broadcastOne: async order => {
    const io = require('../../../socket').io()
    io['orders'].emit(`order`, order)
  }
}
