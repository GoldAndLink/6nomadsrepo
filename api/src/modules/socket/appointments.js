const Door = require('../../models').door
const Area = require('../../models').area

module.exports = {
  connection: async (namespaceio, socket) => {
  },
  broadcastOne: async appointment => {
    const io = require('../../../socket').io()
    const door = await Door.findOne(
      {
        where: { id: appointment.doorId },
        attributes: ['areaId'],
        include: [{
          model: Area,
          attributes: ['buildingId']
        }]
      }
    )
    io['appointments'].emit(`building:${door.area.buildingId}`, appointment)
  },
  broadcastMany: async appointments => {
    // all must belong to the same building
    // currently only used for downstream validation, which satisfies that
    if (!appointments || !appointments.length) {
      return
    }
    const io = require('../../../socket').io()
    const door = await Door.findOne(
      {
        where: { id: appointments[0].doorId },
        attributes: ['areaId'],
        include: [{
          model: Area,
          attributes: ['buildingId']
        }]
      }
    )
    io['appointments'].emit(`building:${door.area.buildingId}`, appointments)
  }
}
