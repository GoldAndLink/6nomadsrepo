const appointments = require('./appointments')
const orders = require('./orders')
const carrierRequests = require('./carrierRequests')

module.exports = {
  appointments,
  orders,
  carrierRequests
}
