
module.exports = {
  connection: async (namespaceio, socket) => {
  },
  broadcastOne: async carrierRequest => {
    const io = require('../../../socket').io()
    io['carrierRequests'].emit(`carrierRequest`, carrierRequest)
  }
}
