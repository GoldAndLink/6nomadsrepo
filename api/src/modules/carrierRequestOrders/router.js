const controller = require('./carrierRequestOrders.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/carrierRequestOrders'
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.getCarrierRequestOrders })
)

router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  controller.validateCreateCarrierRequestOrder,
  version({ '1.0.0': controller.createCarrierRequestOrder })
)

module.exports = {
  router,
  baseUrl
}
