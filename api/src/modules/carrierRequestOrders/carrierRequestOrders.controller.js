const CarrierRequestOrder = require('../../models').carrierRequestOrder

const { check, validationResult } = require('express-validator/check')

module.exports = {
  getCarrierRequestOrders: async (req, res, next) => {
    try {
      const CarrierRequestOrders = await CarrierRequestOrder.findAll()

      res.send(CarrierRequestOrders)
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  },
  createCarrierRequestOrder: async (req, res, next) => {
    const allowedFields = ['carrierRequestId', 'poNumber']

    const entity = {}
    for (const field of allowedFields) {
      entity[field] = req.body[field]
    }

    try {
      const CRO = new CarrierRequestOrder(entity)
      const result = await CRO.save()

      res.status(201).send(result)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  },
  validateCreateCarrierRequestOrder: [
    check('carrierRequestId').not().isEmpty(),
    check('poNumber').not().isEmpty(),
    (req, res, next) => {
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      next()
    }
  ]
}
