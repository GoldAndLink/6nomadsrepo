module.exports = (sequelize, DataTypes) => {
  const CarrierRequestOrder = sequelize.define(
    'carrierRequestOrder',
    {
      carrierRequestId: DataTypes.INTEGER,
      poNumber: DataTypes.STRING
    },
    {}
  )
  CarrierRequestOrder.associate = function (models) {
    CarrierRequestOrder.belongsTo(models.carrierRequest)
  }
  return CarrierRequestOrder
}
