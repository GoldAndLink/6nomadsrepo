const controller = require('./feeds.controller')
const router = require('express').Router()
const version = require('express-routes-versioning')()
const passport = require('passport')
const baseUrl = '/feeds'

router.get(
  '/bootstrap',
  passport.authenticate('jwt', { session: false }),
  version({ '1.0.0': controller.bootstrap })
)

module.exports = {
  router,
  baseUrl
}
