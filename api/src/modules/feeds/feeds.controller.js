const models = require('../../models')

module.exports = {
  bootstrap: async (req, res, next) => {
    try {
      const sites = await models.site.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
        include: [{
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          model: models.building,
          include: [{
            attributes: { exclude: ['createdAt', 'updatedAt'] },
            model: models.area,
            include: [{
              attributes: { exclude: ['createdAt', 'updatedAt'] },
              model: models.door,
              include: [{
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: models.doorDuration
              }]
            }]
          }, {
            model: models.location
          }]
        }]
      })

      if (req.query.carrierPortal) {
        return res.send({ sites })
      }

      const [
        orderStatuses,
        appointmentStatuses,
        config,
        roles,
        drivers,
        carriers,
        schedules,
        doorSchedules,
        accounts,
        items
      ] = await Promise.all([
        models.orderStatus.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.appointmentStatus.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.config.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.role.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.driver.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.carrier.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.location.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.customer.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.schedule.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt', 'deletedAt'] }
        }),
        models.doorSchedule.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt', 'deletedAt'] }
        }),
        models.account.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        }),
        models.item.findAll({
          attributes: { exclude: ['createdAt', 'updatedAt', 'createdBy', 'updatedBy', 'deletedAt'] }
        })
      ])

      res.send({
        config,
        roles,
        sites,
        orderStatuses,
        appointmentStatuses,
        drivers,
        carriers,
        schedules,
        doorSchedules,
        accounts,
        items
      })
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: err })
    }
  }
}
