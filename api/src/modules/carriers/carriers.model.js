module.exports = (sequelize, DataTypes) => {
  const Carrier = sequelize.define(
    'carrier',
    {
      code: DataTypes.STRING,
      name: DataTypes.STRING
    },
    {}
  )
  Carrier.associate = function (models) {
    Carrier.hasMany(models.appointment)
    Carrier.hasMany(models.driver)
  }
  return Carrier
}
