const Carrier = require('../../models').carrier

module.exports = {
  getAllCarriers: async (req, res, next) => {
    try {
      const carriers = await Carrier.findAll()

      res.send(carriers)
    } catch (err) {
      res.status(500).send({ message: err })
    }
  }
}
