const execSync = require('child_process').execSync
const configController = require('./src/modules/config/config.controller')
const options = {
  encoding: 'utf8',
  env: {
    NODE_ENV: 'test'
  }
}

module.exports = async jest => {
  try {
    execSync('"node_modules/.bin/sequelize" db:drop --env test', options)
  } catch (e) {
    // most likely the db isn't there, move along
    // https://github.com/sequelize/cli/issues/599
  }

  // do not catch from here on out, we want to abort if things go wrong
  execSync('"node_modules/.bin/sequelize" db:create --env test', options)
  execSync('"node_modules/.bin/sequelize" db:migrate --env test', options)
  await configController.dbSeed()
}
