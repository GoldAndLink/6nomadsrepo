module.exports = {
  root: true,
  plugins: [
    'testcafe'
  ],
  extends: ['standard', 'plugin:testcafe/recommended'],
  rules: {
    'no-var': 'error',
    'prefer-const': 'error'
  },
  env: {
    node: true,
    jest: true
  }
}
