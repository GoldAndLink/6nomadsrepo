require('dotenv').config()
const express = require('express')
const path = require('path')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const modules = require('./src/modules')
const passport = require('passport')
const authentication = require('./config/passport')
const responseTime = require('response-time')
const compression = require('compression')
const app = express()

app.use(compression())

/* ENABLE CORS */
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With')
  next()
})

// uncomment after placing your favicon in /public
app.use(logger('dev'))
app.use(bodyParser.json({ limit: '10mb' }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.get('env') !== 'production' && app.use(responseTime())

// load modules
app.get('/', (req, res) => {
  res.json({ message: 'Welcome to deleted_names API!' })
})

app.get('/healthz', (req, res) => {
  res.status(200).json({ status: 'OK' })
})

authentication(passport)
app.use(passport.initialize())
app.use(passport.session())

modules(app)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.json({
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.json({
    message: err.message,
    error: {}
  })
})

module.exports = app
