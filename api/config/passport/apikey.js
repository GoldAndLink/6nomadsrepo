const HeaderAPIKeyStrategy = require('passport-headerapikey').HeaderAPIKeyStrategy

const User = require('../../src/models').user

module.exports = new HeaderAPIKeyStrategy({
  header: 'Authorization',
  prefix: 'Key '
},
false,
async function (apikey, done) {
  try {
    const user = await User.findOne({
      where: { apikey: apikey }
    })

    if (user) {
      done(null, user)
      return null
    } else {
      done(null, false, { message: 'Unknown user' })
      return null
    }
  } catch (err) {
    done(err)
    return null
  }
})
