const passportJWT = require('passport-jwt')
const JwtStrategy = passportJWT.Strategy
const passport = require('./')

const User = require('../../src/models').user

module.exports = new JwtStrategy(passport.jwtOptions, async function (
  jwtPayload,
  done
) {
  try {
    const user = await User.findOne({
      where: { id: jwtPayload.id }
    })

    if (user) {
      done(null, user)
      return null
    } else {
      done(null, false, { message: 'Unknown user' })
      return null
    }
  } catch (err) {
    done(err)
    return null
  }
})
