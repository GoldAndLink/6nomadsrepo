const passport = require('passport')
const jwt = require('./passport/jwt')
const apikey = require('./passport/apikey')
const User = require('../src/models').user

async function getUser (userId) {
  try {
    const user = await User.findOne({
      where: { id: userId },
      raw: true,
      attributes: {
        exclude: ['password']
      }
    })
    return user
  } catch (err) {
    return err
  }
}

module.exports = function () {
  // serialize and deserialize sessions
  passport.serializeUser((user, done) => done(null, user.id))
  passport.deserializeUser((id, done) => {
    done(null, getUser(id))
  })

  // use these strategies
  passport.use(jwt)
  passport.use(apikey)
}
