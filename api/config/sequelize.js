require('dotenv').config()

module.exports = {
  local: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    pool: {
      min: process.env.DB_POOL_MIN,
      max: process.env.DB_POOL_MAX
    },
    dialect: 'postgres',
    dialectOptions: {
      useUTC: false
    },
    operatorsAliases: false
  },
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    pool: {
      min: process.env.DB_POOL_MIN,
      max: process.env.DB_POOL_MAX
    },
    dialect: 'postgres',
    dialectOptions: {
      useUTC: false
    },
    operatorsAliases: false
  },
  staging: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    pool: {
      min: process.env.DB_POOL_MIN,
      max: process.env.DB_POOL_MAX
    },
    dialect: 'postgres',
    dialectOptions: {
      useUTC: false
    },
    operatorsAliases: false
  },
  test: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE + '_test',
    pool: {
      min: process.env.DB_POOL_MIN,
      max: process.env.DB_POOL_MAX
    },
    dialect: 'postgres',
    dialectOptions: {
      useUTC: false
    },
    operatorsAliases: false,
    logging: false
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    pool: {
      min: process.env.DB_POOL_MIN,
      max: process.env.DB_POOL_MAX
    },
    host: process.env.DB_HOST,
    dialect: 'postgres',
    dialectOptions: {
      useUTC: false
    },
    operatorsAliases: false
  }
}
