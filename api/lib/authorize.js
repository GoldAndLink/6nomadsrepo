const _ = require('lodash')
module.exports = {
  authorize: roles => {
    return async (req, res, next) => {
      const userRoles = req.user.roles

      const existRole = _.find(roles, role => {
        return _.find(userRoles, ur => {
          return ur.name === role
        })
      })

      if (existRole) {
        next()
      } else {
        res.status(401).send('Unauthorized')
      }
    }
  }
}
